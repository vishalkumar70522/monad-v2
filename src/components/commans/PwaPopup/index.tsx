import React, { useEffect, useState } from "react";
import { MdOutlineInstallMobile } from "react-icons/md";

interface AppInfo {
  id?: string;
  url?: string;
  platform?: string;
  version?: string;
}

declare global {
  interface Navigator {
    getInstalledRelatedApps: () => Promise<AppInfo[]>;
  }
}

export const InstallPWA: React.FC = () => {
  const [installPromptEvent, setInstallPromptEvent] = useState<any>(null);

  useEffect(() => {
    window.addEventListener("beforeinstallprompt", (e: Event) => {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later.
      setInstallPromptEvent(e);
    });
  }, []);

  const handleInstallClick = () => {
    if (!installPromptEvent) return;
    // Show the prompt
    installPromptEvent.prompt();
    // Wait for the user to respond to the prompt
    installPromptEvent.userChoice.then((choiceResult: any) => {
      if (choiceResult.outcome === "accepted") {
        console.log("User accepted the install prompt");
      } else {
        console.log("User dismissed the install prompt");
      }
      setInstallPromptEvent(null);
    });
  };

  return (
    <>
      {!installPromptEvent ? null : (
        <MdOutlineInstallMobile
          size="20px"
          color="#0EBCE9"
          onClick={handleInstallClick}
        />
      )}
    </>

    // <button
    //   type="submit"
    //   onClick={handleInstallClick}
    //   disabled={!installPromptEvent}
    // >
    //   Install App
    // </button>
  );
};

export const CheckPWAInstalled: React.FC = () => {
  useEffect(() => {
    if ("getInstalledRelatedApps" in navigator) {
      navigator.getInstalledRelatedApps().then((relatedApps: any) => {
        relatedApps.forEach((app: any) => {
          if (app.id === "/") {
            console.log("PWA is installed");
          } else {
            return <InstallPWA />;
          }
        });
      });
    }
  }, []);

  return null;
};
