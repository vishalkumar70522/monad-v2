import {
  Box,
  Flex,
  VStack,
  HStack,
  Text,
  Spacer,
  Image,
  Stack,
  Button,
  Hide,
  Tooltip,
  SimpleGrid,
} from "@chakra-ui/react";
import { AiFillStar } from "react-icons/ai";
import { useNavigate } from "react-router-dom";

export function Screen(props: any) {
  const navigate = useNavigate();
  const { screen } = props;
  // const openInNewTab = (path: string) => {
  //   window.open(
  //     `${window.location.host}${path}`,
  //     "_blank",
  //     "noopener,noreferrer"
  //   );
  // };
  // console.log("SingleScreen : ", JSON.stringify(screen));
  return (
    <Box>
      <Flex gap={{ base: "5", lg: "10" }}>
        <Box onClick={() => navigate(`/screenDetails/${screen?._id}`)}>
          <Image
            src={screen?.image}
            alt="screen"
            width={{ base: "296px", lg: "408px" }}
            height={{ base: "165px", lg: "257px" }}
            borderRadius="12px"
          />
        </Box>
        <Stack width="100%" spacing="0">
          <Flex justifyContent="space-bitween" alignItems="center">
            <Tooltip label="Click to see more details">
              <Text
                fontSize={{ base: "sm", lg: "2xl" }}
                color="#403F49"
                align="left"
                fontWeight="semibold"
                m="0"
                _hover={{ color: "#0EBCF5" }}
                onClick={() => navigate(`/screenDetails/${screen?._id}`)}
              >
                {screen?.name}
              </Text>
            </Tooltip>
          </Flex>
          <Text fontSize="sm" color="#7D7D7D" align="left">
            {`${screen?.districtCity}`}
          </Text>
          <Flex align="center" justify="space-between">
            <Text
              fontSize={{ base: "11px", lg: "md" }}
              color="#7D7D7D"
              align="left"
            >
              {`${screen?.slotsTimePeriod} sec / slot | 2120 slots available`}
            </Text>
            <Hide below="md">
              <Spacer />
              <Flex align="center">
                <AiFillStar size="16px" color="#403F49" />
                <Text
                  pl="1"
                  color="#403F49"
                  fontSize="md"
                  align="left"
                  m="0"
                  justifyContent="center"
                >
                  {screen?.rating}
                </Text>
              </Flex>
            </Hide>
          </Flex>
          <Stack spacing="0">
            <Text
              color="#403F49"
              fontSize={{ base: "9px", lg: "sm" }}
              align="left"
              m="0"
            >
              Locality
            </Text>
            <SimpleGrid columns={[2, 3, 4]} gap={{ base: "0.5", lg: "2" }}>
              {screen?.screenHighlights?.map((highlight: any, index: any) => (
                <Text
                  key={index}
                  border="1px"
                  px="2"
                  py="1"
                  borderRadius="50px"
                  align="center"
                  color="#403F49"
                  fontSize={{ base: "9px", lg: "sm" }}
                >
                  {highlight}
                </Text>
              ))}
            </SimpleGrid>
          </Stack>
          <HStack>
            <VStack spacing={0}>
              <Text
                color="#403F49"
                fontSize={{ base: "11px", lg: "sm" }}
                align="left"
                m="0"
              >
                Starts from
              </Text>
              <Flex align="baseline" gap={1}>
                <Text
                  color="#222222"
                  fontSize={{ base: "16px", lg: "xl" }}
                  align="left"
                >
                  {`₹${screen?.rentPerSlot}`}
                </Text>
                <Text color="#222222" align="left" fontSize={{ base: "15px" }}>
                  slot
                </Text>
              </Flex>
            </VStack>
            <Spacer />
            <Button
              variant="outline"
              color="#D7380E"
              fontSize={{ base: "10px", lg: "md" }}
              borderRadius={{ base: "lg", lg: "2xl" }}
              py="3"
              onClick={() => props?.selectedScreen(screen)}
            >
              Calculate Budget
            </Button>
          </HStack>
        </Stack>
      </Flex>
    </Box>
  );
}
