import { useRef } from "react";
import { Image, Stack, Flex } from "@chakra-ui/react";
import { Modal, Carousel } from "antd";
import { RiArrowLeftSLine, RiArrowRightSLine } from "react-icons/ri";
// import SimpleImageSlider from "react-simple-image-slider";

export function ImageSliderPopup(props: any) {
  // console.log("props : ", props);
  const slider = useRef<any>();
  const images = props?.images?.map(
    (img: any) => `https://ipfs.io/ipfs/${img}`
  );
  return (
    <Modal
      // style={{}}
      footer={[]}
      centered
      width={1000}
      open={props?.imageShow}
      onOk={() => props?.setImageShow(false)}
      onCancel={() => props?.setImageShow(false)}
    >
      <Carousel
        autoplay
        effect="fade"
        pauseOnHover={true}
        pauseOnDotsHover={true}
        draggable={true}
        ref={slider}
      >
        {images?.map((img: any, index: any) => (
          <Stack
            key={index}
            align="center"
            // justify="space-around"
            // border="1px solid black"
          >
            <Flex align="center" justify="space-between">
              <RiArrowLeftSLine
                fontSize="20px"
                onClick={() => slider.current.prev()}
              />
              <Image
                src={img}
                alt={img}
                align="center"
                // justify="center"]
                width="90%"
                height={480}
                objectFit="fill"
                zIndex={0}
              />
              <RiArrowRightSLine
                fontSize="20px"
                onClick={() => slider.current.next()}
              />
            </Flex>
          </Stack>
        ))}
      </Carousel>
    </Modal>
  );
}
