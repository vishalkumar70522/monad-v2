import { Stack } from "@chakra-ui/react";
import { Modal } from "antd";
import "./index.css";

export function ImageView(props: any) {
  const { image } = props;

  return (
    <Modal
      style={{}}
      footer={[]}
      open={props?.imageShow}
      onOk={() => props?.setImageShow(false)}
      onCancel={() => props?.setImageShow(false)}
      width={1000}
      //width="auto"
    >
      <Stack align="" width="100%" pt="7">
        <div className="geek">
          <img src={image} alt="" />
        </div>
      </Stack>
    </Modal>
  );
}
