import { useState } from "react";
import { Box, Text, HStack, Flex, Button } from "@chakra-ui/react";
import { AiOutlineClockCircle } from "react-icons/ai";
import { MdKeyboardArrowRight } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import { CampaignPopup } from "../CampaignPopup";

export function SingleScreen(props: any) {
  const screen = props?.screen;
  // console.log(props);
  const [campaignPopupView, setCampaignPopupView] = useState<any>(false);
  const navigate = useNavigate();
  return (
    <Box
      mx="1"
      width="400px"
      height="251px"
      bgImage={screen?.image}
      bgPosition="center"
      bgSize="cover"
      borderRadius="8px"
      // key={props?.}
    >
      <CampaignPopup
        open={campaignPopupView}
        vid={screen?.lastPlayed}
        onCancel={() => setCampaignPopupView(false)}
      />
      <Box p="2">
        <HStack justifyContent="space-between">
          <Text color="#FFFFFF" fontSize="md" fontWeight="700" m="0">
            {screen?.name}
          </Text>
          <Button
            rightIcon={<MdKeyboardArrowRight size="20px" />}
            leftIcon={
              <Box
                alignSelf="center"
                p="2px"
                mx="2px"
                height="12px"
                width="12px"
                borderRadius="100%"
                bgColor={
                  !screen?.lastActive
                    ? "yellow"
                    : Math.floor(
                        new Date().getTime() -
                          new Date(screen?.lastActive).getTime()
                      ) /
                        1000 >
                      200
                    ? "red"
                    : "green"
                }
              ></Box>
            }
            color="#FFFFFF"
            fontSize="sm"
            fontWeight="513"
            bgColor="#626262"
            p="2"
            px="3"
            borderRadius="16px"
            onClick={() =>
              navigate(`/screenDashboard/${screen?._id}`, {
                state: { screen },
              })
            }
          >
            Enter
          </Button>
        </HStack>
        <Text color="#FFFFFF" fontSize="md" fontWeight="700" align="left" m="0">
          {`${screen?.districtCity}, ${screen?.stateUT}`}
        </Text>
        <Flex pt="120" align="center" gap="5" justify="space-between">
          <HStack>
            <Box bgColor="#FFFFFF" borderRadius="4px">
              <Text
                color="#030315"
                fontSize="md"
                fontWeight="700"
                m="0"
                py="1"
                px="5"
              >
                {`₹${screen?.rentPerSlot}/Slot`}
              </Text>
            </Box>
            <HStack pt="10px">
              <AiOutlineClockCircle color="#FFFFFF" size="18px" />
              <Text color="#FFFFFF" fontSize="16px" fontWeight="500" m="0">
                {`${screen?.slotsTimePeriod} sec`}
              </Text>
            </HStack>
          </HStack>
          <Box border="1px solid #FFFFFF" borderRadius="4px">
            <Text
              color="#ffffff"
              fontSize="md"
              fontWeight="700"
              m="0"
              py="1"
              px="5"
              onClick={() => setCampaignPopupView(true)}
            >
              Last Ad
            </Text>
          </Box>
        </Flex>
      </Box>
    </Box>
  );
}
