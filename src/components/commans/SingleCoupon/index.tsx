import { useEffect } from "react";
import { Button, Center, Image, Text, Box } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { getBrandDetailsById } from "../../../Actions/brandAction";
import { useDispatch, useSelector } from "react-redux";

export function SingleCoupon(props: any) {
  const { coupon, userInfo } = props;
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const brandDetails = useSelector((state: any) => state.brandDetails);
  const {
    loading: loadingBrandData,
    brand: brandData,
    error: errorBrandData,
  } = brandDetails;

  useEffect(() => {
    dispatch(getBrandDetailsById(coupon?.brand));
  }, [dispatch]);
  // console.log(
  //   coupon?.rewardCoupons.filter((cp: any) => cp.email === userInfo?.email)
  //     .length !== 0
  // );
  return (
    <Box
      border="1px"
      borderColor="#D9D9D9"
      p="5"
      _hover={{ boxShadow: "2xl" }}
      borderRadius="8px"
      // width={{ base: "290px", lg: "307px" }}
      // height={{ base: "337px", lg: "357px" }}
      width="307px"
      height="357px"
    >
      <Center>
        <Image
          src={
            coupon?.couponRewardInfo?.images.length > 0
              ? coupon?.couponRewardInfo?.images[0]
              : coupon?.brandLogo
          }
          alt=" "
          width="100%"
          height="150px"
        />
      </Center>

      <Text
        pt="5"
        color="#303030"
        fontSize={{ base: "12px", lg: "lg" }}
        fontWeight="638"
        m="0"
        align="center"
      >
        {coupon?.brandName}
      </Text>
      <Box>
        {/* <Text
          pt="5"
          color="#303030"
          fontSize={{ base: "12px", lg: "lg" }}
          fontWeight="638"
          m="0"
          align="center"
        >
          {brandData?.address}
        </Text> */}
      </Box>
      <Text
        py="3"
        color="#585858"
        fontSize={{ base: "lg", lg: "md" }}
        fontWeight="400"
        align="center"
        height="60px"
        className="text"
      >
        {coupon?.offerName}
      </Text>
      <Center>
        <Button
          color="#FFFFFF"
          bgColor="#D7380E"
          py="3"
          px="10"
          width="80%"
          fontSize={{ base: "", lg: "md" }}
          fontWeight="638"
          onClick={() =>
            navigate("/couponDetils", {
              state: {
                data: coupon,
                path: "/",
                showCouponCode:
                  coupon?.rewardCoupons.filter(
                    (cp: any) => cp.email === userInfo?.email
                  ).length !== 0,
              },
            })
          }
        >
          Get Deal
        </Button>
      </Center>
    </Box>
  );
}
