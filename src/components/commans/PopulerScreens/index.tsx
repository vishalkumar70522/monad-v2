import {
  Box,
  Button,
  SimpleGrid,
  Center,
  Divider,
  Flex,
  HStack,
  Spacer,
  Stack,
  Text,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverBody,
  PopoverArrow,
  Show,
  Hide,
  Badge,
} from "@chakra-ui/react";
import { Slider, Checkbox, Select } from "antd";
import { useRef, useState } from "react";
import { AiOutlineDown } from "react-icons/ai";
import { Screen } from "../Screen";
import { CalculateBudgetPopup } from "../../../Pages/Models/CalculateBudgetPopup";
import { CheckboxChangeEvent } from "antd/es/checkbox";
import { HiOutlineShoppingCart } from "react-icons/hi";
import { CreateCampaignName } from "../../../Pages/Models/CreateCampaignName";
import { UploadMedia } from "../../../Pages/Models/UploadMedia";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createVideoFromImage } from "../../../Actions/videoFromImage";
import { uploadMedia } from "../../../Actions/mediaActions";
import { filteredScreenListByAudiance } from "../../../Actions/screenActions";
import { ShowScreensLocation } from "../../../Pages/MyMap/ShowScreensLocation";

export function PopulerScreens(props: any) {
  const { screensList = [], jsonData } = props;
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();

  const items: string | null = window.localStorage.getItem("carts");
  const value = items ? JSON.parse(items) : [];

  const [listView, setListView] = useState<boolean>(true);
  const [showCheckbox, setShowCheckbox] = useState<Boolean>(false);
  const [checkedItems, setCheckedItems] = useState(
    new Array(screensList?.length).fill(false)
  );
  const localityFocusRef = useRef<any>(null);
  const [showFilterOption, setShowFilterOption] = useState<boolean>(false);
  const [showCalculateBudget, setShowCalculateBudgetPopup] =
    useState<boolean>(false);
  const [selectedScreen, setSelectedScreen] = useState<any>(null);

  const [openCreateCampaignName, setOpenCreateCampaignName] =
    useState<boolean>(false);
  const [openUploadMedia, setOpenUploadMedia] = useState<boolean>(false);

  const [campaignName, setCampaignName] = useState<any>("");
  const [fileUrl, setFileUrl] = useState<any>("");

  const [screenHighlights, setScreenHighlights] = useState<any>([]);
  const [mobility, setMobility] = useState<any>([]);
  const [ageRange, setAgeRange] = useState<any>([10, 90]);
  const [genders, setGenders] = useState<any>([]);
  const [employmentType, setEmploymentType] = useState<any>([]);

  const options = [
    "School",
    "Colleges",
    "Swimming pool",
    "Appartments",
    "Metro",
    "Airports",
    "Military",
    "Tourist attractions",
    "Railways",
    "Beachs",
  ];

  const filteredOptions = options.filter(
    (data: any) => !screenHighlights.includes(data)
  );

  const handleAddOrRemoveGenter = (value: string, reason: string) => {
    if (reason) {
      setGenders([...genders, value]);
    } else {
      const newData = genders?.filter((data: string) => data !== value);
      setGenders(newData);
    }
  };
  const handleAddOrRemoveEmployment = (value: string, reason: string) => {
    if (reason) {
      setEmploymentType([...employmentType, value]);
    } else {
      const newData = employmentType?.filter((data: string) => data !== value);
      setEmploymentType(newData);
    }
  };

  const handleAddMobility = (value: string) => {
    if (value === "All") {
      setMobility(["Sitting", "Moving", "Walking"]);
    } else if (value === "None") {
      setMobility([]);
    } else {
      if (mobility.includes(value)) {
        const newData = mobility?.filter((data: string) => data !== value);
        setMobility(newData);
      } else {
        setMobility([...mobility, value]);
      }
    }
  };

  const handelAddScreenHighlights = (value: string | string[]) => {
    setScreenHighlights(value);
  };

  const onChange = (value: number | [number, number]) => {
    //console.log("onChange: ", value);
    setAgeRange(value);
  };

  const handelFilterScreens = () => {
    dispatch(
      filteredScreenListByAudiance({
        averageAgeGroup: JSON.stringify(ageRange),
        employmentStatus: JSON.stringify(employmentType),
        screenHighlights: JSON.stringify(screenHighlights),
        mobility: JSON.stringify(mobility),
        genders: JSON.stringify(genders),
      })
    );
  };

  const handelSelectCheckBox = (
    e: CheckboxChangeEvent,
    screen: any,
    index: any
  ) => {
    setCheckedItems(
      checkedItems.map((data: boolean, i: Number) => {
        if (i === index) {
          return e.target.checked;
        } else return data;
      })
    );
    if (e.target.checked === true) {
      setShowCalculateBudgetPopup(e.target.checked);
      setSelectedScreen(screen);
    }
  };

  const onAfterChange = (value: number | [number, number]) => {
    setAgeRange(value);
  };
  const handleSelectedScreen = (screen: any) => {
    setSelectedScreen(screen);
    setShowCalculateBudgetPopup(true);
  };

  const handleProcessToPay = () => {
    setOpenCreateCampaignName(true);
  };

  const handleCreateVideoFromImage = (filedata: any) => {
    dispatch(createVideoFromImage(filedata));
    navigate("/singleCampaignOnMultipleScreens", {
      state: {
        campaignName,
        cartItems: value,
      },
    });
    setCampaignName("");
    setFileUrl("");
  };

  const videoUploadHandler = async (e: any) => {
    e.preventDefault();
    const items: string | null = window.localStorage.getItem("carts");
    const value = items ? JSON.parse(items) : [];

    dispatch(
      uploadMedia({
        title: campaignName,
        thumbnail:
          "https://bafybeicduvlghzcrjtuxkro7foazucvuyej25rh3humeujbzt7bmio4hsa.ipfs.w3s.link/raily.png",
        fileUrl,
        media: "",
      })
    );
    navigate("/singleCampaignOnMultipleScreens", {
      state: {
        campaignName,
        cartItems: value,
      },
    });
    setCampaignName("");
    setFileUrl("");
  };

  return (
    <Box>
      <CalculateBudgetPopup
        showCalculateBudget={showCalculateBudget}
        screen={selectedScreen}
        setShowCalculateBudgetPopup={() => setShowCalculateBudgetPopup(false)}
      />
      <CreateCampaignName
        open={openCreateCampaignName}
        onCancel={() => setOpenCreateCampaignName(false)}
        openUploadMedia={() => setOpenUploadMedia(true)}
        setCampaignName={(value: any) => setCampaignName(value)}
        campaignName={campaignName}
      />
      <UploadMedia
        open={openUploadMedia}
        onCancel={() => setOpenUploadMedia(false)}
        createVideo={handleCreateVideoFromImage}
        videoUploadHandler={videoUploadHandler}
        setCampaignName={(value: any) => setCampaignName(value)}
        setFileUrl={(value: any) => setFileUrl(value)}
      />
      <Show below="md">
        <Center>
          <Flex border="1px" borderRadius="60px">
            <Text
              m="0"
              py="2"
              px="2"
              fontSize="10px"
              color="#403F49"
              fontWeight="600"
              bgColor={listView ? "#F3F3F3" : ""}
              borderRadius="60px"
              onClick={() => setListView(true)}
            >
              List view
            </Text>
            <Text
              m="0"
              py="2"
              px="2"
              fontSize="10px"
              color="#403F49"
              fontWeight="600"
              bgColor={listView ? "" : "#F3F3F3"}
              borderRadius="60px"
              onClick={() => setListView(false)}
            >
              Map view
            </Text>
          </Flex>
        </Center>
      </Show>

      {/* Populer screen */}
      {/* {width > 500 && ( */}
      <Box pt="5">
        <Box width={{ base: "100%", lg: "60%" }}>
          <HStack align="center">
            <Hide below="md">
              <Text
                fontSize={{ base: "20px", lg: "3xl" }}
                color="#403F49"
                align="left"
                fontWeight="semibold"
              >
                Popular Adspaces
              </Text>
              <Spacer />
            </Hide>

            <Flex gap={{ base: "10", lg: "5" }}>
              <Button
                px="5"
                py="2"
                color="#6A6A6A"
                bgColor="#F7F7F7"
                borderRadius="20px"
                onClick={() => setShowCheckbox(!showCheckbox)}
              >
                Select
              </Button>
              <Button
                as={Button}
                px="5"
                py="2"
                color="#6A6A6A"
                bgColor="#F7F7F7"
                borderRadius="20px"
                rightIcon={<AiOutlineDown />}
                onClick={() => setShowFilterOption(!showFilterOption)}
              >
                Filter
              </Button>
            </Flex>
          </HStack>
          {/* filter option */}
          {showFilterOption ? (
            <Box py="3">
              <SimpleGrid columns={[3, null, 4]} spacing="2">
                <Popover
                  initialFocusRef={localityFocusRef}
                  placement="bottom"
                  closeOnBlur={false}
                >
                  {({ onClose }) => (
                    <>
                      <PopoverTrigger>
                        <Button
                          variant="outline"
                          px="5"
                          py="2"
                          color="#6A6A6A"
                          _hover={{ bgColor: "#F7F7F7" }}
                          bgColor="#FFFFFF" //
                          borderRadius="20px"
                          rightIcon={<AiOutlineDown />}
                        >
                          Locality
                        </Button>
                      </PopoverTrigger>
                      <PopoverContent
                        color="#838383"
                        bgColor="#FFFFFF"
                        width="400px"
                        borderRadius="16px"
                        borderColor="#838383"
                      >
                        <PopoverBody>
                          <Select
                            mode="multiple"
                            size={"large"}
                            maxTagCount="responsive"
                            placeholder="Please select"
                            value={screenHighlights}
                            onChange={handelAddScreenHighlights}
                            style={{ width: "100%" }}
                            options={filteredOptions.map((item: any) => ({
                              value: item,
                              label: item,
                            }))}
                          />

                          <SimpleGrid columns={[2, 2, 2]} spacing={5} pt="5">
                            {screenHighlights?.map(
                              (data: string, index: any) => (
                                <Button
                                  key={index}
                                  as={Button}
                                  px="5"
                                  _hover={{ bgColor: "#F7F7F7" }}
                                  variant="outline"
                                  py="2"
                                  color="#6A6A6A"
                                  bgColor="#FFFFFF"
                                  borderRadius="20px"
                                >
                                  {data}
                                </Button>
                              )
                            )}
                          </SimpleGrid>
                          <Divider />
                          <Stack align="end">
                            <HStack>
                              <Button
                                py="2"
                                variant="null"
                                color="#D7380E"
                                onClick={() => setScreenHighlights([])}
                              >
                                Clear
                              </Button>
                              <Button
                                py="2"
                                px="10"
                                bgColor="#D7380E"
                                color="#FFFFFF"
                                onClick={onClose}
                                ref={localityFocusRef}
                              >
                                Save
                              </Button>
                            </HStack>
                          </Stack>
                        </PopoverBody>
                      </PopoverContent>
                    </>
                  )}
                </Popover>
                <Popover
                  initialFocusRef={localityFocusRef}
                  placement="bottom"
                  closeOnBlur={false}
                >
                  {({ onClose }) => (
                    <>
                      <PopoverTrigger>
                        <Button
                          as={Button}
                          px="5"
                          _hover={{ bgColor: "#F7F7F7" }}
                          variant="outline"
                          py="2"
                          color="#6A6A6A"
                          bgColor="#FFFFFF"
                          borderRadius="20px"
                          rightIcon={<AiOutlineDown />}
                        >
                          Mobility
                        </Button>
                      </PopoverTrigger>
                      <PopoverContent
                        color="#838383"
                        bgColor="#FFFFFF"
                        width="400px"
                        borderRadius="16px"
                        borderColor="#838383"
                      >
                        <PopoverBody>
                          <Box
                            borderRadius="20px"
                            border="1px"
                            width="40%"
                            px="5"
                            py="2"
                          >
                            <Checkbox
                              onChange={(e: any) => {
                                if (e.target.checked) {
                                  handleAddMobility("All");
                                } else {
                                  handleAddMobility("None");
                                }
                              }}
                              checked={mobility?.length === 3 ? true : false}
                            >
                              Select all
                            </Checkbox>
                          </Box>
                          <HStack pt="5">
                            <Button
                              as={Button}
                              px="5"
                              variant="outline"
                              py="2"
                              color={
                                mobility?.length > 0
                                  ? mobility.includes("Sitting")
                                    ? "#FFFFFF"
                                    : "#6A6A6A"
                                  : "#6A6A6A"
                              }
                              bgColor={
                                mobility?.length > 0
                                  ? mobility.includes("Sitting")
                                    ? "#0EBCF5"
                                    : "#FFFFFF"
                                  : "#FFFFFF"
                              }
                              borderRadius="20px"
                              onClick={() => handleAddMobility("Sitting")}
                            >
                              Sitting
                            </Button>
                            <Button
                              as={Button}
                              px="5"
                              variant="outline"
                              py="2"
                              color={
                                mobility?.length > 0
                                  ? mobility.includes("Moving")
                                    ? "#FFFFFF"
                                    : "#6A6A6A"
                                  : "#6A6A6A"
                              }
                              bgColor={
                                mobility?.length > 0
                                  ? mobility.includes("Moving")
                                    ? "#0EBCF5"
                                    : "#FFFFFF"
                                  : "#FFFFFF"
                              }
                              borderRadius="20px"
                              onClick={() => handleAddMobility("Moving")}
                            >
                              Moving
                            </Button>
                            <Button
                              as={Button}
                              px="5"
                              variant="outline"
                              py="2"
                              color={
                                mobility?.length > 0
                                  ? mobility.includes("Walking")
                                    ? "#FFFFFF"
                                    : "#6A6A6A"
                                  : "#6A6A6A"
                              }
                              bgColor={
                                mobility?.length > 0
                                  ? mobility.includes("Walking")
                                    ? "#0EBCF5"
                                    : "#FFFFFF"
                                  : "#FFFFFF"
                              }
                              borderRadius="20px"
                              onClick={() => handleAddMobility("Walking")}
                            >
                              Walking
                            </Button>
                          </HStack>

                          <Divider />
                          <Stack align="end">
                            <HStack>
                              <Button
                                py="2"
                                variant="null"
                                color="#D7380E"
                                onClick={() => setMobility([])}
                              >
                                Clear
                              </Button>
                              <Button
                                py="2"
                                px="10"
                                bgColor="#D7380E"
                                color="#FFFFFF"
                                onClick={onClose}
                                ref={localityFocusRef}
                              >
                                Save
                              </Button>
                            </HStack>
                          </Stack>
                        </PopoverBody>
                      </PopoverContent>
                    </>
                  )}
                </Popover>
                <Popover
                  initialFocusRef={localityFocusRef}
                  placement="bottom"
                  closeOnBlur={false}
                >
                  {({ onClose }) => (
                    <>
                      <PopoverTrigger>
                        <Button
                          as={Button}
                          px="5"
                          py="2"
                          _hover={{ bgColor: "#F7F7F7" }}
                          variant="outline"
                          color="#6A6A6A"
                          bgColor="#FFFFFF"
                          borderRadius="20px"
                          rightIcon={<AiOutlineDown />}
                        >
                          Age
                        </Button>
                      </PopoverTrigger>

                      <PopoverContent
                        color="#838383"
                        bgColor="#FFFFFF"
                        width="400px"
                        borderRadius="16px"
                        borderColor="#838383"
                      >
                        <PopoverArrow />
                        <PopoverBody>
                          <Box p="5">
                            <Slider
                              range
                              step={2}
                              defaultValue={[20, 50]}
                              onChange={onChange}
                              onAfterChange={onAfterChange}
                            />
                            <Text color="#000000" fontSize="sm" pt="5" m="0">
                              Age group: {ageRange[0]}-{ageRange[1]} years
                            </Text>
                          </Box>

                          <Divider />
                          <Stack align="end">
                            <HStack>
                              <Button
                                py="2"
                                variant="null"
                                color="#D7380E"
                                onClick={onClose}
                              >
                                Clear
                              </Button>
                              <Button
                                py="2"
                                px="10"
                                bgColor="#D7380E"
                                color="#FFFFFF"
                                onClick={onClose}
                                ref={localityFocusRef}
                              >
                                Save
                              </Button>
                            </HStack>
                          </Stack>
                        </PopoverBody>
                      </PopoverContent>
                    </>
                  )}
                </Popover>
                {/* <Popover
                  initialFocusRef={localityFocusRef}
                  placement="bottom"
                  closeOnBlur={false}
                >
                  {({ onClose }) => (
                    <>
                      <PopoverTrigger>
                        <Button
                          variant="outline"
                          _hover={{ bgColor: "#F7F7F7" }}
                          px="5"
                          py="2"
                          color="#6A6A6A"
                          bgColor="#FFFFFF"
                          borderRadius="20px"
                          rightIcon={<AiOutlineDown />}
                        >
                          Gender
                        </Button>
                      </PopoverTrigger>

                      <PopoverContent
                        color="#838383"
                        bgColor="#FFFFFF"
                        width="400px"
                        borderRadius="16px"
                        borderColor="#838383"
                      >
                        <PopoverArrow />
                        <PopoverBody>
                          <Flex p="5" gap="5">
                            <Checkbox
                              onClick={(e: any) => {
                                handleAddOrRemoveGenter(
                                  "Male",
                                  e.target.checked
                                );
                              }}
                              checked={genders?.includes("Male") ? true : false}
                            >
                              <Text color="#000000" fontSize="lg" m="0">
                                Male
                              </Text>
                            </Checkbox>
                            <Checkbox
                              onClick={(e: any) => {
                                handleAddOrRemoveGenter(
                                  "Female",
                                  e.target.checked
                                );
                              }}
                              checked={
                                genders?.includes("Female") ? true : false
                              }
                            >
                              <Text color="#000000" fontSize="lg" m="0">
                                Female
                              </Text>
                            </Checkbox>
                            <Checkbox
                              onClick={(e: any) => {
                                handleAddOrRemoveGenter(
                                  "Unisex",
                                  e.target.checked
                                );
                              }}
                              checked={
                                genders?.includes("Unisex") ? true : false
                              }
                            >
                              <Text color="#000000" fontSize="lg" m="0">
                                Unisex
                              </Text>
                            </Checkbox>
                          </Flex>

                          <Divider />
                          <Stack align="end">
                            <HStack>
                              <Button
                                py="2"
                                variant="null"
                                color="#D7380E"
                                onClick={() => setGenders([])}
                              >
                                Clear
                              </Button>
                              <Button
                                py="2"
                                px="10"
                                bgColor="#D7380E"
                                color="#FFFFFF"
                                onClick={onClose}
                                ref={localityFocusRef}
                              >
                                Save
                              </Button>
                            </HStack>
                          </Stack>
                        </PopoverBody>
                      </PopoverContent>
                    </>
                  )}
                </Popover> */}
                <Popover
                  initialFocusRef={localityFocusRef}
                  placement="bottom"
                  closeOnBlur={false}
                >
                  {({ onClose }) => (
                    <>
                      <PopoverTrigger>
                        <Button
                          variant="outline"
                          _hover={{ bgColor: "#F7F7F7" }}
                          px="5"
                          py="2"
                          color="#6A6A6A"
                          bgColor="#FFFFFF"
                          borderRadius="20px"
                          rightIcon={<AiOutlineDown />}
                        >
                          Employment
                        </Button>
                      </PopoverTrigger>

                      <PopoverContent
                        color="#838383"
                        bgColor="#FFFFFF"
                        width="400px"
                        borderRadius="16px"
                        borderColor="#838383"
                      >
                        <PopoverArrow />
                        <PopoverBody p="5">
                          <HStack gap="5">
                            <Checkbox
                              onClick={(e: any) => {
                                handleAddOrRemoveEmployment(
                                  "Salaried",
                                  e.target.checked
                                );
                              }}
                              checked={
                                employmentType?.includes("Salaried")
                                  ? true
                                  : false
                              }
                            >
                              <Text color="#000000" fontSize="lg" m="0">
                                Salaried
                              </Text>
                            </Checkbox>
                            <Checkbox
                              onClick={(e: any) => {
                                handleAddOrRemoveEmployment(
                                  "Businessman",
                                  e.target.checked
                                );
                              }}
                              checked={
                                employmentType?.includes("Businessman")
                                  ? true
                                  : false
                              }
                            >
                              <Text color="#000000" fontSize="lg" m="0">
                                Businessman
                              </Text>
                            </Checkbox>
                          </HStack>
                          <HStack gap="5" pt="3">
                            <Checkbox
                              onClick={(e: any) => {
                                handleAddOrRemoveEmployment(
                                  "Student",
                                  e.target.checked
                                );
                              }}
                              checked={
                                employmentType?.includes("Student")
                                  ? true
                                  : false
                              }
                            >
                              <Text color="#000000" fontSize="lg" m="0">
                                Student
                              </Text>
                            </Checkbox>
                            <Checkbox
                              onClick={(e: any) => {
                                handleAddOrRemoveEmployment(
                                  "Other",
                                  e.target.checked
                                );
                              }}
                              checked={
                                employmentType?.includes("Other") ? true : false
                              }
                            >
                              <Text color="#000000" fontSize="lg" m="0">
                                Other
                              </Text>
                            </Checkbox>
                          </HStack>

                          <Divider />
                          <Stack align="end">
                            <HStack>
                              <Button
                                py="2"
                                variant="null"
                                color="#D7380E"
                                onClick={() => setEmploymentType([])}
                              >
                                Clear
                              </Button>
                              <Button
                                py="2"
                                px="10"
                                bgColor="#D7380E"
                                color="#FFFFFF"
                                onClick={onClose}
                                ref={localityFocusRef}
                              >
                                Save
                              </Button>
                            </HStack>
                          </Stack>
                        </PopoverBody>
                      </PopoverContent>
                    </>
                  )}
                </Popover>
                <Button
                  py="2"
                  px="10"
                  bgColor="#D7380E"
                  color="#FFFFFF"
                  borderRadius="20px"
                  onClick={handelFilterScreens}
                  ref={localityFocusRef}
                >
                  Apply filter
                </Button>
              </SimpleGrid>
            </Box>
          ) : null}
        </Box>
        {props?.filtered ? (
          <Box>
            <Flex gap="10" align="center" py="2">
              <Text m="0">Filter Applied</Text>
              <Button
                variant="outline"
                color="#D7380E"
                fontSize={{ base: "10px", lg: "md" }}
                borderRadius={{ base: "lg", lg: "2xl" }}
                py="2"
                onClick={() => window.location.reload()}
              >
                Reset filter
              </Button>
            </Flex>

            {props?.filterOption?.averageAgeGroup ? (
              <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
                Age Group : {"    "}
                {props?.filterOption?.averageAgeGroup[0]}
                {" years  "}-{"   "}
                {props?.filterOption?.averageAgeGroup[1]}
                {" years"}
              </Text>
            ) : null}
            {props?.filterOption?.screenHighlights ? (
              <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
                Locality:
                {props?.filterOption?.screenHighlights?.map(
                  (status: any, index: any) => (
                    <Badge colorScheme="gray" ml="2" key={index}>
                      {status}
                    </Badge>
                  )
                )}
              </Text>
            ) : null}
            {props?.filterOption?.maritalStatus ? (
              <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
                Matearital Status:{"    "}
                {props?.filterOption?.maritalStatus?.map(
                  (status: any, index: any) => (
                    <Badge colorScheme="gray" ml="2" key={index}>
                      {status}
                    </Badge>
                  )
                )}
              </Text>
            ) : null}
            {props?.filterOption?.mobility.length > 0 ? (
              <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
                Crowd mobility:{"    "}
                {props?.filterOption?.mobility?.map(
                  (status: any, index: any) => (
                    <Badge colorScheme="gray" ml="2" key={index}>
                      {status}
                    </Badge>
                  )
                )}
              </Text>
            ) : null}
          </Box>
        ) : null}
        <Hide below="md">
          <Flex gap={{ base: "0", lg: "10" }}>
            <Stack
              spacing="5"
              pt={{ base: "5", lg: "5" }}
              pr={{ base: "2", lg: "5" }}
              width="100%"
              overflowY="scroll"
              height={{ base: "700px", lg: "700px" }}
            >
              {screensList?.map((screen: any, index: number) => (
                <Box key={index}>
                  <Flex gap="5">
                    {showCheckbox ? (
                      <Checkbox
                        checked={value.find(
                          (data: any) => data?.screen === screen?._id
                        )}
                        onChange={(e) => {
                          handelSelectCheckBox(e, screen, index);
                        }}
                      ></Checkbox>
                    ) : null}
                    <Box width="100%">
                      <Screen
                        key={index}
                        screen={screen}
                        selectedScreen={handleSelectedScreen}
                      />
                    </Box>
                  </Flex>
                  <Divider orientation="horizontal" color="#D2D2D2" />
                </Box>
              ))}
              {value?.length > 0 && (
                <Box borderRadius={1} borderColor="#EBEBEB">
                  <Flex justifyContent="space-between">
                    <Flex gap="10">
                      <HiOutlineShoppingCart size="25px" />
                      <Text
                        color="#000000"
                        fontWeight="600"
                        fontSize="xl"
                        m="0"
                      >
                        {" "}
                        {value?.length} screens
                      </Text>
                    </Flex>
                    <Flex align="center" gap={5}>
                      <Text
                        color="#000000"
                        fontWeight="600"
                        fontSize="xl"
                        m="0"
                      >
                        ₹
                        {value
                          ?.reduce(
                            (accum: any, current: any) =>
                              accum + current?.totalAmount,
                            0
                          )
                          .toFixed(2)}
                      </Text>
                      <Button
                        color="#ffffff"
                        bgColor="#D7380E"
                        fontSize="md"
                        width="100%"
                        py="3"
                        px="10"
                        onClick={handleProcessToPay}
                      >
                        Proceed to checkout
                      </Button>
                    </Flex>
                  </Flex>
                </Box>
              )}
            </Stack>
            <Box
              px="5px"
              width={{ base: "100%", lg: "60%" }}
              height={{ base: "700px", lg: "700px" }}
              borderRadius="20px"
            >
              {jsonData ? <ShowScreensLocation data={jsonData} /> : null}
            </Box>
          </Flex>
        </Hide>

        <Show below="md">
          {listView ? (
            <Stack
              spacing="5"
              pt={{ base: "5", lg: "5" }}
              // overflowY="scroll"
              width="100%"
            >
              {screensList?.map((screen: any, index: number) => (
                <Box key={index}>
                  <Flex gap="5">
                    {showCheckbox ? (
                      <Checkbox
                        onChange={(e) => handleSelectedScreen(screen)}
                      ></Checkbox>
                    ) : null}
                    <Box width="100%">
                      <Screen
                        key={index}
                        screen={screen}
                        selectedScreen={handleSelectedScreen}
                      />
                    </Box>
                  </Flex>
                  <Divider orientation="horizontal" color="#D2D2D2" />
                </Box>
              ))}
              {value?.length > 0 && (
                <Box borderRadius={1} borderColor="#EBEBEB">
                  <Flex justifyContent="space-between" align="center">
                    <Flex gap="2">
                      <HiOutlineShoppingCart size="25px" />
                      <Text
                        color="#000000"
                        fontWeight="600"
                        fontSize="sm"
                        m="0"
                      >
                        {" "}
                        {value?.length} screens
                      </Text>
                    </Flex>
                    <Flex align="center" gap={5}>
                      <Text
                        color="#000000"
                        fontWeight="600"
                        fontSize="sm"
                        m="0"
                      >
                        ₹
                        {value
                          ?.reduce(
                            (accum: any, current: any) =>
                              accum + current?.totalAmount,
                            0
                          )
                          .toFixed(2)}
                      </Text>
                      <Button
                        color="#ffffff"
                        bgColor="#D7380E"
                        fontSize="sm"
                        py="3"
                        px="5"
                        onClick={handleProcessToPay}
                      >
                        Proceed to checkout
                      </Button>
                    </Flex>
                  </Flex>
                </Box>
              )}
            </Stack>
          ) : (
            <Box
              width={{ base: "100%", lg: "40%" }}
              height="600px"
              borderRadius="20px"
              pt="5"
            >
              {jsonData?.features?.length > 0 ? (
                <ShowScreensLocation data={jsonData} zoom="4" />
              ) : (
                <Text>Loading map.........</Text>
              )}
            </Box>
          )}
        </Show>
      </Box>
    </Box>
  );
}
