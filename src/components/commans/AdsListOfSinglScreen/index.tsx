import {
  Flex,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";

import React, { useState } from "react";
import { BsDot } from "react-icons/bs";
import { convertIntoDateAndTime } from "../../../utils/dateAndTime";
import { CampaignPopup } from "../CampaignPopup";
import { useDispatch } from "react-redux";
import { CampaignLogPopup } from "../../../Pages/Models/CampaignLogPopup";
import { getCampaignLogs } from "../../../Actions/campaignAction";

export function AdsListOfSinglScreen(props: any) {
  const { videos } = props;
  const dispatch = useDispatch<any>();

  const [open, setOpen] = useState<boolean>(false);
  const [campaignPopupView, setCampaignPopupView] = useState<any>(false);
  const [selectedCampaign, setSelectedCampaign] = useState<any>(null);
  const [vid, setVid] = useState<any>();

  const handlePopup = (v: any) => {
    setVid(v.cid);
    setCampaignPopupView(true);
  };

  const handleLogsPopup = (campaign: any) => {
    setSelectedCampaign(campaign);
    dispatch(
      getCampaignLogs({ cid: campaign?.cid, screenId: campaign?.screen })
    );
    setOpen(true);
  };

  return (
    <TableContainer borderRadius="5px" bgColor="#FFFFFF">
      <CampaignPopup
        open={campaignPopupView}
        vid={vid}
        onCancel={() => setCampaignPopupView(false)}
      />

      <CampaignLogPopup
        open={open}
        onCancel={() => setOpen(false)}
        campaign={selectedCampaign}
      />

      <Table variant="simple" size="sm">
        <Thead>
          <Tr>
            <Th>
              <Text fontSize="8px">Name</Text>
            </Th>
            <Th>
              <Text fontSize="8px">Start date</Text>
            </Th>
            <Th>
              <Text fontSize="8px">Total slots</Text>
            </Th>
            <Th>
              <Text fontSize="8px">Price</Text>
            </Th>
            <Th>
              <Text fontSize="8px">Status</Text>
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          {videos?.map((video: any, index: any) => (
            <Tr
              key={index + 1}
              // onClick={() => props.handleShowCampaignLogs(video.cid)}
              _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
            >
              <Td
                onClick={() => handlePopup(video)}
                // isNumeric
                fontSize="10px"
                color="#403F49"
                py="1"
              >
                {video.campaignName}
              </Td>
              <Td color="#575757" fontSize="10px" width="10px" py="1">
                <Text width="10px" m="0">
                  {convertIntoDateAndTime(video.startDate)}
                </Text>
              </Td>
              <Td isNumeric fontSize="10px" color="#403F49" py="1">
                {video.totalSlotBooked}
              </Td>
              <Td fontSize="10px" color="#403F49" py="1">
                ₹{video.rentPerSlot * video.totalSlotBooked}
              </Td>
              {video.status === "Active" ? (
                <Td py="1" onClick={() => handleLogsPopup(video)}>
                  <Flex align="center">
                    <BsDot color="#00D615" size="20px" />
                    <Text color="#403F45" fontSize="10px" pl="0" m="0">
                      Active
                    </Text>
                  </Flex>
                </Td>
              ) : (
                <Td>
                  <Flex align="center">
                    <BsDot size="20px" color="#E93A03" />
                    <Text color="#403F45" fontSize="10px" pl="0" m="0">
                      {video.status}
                    </Text>
                  </Flex>
                </Td>
              )}
            </Tr>
          ))}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
