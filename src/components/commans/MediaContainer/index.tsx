// ui
import { Box, Image, Skeleton } from "@chakra-ui/react";
// utils
import { getMediaType } from "../../../services/utils";
import { useMedia } from "../../../hooks";

export const MediaContainer = ({ cid, height, width, autoPlay }: any) => {
  // console.log("cid :::::::: ", cid);
  const { data: media, isLoading, isError } = useMedia({ id: cid });
  const contentType = getMediaType(media?.headers["content-type"]);
  const ipfsUrl = media?.request["responseURL"];
  // const ipfsUrl = nft;

  // console.log(ipfsUrl);
  // console.log(arweaveUrl);
  const IframeContainer = () => (
    <Box
      as="iframe"
      src={ipfsUrl}
      // onLoad={() =>
      //   triggerPort(media.request["responseURL"].split("/").slice()[4])
      // }
      boxSize="100%"
    />
  );
  const ImageContainer = () => (
    <Image
      src={ipfsUrl}
      // onLoad={() =>
      //   triggerPort(media.request["responseURL"].split("/").slice()[4])
      // }
      boxSize="100%"
      objectFit="cover"
    />
  );
  const VideoContainer = () => (
    <Box
      as="video"
      autoPlay={false}
      loop
      controls
      muted
      // onLoadedData={() =>
      //   triggerPort(media.request["responseURL"].split("/").slice()[4])
      // }
      boxSize="100%"
      borderRadius="sm"
      p="0"
    >
      <source src={ipfsUrl} />
    </Box>
  );

  const renderContainer = () => {
    switch (contentType) {
      case "image":
        return <ImageContainer />;
      case "video":
        return <VideoContainer />;
      case "iframe":
        return <IframeContainer />;
      default:
        return <></>;
    }
  };
  return (
    <Box rounded="xl" height={height} width={width} overflow="hidden">
      {isLoading ? (
        <Skeleton>
          <Box height={height} width={width}></Box>{" "}
        </Skeleton>
      ) : (
        renderContainer()
      )}
    </Box>
  );
};
