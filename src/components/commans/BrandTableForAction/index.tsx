import {
  Text,
  Flex,
  Tooltip,
  IconButton,
  Show,
  Box,
  Hide,
} from "@chakra-ui/react";
import { BsDot, BsPauseCircle } from "react-icons/bs";
import { ColumnsType } from "antd/es/table";
import { Table, message } from "antd";
import { FaPlayCircle } from "react-icons/fa";
import { RiDeleteBin6Line } from "react-icons/ri";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";

import { useEffect } from "react";
import { convertIntoDateAndTime } from "../../../utils/dateAndTime";
import { CAMPAIGN_DELETE_RESET } from "../../../Constants/campaignConstants";
import {
  changeCampaignStatus,
  getCampaignListByScreenId,
} from "../../../Actions/campaignAction";
import { AdsListOfSignleScreenMobileView } from "../AdsListOfSignleScreenMobileView";

export function BrandTableForAction(props: any) {
  const columns: ColumnsType<any> = [
    {
      title: "preview",
      dataIndex: "logo",
      key: "logo",
      render: (_, value) => (
        <Box
          as="video"
          src={`https://ipfs.io/ipfs/${value.cid}`}
          width="70px"
          height="70px"
        />
      ),
    },
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Name
        </Text>
      ),
      dataIndex: "campaignName",
      key: "campaignName",
      render: (value) => (
        <Text color="#000000" fontSize="lg" fontWeight="400">
          {value}
        </Text>
      ),
    },

    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Start
        </Text>
      ),
      dataIndex: "startDate",
      key: "startDate",
      render: (value) => (
        <Text
          color="convertIntoDateAndTime#000000"
          fontSize="md"
          fontWeight="500"
        >
          {convertIntoDateAndTime(value)}
        </Text>
      ),
    },
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Total slots
        </Text>
      ),
      dataIndex: "totalSlotBooked",
      key: "totalSlotBooked",
      render: (value) => (
        <Text color="#000000" fontSize="22px" fontWeight="400">
          {value}
        </Text>
      ),
    },
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Price
        </Text>
      ),
      dataIndex: "totalAmount",
      key: "totalAmount",
      render: (value) => (
        <Text color="#4339F2" fontSize="22px" fontWeight="400">
          ₹ {value}
        </Text>
      ),
    },
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Status
        </Text>
      ),
      dataIndex: "status",
      key: "status",
      render: (value) => (
        <Flex>
          <BsDot
            color={
              value === "Active"
                ? "#00D615"
                : value === "Deleted"
                ? "#E93A03"
                : "yellow"
            }
            size="20px"
          />
          <Text color="#403F45" fontSize="sm" pl="2">
            {value}
          </Text>
        </Flex>
      ),
    },
    {
      title: "Action",
      dataIndex: "_id",
      key: "_id",
      render: (_, video) => (
        <Flex gap={3}>
          {!video.isPause ? (
            <Tooltip
              hasArrow
              label="Pause campaign"
              fontSize="md"
              bg="gray.300"
              color="black"
            >
              <IconButton
                bg="none"
                icon={<BsPauseCircle size="25px" color="black" />}
                aria-label="Home"
                onClick={() => handleChangeCampaignStatus(video?._id, "Pause")}
              ></IconButton>
            </Tooltip>
          ) : (
            <Tooltip
              hasArrow
              label="Resume campaign"
              fontSize="md"
              bg="gray.300"
              color="black"
            >
              <IconButton
                bg="none"
                icon={<FaPlayCircle size="25px" color="black" />}
                aria-label="Home"
                onClick={() => handleChangeCampaignStatus(video?._id, "Resume")}
              ></IconButton>
            </Tooltip>
          )}
          <Tooltip
            hasArrow
            label="Delete campaign"
            fontSize="md"
            bg="gray.300"
            color="black"
          >
            <IconButton
              bg="nome"
              icon={<RiDeleteBin6Line size="25px" color="red" />}
              aria-label="Home"
              onClick={() => handleChangeCampaignStatus(video?._id, "Deleted")}
            ></IconButton>
          </Tooltip>
        </Flex>
      ),
    },
  ];

  const dispatch = useDispatch<any>();
  const campaignDelete = useSelector((state: any) => state.campaignDelete);
  const {
    loading: loadingCampaignDelete,
    error: errorCampaignDelete,
    success: changeCampaignStatusStatus,
  } = campaignDelete;

  const resetDelete = () => {
    dispatch({
      type: CAMPAIGN_DELETE_RESET,
    });
  };

  const handleChangeCampaignStatus = (campaignId: any, status: any) => {
    dispatch(changeCampaignStatus(campaignId, status));
  };
  const activeCampaignListByScreenID = useSelector(
    (state: any) => state.activeCampaignListByScreenID
  );
  const {
    loading: loadingCampaign,
    error: errorCampaign,
    campaigns: videosList,
  } = activeCampaignListByScreenID;

  useEffect(() => {
    if (errorCampaignDelete) {
      message.error(errorCampaignDelete);
      resetDelete();
    } else if (changeCampaignStatusStatus) {
      message.success("status change successfully");
      resetDelete();
    }
    dispatch(getCampaignListByScreenId(props?.screenId));
  }, [dispatch, props, changeCampaignStatusStatus, errorCampaignDelete]);
  return (
    <Box>
      <Hide below="md">
        <Table columns={columns} dataSource={videosList || []} />
      </Hide>
      <Show below="md">
        <AdsListOfSignleScreenMobileView
          videos={videosList || []}
          show={true}
          handleChangeCampaignStatus={(campaignId: any, status: any) =>
            handleChangeCampaignStatus(campaignId, status)
          }
        />
      </Show>
    </Box>
  );
}
