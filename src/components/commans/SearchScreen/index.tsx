import {
  Box,
  Button,
  Center,
  HStack,
  Hide,
  Image,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import { IoSearchCircleSharp } from "react-icons/io5";

import {
  college,
  turist,
  beach,
  tarin,
  railayStation,
  plain,
  military,
  appartment,
  smimmer,
  nav,
} from "../../../assets/svg";
import { FaSchool } from "react-icons/fa";
import { useState } from "react";
import { filteredScreenList } from "../../../Actions/screenActions";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { message } from "antd";

export function SearchScreen(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [locationSearch, setLocationSearch] = useState<any>("");
  const [locality, setLocality] = useState<any>([]);

  // console.log("filterd screens : ", JSON.stringify(screens));

  const handelClearHomeSearch = () => {
    setLocationSearch("");
    setLocality([]);
  };

  const handleSearch = () => {
    if (locationSearch) {
      dispatch(
        filteredScreenList({
          locationSearch,
          locality: JSON.stringify(locality),
        })
      );
      navigate("/myFilteredScreens");
    } else message.error("Please enter your search location");
  };

  const addOrRemoveLocality = (lable: any) => {
    if (locality.includes(lable)) {
      setLocality([...locality.filter((data: any) => data !== lable)]);
    } else {
      setLocality([...locality, lable]);
    }
  };
  return (
    <Box>
      {/* Search Button */}
      <Box width={{ base: "100%", lg: "30%" }}>
        <InputGroup>
          <Input
            placeholder="search by location, screen name"
            py="3"
            borderRadius="50px"
            pl="10"
            value={locationSearch}
            color="#4A4A4A"
            fontSize="lg"
            autoFocus={true}
            onChange={(e: any) => {
              setLocationSearch(e.target.value);
            }}
            onKeyPress={(e: any) => {
              if (e.which === 13) {
                handleSearch();
              }
            }}
          />
          <InputLeftElement p="5">
            <Center>
              <Image src={nav} alt="nav" />
            </Center>
          </InputLeftElement>

          <InputRightElement width="" p="2">
            <Center>
              <IoSearchCircleSharp
                size="40px"
                color="#D7380E"
                onClick={handleSearch}
              />
            </Center>
          </InputRightElement>
        </InputGroup>
      </Box>

      {/* Localities */}
      <Box py="5">
        <Hide below="md">
          <Text fontSize="3xl" fontWeight="bold" color="#403F49">
            Localities
          </Text>
        </Hide>
        <Box className="scrollmenu">
          <VStack
            p="3"
            width="120px"
            onClick={() => addOrRemoveLocality("Swimming pool")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <Image src={smimmer} alt="smimmer" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              align="center"
              color={
                locality?.includes("Swimming pool") ? "#0EBCF5" : "#515151"
              }
            >
              Pool
            </Text>
          </VStack>
          <VStack
            p="3"
            width="120px"
            onClick={() => addOrRemoveLocality("Appartments")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <Image src={appartment} alt="smimmer" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              align="center"
              color={locality?.includes("Appartments") ? "#0EBCF5" : "#515151"}
            >
              Appartment
            </Text>
          </VStack>
          <VStack
            width="120px"
            p="3"
            onClick={() => addOrRemoveLocality("School")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <FaSchool size="30px" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              color={locality?.includes("School") ? "#0EBCF5" : "#515151"}
              align="center"
            >
              School
            </Text>
          </VStack>
          <VStack
            p="3"
            width="120px"
            onClick={() => addOrRemoveLocality("Colleges")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <Image src={college} alt="college" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              color={locality?.includes("Colleges") ? "#0EBCF5" : "#515151"}
              align="center"
            >
              College
            </Text>
          </VStack>
          <VStack
            p="3"
            width="120px"
            onClick={() => addOrRemoveLocality("Metro")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <Image src={tarin} alt="tarin" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              color={locality?.includes("Metro") ? "#0EBCF5" : "#515151"}
              align="center"
            >
              Metro
            </Text>
          </VStack>
          <VStack
            p="3"
            width="120px"
            onClick={() => addOrRemoveLocality("Tourist attractions")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <Image src={turist} alt="turist" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              color={
                locality?.includes("Tourist attractions")
                  ? "#0EBCF5"
                  : "#515151"
              }
              align="center"
            >
              Tourist
            </Text>
          </VStack>
          <VStack
            p="3"
            width="120px"
            onClick={() => addOrRemoveLocality("Airport")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <Image src={plain} alt="plain" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              color={locality?.includes("Airport") ? "#0EBCF5" : "#515151"}
              align="center"
            >
              Airport
            </Text>
          </VStack>
          <VStack
            p="3"
            width="120px"
            onClick={() => addOrRemoveLocality("Beach")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <Image src={beach} alt="beach" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              color={locality?.includes("Beach") ? "#0EBCF5" : "#515151"}
              align="center"
            >
              Beach
            </Text>
          </VStack>
          <VStack
            p="3"
            width="120px"
            onClick={() => addOrRemoveLocality("Military")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <Image src={military} alt="military" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              color={locality?.includes("Military") ? "#0EBCF5" : "#515151"}
              align="center"
            >
              Military
            </Text>
          </VStack>

          <VStack
            p="3"
            width="120px"
            onClick={() => addOrRemoveLocality("Railways Station")}
          >
            <Center
              border="1px"
              borderRadius="100%"
              borderColor="#403F49"
              p="3"
            >
              <Image src={railayStation} alt="railayStation" />
            </Center>
            <Text
              fontSize={{ base: "12px", lg: "sm" }}
              pt="3"
              color={
                locality?.includes("Railways Station") ? "#0EBCF5" : "#515151"
              }
              align="center"
            >
              Railway
            </Text>
          </VStack>
        </Box>
        {locationSearch ? (
          <Stack align="end">
            <HStack>
              <Button
                py="2"
                variant="null"
                color="#D7380E"
                onClick={handelClearHomeSearch}
              >
                Clear
              </Button>
              <Button
                py="2"
                bgColor="#D7380E"
                color="#FFFFFF"
                onClick={handleSearch}
              >
                Search
              </Button>
            </HStack>
          </Stack>
        ) : null}
      </Box>
    </Box>
  );
}
