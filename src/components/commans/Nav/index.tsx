import { useSelector, useDispatch } from "react-redux";
import * as React from "react";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import {
  Box,
  Stack,
  Image,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Tooltip,
  Center,
  Button,
  HStack,
  Text,
  Hide,
  Show,
  Divider,
} from "@chakra-ui/react";
// import {
//   AiOutlineFundProjectionScreen,
//   AiOutlineBell,
//   AiOutlineUser,
//   AiOutlineLogout,
// } from "react-icons/ai";

import { useEffect } from "react";
import { BiChevronDown } from "react-icons/bi";
// import { HiOutlineShoppingCart } from "react-icons/hi";
import { TbUser, TbBell } from "react-icons/tb";
import Logo from "../../../assets/Images/logo.png";
import {
  confirmPwaInstalledByUser,
  signout,
} from "../../../Actions/userActions";
import { Popover, Skeleton, message } from "antd";
import { listAllPleasByUserId } from "../../../Actions/pleaActions";
import { GiHamburgerMenu } from "react-icons/gi";
import { name } from "../../../assets/funkfeets";
import { HiOutlineShoppingCart } from "react-icons/hi";
import { isPWA } from "../../../utils";
import { CheckPWAInstalled, InstallPWA } from "../PwaPopup";

export const Nav = () => {
  const navigate = useNavigate();
  const style = {
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    position: "fixed",
    left: "0",
    top: "0",
    width: "100%",
    zIndex: window.location.pathname.split("/").includes("myplaylistfullscreen")
      ? -1
      : 1,
    backgroundColor: "#ffffff90",
  };
  const [cartLength, setCartLength] = React.useState<any>(0);

  const btnRef = React.useRef(null);

  const dispatch = useDispatch<any>();
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const allPleasListByUser = useSelector(
    (state: any) => state.allPleasListByUser
  );
  const {
    allPleas,
    loading: loadingAllPleas,
    error: errorAllPleas,
  } = allPleasListByUser;

  const signoutHandler = () => {
    dispatch(signout());
    navigate("/");
  };

  useEffect(() => {
    if (errorAllPleas) {
      message.error(errorAllPleas);
    }
  }, [errorAllPleas]);

  useEffect(() => {
    if (userInfo) {
      dispatch(listAllPleasByUserId());
      const items: string | null = window.localStorage.getItem("carts");
      // Parse stored json or if none return initialValue
      const value = items ? JSON.parse(items) : [];
      setCartLength(value?.length);
      if (
        (!userInfo.pwaInstalled || userInfo.pwaInstalled === false) &&
        isPWA()
      ) {
        dispatch(confirmPwaInstalledByUser({ userId: userInfo._id }));
      }
    }
  }, [dispatch, navigate, userInfo]);

  const content = (
    <Box>
      {loadingAllPleas ? (
        <Skeleton active />
      ) : (
        <Box>
          {allPleas?.slice(0, 5)?.map((plea: any) => (
            <Text
              p="2"
              _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
              key={plea?._id}
            >
              {plea?.remarks[plea?.remarks?.length - 1]}
            </Text>
          ))}
          <Center>
            <Button
              type="submit"
              color="#0EBCF5"
              variant="outline"
              bgColor="#FFFFFF"
              _hover={{ color: "#FFFFFF", bgColor: "#0EBCF5" }}
              py="2"
              px="10"
              mt="2"
              fontWeight="600"
              fontSize={{ base: "sm", lg: "md" }}
              onClick={() => navigate(`/pleaBucket`)}
            >
              See all
            </Button>
          </Center>
        </Box>
      )}
    </Box>
  );

  return (
    <Box __css={style}>
      <Center>
        <Stack
          align="center"
          direction="row"
          justifyContent="space-between"
          width="100%"
          px={{ base: "5", lg: "20" }}
          py="3"
        >
          <Stack as={RouterLink} to="/" direction="row" align="center">
            <Image width={{ base: "30px", lg: "45px" }} src={Logo} />
            {/* <Hide below="360px"> */}
            <Image mt="1" width={{ base: "70px", lg: "100px" }} src={name} />
            {/* </Hide> */}
            {(userInfo?.isMaster || userInfo?.isBrand) && (
              <Box height="100%">
                <Text mb="-2" fontSize="xs" fontWeight="600">
                  Business
                </Text>
              </Box>
            )}
          </Stack>
          {!userInfo ? (
            <Stack flexDirection="row" align="center">
              <Button
                as={RouterLink}
                to={`/signup`}
                size="sm"
                fontSize={{ base: "xs", lg: "sm" }}
                ref={btnRef}
                py="2"
                px={{ base: "2", lg: "5" }}
                color="#0EBCF5"
                bgColor="#FFFFFF"
                _hover={{ bgColor: "#0EBCF5", color: "#FFFFFF" }}
                borderRadius="53px"
                variant="outline"
              >
                Create Account
              </Button>
              <Button
                as={RouterLink}
                to={`/signin`}
                size="sm"
                fontSize={{ base: "xs", lg: "sm" }}
                py="2"
                px={{ base: "2", lg: "5" }}
                color="#FFFFFF"
                bgColor="#D7380E"
                _hover={{ bgColor: "#FFFFFF", color: "#D7380E" }}
                variant="outline"
                borderRadius="53px"
              >
                Log in
              </Button>
              {isPWA() ? <CheckPWAInstalled /> : <InstallPWA />}
            </Stack>
          ) : (
            <>
              <Hide below="md">
                <Stack
                  align="center"
                  justifyContent="flex-end"
                  direction="row"
                  width="80%"
                  mt="1px"
                  display={{ base: "none", md: "flex" }}
                >
                  <Popover
                    placement="bottom"
                    title="Notification"
                    content={content}
                    arrow={true}
                    trigger="hover"
                    overlayStyle={{
                      width: "25vw",
                    }}
                  >
                    <Box
                      borderRadius="100%"
                      border="2px"
                      height="40px"
                      width="40px"
                      borderColor="#403F49"
                      alignItems="center"
                      color="#403F49"
                      display="flex"
                      _hover={{ color: "teal.600", borderColor: "teal.600" }}
                    >
                      <Stack mt="2" ml="2">
                        <TbBell size="20px" fontWeight="1" />
                      </Stack>
                      <Text
                        color="#F23309"
                        fontSize="10"
                        mx="-1"
                        fontWeight="600"
                      >
                        {allPleas?.length}
                      </Text>
                    </Box>
                  </Popover>
                  {(userInfo?.isMaster || userInfo?.isBrand) && (
                    <Box
                      borderRadius="100%"
                      border="2px"
                      height="40px"
                      width="40px"
                      borderColor="#403F49"
                      color="#403F49"
                      display="flex"
                      onClick={() => navigate("/cart")}
                      _hover={{ color: "teal.600", borderColor: "teal.600" }}
                    >
                      <Stack mt="2" ml="2">
                        <HiOutlineShoppingCart size="20px" fontWeight="1" />
                      </Stack>
                      <Text
                        color="#F23309"
                        fontSize="10"
                        mx="-1"
                        fontWeight="600"
                      >
                        {cartLength}
                      </Text>
                    </Box>
                  )}
                  <Box
                    borderRadius="100%"
                    border="2px"
                    height="40px"
                    width="40px"
                    borderColor="#403F49"
                    color="#403F49"
                    _hover={{
                      color: "teal.600",
                      borderColor: "teal.600",
                    }}
                  >
                    <Stack mt="2" ml="2">
                      <TbUser
                        size="20px"
                        fontWeight="1"
                        onClick={() => navigate("/userprofile")}
                      />
                    </Stack>
                  </Box>
                  <Menu>
                    <MenuButton>
                      <Tooltip
                        bg="violet.500"
                        color="white"
                        hasArrow
                        placement="bottom"
                        label="Click for Menu"
                      >
                        <Center as={RouterLink} to="/">
                          <HStack>
                            <BiChevronDown
                              size="30px"
                              fontWeight="1"
                              color="#403F49"
                            />
                          </HStack>
                        </Center>
                      </Tooltip>
                    </MenuButton>
                    <MenuList width="300px">
                      <MenuItem
                        as={RouterLink}
                        to={`/userProfile`}
                        color="black"
                        justifyContent="space-between"
                        // icon={<AiOutlineUser size="20px" />}
                      >
                        <Text my="1" textAlign="center">
                          Account
                        </Text>
                        <Image
                          // border="1px"
                          height="45px"
                          width="45px"
                          rounded="45px"
                          src={userInfo?.avatar}
                        />
                      </MenuItem>
                      <Divider py="" />
                      {(userInfo?.isMaster || userInfo?.isBrand) && (
                        <MenuItem
                          as={RouterLink}
                          to={`/myCampaigns`}
                          color="black"
                          // icon={<AiOutlineFundProjectionScreen size="20px" />}
                        >
                          My Campaigns
                        </MenuItem>
                      )}
                      {userInfo?.isBrand && (
                        <MenuItem
                          as={RouterLink}
                          to={`/coupon/brand`}
                          color="black"
                          // icon={<AiOutlineFundProjectionScreen size="20px" />}
                        >
                          My Offers
                        </MenuItem>
                      )}
                      {(userInfo?.isMaster || userInfo?.isBrand) && (
                        <Divider py="" />
                      )}
                      {userInfo?.isMaster && (
                        <MenuItem
                          as={RouterLink}
                          to={`/myScreens`}
                          color="black"
                          // icon={<AiOutlineFundProjectionScreen size="20px" />}
                        >
                          My Adspaces
                        </MenuItem>
                      )}
                      {userInfo?.isMaster && <Divider py="" />}
                      <MenuItem
                        as={RouterLink}
                        to={`/userCoupons`}
                        color="black"
                        onClick={() => navigate(`/userCoupons`)}
                        // icon={<AiOutlineBell size="20px" />}
                      >
                        My coupons
                      </MenuItem>
                      {!userInfo?.isMaster && !userInfo?.isBrand && (
                        <>
                          <MenuItem
                            as={RouterLink}
                            to={`/myWishlist`}
                            color="black"
                            // onClick={() => navigate(`/myWishlist`)}
                            // icon={<AiOutlineBell size="20px" />}
                          >
                            My wishlist
                          </MenuItem>
                        </>
                      )}

                      {userInfo?.isItanimulli && (
                        <MenuItem
                          as={RouterLink}
                          to={`/wallet`}
                          color="black"
                          onClick={() => navigate(`/wallet`)}
                        >
                          Wallet
                        </MenuItem>
                      )}
                      <MenuItem
                        as={RouterLink}
                        to={`/pleaBucket`}
                        color="black"
                        // icon={<AiOutlineBell size="20px" />}
                      >
                        Help
                      </MenuItem>
                      <MenuItem
                        onClick={signoutHandler}
                        color="black"
                        // icon={<AiOutlineLogout size="20px" />}
                      >
                        Logout
                      </MenuItem>
                    </MenuList>
                  </Menu>
                  {isPWA() ? <CheckPWAInstalled /> : <InstallPWA />}
                </Stack>
              </Hide>
              <Show below="md">
                <Stack direction="row" align="center">
                  <Menu>
                    <MenuButton>
                      <GiHamburgerMenu size="25px" color="#000000" />
                    </MenuButton>
                    <MenuList width="300px">
                      <MenuItem
                        as={RouterLink}
                        to={`/userProfile`}
                        color="black"
                        // icon={<AiOutlineUser size="20px" />}
                      >
                        <HStack
                          width="100%"
                          justify="space-between"
                          align="center"
                          // border="1px"
                        >
                          <Text my="1" textAlign="center">
                            Account
                          </Text>
                          <Image
                            // border="1px"
                            height="45px"
                            width="45px"
                            rounded="45px"
                            src={userInfo?.avatar}
                          />
                        </HStack>
                      </MenuItem>
                      <Divider py="" />

                      {(userInfo?.isMaster || userInfo?.isBrand) && (
                        <MenuItem
                          as={RouterLink}
                          to={`/myCampaigns`}
                          color="black"
                          // icon={<AiOutlineFundProjectionScreen size="20px" />}
                        >
                          My Campaigns
                        </MenuItem>
                      )}
                      {userInfo?.isBrand && (
                        <MenuItem
                          as={RouterLink}
                          to={`/coupon/brand`}
                          color="black"
                          // icon={<AiOutlineFundProjectionScreen size="20px" />}
                        >
                          My Offers
                        </MenuItem>
                      )}
                      {(userInfo?.isMaster || userInfo?.isBrand) && (
                        <Divider py="" />
                      )}
                      {userInfo?.isMaster && (
                        <MenuItem
                          as={RouterLink}
                          to={`/myScreens`}
                          color="black"
                          // icon={<AiOutlineFundProjectionScreen size="20px" />}
                        >
                          My Adspaces
                        </MenuItem>
                      )}
                      {userInfo?.isMaster && <Divider py="" />}
                      {/* {!userInfo?.isMaster && !userInfo?.isBrand && ( */}
                      <MenuItem
                        as={RouterLink}
                        to={`/userCoupons`}
                        color="black"
                        onClick={() => navigate(`/userCoupons`)}
                        // icon={<AiOutlineBell size="20px" />}
                      >
                        My coupons
                      </MenuItem>
                      {/* )} */}
                      {!userInfo?.isMaster && !userInfo?.isBrand && (
                        <>
                          <MenuItem
                            as={RouterLink}
                            to={`/myWishlist`}
                            color="black"
                            // onClick={() => navigate(`/myWishlist`)}
                            // icon={<AiOutlineBell size="20px" />}
                          >
                            My wishlist
                          </MenuItem>
                        </>
                      )}
                      {userInfo?.isItanimulli && (
                        <MenuItem
                          as={RouterLink}
                          to={`/wallet`}
                          color="black"
                          onClick={() => navigate(`/wallet`)}
                        >
                          Wallet
                        </MenuItem>
                      )}

                      <MenuItem
                        as={RouterLink}
                        to={`/pleaBucket`}
                        color="black"
                        onClick={() => navigate(`/pleaBucket`)}
                        // icon={<AiOutlineBell size="20px" />}
                      >
                        Notification
                      </MenuItem>

                      <MenuItem
                        onClick={signoutHandler}
                        color="black"
                        // icon={<AiOutlineLogout size="20px" />}
                      >
                        Logout
                      </MenuItem>
                    </MenuList>
                  </Menu>
                  {isPWA() ? <CheckPWAInstalled /> : <InstallPWA />}
                </Stack>
              </Show>
            </>
          )}
        </Stack>
      </Center>
    </Box>
  );
};
