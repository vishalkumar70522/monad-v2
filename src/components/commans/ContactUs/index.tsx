import {
  Box,
  Flex,
  Text,
  Button,
  Image,
  Center,
  Stack,
  Divider,
  IconButton,
  Hide,
} from "@chakra-ui/react";
import React from "react";
import { GrDown } from "react-icons/gr";

export function ContactUs() {
  return (
    <Center height={{ base: "", lg: "" }}>
      <Stack width="100%">
        <Box
          backgroundColor="#2BB3E0"
          display="inline-block"
          backgroundRepeat="no-repeat"
          // backgroundAttachment="fixed"
          backgroundSize="100%"
          borderRadius={{ base: "0", lg: "16px" }}
        >
          <Flex align="center" justify="space-between">
            <Box
              width={{ base: "100%", lg: "60%" }}
              color="#EBEBEB"
              alignItems="left"
              py={{ base: "5", lg: "15" }}
              px={{ base: "10", lg: "20" }}
            >
              <Text
                fontSize={{ base: "xl", lg: "4xl" }}
                fontWeight="bold"
                align="left"
              >
                Need help or want to know how it works?
              </Text>
              <Text
                fontSize={{ base: "md", lg: "2xl" }}
                fontWeight="bold"
                align="left"
                pt="5"
              >
                Our industry expersts are here to help you.
              </Text>
              <Stack pt="10">
                <Button
                  width={{ base: "50%", lg: "30%" }}
                  bgColor="#D7380E"
                  color="#FFFFFF"
                  fontSize={{ base: "lg", lg: "xl" }}
                  fontWeight="semibold"
                  py={{ base: "2", lg: "3" }}
                >
                  Contact us
                </Button>
              </Stack>
            </Box>
            <Hide below="md">
              <Box py="3">
                <Image
                  src="https://bafybeianqzktg4aqnrpar75txd6enrc6kc2yvojfzb6t3dfo5hnv3e4t4y.ipfs.w3s.link/girl2.png"
                  alt=""
                  p=""
                  height="377px"
                  width=""
                />
              </Box>
            </Hide>
          </Flex>
        </Box>
        <Divider pt="5" color="#000000" />
        <Flex align="center" justifyContent="space-between" p="0">
          <Text
            color="#000000"
            fontSize={{ base: "lg", lg: "3xl" }}
            fontWeight="semibold"
            align="left"
          >
            All destinations
          </Text>
          <IconButton
            bg="none"
            mr="10"
            icon={<GrDown color="#9A9A9A" size="30px" />}
            aria-label="Star"
          ></IconButton>
        </Flex>
        <Divider color="#000000" />
      </Stack>
    </Center>
  );
}
