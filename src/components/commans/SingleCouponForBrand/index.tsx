import {
  Box,
  Button,
  Divider,
  Flex,
  IconButton,
  Image,
  Text,
} from "@chakra-ui/react";
import { Popconfirm, Switch } from "antd";
import { AiOutlineEdit } from "react-icons/ai";
import { persent } from "../../../assets/svg";
import { useNavigate } from "react-router-dom";
import { RiDeleteBin6Line } from "react-icons/ri";
import {
  COUPON_STATUS_ACTIVE,
  COUPON_STATUS_DELETED,
  COUPON_STATUS_INACTIVE,
} from "../../../Constants/couponConstants";

export function SingleCouponForBrand(props: any) {
  const { coupon } = props;
  const navigate = useNavigate();

  const handelChangeStatus = (value: boolean) => {
    if (value) {
      props?.handelDeleteCoupon(coupon?._id, COUPON_STATUS_ACTIVE);
    } else {
      props?.handelDeleteCoupon(coupon?._id, COUPON_STATUS_INACTIVE);
    }
  };

  return (
    <Box
      border="1px"
      borderColor="#D7D7D7"
      px={{ base: "2", lg: "5" }}
      py={{ base: "2", lg: "5" }}
      bgColor="#FFFFFF"
      borderRadius="8px"
      width={{ base: "100%", lg: "417px" }}
    >
      <Flex justifyContent="space-between" align="center">
        <Flex gap="2" align="center">
          <Image src={persent} alt="" height="24px" width="24px" />
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="600"
            color="#000000"
            textAlign="left"
            m="0"
          >
            {coupon?.offerName}
          </Text>
        </Flex>
        <Switch
          onChange={(checked: boolean) => handelChangeStatus(checked)}
          checked={coupon.status === COUPON_STATUS_ACTIVE ? true : false}
          disabled={
            coupon.status === COUPON_STATUS_ACTIVE ||
            coupon.status === COUPON_STATUS_INACTIVE
              ? false
              : true
          }
        />
      </Flex>
      {/* <Text
        fontSize={{ base: "", lg: "lg" }}
        fontWeight="400"
        color="#717171"
        textAlign="left"
        m="0"
      >
        {coupon?.offerDetails}
      </Text> */}
      <Flex justifyContent="space-between" pt="5">
        <Flex direction="column">
          <Text
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="400"
            color="#000000"
            textAlign="left"
            m="0"
          >
            Times used
          </Text>
          <Text
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="400"
            color="#000000"
            textAlign="left"
            m="0"
          >
            0
          </Text>
        </Flex>
        <Flex direction="column">
          <Text
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="400"
            color="#000000"
            textAlign="left"
            m="0"
          >
            Revenue generated
          </Text>
          <Text
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="400"
            color="#000000"
            textAlign="left"
            m="0"
          >
            ₹0
          </Text>
        </Flex>
      </Flex>
      <Divider variant="dashed" color="#D7D7D7" />
      <Flex justifyContent="space-between" align="center">
        <Text
          fontSize={{ base: "", lg: "lg" }}
          fontWeight="600"
          color="#000000"
          textAlign="left"
          m="0"
          onClick={() =>
            navigate(`/couponManagement/${coupon?._id}`, {
              state: {
                coupon,
              },
            })
          }
        >
          {coupon?.campaigns?.length} campaigns linked
        </Text>
        <Button
          variant="null"
          color="#000000"
          leftIcon={<AiOutlineEdit size="20px" />}
          fontWeight="600"
          fontSize="md"
          py="3"
          _hover={{ bgColor: "red", color: "#FFFFFF" }}
          onClick={() =>
            navigate("/coupon/editCoupon", {
              state: {
                coupon,
                couponType: coupon?.couponRewardInfo?.couponType,
              },
            })
          }
        >
          Edit
        </Button>
        <Popconfirm
          title="Delete coupon"
          description="Do you really want to delete this coupon?"
          onConfirm={() =>
            props?.handelDeleteCoupon(coupon?._id, COUPON_STATUS_DELETED)
          }
          okText="Yes"
          cancelText="No"
        >
          <IconButton
            bg="nome"
            icon={<RiDeleteBin6Line size="25px" color="red" />}
            aria-label="Delete"
          ></IconButton>
        </Popconfirm>
      </Flex>
    </Box>
  );
}
