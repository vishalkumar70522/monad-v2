import {
  Card,
  CardBody,
  Stack,
  Text,
  HStack,
  Flex,
  Tooltip,
  IconButton,
} from "@chakra-ui/react";
import { BsPauseCircle } from "react-icons/bs";
import { RiDeleteBin6Line } from "react-icons/ri";
import { FaPlayCircle } from "react-icons/fa";
import { convertIntoDateAndTime } from "../../../utils/dateAndTime";
import { BsDot } from "react-icons/bs";
import { useState } from "react";
import { CampaignPopup } from "../CampaignPopup";
import { useDispatch } from "react-redux";
import { CampaignLogPopup } from "../../../Pages/Models/CampaignLogPopup";
import { getCampaignLogs } from "../../../Actions/campaignAction";

export function AdsListOfSignleScreenMobileView(props: any) {
  const { videos } = props;
  const dispatch = useDispatch<any>();

  const [selectedIndex, setSelectedIndex] = useState<any>(-1);
  const [open, setOpen] = useState<boolean>(false);
  const [campaignPopupView, setCampaignPopupView] = useState<any>(false);
  const [vid, setVid] = useState<any>();
  const [selectedCampaign, setSelectedCampaign] = useState<any>(null);

  const handlePopup = (v: any) => {
    setVid(v.cid);
    setCampaignPopupView(true);
  };

  const handleLogsPopup = (campaign: any) => {
    setSelectedCampaign(campaign);
    dispatch(
      getCampaignLogs({ cid: campaign?.cid, screenId: campaign?.screen })
    );
    setOpen(true);
  };

  return (
    <Stack spacing={3}>
      <CampaignPopup
        open={campaignPopupView}
        vid={vid}
        onCancel={() => setCampaignPopupView(false)}
      />
      <CampaignLogPopup
        open={open}
        onCancel={() => setOpen(false)}
        campaign={selectedCampaign}
      />
      {videos?.length > 0
        ? videos.map((video: any, index: any) => (
            <Card
              key={index}
              bg="#F9F9F9"
              onClick={() =>
                setSelectedIndex(index === selectedIndex ? -1 : index)
              }
            >
              <CardBody>
                <HStack justifyContent="space-between">
                  <Stack>
                    <Text
                      onClick={() => handlePopup(video)}
                      color="#403F49"
                      fontWeight="semibold"
                      fontSize="md"
                      // align="left"
                    >
                      {video.campaignName}
                    </Text>
                    <Text color="#575757" fontSize="sm" align="left">
                      {convertIntoDateAndTime(video.startDate)}
                    </Text>
                    {selectedIndex === index ? (
                      <>
                        <Text
                          color="#403F49"
                          fontWeight="semibold"
                          fontSize="sm"
                          align="left"
                        >
                          {video.totalSlotBooked} slots
                        </Text>
                        <HStack justifyContent="space-between">
                          <Text
                            color="#403F49"
                            fontWeight="semibold"
                            fontSize="sm"
                            align="left"
                          >
                            ₹ {video.totalAmount}
                          </Text>
                        </HStack>
                        {props?.show ? (
                          <Flex gap={3}>
                            {!video.isPause ? (
                              <Tooltip
                                hasArrow
                                label="Pause campaign"
                                fontSize="md"
                                bg="gray.300"
                                color="black"
                              >
                                <IconButton
                                  bg="none"
                                  icon={
                                    <BsPauseCircle size="25px" color="black" />
                                  }
                                  aria-label="Home"
                                  onClick={() =>
                                    props?.handleChangeCampaignStatus(
                                      video?._id,
                                      "Pause"
                                    )
                                  }
                                ></IconButton>
                              </Tooltip>
                            ) : (
                              <Tooltip
                                hasArrow
                                label="Resume campaign"
                                fontSize="md"
                                bg="gray.300"
                                color="black"
                              >
                                <IconButton
                                  bg="none"
                                  icon={
                                    <FaPlayCircle size="25px" color="black" />
                                  }
                                  aria-label="Home"
                                  onClick={() =>
                                    props?.handleChangeCampaignStatus(
                                      video?._id,
                                      "Resume"
                                    )
                                  }
                                ></IconButton>
                              </Tooltip>
                            )}
                            <Tooltip
                              hasArrow
                              label="Delete campaign"
                              fontSize="md"
                              bg="gray.300"
                              color="black"
                            >
                              <IconButton
                                bg="nome"
                                icon={
                                  <RiDeleteBin6Line size="25px" color="red" />
                                }
                                aria-label="Home"
                                onClick={() =>
                                  props?.handleChangeCampaignStatus(
                                    video?._id,
                                    "Deleted"
                                  )
                                }
                              ></IconButton>
                            </Tooltip>
                          </Flex>
                        ) : null}
                      </>
                    ) : null}
                  </Stack>
                  <Flex onClick={() => handleLogsPopup(video)} mt="0" pt="0">
                    <BsDot
                      size="25px"
                      color={video.status === "Deleted" ? "#E93A03" : "#00D615"}
                    />
                    <Text color="#403F45" fontSize="sm" pl="2">
                      {video.status}
                    </Text>
                  </Flex>
                </HStack>
              </CardBody>
            </Card>
          ))
        : null}
    </Stack>
  );
}
