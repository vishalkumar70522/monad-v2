import React from "react";
import { Box, Stack, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export function AdvertiseBox(props: any) {
  const { video } = props;
  const navigate = useNavigate();

  //console.log(video);
  return (
    <Box
      mx="1"
      width="100%"
      height="100%"
      bgColor="#F6F5F5"
      borderRadius="lg"
      boxShadow="2xl"
      key={video?._id}
      // onClick={() =>
      //   navigate(`/campaignDetails/${video.cid}/${video.campaignName}`)
      // }
      p="3"
    >
      <Box height={{ base: "150px", lg: "250px" }} width={""}>
        <Box
          as="video"
          // autoPlay
          loop
          controls
          muted
          // onLoadedData={() =>
          //   triggerPort(media.request["responseURL"].split("/").slice()[4])
          // }
          boxSize="100%"
        >
          <source src={video.video} />
        </Box>
      </Box>
      <Stack pt="1">
        <Text
          px="1"
          color="#403F49"
          fontSize={{ base: "md", lg: "xl" }}
          fontWeight="bold"
          align="left"
          m="0"
        >
          {video.campaignName}
        </Text>
        <Text
          px="1"
          color="#666666"
          fontSize={{ base: "xs", lg: "sm" }}
          fontWeight="semibold"
          align="left"
          m="0"
        >
          Location :{" "}
          {`${video.districtCity}, ${video.stateUT}, ${video.country}`}
        </Text>
      </Stack>
    </Box>
  );
}
