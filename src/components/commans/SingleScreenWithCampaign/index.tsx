import {
  Box,
  HStack,
  Text,
  Divider,
  Image,
  Flex,
  Stack,
} from "@chakra-ui/react";
import { AiFillStar } from "react-icons/ai";
import { useEffect, useState } from "react";
import {
  consvertSecondToHrAndMinutes,
  convertIntoDateAndTime,
} from "../../../utils/dateAndTime";
import Axios from "axios";
import { Skeleton, message } from "antd";

export function SingleScreenWithCampaign(props: any) {
  const [screen, setScreen] = useState<any>(null);
  const [loadingScreen, setLoadingScreen] = useState<any>(true);

  const getScreenDetail = async (screenId: any) => {
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenId}`
      );
      setScreen(data);
      setLoadingScreen(false);
    } catch (error: any) {
      message.error(error);
    }
  };

  useEffect(() => {
    getScreenDetail(props?.data?.screen);
  }, [props]);
  return (
    <>
      {loadingScreen ? (
        <Skeleton active={true} avatar paragraph={{ rows: 4 }} />
      ) : (
        <Box
          border="1px"
          borderRadius="12px"
          borderColor="#DDDDDD"
          p="5"
          width={{ base: "100%", lg: "100%" }}
        >
          <Flex gap={{ base: "5", lg: "5" }}>
            <Image
              src={screen?.image}
              alt="screen"
              width="124px"
              height="110px"
              borderRadius="8px"
            />
            <Box alignContent="top">
              <Text
                color="#222222"
                fontSize={{ base: "lg", lg: "xl" }}
                fontWeight="432"
                m="0"
              >
                {screen?.name}
              </Text>
              <Text
                color="#676767"
                fontSize={{ base: "13px", lg: "sm" }}
                fontWeight="600"
                m="0"
              >
                {screen?.screenAddress}, {screen?.districtCity},{" "}
                {screen?.stateUT}
              </Text>
              <HStack>
                <Flex align="center">
                  <AiFillStar size="12px" color="#222222" />
                  <Text
                    pl="1"
                    color="#222222"
                    fontSize="12px"
                    align="left"
                    m="0"
                    fontWeight="600"
                    justifyContent="center"
                  >
                    {screen?.rating}
                  </Text>
                  <Text m="0" fontSize="12px" pl="1">{`(14 reviews)`}</Text>
                </Flex>
              </HStack>
            </Box>
          </Flex>
          <Box
            border="1px"
            borderRadius="4px"
            borderColor="#DDDDDD"
            p={{ base: "2", lg: "2" }}
            mt={{ base: "2", lg: "5" }}
          >
            <Flex justifyContent="space-between">
              <Stack>
                <Text
                  color="#000000"
                  fontSize={{ base: "", lg: "15px" }}
                  fontWeight="600"
                  m="0"
                >
                  Start time
                </Text>
                <Text
                  color="#000000"
                  fontSize={{ base: "", lg: "15px" }}
                  fontWeight="600"
                  m="0"
                >
                  End time
                </Text>
                <Text
                  color="#000000"
                  fontSize={{ base: "", lg: "15px" }}
                  fontWeight="600"
                  m="0"
                >
                  Total number of slots
                </Text>
                <Text
                  color="#000000"
                  fontSize={{ base: "", lg: "15px" }}
                  fontWeight="600"
                  m="0"
                >
                  Playing time
                </Text>
              </Stack>
              <Stack>
                <Text
                  color="#000000"
                  fontSize={{ base: "", lg: "15px" }}
                  fontWeight="400"
                  m="0"
                >
                  {convertIntoDateAndTime(props?.data?.startDateAndTime)}
                </Text>
                <Text
                  color="#000000"
                  fontSize={{ base: "", lg: "15px" }}
                  fontWeight="400"
                  m="0"
                >
                  {convertIntoDateAndTime(props?.data?.endDateAndTime)}
                </Text>
                <Text
                  color="#000000"
                  fontSize={{ base: "", lg: "15px" }}
                  fontWeight="400"
                  m="0"
                >
                  {props?.data?.totalSlotBooked}
                </Text>
                <Text
                  color="#000000"
                  fontSize={{ base: "", lg: "15px" }}
                  fontWeight="400"
                  m="0"
                >
                  {consvertSecondToHrAndMinutes(
                    props?.data?.totalSlotBooked * screen?.slotsTimePeriod
                  )}
                </Text>
              </Stack>
            </Flex>
          </Box>
          <Text
            color="#222222"
            fontSize={{ base: "sm", lg: "md" }}
            pt="2"
            fontWeight="600"
            m="0"
          >
            Price details
          </Text>
          <HStack justifyContent="space-between" m="0" pt="2">
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              ₹{screen?.rentPerSlot} x {props?.data?.totalSlotBooked} Slots
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              ₹{screen?.rentPerSlot * props?.data?.totalSlotBooked}
            </Text>
          </HStack>
          <HStack justifyContent="space-between" m="0">
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              SGST
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              ₹123
            </Text>
          </HStack>
          <HStack justifyContent="space-between" m="0">
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              CGST
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              ₹123
            </Text>
          </HStack>
          <Divider color="#DDDDDD" />
          <HStack justifyContent="space-between" m="0">
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="600"
              m="0"
            >
              Total
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="600"
              m="0"
            >
              ₹{screen?.rentPerSlot * props?.data?.totalSlotBooked}
            </Text>
          </HStack>
        </Box>
      )}
    </>
  );
}
