import { Text, Flex, Box, Button } from "@chakra-ui/react";
// import { AiOutlineRight } from "react-icons/ai";
import { BsDot } from "react-icons/bs";
import { ColumnsType } from "antd/es/table";
import { Table } from "antd";
import { convertIntoDateAndTime } from "../../../utils/dateAndTime";
import { useState } from "react";
import { CampaignPopup } from "../CampaignPopup";
import { CampaignLogPopup } from "../../../Pages/Models/CampaignLogPopup";
import { useDispatch } from "react-redux";
import { getCampaignLogs } from "../../../Actions/campaignAction";

export function BrandTable(props: any) {
  // console.log(props.videosList.map((v: any) => v.screen));
  const dispatch = useDispatch<any>();
  const [open, setOpen] = useState<boolean>(false);
  const [campaignPopupView, setCampaignPopupView] = useState<any>(false);
  const [vid, setVid] = useState<any>();
  const [selectedCampaign, setSelectedCampaign] = useState<any>(null);

  const handlePopup = (v: any) => {
    // console.log(v.cid);

    setVid(v.cid);
    setCampaignPopupView(true);
  };

  const handleLogsPopup = (campaign: any) => {
    setSelectedCampaign(campaign);
    dispatch(
      getCampaignLogs({ cid: campaign?.cid, screenId: campaign?.screen })
    );
    setOpen(true);
  };

  const columns: ColumnsType<any> = [
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Preview
        </Text>
      ),
      dataIndex: "logo",
      key: "logo",
      render: (_, value) => (
        <Box
          onClick={() => handlePopup(value)}
          as="video"
          src={`https://ipfs.io/ipfs/${value.cid}`}
          width="70px"
          height="70px"
        />
      ),
    },
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Name
        </Text>
      ),
      dataIndex: "campaignName",
      key: "campaignName",
      render: (value) => (
        <Text color="#000000" fontSize="lg" fontWeight="400">
          {value}
        </Text>
      ),
    },

    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Start
        </Text>
      ),
      dataIndex: "startDate",
      key: "startDate",
      render: (value) => (
        <Text color="#000000" fontSize="md" fontWeight="500">
          {convertIntoDateAndTime(value)}
        </Text>
      ),
    },
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Total slots
        </Text>
      ),
      dataIndex: "totalSlotBooked",
      key: "totalSlotBooked",
      render: (value) => (
        <Text color="#000000" fontSize="22px" fontWeight="400">
          {value}
        </Text>
      ),
    },
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Price
        </Text>
      ),
      dataIndex: "totalAmount",
      key: "totalAmount",
      render: (value) => (
        <Text color="#4339F2" fontSize="22px" fontWeight="400">
          ₹ {value}
        </Text>
      ),
    },
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Status
        </Text>
      ),
      dataIndex: "status",
      key: "status",
      render: (value) => (
        <Flex>
          <BsDot
            color={
              value === "Active"
                ? "#00D615"
                : value === "Deleted"
                ? "#E93A03"
                : "yellow"
            }
            size="20px"
          />
          <Text color="#403F45" fontSize="sm" pl="2">
            {value}
          </Text>
        </Flex>
      ),
    },
    {
      title: "Action",
      dataIndex: "_id",
      key: "_id",
      render: (_, value) => (
        <Button
          py="2"
          variant="outline"
          _hover={{ color: "green" }}
          onClick={() => handleLogsPopup(value)}
          isLoading={props?.loadingScreenLogs}
          loadingText="Logs is loading"
          isDisabled={props?.isDisabled}
        >
          Show Campaign
        </Button>
      ),
    },
  ];

  return (
    <>
      <CampaignPopup
        open={campaignPopupView}
        vid={vid}
        onCancel={() => setCampaignPopupView(false)}
      />
      <CampaignLogPopup
        open={open}
        onCancel={() => setOpen(false)}
        campaign={selectedCampaign}
        screen={props?.screen}
      />
      <Table
        rowKey="_id"
        columns={columns}
        dataSource={props?.videosList}
        pagination={{ pageSize: 4 }}
      />
    </>
  );
}
