import { Box, Flex, Text, Hide, VStack, Center, Show } from "@chakra-ui/react";
import { BsDot } from "react-icons/bs";
import { AiOutlineRight } from "react-icons/ai";
import { MediaContainer } from "../MediaContainer";
import { convertIntoDateAndTime } from "../../../utils/dateAndTime";
import { Tooltip } from "antd";
import { useNavigate } from "react-router";

export function MySingleCampaign(props: any) {
  const navigate = useNavigate();
  const status = "Active";
  const { campaign } = props;
  return (
    <Box boxShadow="xl" p={{ base: "0", lg: "5" }} py={{ base: "3" }} mt="5">
      <Flex justifyContent="space-between">
        <Flex gap={{ base: "2", lg: "5" }}>
          <MediaContainer
            cid={campaign?.cid}
            width={{ base: "104px", lg: "165px" }}
            height={{ base: "80px", lg: "104px" }}
            autoPlay="false"
          />
          <Box>
            <Text
              color="#222222"
              fontSize={{ base: "sm", lg: "xl" }}
              fontWeight="452"
              m="0"
              p={{ base: "0", lg: "1" }}
            >
              {campaign?.campaignName}
            </Text>
            <Text
              color="#717171"
              fontSize={{ base: "11px", lg: "sm" }}
              fontWeight="400"
              m="0"
              p={{ base: "0", lg: "1" }}
            >
              1 screens playing
            </Text>
            <Text
              color="#717171"
              fontSize={{ base: "11px", lg: "sm" }}
              fontWeight="400"
              m="0"
              p={{ base: "0", lg: "1" }}
            >
              {convertIntoDateAndTime(campaign?.startDate)}
            </Text>
          </Box>
        </Flex>
        <Flex justifyContent="space-between" gap={{ base: "5", lg: "200" }}>
          <Hide below="md">
            <VStack align="left">
              <Flex gap={{ base: "1", lg: "2" }}>
                <BsDot
                  color={
                    campaign?.status === "Active"
                      ? "#00D615"
                      : campaign?.status === "Deleted"
                      ? "#E93A03"
                      : "yellow"
                  }
                  size="20px"
                />
                <Text color="#403F45" fontSize={{ base: "11px", lg: "sm" }}>
                  {campaign?.status}
                </Text>
              </Flex>
              <Flex gap={{ base: "", lg: "20" }}>
                <Text
                  color="#222222"
                  fontSize={{ base: "sm", lg: "md" }}
                  fontWeight="600"
                  m="0"
                >
                  Total spent
                </Text>
                <Text
                  color="#222222"
                  fontSize={{ base: "sm", lg: "md" }}
                  fontWeight="600"
                  m="0"
                >
                  ₹{campaign?.totalAmount}
                </Text>
              </Flex>
            </VStack>
          </Hide>
          <Show below="md">
            <Center>
              <Flex gap={1}>
                <BsDot
                  color={
                    campaign.status === "Active"
                      ? "#00D615"
                      : campaign.status === "Deleted"
                      ? "red"
                      : "blue"
                  }
                  size="20px"
                />
                <Text
                  color="#403F45"
                  fontSize={{ base: "11px", lg: "sm" }}
                  m="0"
                >
                  {campaign?.status}
                </Text>
              </Flex>
            </Center>
          </Show>
          <Tooltip
            title="See campaign details"
            color={"pink"}
            placement="topRight"
          >
            <Center
              onClick={() =>
                navigate(
                  `/campaignDetails/${campaign?.cid}/${campaign?.campaignName}`
                )
              }
            >
              <AiOutlineRight size="20px" color="#000000" />
            </Center>
          </Tooltip>
        </Flex>
      </Flex>
    </Box>
  );
}
