//SearchWithCreateCampaign;

import {
  Box,
  Button,
  Center,
  Flex,
  Hide,
  Image,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Show,
} from "@chakra-ui/react";
import { useState } from "react";
import { IoSearchCircleSharp } from "react-icons/io5";
import { nav } from "../../../assets/svg";
import { BsPlusCircleFill } from "react-icons/bs";
import { CreateCampaignNamePopup } from "../../../Pages/Models/CreateCampaignNamePopup";
import { UploadMediaWithCampaignNamePopup } from "../../../Pages/Models/UploadMediaWithCampaignNamePopup";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createVideoFromImage } from "../../../Actions/videoFromImage";
import { uploadMedia } from "../../../Actions/mediaActions";
import { useSelector } from "react-redux";
import { CampaignGoal } from "../../../Pages/Models/CampaignGoal";

export function SearchWithCreateCampaign(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [openCreateCampaignName, setOpenCreateCampaignName] =
    useState<boolean>(false);
  const [openUploadMedia, setOpenUploadMedia] = useState<boolean>(false);
  const [openCampaignGoalModel, setOpenCampaignGoalModel] =
    useState<boolean>(false);

  const [campaignName, setCampaignName] = useState<any>("");
  const [fileUrl, setFileUrl] = useState<any>("");
  const [campaignGoal, setCampaignGoal] = useState<any>(null);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const handleCreateVideoFromImage = (filedata: any) => {
    dispatch(createVideoFromImage(filedata));
    setFileUrl("");
  };

  const videoUploadHandler = async (e: any) => {
    e.preventDefault();

    dispatch(
      uploadMedia({
        title: campaignName,
        thumbnail:
          "https://bafybeicduvlghzcrjtuxkro7foazucvuyej25rh3humeujbzt7bmio4hsa.ipfs.w3s.link/raily.png",
        fileUrl,
        media: "",
      })
    );

    setFileUrl("");
  };

  const handelOpenCreateCampaignName = () => {
    // first check user is logged in or not
    if (!userInfo) {
      navigate("/signin", {
        state: {
          path: "/",
        },
      });
    } else {
      setOpenCreateCampaignName(true);
    }
  };

  const handelNext = (e: any) => {
    navigate("/createCampaignBaseOnAudianceData", {
      state: {
        campaignName,
        campaignGoal,
      },
    });
  };

  const handelCancle = () => {
    setCampaignName("");
    setFileUrl("");
  };

  return (
    <Box>
      <UploadMediaWithCampaignNamePopup
        open={openUploadMedia}
        onCancel={() => setOpenUploadMedia(false)}
        createVideo={handleCreateVideoFromImage}
        videoUploadHandler={videoUploadHandler}
        setCampaignName={(value: any) => setCampaignName(value)}
        setFileUrl={(value: any) => setFileUrl(value)}
        campaignName={campaignName}
        setOpenCampaignGoalModel={() => setOpenCampaignGoalModel(true)}
      />
      <CreateCampaignNamePopup
        open={openCreateCampaignName}
        onCancel={() => setOpenCreateCampaignName(false)}
        openUploadMedia={() => setOpenUploadMedia(true)}
        setCampaignName={(value: any) => setCampaignName(value)}
        campaignName={campaignName}
      />
      <CampaignGoal
        open={openCampaignGoalModel}
        onCancel={() => setOpenCampaignGoalModel(false)}
        handelNext={handelNext}
        handelCancle={handelCancle}
        setCampaignGoal={(value: any) => setCampaignGoal(value)}
      />
      <Hide below="md">
        <Flex justifyContent="space-between">
          <Box width={{ base: "100%", lg: "30%" }}>
            <InputGroup>
              <Input
                placeholder="search by location, screen name"
                py="3"
                borderRadius="50px"
                pl="10"
                color="#4A4A4A"
                fontSize="lg"
                onClick={() =>
                  navigate("/searchScreenBasedOnLocationAndLocality")
                }
              />
              <InputLeftElement p="5">
                <Center>
                  <Image src={nav} alt="nav" />
                </Center>
              </InputLeftElement>

              <InputRightElement p="2">
                <Center>
                  <IoSearchCircleSharp size="40px" color="#D7380E" />
                </Center>
              </InputRightElement>
            </InputGroup>
          </Box>
          <Button
            color="#FFFFFF"
            bgColor="#0EBCF5"
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="600"
            leftIcon={<BsPlusCircleFill size="20px" />}
            onClick={handelOpenCreateCampaignName}
          >
            Create campaign
          </Button>
        </Flex>
      </Hide>
      <Show below="md">
        <Flex direction="column" gap={2} pb="2">
          <Box width={{ base: "100%", lg: "30%" }}>
            <InputGroup
              onClick={() =>
                navigate("/searchScreenBasedOnLocationAndLocality")
              }
            >
              <Input
                placeholder="search by location, screen name"
                py="3"
                borderRadius="50px"
                pl="10"
                color="#4A4A4A"
                fontSize="lg"
              />
              <InputLeftElement p="5">
                <Center>
                  <Image src={nav} alt="nav" />
                </Center>
              </InputLeftElement>

              <InputRightElement p="2">
                <Center>
                  <IoSearchCircleSharp size="40px" color="#D7380E" />
                </Center>
              </InputRightElement>
            </InputGroup>
          </Box>
          <Button
            color="#FFFFFF"
            bgColor="#0EBCF5"
            fontSize={{ base: "", lg: "lg" }}
            py="3"
            fontWeight="600"
            leftIcon={<BsPlusCircleFill size="20px" />}
            onClick={handelOpenCreateCampaignName}
          >
            Create campaign
          </Button>
        </Flex>
      </Show>
    </Box>
  );
}
