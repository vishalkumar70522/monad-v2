// versi "react-qr-reader" 1.0.0. component API harus disesuaikan dengan yg baru

import { Modal } from "antd";
// import "./style.css";
import { Box } from "@chakra-ui/react";
// import { useNavigate } from "react-router-dom";

export const CampaignPopup = (props: any) => {
  // console.log("vid", props);
  // const navigate = useNavigate();
  return (
    <Modal
      title=""
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={null}
      closable={true}
      maskClosable={true}
      destroyOnClose={true}
    >
      <Box
        as="video"
        src={`https://ipfs.io/ipfs/${props?.vid}`}
        // autoPlay={true}
        // loop={true}
        controls={true}
      />
    </Modal>
  );
};
