import { Text, Image } from "@chakra-ui/react";
// import { AiOutlineRight } from "react-icons/ai";
// import { BsDot } from "react-icons/bs";
import { ColumnsType } from "antd/es/table";
import { Table } from "antd";
import { convertIntoDateAndTime } from "../../../utils/dateAndTime";

const columns: ColumnsType<any> = [
  {
    title: (
      <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
        Dvice id
      </Text>
    ),
    dataIndex: "logo",
    key: "logo",
    render: () => <Image src="" alt="logo" width="70px" height="70px" />,
  },
  {
    title: (
      <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
        Device
      </Text>
    ),
    dataIndex: "campaignName",
    key: "campaignName",
    render: (value) => (
      <Text color="#000000" fontSize="lg" fontWeight="400">
        {value}
      </Text>
    ),
  },

  {
    title: (
      <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
        Start Time
      </Text>
    ),
    dataIndex: "startDate",
    key: "startDate",
    render: (value) => (
      <Text color="#000000" fontSize="md" fontWeight="500">
        {convertIntoDateAndTime(value)}
      </Text>
    ),
  },
  {
    title: (
      <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
        Address
      </Text>
    ),
    dataIndex: "totalSlotBooked",
    key: "totalSlotBooked",
    render: (value) => (
      <Text color="#000000" fontSize="22px" fontWeight="400">
        {value}
      </Text>
    ),
  },
];

export function CampaignLogTable(props: any) {
  return <Table columns={columns} dataSource={props?.videosList || []} />;
}
