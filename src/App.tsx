import React from "react";
import "./App.css";
import { ChakraProvider, GlobalStyle } from "@chakra-ui/react";
import BasicStyle from "./theme/basicStyle";
import { BrowserRouter as Router } from "react-router-dom";
import { theme } from "./theme/Theme.base";
import { PublicRoutes } from "./routes";
import { QueryClient, QueryClientProvider } from "react-query";
const queryClient = new QueryClient();

function App() {
  return (
    <ChakraProvider theme={theme}>
      <BasicStyle />
      <GlobalStyle />
      <QueryClientProvider client={queryClient}>
        <Router>
          <PublicRoutes />
        </Router>
      </QueryClientProvider>
    </ChakraProvider>
  );
}

export default App;
