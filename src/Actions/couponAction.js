import {
  ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_FAIL,
  ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_REQUEST,
  ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_SUCCESS,
} from "../Constants/campaignConstants";
import {
  ADD_PURCHASE_DETAILS_TO_COUPON_FAIL,
  ADD_PURCHASE_DETAILS_TO_COUPON_REQUEST,
  ADD_PURCHASE_DETAILS_TO_COUPON_SUCCESS,
  ADD_USER_TO_COUPON_FAIL,
  ADD_USER_TO_COUPON_REQUEST,
  ADD_USER_TO_COUPON_SUCCESS,
  ALL_ACTIVE_COUPON_LIST_FAIL,
  ALL_ACTIVE_COUPON_LIST_REQUEST,
  ALL_ACTIVE_COUPON_LIST_SUCCESS,
  COUPON_LIST_FOR_BRAND_FAIL,
  COUPON_LIST_FOR_BRAND_REQUEST,
  COUPON_LIST_FOR_BRAND_SUCCESS,
  COUPON_LIST_FOR_USER_FAIL,
  COUPON_LIST_FOR_USER_REQUEST,
  COUPON_LIST_FOR_USER_SUCCESS,
  CREATE_COUPON_FAIL,
  CREATE_COUPON_REQUEST,
  CREATE_COUPON_SUCCESS,
  DELETE_COUPON_FAIL,
  DELETE_COUPON_REQUEST,
  DELETE_COUPON_SUCCESS,
  EDIT_COUPON_FAIL,
  EDIT_COUPON_REQUEST,
  EDIT_COUPON_SUCCESS,
  GET_COUPON_FULL_DETAILS_FAIL,
  GET_COUPON_FULL_DETAILS_REQUEST,
  GET_COUPON_FULL_DETAILS_SUCCESS,
} from "../Constants/couponConstants";

import Axios from "axios";

export const createNewCoupon = (requestBody) => async (dispatch, getState) => {
  dispatch({
    type: CREATE_COUPON_REQUEST,
    payload: requestBody,
  });
  const {
    userSignin: { userInfo },
  } = getState();
  try {
    const { data } = await Axios.post(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/coupon/create/${userInfo?._id}/${userInfo?.brand[0]}`,
      {
        user: userInfo,
        ...requestBody,
      },
      {
        headers: {
          authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: CREATE_COUPON_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: CREATE_COUPON_FAIL,
      payload: message,
    });
  }
};

export const updateCouponDetails =
  (requestBody, couponId) => async (dispatch, getState) => {
    dispatch({
      type: EDIT_COUPON_REQUEST,
      payload: requestBody,
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/coupon/${couponId}`,
        {
          user: userInfo,
          ...requestBody,
        },
        {
          headers: {
            authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: EDIT_COUPON_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: EDIT_COUPON_FAIL,
        payload: message,
      });
    }
  };

export const getCouponListForBrand =
  (brandId) => async (dispatch, getState) => {
    dispatch({
      type: COUPON_LIST_FOR_BRAND_REQUEST,
      payload: { brandId },
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/coupon/${brandId}`,
        {
          user: userInfo,
        },
        {
          headers: {
            authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: COUPON_LIST_FOR_BRAND_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: COUPON_LIST_FOR_BRAND_FAIL,
        payload: message,
      });
    }
  };

export const getCouponListForUser = (userId) => async (dispatch, getState) => {
  dispatch({
    type: COUPON_LIST_FOR_USER_REQUEST,
    payload: { userId },
  });
  const {
    userSignin: { userInfo },
  } = getState();
  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/coupon/user/${userId}`,
      {
        user: userInfo,
      },
      {
        headers: {
          authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: COUPON_LIST_FOR_USER_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: COUPON_LIST_FOR_USER_FAIL,
      payload: message,
    });
  }
};

export const getAllActiveCouponList = () => async (dispatch) => {
  dispatch({
    type: ALL_ACTIVE_COUPON_LIST_REQUEST,
    payload: {},
  });

  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/coupon/getActiveCoupons`
    );
    dispatch({
      type: ALL_ACTIVE_COUPON_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: ALL_ACTIVE_COUPON_LIST_FAIL,
      payload: message,
    });
  }
};

export const deleteBrandCoupon =
  (couponId, status) => async (dispatch, getState) => {
    dispatch({
      type: DELETE_COUPON_REQUEST,
      payload: couponId,
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      // let startTime = performance.now();

      const { data } = await Axios.delete(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/coupon/${couponId}/${status}`,
        {
          user: userInfo,
        },
        {
          headers: {
            authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: DELETE_COUPON_SUCCESS,
        payload: data,
      });
      // let endTime = performance.now();
      // let timeElapsed = endTime - startTime;
      // console.log("total time taken to update coupon in MS: ", timeElapsed);
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: DELETE_COUPON_FAIL,
        payload: message,
      });
    }
  };

export const addUserToCoupon =
  ({ couponId, screenId }) =>
  async (dispatch, getState) => {
    dispatch({
      type: ADD_USER_TO_COUPON_REQUEST,
      payload: { couponId, screenId },
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/coupon/${couponId}/${userInfo?._id}/${screenId}`,
        {
          user: userInfo,
        },
        {
          headers: {
            authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: ADD_USER_TO_COUPON_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: ADD_USER_TO_COUPON_FAIL,
        payload: message,
      });
    }
  };

export const getCouponFullDetails = (couponId) => async (dispatch) => {
  dispatch({
    type: GET_COUPON_FULL_DETAILS_REQUEST,
    payload: couponId,
  });

  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/coupon/couponDetails/${couponId}`
    );
    dispatch({
      type: GET_COUPON_FULL_DETAILS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: GET_COUPON_FULL_DETAILS_FAIL,
      payload: message,
    });
  }
};

export const addOrRemoveCouponInWishlist =
  (requestBody) => async (dispatch, getState) => {
    dispatch({
      type: ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_REQUEST,
      payload: requestBody,
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/coupon/addOrRemoveWishlist`,
        {
          user: userInfo,
          ...requestBody,
        },
        {
          headers: {
            authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_FAIL,
        payload: message,
      });
    }
  };

export const addPurchaseDetailsInCoupon =
  ({ couponId, fromUser, toUser, plaeId, purchaseAmount, savedAmount }) =>
  async (dispatch, getState) => {
    dispatch({
      type: ADD_PURCHASE_DETAILS_TO_COUPON_REQUEST,
      payload: {
        couponId,
        fromUser,
        toUser,
        plaeId,
        purchaseAmount,
        savedAmount,
      },
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/pleas/addPurchaseDetailsInCoupon`,
        {
          couponId,
          fromUser,
          toUser,
          plaeId,
          purchaseAmount,
          savedAmount,
        },
        {
          headers: { Authorization: "Bearer " + userInfo?.token },
        }
      );
      dispatch({
        type: ADD_PURCHASE_DETAILS_TO_COUPON_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: ADD_PURCHASE_DETAILS_TO_COUPON_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };
