import {
  MEDIA_UPLOAD_FAIL,
  MEDIA_UPLOAD_REQUEST,
  MEDIA_UPLOAD_SUCCESS,
} from "../Constants/mediaConstants";
import Axios from "axios";

export const createVideoFromImage =
  (formData) => async (dispatch, getState) => {
    dispatch({
      type: MEDIA_UPLOAD_REQUEST,
      payload: formData,
    });

    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${userInfo?.token}`,
        },
      };

      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/createVideoFromImage/${userInfo?._id}`,
        formData,
        config
      );

      dispatch({
        type: MEDIA_UPLOAD_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: MEDIA_UPLOAD_FAIL,
        payload: message,
      });
    }
  };
