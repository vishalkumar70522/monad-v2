import Axios from "axios";
import {
  GENERATE_QRCODE_FOR_SCREEN_FAIL,
  GENERATE_QRCODE_FOR_SCREEN_REQUEST,
  GENERATE_QRCODE_FOR_SCREEN_SUCCESS,
} from "../Constants/generateQRCodeConstants";

export const createQRCodeForScreen =
  (screenId) => async (dispatch, getState) => {
    dispatch({
      type: GENERATE_QRCODE_FOR_SCREEN_REQUEST,
      payload: screenId,
    });

    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${userInfo?.token}`,
        },
      };

      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/qrcode/create/${screenId}`
      );

      dispatch({
        type: GENERATE_QRCODE_FOR_SCREEN_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: GENERATE_QRCODE_FOR_SCREEN_FAIL,
        payload: message,
      });
    }
  };

export const sendScanDataInLogs =
  ({ scanResult, scanUser, screenId }) =>
  async (dispatch) => {
    dispatch({
      type: "SEND_SCAN_DATA_LOADING",
      payload: { scanResult, scanUser, screenId },
    });
    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/scanqrdata/${screenId}`,
        {
          scanResult,
          scanUser,
          screenId,
        }
      );
      dispatch({
        type: "SEND_SCAN_DATA_SUCCESS",
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: "SEND_SCAN_DATA_FAIL",
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };
