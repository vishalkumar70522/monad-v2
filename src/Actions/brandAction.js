import Axios from "axios";
import {
  CREATE_BRAND_FAIL,
  CREATE_BRAND_REQUEST,
  CREATE_BRAND_SUCCESS,
  GET_BRAND_DETAILS_FAIL,
  GET_BRAND_DETAILS_REQUEST,
  GET_BRAND_DETAILS_SUCCESS,
  UPDATE_BRAND_FAIL,
  UPDATE_BRAND_REQUEST,
  UPDATE_BRAND_SUCCESS,
} from "../Constants/brandConstants";
import { USER_SIGNIN_SUCCESS } from "../Constants/userConstants";

export const changeUserAsBrand =
  ({
    brandName,
    website,
    aboutBrand,
    tagline,
    address,
    logo,
    images,
    phone,
    email,
    instagramId,
    facebookId,
    snapchat,
    linkedin,
    youtube,
    twitter,
    brandCategory,
    brandType,
    affiliatedUsers,
  }) =>
  async (dispatch, getState) => {
    dispatch({
      type: CREATE_BRAND_REQUEST,
      payload: {
        brandName,
        website,
        aboutBrand,
        tagline,
        address,
        logo,
        images,
        phone,
        email,
        instagramId,
        facebookId,
        snapchat,
        linkedin,
        youtube,
        twitter,
        brandCategory,
        brandType,
        affiliatedUsers,
      },
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/brands/${userInfo?._id}/create`,
        {
          user: userInfo,
          brandName,
          website,
          aboutBrand,
          tagline,
          address,
          logo,
          images,
          phone,
          email,
          instagramId,
          facebookId,
          snapchat,
          linkedin,
          youtube,
          twitter,
          brandCategory,
          brandType,
          affiliatedUsers,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: CREATE_BRAND_SUCCESS,
        payload: data,
      });
      dispatch({
        type: USER_SIGNIN_SUCCESS,
        payload: data,
      });

      localStorage.setItem("userInfo", JSON.stringify(data));
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: CREATE_BRAND_FAIL,
        payload: message,
      });
    }
  };

export const getBrandDetailsById = (brandId) => async (dispatch, getState) => {
  dispatch({
    type: GET_BRAND_DETAILS_REQUEST,
    payload: brandId,
  });
  const {
    userSignin: { userInfo },
  } = getState();
  // console.log(userInfo);
  // console.log(brandId);
  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/brands/details/${brandId}`
      // {
      //   headers: {
      //     Authorization: `Bearer ${userInfo?.token}`,
      //   },
      // }
    );
    dispatch({
      type: GET_BRAND_DETAILS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: GET_BRAND_DETAILS_FAIL,
      payload: message,
    });
  }
};

export const updateBrandDetails =
  ({
    brandName,
    website,
    aboutBrand,
    tagline,
    address,
    logo,
    images,
    phone,
    email,
    instagramId,
    facebookId,
    snapchat,
    linkedin,
    youtube,
    twitter,
    brandCategory,
    brandType,
    affiliatedUsers,
  }) =>
  async (dispatch, getState) => {
    // console.log("action 1");

    dispatch({
      type: UPDATE_BRAND_REQUEST,
      payload: {
        brandName,
        website,
        aboutBrand,
        tagline,
        address,
        logo,
        images,
        phone,
        email,
        instagramId,
        facebookId,
        snapchat,
        linkedin,
        youtube,
        twitter,
        brandCategory,
        brandType,
        affiliatedUsers,
      },
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/brands/update/${userInfo?.brand[0]}`,
        {
          user: userInfo,
          brandName,
          website,
          aboutBrand,
          tagline,
          address,
          logo,
          images,
          phone,
          email,
          instagramId,
          facebookId,
          snapchat,
          linkedin,
          youtube,
          twitter,
          brandCategory,
          brandType,
          affiliatedUsers,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: UPDATE_BRAND_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: UPDATE_BRAND_FAIL,
        payload: message,
      });
    }
  };

export const getAllBrands = () => async (dispatch, getState) => {
  dispatch({
    type: "GET_ALL_BRANDS_REQUEST",
    // payload: brandId,
  });
  const {
    userSignin: { userInfo },
  } = getState();
  // console.log(userInfo);
  // console.log(brandId);
  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/brands/all`
      // {
      //   headers: {
      //     Authorization: `Bearer ${userInfo?.token}`,
      //   },
      // }
    );
    dispatch({
      type: "GET_ALL_BRANDS_SUCCESS",
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: "GET_ALL_BRANDS_FAIL",
      payload: message,
    });
  }
};
