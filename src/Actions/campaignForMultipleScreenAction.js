import {
  CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_FAIL,
  CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_REQUEST,
  CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_SUCCESS,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_FAIL,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_REQUEST,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_SUCCESS,
  FILTER_SCREENS_FOR_CREATE_CAMPAIGN_FAIL,
  FILTER_SCREENS_FOR_CREATE_CAMPAIGN_REQUEST,
  FILTER_SCREENS_FOR_CREATE_CAMPAIGN_SUCCESS,
} from "../Constants/campaignForMultipleScreen";
import Axios from "axios";

export const createCamapaignForMultipleScreens =
  (requestBody) => async (dispatch, getState) => {
    dispatch({
      type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_REQUEST,
      payload: requestBody,
    });
    const {
      userSignin: { userInfo },
    } = getState();
    const { campaignName, cid, campaignWithScreens, media } = requestBody;

    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaignForMultipleScreens/create`,
        {
          user: userInfo,
          campaignName,
          cid,
          campaignWithScreens,
          media,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_FAIL,
        payload: message,
      });
    }
  };

export const createCamapaignBasedOnAudiancesProfile =
  (requestBody) => async (dispatch, getState) => {
    dispatch({
      type: CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_REQUEST,
      payload: requestBody,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaignForMultipleScreens/createCampaign`,
        {
          user: userInfo,
          ...requestBody,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_FAIL,
        payload: message,
      });
    }
  };

export const getScreensBasedOnAudiancesProfile =
  (requestBody) => async (dispatch, getState) => {
    dispatch({
      type: FILTER_SCREENS_FOR_CREATE_CAMPAIGN_REQUEST,
      payload: requestBody,
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaignForMultipleScreens/getScreens`,
        {
          user: userInfo,
          ...requestBody,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: FILTER_SCREENS_FOR_CREATE_CAMPAIGN_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: FILTER_SCREENS_FOR_CREATE_CAMPAIGN_FAIL,
        payload: message,
      });
    }
  };
