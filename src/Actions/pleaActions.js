import Axios from "axios";
import {
  ALL_PLEA_LIST_FAIL,
  ALL_PLEA_LIST_REQUEST,
  ALL_PLEA_LIST_SUCCESS,
  ALL_PLEA_LIST_BY_USER_FAIL,
  ALL_PLEA_LIST_BY_USER_REQUEST,
  ALL_PLEA_LIST_BY_USER_SUCCESS,
  CAMPAIGN_ALLY_GRANT_REQUEST,
  CAMPAIGN_ALLY_GRANT_SUCCESS,
  CAMPAIGN_ALLY_GRANT_FAIL,
  CAMPAIGN_ALLY_REJECT_REQUEST,
  CAMPAIGN_ALLY_REJECT_SUCCESS,
  CAMPAIGN_ALLY_REJECT_FAIL,
  COUPOM_REDEEM_PLEA_REQUEST,
  COUPOM_REDEEM_PLEA_SUCCESS,
  COUPOM_REDEEM_PLEA_FAIL,
} from "../Constants/pleaConstants";

export const listMyPleas = () => async (dispatch, getState) => {
  dispatch({
    type: "LIST_MY_PLEAS_REQUEST",
  });
  try {
    const {
      userSignin: { userInfo },
    } = getState();

    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/pleas/user/${userInfo?._id}`,
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: "LIST_MY_PLEAS_SUCCESS",
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: "LIST_MY_PLEAS_FAIL",
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const listAllPleas = () => async (dispatch, getState) => {
  dispatch({
    type: ALL_PLEA_LIST_REQUEST,
  });

  try {
    const {
      userSignin: { userInfo },
    } = getState();

    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/pleas/allPleas`,
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: ALL_PLEA_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ALL_PLEA_LIST_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const listAllPleasByUserId = () => async (dispatch, getState) => {
  dispatch({
    type: ALL_PLEA_LIST_BY_USER_REQUEST,
  });

  try {
    const {
      userSignin: { userInfo },
    } = getState();

    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/pleas/${userInfo?._id}/user`,
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: ALL_PLEA_LIST_BY_USER_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ALL_PLEA_LIST_BY_USER_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const grantCampaignAllyPlea = (pleaId) => async (dispatch, getState) => {
  dispatch({
    type: CAMPAIGN_ALLY_GRANT_REQUEST,
    payload: pleaId,
  });
  const {
    userSignin: { userInfo },
  } = getState();
  try {
    const { data } = await Axios.put(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/pleas/${pleaId}/campaignAllayPlea/giveAccess`,
      { pleaId },
      {
        headers: { Authorization: "Bearer " + userInfo?.token },
      }
    );
    dispatch({
      type: CAMPAIGN_ALLY_GRANT_SUCCESS,
      payload: data.plea,
    });
  } catch (error) {
    dispatch({
      type: CAMPAIGN_ALLY_GRANT_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

// reject ally plea

export const rejectCampaignAllyPlea =
  (pleaId) => async (dispatch, getState) => {
    dispatch({
      type: CAMPAIGN_ALLY_REJECT_REQUEST,
      payload: pleaId,
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      // const {data} = await Axios.put(`http://localhost:3333/api/screens/${pleaId}/allyPlea/reject`, {pleaId}, {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/pleas/${pleaId}/campaignAllayPlea/reject`,
        { pleaId },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );

      dispatch({
        type: CAMPAIGN_ALLY_REJECT_SUCCESS,
        payload: data.plea,
      });
    } catch (error) {
      dispatch({
        type: CAMPAIGN_ALLY_REJECT_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const addNewPleaForUserRedeemCouponOffer =
  ({ couponId, toUser, couponCode }) =>
  async (dispatch, getState) => {
    dispatch({
      type: COUPOM_REDEEM_PLEA_REQUEST,
      payload: { couponId, toUser },
    });
    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/pleas/addCouponRedeemPlea/${userInfo?._id}/${toUser}/${couponId}/${couponCode}`,
        {
          headers: { Authorization: "Bearer " + userInfo?.token },
        }
      );
      dispatch({
        type: COUPOM_REDEEM_PLEA_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: COUPOM_REDEEM_PLEA_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };
