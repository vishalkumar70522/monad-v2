import { Route, Routes } from "react-router-dom";
import {
  CreatePassword,
  ForgetPassword,
  HomePage,
  JoinAs,
  ScreenDetails,
  MyScreenList,
  SetupAccount,
  SignUp,
  Signin,
  SingleCampaign,
  ScreenDashBoard,
  EditScreen,
  AllScreenPage,
  MyCampaigns,
  CampaignDetails,
  SingleCampaignOnMultipleScreen,
  CreateCampaignBasedOnAudianceData,
  PleaRequestListByUser,
  FilterScreenByLocationAndLocality,
  UserProfile,
  CreateNewCoupon,
  CouponDashboardForBrand,
  EditCouponDetails,
  CouponDetails,
  CouponLandingPage,
  EditBrandProfileData,
  EditCreatorProfileData,
  BrandProfileView,
  CreatorProfileView,
  RootPage,
  CouponsForUser,
  Cart,
  CouponManagement,
  CouponListByScreen,
  CouponWishlistForUser,
  FullScreenPlaylist,
} from "../Pages";
import { Nav } from "../components/commans/Nav";
import { FilteredScreens } from "../Pages/FilteredScreens";
import { Page404 } from "../Pages/404";
import { Wallet } from "../Pages/Wallet";

export const PublicRoutes = () => {
  // console.log(window.location.pathname.split("/")[1]);
  return (
    <>
      {window.location.pathname.split("/")[1] !== "myplaylistfullscreen" && (
        <Nav />
      )}
      <Routes>
        <Route path="/homepage" element={<HomePage />} />
        <Route path="/" element={<RootPage />} />

        <Route path="/allScreen" element={<AllScreenPage />} />

        <Route path="/screenDetails/:id" element={<ScreenDetails />} />
        <Route path="/myFilteredScreens" element={<FilteredScreens />} />
        <Route
          path="/searchScreenBasedOnLocationAndLocality"
          element={<FilterScreenByLocationAndLocality />}
        />

        <Route path="/singleCampaign" element={<SingleCampaign />} />
        <Route
          path="/singleCampaignOnMultipleScreens"
          element={<SingleCampaignOnMultipleScreen />}
        />

        {/* auth */}
        <Route path="/signin" element={<Signin />} />
        <Route path="/signup" element={<SignUp />} />
        <Route
          path="/create-reset-password/:email/:name"
          element={<CreatePassword />}
        />
        <Route path="/setupAccount" element={<SetupAccount />} />
        <Route path="/joinUsAs" element={<JoinAs />} />
        <Route path="/forgetPassword" element={<ForgetPassword />} />

        {/* Screen owner dashboard */}
        <Route path="/myScreens" element={<MyScreenList />} />
        <Route
          path="/screenDashboard/:screenId"
          element={<ScreenDashBoard />}
        />
        <Route path="/edit-screen/:id" element={<EditScreen />} />
        {/* <Route path="/screenOwnerCamapign" element={<ScreenOwnerCampaign />} /> */}
        <Route path="/pleaBucket" element={<PleaRequestListByUser />} />

        {/* campaign owner */}
        <Route path="/myCampaigns" element={<MyCampaigns />} />
        <Route
          path="/campaignDetails/:cid/:campaignName"
          element={<CampaignDetails />}
        />
        <Route
          path="/createCampaignBaseOnAudianceData"
          element={<CreateCampaignBasedOnAudianceData />}
        />
        <Route
          path="/myplaylistfullscreen/:id"
          element={<FullScreenPlaylist />}
        />
        {/* user profile */}
        <Route path="/userProfile" element={<UserProfile />} />
        <Route path="/editBrand" element={<EditBrandProfileData />} />
        <Route path="/editCreator" element={<EditCreatorProfileData />} />

        {/* Brand */}
        <Route path="/brandProfile/:id" element={<BrandProfileView />} />

        {/* Creator */}
        <Route path="/creatorProfile/:id" element={<CreatorProfileView />} />

        {/* coupons */}
        <Route path="/coupon/createCoupon" element={<CreateNewCoupon />} />
        <Route path="/coupon/editCoupon" element={<EditCouponDetails />} />
        <Route path="/coupon/brand" element={<CouponDashboardForBrand />} />
        <Route path="/coupon/screen/:id" element={<CouponListByScreen />} />
        <Route path="/couponManagement/:id" element={<CouponManagement />} />

        <Route path="/couponDetils" element={<CouponDetails />} />
        <Route path="/getCoupon" element={<CouponLandingPage />} />
        <Route path="/userCoupons" element={<CouponsForUser />} />
        <Route path="/myWishlist" element={<CouponWishlistForUser />} />

        <Route path="/wallet" element={<Wallet />} />

        <Route path="/cart" element={<Cart />} />
        <Route path="*" element={<Page404 />} />
      </Routes>
      {/* <Footer /> */}
    </>
  );
};
