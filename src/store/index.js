import { createStore, compose, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";

import {
  createCampaignReducer,
  campaignListAllReducer,
  campaignDetailsReducer,
  campaignListByScreenIDReducer,
  activeCampaignListByScreenIDReducer,
  campaignDeleteReducer,
  filteredCampaignListDateWiseReducer,
  campaignsWithScreensReducer,
  campaignListForBrandReducer,
  campaignLogsReducer,
} from "../Reducers/campaignReducers";

import {
  mailSendReducer,
  userDeleteReducer,
  userDetailsReducer,
  userListReducer,
  userScreensReducer,
  userSigninReducer,
  userSignupReducer,
  userUpdateProfileReducer,
  userCampaignReducer,
  userMediaReducer,
  userUpdatePasswordReducer,
  userCouponRewardListReducer,
  userActiveCampaignsReducer,
  sendEmailReducer,
  filterUserReducer,
  userWishlistReducer,
  pwaInstalledByUserConfirmReducer,
} from "../Reducers/userReducers";

import {
  allMediaReducer,
  mediaGetReducer,
  videoFromImagesReducer,
  mediaUploadReducer,
  myMediaReducer,
} from "../Reducers/mediaReducers";

import {
  allScreensGetReducer,
  filterScreenListByAudianceReducer,
  filterScreenListReducer,
  getCouponListByScreenIdReducer,
  qrScanDataGetReducer,
  screenAllyPleaGrantReducer,
  screenAllyPleaRejectReducer,
  screenAllyPleaRequestReducer,
  screenCamDataGetReducer,
  screenCreateReducer,
  screenDataGetReducer,
  screenDeleteReducer,
  screenDetailsReducer,
  screenFlagReducer,
  screenLikeReducer,
  screenListByCampaignIdsReducer,
  screenListByScreenIdsReducer,
  screenListByUserIdsReducer,
  screenListReducer,
  screenLogsReducer,
  screenParamsReducer,
  screenPinDetailsReducer,
  screenReviewCreateReducer,
  screenSubscribeReducer,
  screenUnlikeReducer,
  screenUnsubscribeReducer,
  screenUpdateReducer,
  screenVideoDeleteReducer,
  screenVideosReducer,
} from "../Reducers/screenReducers";
import {
  jsonPinsReducer,
  pinDetailsReducer,
  pinUpdateReducer,
} from "../Reducers/pinReducers";
import {
  createCampaignBasedOnAudienceProfileReducer,
  createCampaignForMultipleScreenReducer,
  getScreensBasedOnAudienceProfileReducer,
} from "../Reducers/campaignForMultipleScreenReducers";
import {
  allPleasListByUserReducer,
  allPleasListReducer,
  campaignAllyPleaGrantReducer,
  campaignAllyPleaRejectReducer,
  coupomRedeemUserPleaReducer,
  myAllPleasReducer,
} from "../Reducers/pleaReducers";
import {
  createNewBrandReducers,
  brandDetailsGetReducer,
  updateBrandDetailsReducer,
  allBrandsGetReducer,
} from "../Reducers/brandReducers";
import {
  addOrRemoveCouponInWishlistReducer,
  addPurchaseDetailsInCouponReducer,
  createNewCouponReducer,
  deleteCouponReducer,
  getAllActiveCouponListReducer,
  getCouponListForBrandReducer,
  getCouponListForUserReducer,
  getCouponfullDetailsReducer,
  redeemCouponReducer,
  updateCouponReducer,
} from "../Reducers/couponRedcers";
import {
  generateQRCodeReducer,
  qrcodeScanDataReducer,
} from "../Reducers/qrcodeReducer";
import {
  createNewCreatorReducer,
  getCreatorDetailsReducer,
  updateCreatorDetailsReducer,
} from "../Reducers/creatorReducers";
import {
  createWalletReducers,
  getTransactiionListReducer,
  getWalletBalanceReducer,
  rechargeWalletReducer,
} from "../Reducers/wallletReducers";

const initialState = {
  userSignin: {
    userInfo: localStorage.getItem("userInfo")
      ? JSON.parse(localStorage.getItem("userInfo"))
      : null,
  },
};

const reducer = combineReducers({
  //screen reducer
  allScreensGet: allScreensGetReducer,
  screenList: screenListReducer,
  filterScreenList: filterScreenListReducer,
  screenLogs: screenLogsReducer,
  screenDetails: screenDetailsReducer,
  screenCreate: screenCreateReducer,
  screenUpdate: screenUpdateReducer,
  screenDelete: screenDeleteReducer,
  screenVideos: screenVideosReducer,
  screenPinDetails: screenPinDetailsReducer,
  screenAllyPleaRequest: screenAllyPleaRequestReducer,
  screenParams: screenParamsReducer,
  screenVideoDelete: screenVideoDeleteReducer,
  screenReviewCreate: screenReviewCreateReducer,
  screenLike: screenLikeReducer,
  screenUnlike: screenUnlikeReducer,
  screenSubscribe: screenSubscribeReducer,
  screenUnsubscribe: screenUnsubscribeReducer,
  screenFlag: screenFlagReducer,
  screenAllyPleaReject: screenAllyPleaRejectReducer,
  screenAllyPleaGrant: screenAllyPleaGrantReducer,
  screenCoupons: getCouponListByScreenIdReducer,
  filterScreenListByAudiance: filterScreenListByAudianceReducer,
  qrScanDataGet: qrScanDataGetReducer,
  screenListByUserIds: screenListByUserIdsReducer,
  screenListByScreenIds: screenListByScreenIdsReducer,
  screenCamDataGet: screenCamDataGetReducer,
  screenListByCampaignIds: screenListByCampaignIdsReducer,
  screenDataGet: screenDataGetReducer,

  //campaign reducer
  createCampaign: createCampaignReducer,
  campaignListAll: campaignListAllReducer,
  campaignDetail: campaignDetailsReducer,
  campaignListByScreenId: campaignListByScreenIDReducer,
  activeCampaignListByScreenID: activeCampaignListByScreenIDReducer,
  campaignDelete: campaignDeleteReducer,
  campaignListDateWise: filteredCampaignListDateWiseReducer,
  campaignsWithScreens: campaignsWithScreensReducer,
  campaignListForBrand: campaignListForBrandReducer,
  campaignLogs: campaignLogsReducer,

  // user reducer
  userSignin: userSigninReducer,
  userSignup: userSignupReducer,
  userDetails: userDetailsReducer,
  userUpdateProfile: userUpdateProfileReducer,
  userUpdatePassword: userUpdatePasswordReducer,
  userList: userListReducer,
  userDelete: userDeleteReducer,
  userScreens: userScreensReducer,
  userCampaign: userCampaignReducer,
  userActiveCampaigns: userActiveCampaignsReducer,
  userMedia: userMediaReducer,
  mailSend: mailSendReducer,
  userCouponRewardList: userCouponRewardListReducer,
  sendEmail: sendEmailReducer,
  filterUsers: filterUserReducer,
  userWishlist: userWishlistReducer,
  pwaInstalledByUserConfirm: pwaInstalledByUserConfirmReducer,

  //media reducers
  mediaUpload: mediaUploadReducer,
  mediaGet: mediaGetReducer,
  allMedia: allMediaReducer,
  myMedia: myMediaReducer,
  videoFromImages: videoFromImagesReducer,

  //pin reducers
  jsonPins: jsonPinsReducer,
  pinDetails: pinDetailsReducer,
  pinUpdate: pinUpdateReducer,

  //plea reducer
  myAllPleas: myAllPleasReducer,
  allPleasList: allPleasListReducer,
  allPleasListByUser: allPleasListByUserReducer,
  campaignAllyPleaReject: campaignAllyPleaRejectReducer,
  campaignAllyPleaGrant: campaignAllyPleaGrantReducer,
  coupomRedeemUserPlea: coupomRedeemUserPleaReducer,

  //campaign for multiple screens reducers
  createCampaignForMultipleScreen: createCampaignForMultipleScreenReducer,
  createCampaignBasedOnAudienceProfile:
    createCampaignBasedOnAudienceProfileReducer,
  getScreens: getScreensBasedOnAudienceProfileReducer,

  // BRAND REDUCERS
  createNewBrand: createNewBrandReducers,
  brandDetails: brandDetailsGetReducer,
  updateBrand: updateBrandDetailsReducer,
  allBrandsGet: allBrandsGetReducer,

  // ALLY REDUCERS
  createNewCreator: createNewCreatorReducer,
  creatorDetails: getCreatorDetailsReducer,
  updateCreator: updateCreatorDetailsReducer,

  //COUPON
  createCoupon: createNewCouponReducer,
  couponListForBrand: getCouponListForBrandReducer,
  couponListForUser: getCouponListForUserReducer,
  couponFullDetails: getCouponfullDetailsReducer,
  addPurchaseDetails: addPurchaseDetailsInCouponReducer,

  updateCoupon: updateCouponReducer,
  deleteCoupon: deleteCouponReducer,
  activeCouponList: getAllActiveCouponListReducer,
  redeemCoupon: redeemCouponReducer,

  addRemoveCouponInWishlist: addOrRemoveCouponInWishlistReducer,

  //QRCODE
  generateQRCode: generateQRCodeReducer,
  qrcodeScanData: qrcodeScanDataReducer,

  //wallet
  createWallet: createWalletReducers,
  walletBalance: getWalletBalanceReducer,
  walletTransaction: getTransactiionListReducer,
  rechargeWallet: rechargeWalletReducer,
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  initialState,
  composeEnhancer(applyMiddleware(thunk))
);

export default store;
