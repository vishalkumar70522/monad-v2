import {
  CREATE_CAMPAIGN_REQUEST,
  CREATE_CAMPAIGN_SUCCESS,
  CREATE_CAMPAIGN_FAIL,
  CAMPAIGN_LIST_REQUEST,
  CAMPAIGN_LIST_SUCCESS,
  CAMPAIGN_LIST_FAIL,
  CAMPAIGN_DETAILS_REQUEST,
  CAMPAIGN_DETAILS_SUCCESS,
  CAMPAIGN_DETAILS_FAIL,
  CAMPAIGN_LIST_BY_SCREENID_FAIL,
  CAMPAIGN_LIST_BY_SCREENID_REQUEST,
  CAMPAIGN_LIST_BY_SCREENID_SUCCESS,
  CAMPAIGN_DELETE_REQUEST,
  CAMPAIGN_DELETE_SUCCESS,
  CAMPAIGN_DELETE_FAIL,
  CAMPAIGN_DELETE_RESET,
  CREATE_CAMPAIGN_RESET,
  ACTIVE_CAMPAIGN_LIST_BY_SCREENID_REQUEST,
  ACTIVE_CAMPAIGN_LIST_BY_SCREENID_SUCCESS,
  ACTIVE_CAMPAIGN_LIST_BY_SCREENID_FAIL,
  ACTIVE_CAMPAIGN_LIST_BY_SCREENID_RESET,
  FILTERED_CAMPAIGN_LIST_REQUEST,
  FILTERED_CAMPAIGN_LIST_SUCCESS,
  FILTERED_CAMPAIGN_LIST_FAIL,
  FILTERED_CAMPAIGN_LIST_RESET,
  CAMPAIGNS_WITH_SCREENS_REQUESR,
  CAMPAIGNS_WITH_SCREENS_SUCCESS,
  CAMPAIGNS_WITH_SCREENS_FAIL,
  CAMPAIGNS_WITH_SCREENS_RESET,
  CAMPAIGN_LOGS_REQUEST,
  CAMPAIGN_LOGS_SUCCESS,
  CAMPAIGN_LOGS_FAIL,
  CAMPAIGN_LOGS_RESET,
  CAMPAIGN_LIST_BY_SCREENID_RESET,
} from "../Constants/campaignConstants.js";

export function createCampaignReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_CAMPAIGN_REQUEST:
      return { loading: true };
    case CREATE_CAMPAIGN_SUCCESS:
      return {
        loading: false,
        uploadedCampaign: action.payload,
        success: true,
      };
    case CREATE_CAMPAIGN_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_CAMPAIGN_RESET:
      return {};
    default:
      return state;
  }
}

export function campaignListAllReducer(state = { allCampaign: [] }, action) {
  switch (action.type) {
    case CAMPAIGN_LIST_REQUEST:
      return { loading: true };
    case CAMPAIGN_LIST_SUCCESS:
      return { loading: false, allCampaign: action.payload };
    case CAMPAIGN_LIST_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function campaignsWithScreensReducer(state = { campaigns: [] }, action) {
  switch (action.type) {
    case CAMPAIGNS_WITH_SCREENS_REQUESR:
      return { loading: true };
    case CAMPAIGNS_WITH_SCREENS_SUCCESS:
      return { loading: false, campaigns: action.payload };
    case CAMPAIGNS_WITH_SCREENS_FAIL:
      return { loading: false, error: action.payload };
    case CAMPAIGNS_WITH_SCREENS_RESET:
      return {};
    default:
      return state;
  }
}

export function filteredCampaignListDateWiseReducer(
  state = { campaigns: [] },
  action
) {
  switch (action.type) {
    case FILTERED_CAMPAIGN_LIST_REQUEST:
      return { loading: true };
    case FILTERED_CAMPAIGN_LIST_SUCCESS:
      return { loading: false, campaigns: action.payload };
    case FILTERED_CAMPAIGN_LIST_FAIL:
      return { loading: false, error: action.payload };
    case FILTERED_CAMPAIGN_LIST_RESET:
      return {};
    default:
      return state;
  }
}

export function campaignDetailsReducer(state = { loading: true }, action) {
  switch (action.type) {
    case CAMPAIGN_DETAILS_REQUEST:
      return { loading: true };
    case CAMPAIGN_DETAILS_SUCCESS:
      return { loading: false, campaign: action.payload };
    case CAMPAIGN_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}
export function activeCampaignListByScreenIDReducer(
  state = { campaigns: [] },
  action
) {
  switch (action.type) {
    case ACTIVE_CAMPAIGN_LIST_BY_SCREENID_REQUEST:
      return { loading: true };
    case ACTIVE_CAMPAIGN_LIST_BY_SCREENID_SUCCESS:
      return { loading: false, campaigns: action.payload };
    case ACTIVE_CAMPAIGN_LIST_BY_SCREENID_FAIL:
      return { loading: false, error: action.payload };
    case ACTIVE_CAMPAIGN_LIST_BY_SCREENID_RESET:
      return {};
    default:
      return state;
  }
}

export function campaignListByScreenIDReducer(
  state = { campaigns: [] },
  action
) {
  switch (action.type) {
    case CAMPAIGN_LIST_BY_SCREENID_REQUEST:
      return { loading: true };
    case CAMPAIGN_LIST_BY_SCREENID_SUCCESS:
      return { loading: false, campaigns: action.payload };
    case CAMPAIGN_LIST_BY_SCREENID_FAIL:
      return { loading: false, error: action.payload };
    case CAMPAIGN_LIST_BY_SCREENID_RESET:
      return {};
    default:
      return state;
  }
}

export function campaignDeleteReducer(state = {}, action) {
  switch (action.type) {
    case CAMPAIGN_DELETE_REQUEST:
      return { loading: true };
    case CAMPAIGN_DELETE_SUCCESS:
      return { loading: false, success: true };
    case CAMPAIGN_DELETE_FAIL:
      return { loading: false, error: action.payload };
    case CAMPAIGN_DELETE_RESET:
      return {};
    default:
      return state;
  }
}

export function campaignListForBrandReducer(state = { campaigns: [] }, action) {
  switch (action.type) {
    case "CAMPAIGN_LIST_BRAND_REQUEST":
      return { loading: true };
    case "CAMPAIGN_LIST_BRAND_SUCCESS":
      return { loading: false, campaigns: action.payload };
    case "CAMPAIGN_LIST_BRAND_FAIL":
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function campaignLogsReducer(state = {}, action) {
  switch (action.type) {
    case CAMPAIGN_LOGS_REQUEST:
      return { loading: true };
    case CAMPAIGN_LOGS_SUCCESS:
      return {
        loading: false,
        last50: action.payload.last50,
        totalCount: action.payload.totalCount,
      };
    case CAMPAIGN_LOGS_FAIL:
      return { loading: false, error: action.payload };
    case CAMPAIGN_LOGS_RESET:
      return {};
    default:
      return state;
  }
}
