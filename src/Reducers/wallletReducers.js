import {
  CREATE_USER_WALLET_FAIL,
  CREATE_USER_WALLET_REQUEST,
  CREATE_USER_WALLET_RESET,
  CREATE_USER_WALLET_SUCCESS,
  GET_TRANSACTION_LIST_FAIL,
  GET_TRANSACTION_LIST_REQUEST,
  GET_TRANSACTION_LIST_RESET,
  GET_TRANSACTION_LIST_SUCCESS,
  GET_WALLET_BALANCE_FAIL,
  GET_WALLET_BALANCE_REQUEST,
  GET_WALLET_BALANCE_RESET,
  GET_WALLET_BALANCE_SUCCESS,
  RECHARGE_WALLET_FAIL,
  RECHARGE_WALLET_REQUEST,
  RECHARGE_WALLET_RESET,
  RECHARGE_WALLET_SUCCESS,
} from "../Constants/walletConstants";

export function createWalletReducers(state = {}, action) {
  switch (action.type) {
    case CREATE_USER_WALLET_REQUEST:
      return { loading: true };
    case CREATE_USER_WALLET_SUCCESS:
      return { loading: false, success: true };
    case CREATE_USER_WALLET_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_USER_WALLET_RESET:
      return {};
    default:
      return state;
  }
}

export function getWalletBalanceReducer(state = {}, action) {
  switch (action.type) {
    case GET_WALLET_BALANCE_REQUEST:
      return { loading: true };
    case GET_WALLET_BALANCE_SUCCESS:
      return { loading: false, wallet: action.payload, success: true };
    case GET_WALLET_BALANCE_FAIL:
      return { loading: false, error: action.payload };
    case GET_WALLET_BALANCE_RESET:
      return {};
    default:
      return state;
  }
}

export function rechargeWalletReducer(state = {}, action) {
  switch (action.type) {
    case RECHARGE_WALLET_REQUEST:
      return { loading: true };
    case RECHARGE_WALLET_SUCCESS:
      return { loading: false, wallet: action.payload, success: true };
    case RECHARGE_WALLET_FAIL:
      return { loading: false, error: action.payload };
    case RECHARGE_WALLET_RESET:
      return {};
    default:
      return state;
  }
}

export function getTransactiionListReducer(state = {}, action) {
  switch (action.type) {
    case GET_TRANSACTION_LIST_REQUEST:
      return { loading: true };
    case GET_TRANSACTION_LIST_SUCCESS:
      return { loading: false, transaction: action.payload, success: true };
    case GET_TRANSACTION_LIST_FAIL:
      return { loading: false, error: action.payload };
    case GET_TRANSACTION_LIST_RESET:
      return {};
    default:
      return state;
  }
}
