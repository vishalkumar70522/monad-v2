import {
  ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_FAIL,
  ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_REQUEST,
  ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_RESET,
  ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_SUCCESS,
} from "../Constants/campaignConstants";
import {
  ADD_PURCHASE_DETAILS_TO_COUPON_FAIL,
  ADD_PURCHASE_DETAILS_TO_COUPON_REQUEST,
  ADD_PURCHASE_DETAILS_TO_COUPON_RESET,
  ADD_PURCHASE_DETAILS_TO_COUPON_SUCCESS,
  ADD_USER_TO_COUPON_FAIL,
  ADD_USER_TO_COUPON_REQUEST,
  ADD_USER_TO_COUPON_RESET,
  ADD_USER_TO_COUPON_SUCCESS,
  ALL_ACTIVE_COUPON_LIST_FAIL,
  ALL_ACTIVE_COUPON_LIST_REQUEST,
  ALL_ACTIVE_COUPON_LIST_RESET,
  ALL_ACTIVE_COUPON_LIST_SUCCESS,
  COUPON_LIST_FOR_BRAND_FAIL,
  COUPON_LIST_FOR_BRAND_REQUEST,
  COUPON_LIST_FOR_BRAND_RESET,
  COUPON_LIST_FOR_BRAND_SUCCESS,
  COUPON_LIST_FOR_USER_FAIL,
  COUPON_LIST_FOR_USER_REQUEST,
  COUPON_LIST_FOR_USER_RESET,
  COUPON_LIST_FOR_USER_SUCCESS,
  CREATE_COUPON_FAIL,
  CREATE_COUPON_REQUEST,
  CREATE_COUPON_RESET,
  CREATE_COUPON_SUCCESS,
  DELETE_COUPON_FAIL,
  DELETE_COUPON_REQUEST,
  DELETE_COUPON_RESET,
  DELETE_COUPON_SUCCESS,
  EDIT_COUPON_FAIL,
  EDIT_COUPON_REQUEST,
  EDIT_COUPON_RESET,
  EDIT_COUPON_SUCCESS,
  GET_COUPON_FULL_DETAILS_FAIL,
  GET_COUPON_FULL_DETAILS_REQUEST,
  GET_COUPON_FULL_DETAILS_RESET,
  GET_COUPON_FULL_DETAILS_SUCCESS,
} from "../Constants/couponConstants";

export function createNewCouponReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_COUPON_REQUEST:
      return { loading: true };
    case CREATE_COUPON_SUCCESS:
      return { loading: false, success: true, coupon: action.payload };
    case CREATE_COUPON_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_COUPON_RESET:
      return {};
    default:
      return state;
  }
}

export function deleteCouponReducer(state = {}, action) {
  switch (action.type) {
    case DELETE_COUPON_REQUEST:
      return { loading: true };
    case DELETE_COUPON_SUCCESS:
      return { loading: false, success: true, coupon: action.payload };
    case DELETE_COUPON_FAIL:
      return { loading: false, error: action.payload };
    case DELETE_COUPON_RESET:
      return {};
    default:
      return state;
  }
}

export function getCouponListForBrandReducer(state = { coupons: [] }, action) {
  switch (action.type) {
    case COUPON_LIST_FOR_BRAND_REQUEST:
      return { loading: true };
    case COUPON_LIST_FOR_BRAND_SUCCESS:
      return { loading: false, success: true, coupons: action.payload };
    case COUPON_LIST_FOR_BRAND_FAIL:
      return { loading: false, error: action.payload };
    case COUPON_LIST_FOR_BRAND_RESET:
      return {};
    default:
      return state;
  }
}

export function getCouponListForUserReducer(state = { coupons: [] }, action) {
  switch (action.type) {
    case COUPON_LIST_FOR_USER_REQUEST:
      return { loading: true };
    case COUPON_LIST_FOR_USER_SUCCESS:
      return { loading: false, success: true, coupons: action.payload };
    case COUPON_LIST_FOR_USER_FAIL:
      return { loading: false, error: action.payload };
    case COUPON_LIST_FOR_USER_RESET:
      return {};
    default:
      return state;
  }
}

export function getAllActiveCouponListReducer(state = { coupons: [] }, action) {
  switch (action.type) {
    case ALL_ACTIVE_COUPON_LIST_REQUEST:
      return { loading: true };
    case ALL_ACTIVE_COUPON_LIST_SUCCESS:
      return { loading: false, success: true, coupons: action.payload };
    case ALL_ACTIVE_COUPON_LIST_FAIL:
      return { loading: false, error: action.payload };
    case ALL_ACTIVE_COUPON_LIST_RESET:
      return {};
    default:
      return state;
  }
}

export function updateCouponReducer(state = {}, action) {
  switch (action.type) {
    case EDIT_COUPON_REQUEST:
      return { loading: true };
    case EDIT_COUPON_SUCCESS:
      return { loading: false, success: true, coupon: action.payload };
    case EDIT_COUPON_FAIL:
      return { loading: false, error: action.payload };
    case EDIT_COUPON_RESET:
      return {};
    default:
      return state;
  }
}

export function redeemCouponReducer(state = {}, action) {
  switch (action.type) {
    case ADD_USER_TO_COUPON_REQUEST:
      return { loading: true };
    case ADD_USER_TO_COUPON_SUCCESS:
      return { loading: false, success: true, coupon: action.payload };
    case ADD_USER_TO_COUPON_FAIL:
      return { loading: false, error: action.payload };
    case ADD_USER_TO_COUPON_RESET:
      return {};
    default:
      return state;
  }
}

export function getCouponfullDetailsReducer(state = {}, action) {
  switch (action.type) {
    case GET_COUPON_FULL_DETAILS_REQUEST:
      return { loading: true };
    case GET_COUPON_FULL_DETAILS_SUCCESS:
      return {
        loading: false,
        success: true,
        coupon: action.payload.coupon,
        screens: action.payload.screens,
        campaigns: action.payload.campaigns,
      };
    case GET_COUPON_FULL_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case GET_COUPON_FULL_DETAILS_RESET:
      return {};
    default:
      return state;
  }
}

export function addOrRemoveCouponInWishlistReducer(state = {}, action) {
  switch (action.type) {
    case ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_REQUEST:
      return { loading: true };
    case ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_SUCCESS:
      return { loading: false, success: true };
    case ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_FAIL:
      return { loading: false, error: action.payload };
    case ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_RESET:
      return {};
    default:
      return state;
  }
}

export function addPurchaseDetailsInCouponReducer(state = {}, action) {
  switch (action.type) {
    case ADD_PURCHASE_DETAILS_TO_COUPON_REQUEST:
      return { loading: true };
    case ADD_PURCHASE_DETAILS_TO_COUPON_SUCCESS:
      return { loading: false, success: true };
    case ADD_PURCHASE_DETAILS_TO_COUPON_FAIL:
      return { loading: false, error: action.payload };
    case ADD_PURCHASE_DETAILS_TO_COUPON_RESET:
      return {};
    default:
      return state;
  }
}
