import {
  COUPOM_REDEEM_PLEA_REQUEST,
  COUPOM_REDEEM_PLEA_FAIL,
  COUPOM_REDEEM_PLEA_RESET,
  COUPOM_REDEEM_PLEA_SUCCESS,
  LIST_MY_PLEAS_FAIL,
  LIST_MY_PLEAS_REQUEST,
  LIST_MY_PLEAS_RESET,
  LIST_MY_PLEAS_SUCCESS,
  ALL_PLEA_LIST_REST,
  ALL_PLEA_LIST_FAIL,
  ALL_PLEA_LIST_REQUEST,
  ALL_PLEA_LIST_SUCCESS,
  ALL_PLEA_LIST_BY_USER_FAIL,
  ALL_PLEA_LIST_BY_USER_REQUEST,
  ALL_PLEA_LIST_BY_USER_SUCCESS,
  ALL_PLEA_LIST_BY_USER_REST,
  CAMPAIGN_ALLY_REJECT_REQUEST,
  CAMPAIGN_ALLY_REJECT_SUCCESS,
  CAMPAIGN_ALLY_REJECT_FAIL,
  CAMPAIGN_ALLY_REJECT_RESET,
  CAMPAIGN_ALLY_GRANT_REQUEST,
  CAMPAIGN_ALLY_GRANT_SUCCESS,
  CAMPAIGN_ALLY_GRANT_FAIL,
  CAMPAIGN_ALLY_GRANT_RESET,
} from "../Constants/pleaConstants";

export function myAllPleasReducer(state = { allPleas: [] }, action) {
  switch (action.type) {
    case LIST_MY_PLEAS_REQUEST:
      return { loading: true };
    case LIST_MY_PLEAS_SUCCESS:
      return { loading: false, success: true, allPleas: action.payload };
    case LIST_MY_PLEAS_FAIL:
      return { loading: false, error: action.payload };
    case LIST_MY_PLEAS_RESET:
      return {};
    default:
      return state;
  }
}
export function allPleasListReducer(state = { allPleas: [] }, action) {
  switch (action.type) {
    case ALL_PLEA_LIST_REQUEST:
      return { loading: true };
    case ALL_PLEA_LIST_SUCCESS:
      return { loading: false, success: true, allPleas: action.payload };
    case ALL_PLEA_LIST_FAIL:
      return { loading: false, error: action.payload };
    case ALL_PLEA_LIST_REST:
      return {};
    default:
      return state;
  }
}
export function allPleasListByUserReducer(state = { allPleas: [] }, action) {
  switch (action.type) {
    case ALL_PLEA_LIST_BY_USER_REQUEST:
      return { loading: true };
    case ALL_PLEA_LIST_BY_USER_SUCCESS:
      return { loading: false, success: true, allPleas: action.payload };
    case ALL_PLEA_LIST_BY_USER_FAIL:
      return { loading: false, error: action.payload };
    case ALL_PLEA_LIST_BY_USER_REST:
      return {};
    default:
      return state;
  }
}

export function campaignAllyPleaRejectReducer(state = {}, action) {
  switch (action.type) {
    case CAMPAIGN_ALLY_REJECT_REQUEST:
      return { loading: true };
    case CAMPAIGN_ALLY_REJECT_SUCCESS:
      return { loading: false, success: true, plea: action.payload };
    case CAMPAIGN_ALLY_REJECT_FAIL:
      return { loading: false, error: action.payload };
    case CAMPAIGN_ALLY_REJECT_RESET:
      return {};
    default:
      return state;
  }
}

export function campaignAllyPleaGrantReducer(state = {}, action) {
  switch (action.type) {
    case CAMPAIGN_ALLY_GRANT_REQUEST:
      return { loading: true };
    case CAMPAIGN_ALLY_GRANT_SUCCESS:
      return { loading: false, success: true, plea: action.payload };
    case CAMPAIGN_ALLY_GRANT_FAIL:
      return { loading: false, error: action.payload };
    case CAMPAIGN_ALLY_GRANT_RESET:
      return {};
    default:
      return state;
  }
}

export function coupomRedeemUserPleaReducer(state = {}, action) {
  switch (action.type) {
    case COUPOM_REDEEM_PLEA_REQUEST:
      return { loading: true };
    case COUPOM_REDEEM_PLEA_SUCCESS:
      return { loading: false, success: true, plea: action.payload };
    case COUPOM_REDEEM_PLEA_FAIL:
      return { loading: false, error: action.payload };
    case COUPOM_REDEEM_PLEA_RESET:
      return {};
    default:
      return state;
  }
}
