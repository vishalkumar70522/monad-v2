export function createNewCreatorReducer(state = {}, action) {
  switch (action.type) {
    case "CREATE_CREATOR_REQUEST":
      return { loading: true };
    case "CREATE_CREATOR_SUCCESS":
      return { loading: false, success: true, creator: action.payload };
    case "CREATE_CREATOR_FAIL":
      return { loading: false, error: action.payload };
    case "CREATE_CREATOR_RESET":
      return {};
    default:
      return state;
  }
}

export function getCreatorDetailsReducer(state = {}, action) {
  switch (action.type) {
    case "GET_CREATOR_DETAILS_REQUEST":
      return { loading: true };
    case "GET_CREATOR_DETAILS_SUCCESS":
      return { loading: false, success: true, creator: action.payload };
    case "GET_CREATOR_DETAILS_FAIL":
      return { loading: false, error: action.payload };
    case "GET_CREATOR_DETAILS_RESET":
      return {};
    default:
      return state;
  }
}

export function updateCreatorDetailsReducer(state = {}, action) {
  switch (action.type) {
    case "UPDATE_CREATOR_REQUEST":
      return { loading: true };
    case "UPDATE_CREATOR_SUCCESS":
      return { loading: false, success: true, creator: action.payload };
    case "UPDATE_CREATOR_FAIL":
      return { loading: false, error: action.payload };
    case "UPDATE_CREATOR_RESET":
      return {};
    default:
      return state;
  }
}
