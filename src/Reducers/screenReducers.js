import {
  FILTERED_SCREEN_LIST_BY_AUDIANCE_FAIL,
  FILTERED_SCREEN_LIST_BY_AUDIANCE_REQUEST,
  FILTERED_SCREEN_LIST_BY_AUDIANCE_RESET,
  FILTERED_SCREEN_LIST_BY_AUDIANCE_SUCCESS,
  FILTERED_SCREEN_LIST_FAIL,
  FILTERED_SCREEN_LIST_REQUEST,
  FILTERED_SCREEN_LIST_RESET,
  FILTERED_SCREEN_LIST_SUCCESS,
  SCREEN_ALLY_GRANT_FAIL,
  SCREEN_ALLY_GRANT_REQUEST,
  SCREEN_ALLY_GRANT_RESET,
  SCREEN_ALLY_GRANT_SUCCESS,
  SCREEN_ALLY_PLEA_FAIL,
  SCREEN_ALLY_PLEA_REQUEST,
  SCREEN_ALLY_PLEA_RESET,
  SCREEN_ALLY_PLEA_SUCCESS,
  SCREEN_ALLY_REJECT_FAIL,
  SCREEN_ALLY_REJECT_REQUEST,
  SCREEN_ALLY_REJECT_RESET,
  SCREEN_ALLY_REJECT_SUCCESS,
  SCREEN_COUPONS_FAIL,
  SCREEN_COUPONS_REQUEST,
  SCREEN_COUPONS_RESET,
  SCREEN_COUPONS_SUCCESS,
  SCREEN_CREATE_FAIL,
  SCREEN_CREATE_REQUEST,
  SCREEN_CREATE_RESET,
  SCREEN_CREATE_SUCCESS,
  SCREEN_DELETE_FAIL,
  SCREEN_DELETE_REQUEST,
  SCREEN_DELETE_RESET,
  SCREEN_DELETE_SUCCESS,
  SCREEN_DETAILS_FAIL,
  SCREEN_DETAILS_REQUEST,
  SCREEN_DETAILS_RESET,
  SCREEN_DETAILS_SUCCESS,
  SCREEN_FLAG_FAIL,
  SCREEN_FLAG_REQUEST,
  SCREEN_FLAG_SUCCESS,
  SCREEN_LIKE_FAIL,
  SCREEN_LIKE_REQUEST,
  SCREEN_LIKE_SUCCESS,
  SCREEN_LIST_BY_CAMPAIGN_IDS_FAIL,
  SCREEN_LIST_BY_CAMPAIGN_IDS_REQUEST,
  SCREEN_LIST_BY_CAMPAIGN_IDS_RESET,
  SCREEN_LIST_BY_CAMPAIGN_IDS_SUCCESS,
  SCREEN_LIST_BY_SCREEN_IDS_FAIL,
  SCREEN_LIST_BY_SCREEN_IDS_REQUEST,
  SCREEN_LIST_BY_SCREEN_IDS_RESET,
  SCREEN_LIST_BY_SCREEN_IDS_SUCCESS,
  SCREEN_LIST_BY_USER_IDS_FAIL,
  SCREEN_LIST_BY_USER_IDS_REQUEST,
  SCREEN_LIST_BY_USER_IDS_RESET,
  SCREEN_LIST_BY_USER_IDS_SUCCESS,
  SCREEN_LIST_FAIL,
  SCREEN_LIST_REQUEST,
  SCREEN_LIST_SUCCESS,
  SCREEN_LOGS_FAIL,
  SCREEN_LOGS_REQUEST,
  SCREEN_LOGS_SUCCESS,
  SCREEN_PARAMS_FAIL,
  SCREEN_PARAMS_REQUEST,
  SCREEN_PARAMS_SUCCESS,
  SCREEN_PIN_DETAILS_FAIL,
  SCREEN_PIN_DETAILS_REQUEST,
  SCREEN_PIN_DETAILS_SUCCESS,
  SCREEN_REVIEW_CREATE_FAIL,
  SCREEN_REVIEW_CREATE_REQUEST,
  SCREEN_REVIEW_CREATE_RESET,
  SCREEN_REVIEW_CREATE_SUCCESS,
  SCREEN_SUBSCRIBE_FAIL,
  SCREEN_SUBSCRIBE_REQUEST,
  SCREEN_SUBSCRIBE_SUCCESS,
  SCREEN_UNLIKE_FAIL,
  SCREEN_UNLIKE_REQUEST,
  SCREEN_UNLIKE_SUCCESS,
  SCREEN_UNSUBSCRIBE_FAIL,
  SCREEN_UNSUBSCRIBE_REQUEST,
  SCREEN_UNSUBSCRIBE_SUCCESS,
  SCREEN_UPDATE_FAIL,
  SCREEN_UPDATE_REQUEST,
  SCREEN_UPDATE_RESET,
  SCREEN_UPDATE_SUCCESS,
  SCREEN_VIDEOS_FAIL,
  SCREEN_VIDEOS_REQUEST,
  SCREEN_VIDEOS_SUCCESS,
  SCREEN_VIDEO_DELETE_FAIL,
  SCREEN_VIDEO_DELETE_REQUEST,
  SCREEN_VIDEO_DELETE_SUCCESS,
} from "../Constants/screenConstants";

export function allScreensGetReducer(state = [], action) {
  switch (action.type) {
    case "GET_ALL_SCREENS_REQUEST":
      return { loading: true };
    case "GET_ALL_SCREENS_SUCCESS":
      return {
        loading: false,
        screens: action.payload,
      };
    case "GET_ALL_SCREENS_FAIL":
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function screenListReducer(
  state = { loading: true, screens: [] },
  action
) {
  switch (action.type) {
    case SCREEN_LIST_REQUEST:
      return { loading: true };
    case SCREEN_LIST_SUCCESS:
      return {
        loading: false,
        screens: action.payload.screens,
        pages: action.payload.pages,
        page: action.payload.page,
      };
    case SCREEN_LIST_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function screenListByUserIdsReducer(
  state = { loading: true, screens: [] },
  action
) {
  switch (action.type) {
    case SCREEN_LIST_BY_USER_IDS_REQUEST:
      return { loading: true };
    case SCREEN_LIST_BY_USER_IDS_SUCCESS:
      return {
        loading: false,
        screens: action.payload,
      };
    case SCREEN_LIST_BY_USER_IDS_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_LIST_BY_USER_IDS_RESET:
      return {};
    default:
      return state;
  }
}

export function screenListByScreenIdsReducer(
  state = { loading: false, screens: [] },
  action
) {
  switch (action.type) {
    case SCREEN_LIST_BY_SCREEN_IDS_REQUEST:
      return { loading: true };
    case SCREEN_LIST_BY_SCREEN_IDS_SUCCESS:
      return {
        loading: false,
        screens: action.payload,
      };
    case SCREEN_LIST_BY_SCREEN_IDS_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_LIST_BY_SCREEN_IDS_RESET:
      return {};
    default:
      return state;
  }
}

export function screenListByCampaignIdsReducer(
  state = { loading: false, screens: [] },
  action
) {
  switch (action.type) {
    case SCREEN_LIST_BY_CAMPAIGN_IDS_REQUEST:
      return { loading: true };
    case SCREEN_LIST_BY_CAMPAIGN_IDS_SUCCESS:
      return {
        loading: false,
        screens: action.payload,
      };
    case SCREEN_LIST_BY_CAMPAIGN_IDS_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_LIST_BY_CAMPAIGN_IDS_RESET:
      return {};
    default:
      return state;
  }
}

export function filterScreenListReducer(
  state = { loading: true, screens: [] },
  action
) {
  switch (action.type) {
    case FILTERED_SCREEN_LIST_REQUEST:
      return { loading: true };
    case FILTERED_SCREEN_LIST_SUCCESS:
      return {
        loading: false,
        screens: action.payload,
      };
    case FILTERED_SCREEN_LIST_FAIL:
      return { loading: false, error: action.payload };
    case FILTERED_SCREEN_LIST_RESET:
      return {};
    default:
      return state;
  }
}
export function screenDetailsReducer(state = { loading: true }, action) {
  switch (action.type) {
    case SCREEN_DETAILS_REQUEST:
      return { loading: true };
    case SCREEN_DETAILS_SUCCESS:
      return { loading: false, screen: action.payload };
    case SCREEN_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_DETAILS_RESET:
      return {};
    default:
      return state;
  }
}

//

export function screenCreateReducer(state = {}, action) {
  switch (action.type) {
    case SCREEN_CREATE_REQUEST:
      return { loading: true };
    case SCREEN_CREATE_SUCCESS:
      return { loading: false, success: true, screen: action.payload };
    case SCREEN_CREATE_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_CREATE_RESET:
      return {};
    default:
      return state;
  }
}

export function getCouponListByScreenIdReducer(
  state = { coupons: [] },
  action
) {
  switch (action.type) {
    case SCREEN_COUPONS_REQUEST:
      return { loading: true };
    case SCREEN_COUPONS_SUCCESS:
      return { loading: false, success: true, coupons: action.payload };
    case SCREEN_COUPONS_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_COUPONS_RESET:
      return {};
    default:
      return state;
  }
}

//
export function filterScreenListByAudianceReducer(
  state = { screens: [] },
  action
) {
  switch (action.type) {
    case FILTERED_SCREEN_LIST_BY_AUDIANCE_REQUEST:
      return { loading: true };
    case FILTERED_SCREEN_LIST_BY_AUDIANCE_SUCCESS:
      return {
        loading: false,
        screens: action.payload.screens,
        filterOption: action.payload.filter,
      };
    case FILTERED_SCREEN_LIST_BY_AUDIANCE_FAIL:
      return { loading: false, error: action.payload };
    case FILTERED_SCREEN_LIST_BY_AUDIANCE_RESET:
      return {};
    default:
      return state;
  }
}

export function screenUpdateReducer(state = {}, action) {
  switch (action.type) {
    case SCREEN_UPDATE_REQUEST:
      return { loading: true };
    case SCREEN_UPDATE_SUCCESS:
      return { loading: false, success: true };
    case SCREEN_UPDATE_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_UPDATE_RESET:
      return {};
    default:
      return state;
  }
}

export function screenDeleteReducer(state = {}, action) {
  switch (action.type) {
    case SCREEN_DELETE_REQUEST:
      return { loading: true };
    case SCREEN_DELETE_SUCCESS:
      return { loading: false, success: true };
    case SCREEN_DELETE_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_DELETE_RESET:
      return {};
    default:
      return state;
  }
}

// video videos get

export function screenVideosReducer(state = { videos: [] }, action) {
  switch (action.type) {
    case SCREEN_VIDEOS_REQUEST:
      return { loading: true };
    case SCREEN_VIDEOS_SUCCESS:
      return { loading: false, videos: action.payload };
    case SCREEN_VIDEOS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

//
export function screenLogsReducer(state = { screenLog: [] }, action) {
  switch (action.type) {
    case SCREEN_LOGS_REQUEST:
      return { loading: true };
    case SCREEN_LOGS_SUCCESS:
      return { loading: false, screenLog: action.payload };
    case SCREEN_LOGS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function screenPinDetailsReducer(state = { loading: true }, action) {
  switch (action.type) {
    case SCREEN_PIN_DETAILS_REQUEST:
      return { loading: true };
    case SCREEN_PIN_DETAILS_SUCCESS:
      return { loading: false, screenPin: action.payload };
    case SCREEN_PIN_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

//

export function screenAllyPleaRequestReducer(state = {}, action) {
  switch (action.type) {
    case SCREEN_ALLY_PLEA_REQUEST:
      return { loading: true };
    case SCREEN_ALLY_PLEA_SUCCESS:
      return { loading: false, success: true, plea: action.payload };
    case SCREEN_ALLY_PLEA_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_ALLY_PLEA_RESET:
      return {};
    default:
      return state;
  }
}

export function screenParamsReducer(state = { loading: true }, action) {
  switch (action.type) {
    case SCREEN_PARAMS_REQUEST:
      return { loading: true };
    case SCREEN_PARAMS_SUCCESS:
      return { loading: false, params: action.payload };
    case SCREEN_PARAMS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

// screen videos deleted

export function screenVideoDeleteReducer(state = {}, action) {
  switch (action.type) {
    case SCREEN_VIDEO_DELETE_REQUEST:
      return { loading: true };
    case SCREEN_VIDEO_DELETE_SUCCESS:
      return { loading: false, video: action.payload, success: true };
    case SCREEN_VIDEO_DELETE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function screenReviewCreateReducer(state = {}, action) {
  switch (action.type) {
    case SCREEN_REVIEW_CREATE_REQUEST:
      return { loading: true };
    case SCREEN_REVIEW_CREATE_SUCCESS:
      return { loading: false, success: true, review: action.payload };
    case SCREEN_REVIEW_CREATE_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_REVIEW_CREATE_RESET:
      return {};
    default:
      return state;
  }
}

// screen like

export function screenLikeReducer(state = { screen: {} }, action) {
  switch (action.type) {
    case SCREEN_LIKE_REQUEST:
      return { loading: true };
    case SCREEN_LIKE_SUCCESS:
      return {
        ...state,
        loading: false,
        screen: action.payload,
        success: true,
      };
    case SCREEN_LIKE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

// screen unlike

export function screenUnlikeReducer(state = { screen: {} }, action) {
  switch (action.type) {
    case SCREEN_UNLIKE_REQUEST:
      return { loading: true };
    case SCREEN_UNLIKE_SUCCESS:
      return {
        ...state,
        loading: false,
        screen: action.payload,
        success: true,
      };
    case SCREEN_UNLIKE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

// screen subscribe

export function screenSubscribeReducer(state = { screen: {} }, action) {
  switch (action.type) {
    case SCREEN_SUBSCRIBE_REQUEST:
      return { loading: true };
    case SCREEN_SUBSCRIBE_SUCCESS:
      return {
        ...state,
        loading: false,
        screen: action.payload,
        success: true,
      };
    case SCREEN_SUBSCRIBE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

// screen unsubscribe

export function screenUnsubscribeReducer(state = { screen: {} }, action) {
  switch (action.type) {
    case SCREEN_UNSUBSCRIBE_REQUEST:
      return { loading: true };
    case SCREEN_UNSUBSCRIBE_SUCCESS:
      return {
        ...state,
        loading: false,
        screen: action.payload,
        success: true,
      };
    case SCREEN_UNSUBSCRIBE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

// screen flag

export function screenFlagReducer(state = { screen: {} }, action) {
  switch (action.type) {
    case SCREEN_FLAG_REQUEST:
      return { loading: true };
    case SCREEN_FLAG_SUCCESS:
      return {
        ...state,
        loading: false,
        screen: action.payload,
        success: true,
      };
    case SCREEN_FLAG_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function screenAllyPleaRejectReducer(state = {}, action) {
  switch (action.type) {
    case SCREEN_ALLY_REJECT_REQUEST:
      return { loading: true };
    case SCREEN_ALLY_REJECT_SUCCESS:
      return { loading: false, success: true, plea: action.payload };
    case SCREEN_ALLY_REJECT_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_ALLY_REJECT_RESET:
      return {};
    default:
      return state;
  }
}

export function screenAllyPleaGrantReducer(state = {}, action) {
  switch (action.type) {
    case SCREEN_ALLY_GRANT_REQUEST:
      return { loading: true };
    case SCREEN_ALLY_GRANT_SUCCESS:
      return { loading: false, success: true, plea: action.payload };
    case SCREEN_ALLY_GRANT_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_ALLY_GRANT_RESET:
      return {};
    default:
      return state;
  }
}

export function playlistCheckReducer(state = {}, action) {
  switch (action.type) {
    case "CHECK_PLAYLIST_REQUEST":
      return { loading: true };
    case "CHECK_PLAYLIST_SUCCESS":
      return { loading: false, data: action.payload };
    case "CHECK_PLAYLIST_FAIL":
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function qrScanDataGetReducer(state = {}, action) {
  switch (action.type) {
    case "GET_SCREEN_DATA_DETAILS_REQUEST":
      return { loading: true };
    case "GET_SCREEN_DATA_DETAILS_SUCCESS":
      return { loading: false, data: action.payload };
    case "GET_SCREEN_DATA_DETAILS_FAIL":
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function screenCamDataGetReducer(state = {}, action) {
  switch (action.type) {
    case "GET_SCREEN_CAM_DATA_REQUEST":
      return { loading: true };
    case "GET_SCREEN_CAM_DATA_SUCCESS":
      return { loading: false, data: action.payload };
    case "GET_SCREEN_CAM_DATA_FAIL":
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function screenDataGetReducer(state = {}, action) {
  switch (action.type) {
    case "GET_SCREEN_DATA_REQUEST":
      return { loading: true };
    case "GET_SCREEN_DATA_SUCCESS":
      return { loading: false, data: action.payload };
    case "GET_SCREEN_DATA_FAIL":
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}
