import {
  CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_FAIL,
  CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_REQUEST,
  CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_RESET,
  CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_SUCCESS,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_FAIL,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_REQUEST,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET,
  CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_SUCCESS,
  FILTER_SCREENS_FOR_CREATE_CAMPAIGN_FAIL,
  FILTER_SCREENS_FOR_CREATE_CAMPAIGN_REQUEST,
  FILTER_SCREENS_FOR_CREATE_CAMPAIGN_RESET,
  FILTER_SCREENS_FOR_CREATE_CAMPAIGN_SUCCESS,
} from "../Constants/campaignForMultipleScreen";

export function createCampaignForMultipleScreenReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_REQUEST:
      return { loading: true };
    case CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_SUCCESS:
      return {
        loading: false,
        uploadedCampaign: action.payload,
        success: true,
      };
    case CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET:
      return {};
    default:
      return state;
  }
}

export function createCampaignBasedOnAudienceProfileReducer(
  state = {},
  action
) {
  switch (action.type) {
    case CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_REQUEST:
      return { loading: true };
    case CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_SUCCESS:
      return {
        loading: false,
        uploadedCampaign: action.payload,
        success: true,
      };
    case CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_RESET:
      return {};
    default:
      return state;
  }
}

export function getScreensBasedOnAudienceProfileReducer(state = {}, action) {
  switch (action.type) {
    case FILTER_SCREENS_FOR_CREATE_CAMPAIGN_REQUEST:
      return { loading: true };
    case FILTER_SCREENS_FOR_CREATE_CAMPAIGN_SUCCESS:
      return {
        loading: false,
        screens: action.payload,
      };
    case FILTER_SCREENS_FOR_CREATE_CAMPAIGN_FAIL:
      return { loading: false, error: action.payload };
    case FILTER_SCREENS_FOR_CREATE_CAMPAIGN_RESET:
      return {};
    default:
      return state;
  }
}
