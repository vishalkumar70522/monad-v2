import {
  GENERATE_QRCODE_FOR_SCREEN_FAIL,
  GENERATE_QRCODE_FOR_SCREEN_REQUEST,
  GENERATE_QRCODE_FOR_SCREEN_RESET,
  GENERATE_QRCODE_FOR_SCREEN_SUCCESS,
} from "../Constants/generateQRCodeConstants";

export function generateQRCodeReducer(state = {}, action) {
  switch (action.type) {
    case GENERATE_QRCODE_FOR_SCREEN_REQUEST:
      return { loading: true };
    case GENERATE_QRCODE_FOR_SCREEN_SUCCESS:
      return { loading: false, success: true, message: action.payload };
    case GENERATE_QRCODE_FOR_SCREEN_FAIL:
      return { loading: false, error: action.payload };
    case GENERATE_QRCODE_FOR_SCREEN_RESET:
      return {};
    default:
      return state;
  }
}

export function qrcodeScanDataReducer(state = {}, action) {
  switch (action.type) {
    case "SEND_SCAN_DATA_LOADING":
      return { loading: true };
    case "SEND_SCAN_DATA_SUCCESS":
      return { loading: true, success: true, data: action.payload };
    case "SEND_SCAN_DATA_FAIL":
      return { loading: true, error: action.payload };
    default:
      return state;
  }
}
