import { SelectProps } from "antd";

export const highlights: SelectProps["options"] = [
  {
    value: "School",
    label: "School",
  },
  {
    value: "Colleges",
    label: "Colleges",
  },
  {
    value: "Swimming pool",
    label: "Swimming pool",
  },
  {
    value: "Appartments",
    label: "Appartments",
  },
  {
    value: "Metro",
    label: "Metro",
  },
  {
    value: "Airports",
    label: "Airports",
  },
  {
    value: "Military",
    label: "Military",
  },
  {
    value: "Tourist attractions",
    label: "Tourist attractions",
  },
  {
    value: "Railways",
    label: "Railways",
  },
  {
    value: "Beachs",
    label: "Beachs",
  },
];
