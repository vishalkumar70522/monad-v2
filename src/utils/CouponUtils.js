import {
  BUY_X_GEY_Y,
  FLAT_DISCOUNT,
  PERSENTAGE_DISCOUNT,
} from "../Constants/typeOfCouponsConstatnts";

export const generateOfferDetails = (coupon) => {
  const x =
    coupon?.couponRewardInfo?.minimumOrderCondition === "Order value"
      ? `Min spent: ₹${coupon?.couponRewardInfo?.minimumOrderValue}`
      : `Min Quantity: ${coupon?.couponRewardInfo?.minimumOrderQuantity}`;

  if (coupon?.couponRewardInfo?.couponType === PERSENTAGE_DISCOUNT) {
    return `GET ${coupon?.couponRewardInfo?.discountPersentage}% OFF | ${x}`;
  } else if (coupon?.couponRewardInfo?.couponType === FLAT_DISCOUNT) {
    return `GET Rs. ${coupon?.couponRewardInfo?.discountAmount} OFF | ${x}`;
  } else if (coupon?.couponRewardInfo?.couponType === BUY_X_GEY_Y) {
    return `BUY ${coupon?.couponRewardInfo?.buyItems} AND GET ${coupon?.couponRewardInfo?.freeItems}| ${x}`;
  } else {
    return coupon?.offerName;
  }
};
