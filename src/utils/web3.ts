import { Web3Storage } from "web3.storage";

function makeStorageClient() {
  return new Web3Storage({ token: getAccessToken() });
}
// function jsonFile(filename: any, obj: any) {
//   return new File([JSON.stringify(obj)], filename);
// }
// function makeGatewayURL(cid: any, path: any) {
//   return `https://${cid}.ipfs.dweb.link/${encodeURIComponent(path)}`;
// }
function getAccessToken() {
  return (
    process.env.REACT_APP_WEB3_STORAGE_API_TOKEN ||
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweDNBZjFDOTZiRUNiMzgxODZDNTk1MjFkNTA2ZjMzNzgxMzVjMjYwNDIiLCJpc3MiOiJ3ZWIzLXN0b3JhZ2UiLCJpYXQiOjE2Nzc1NjQ4NzI2ODIsIm5hbWUiOiJXRUIzX1RPS0VOIn0.7MYCo5Q1OBLUqbECuFQUzqwSu5eGam2V2b9rZEVDtgA"
  );
}

export const addFileOnWeb3 = async (file: any) => {
  // const startTime = performance.now();
  try {
    // const metadataFile = jsonFile("metadata.json", {
    //   path: file.name,
    //   caption: new Date(),
    // });
    // console.log("metadataFile : ", metadataFile);
    // console.log("storeFiles file : ", file);
    const client = makeStorageClient();
    const cid = await client.put([file], {
      name: file.name,
      maxRetries: 3,
    });
    // console.log("response cid : ", cid);
    let res = await client.get(cid);
    // console.log("response : ", res);
    const files = (await res?.files()) || []; // Web3File[]
    for (const file of files) {
      // console.log(`${file.cid} ${file.name} ${file.size}`);
      // console.log(
      //   "time taken to upload file in ms ",
      //   performance.now() - startTime
      // );
      return file.cid;
    }
    // const imageGatewayURL = makeGatewayURL(cid, file.name);
    // console.log("stored files with cid:", cid);
    // console.log("imageGatewayURL : ", imageGatewayURL);
    // return { url: imageGatewayURL, cid: cid };
  } catch (error: any) {
    // console.log("Error in uploading file : ", error);
    // console.log(
    //   "time taken to upload file in ms ",
    //   performance.now() - startTime
    // );
    throw new Error(error);
  }
};
