import React, { useEffect, useState } from "react";
import {
  Box,
  Center,
  Stack,
  Text,
  Button,
  Divider,
  AbsoluteCenter,
  Link,
} from "@chakra-ui/react";
import { Form, Input, message } from "antd";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { sendEmailToUser } from "../../../Actions/userActions";
import { SEND_EMAIL_TO_USER_RESET } from "../../../Constants/userConstants";

export function ForgetPassword(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [email, setEmail] = useState<any>("");

  //userSignup;
  const sendEmail = useSelector((state: any) => state.sendEmail);
  const { message: resMessage, loading, error, success } = sendEmail;

  const [messageApi, contextHolder] = message.useMessage();

  const emailSendError = (error: any) => {
    messageApi.open({
      type: "error",
      content: error,
    });
  };

  useEffect(() => {
    if (success) {
      message.success(resMessage);
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
    }
    if (error) {
      emailSendError(error);
      setEmail("");
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
    }
  }, [success, error]);

  const onFinish = (values: any) => {
    dispatch(
      sendEmailToUser({
        email,
        name: email.split("@")[0],
        signup: false,
        forgetPsssword: true,
      })
    );
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Center pt={{ base: 5, lg: 100 }}>
      {contextHolder}
      <Box
        boxShadow="2xl"
        px={{ base: "5", lg: "10" }}
        pt="10"
        bgColor="#FBFBFB"
        width={{ base: "100%", lg: "540px" }}
      >
        <Text
          color="#333333"
          fontSize={{ base: "xl", lg: "2xl" }}
          fontWeight="semibold"
        >
          Forget password
        </Text>
        {success ? (
          <Link
            color="teal.500"
            href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox"
          >
            click hear to inbox
          </Link>
        ) : null}

        <Form
          layout="vertical"
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          fields={[
            {
              name: ["email"],
              value: email,
            },
          ]}
        >
          <Form.Item
            label={
              <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                Enter email
              </Text>
            }
            name="email"
            rules={[
              { required: true, message: "Please input your email!" },
              { type: "email" },
            ]}
          >
            <Input
              placeholder="Enter your email"
              onChange={(e) => setEmail(e.target.value)}
              size="large"
            />
          </Form.Item>

          <Form.Item>
            <Button
              type="submit"
              width="100%"
              backgroundColor="#D7380E"
              py="2"
              fontSize={{ base: "xl", lg: "2xl" }}
              isLoading={loading}
              loadingText="Sending mail"
            >
              Send email
            </Button>
          </Form.Item>
        </Form>
        <Stack py="5">
          <Box position="relative">
            <Divider />
            <AbsoluteCenter bg="white" px="4">
              or continue with
            </AbsoluteCenter>
          </Box>
        </Stack>

        <Stack pt="5">
          <Button
            py="3"
            width="100%"
            bgColor="#FFFFFF"
            color="#333333"
            type="submit"
            border="1px"
            onClick={() => navigate("/signin")}
          >
            Already have an account ? Sign in
          </Button>
        </Stack>
        <Stack pt="5">
          <Button
            py="3"
            width="100%"
            bgColor="#FFFFFF"
            color="#333333"
            type="submit"
            border="1px"
            onClick={() => navigate("/signUp")}
          >
            Create a new account
          </Button>
        </Stack>
        <Text fontSize="xs" textAlign="left" mt="4" pb="10">
          By signing in, I agree to Monad’s{" "}
          <Link color="teal.500" href="#">
            Terms of Use
          </Link>{" "}
          and{" "}
          <Link color="teal.500" href="#">
            Privacy Policy
          </Link>{" "}
          .
        </Text>
      </Box>
    </Center>
  );
}
