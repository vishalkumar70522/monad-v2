import React, { useEffect, useState } from "react";
import {
  Box,
  Center,
  Stack,
  Text,
  Button,
  IconButton,
  Divider,
  AbsoluteCenter,
  Link,
} from "@chakra-ui/react";
import { Form, Input, message } from "antd";
import GoogleLogin from "react-google-login";
import { FcGoogle } from "react-icons/fc";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  googleSignupSignin,
  sendEmailToUser,
} from "../../../Actions/userActions";
import {
  SEND_EMAIL_TO_USER_RESET,
  USER_SIGNOUT,
} from "../../../Constants/userConstants";

export function SignUp() {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [email, setEmail] = useState<any>("");

  const sendEmail = useSelector((state: any) => state.sendEmail);
  const { message: resMessage, loading, error, success } = sendEmail;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { error: errroInGoogleSignin, success: successInGoogleSignin } =
    userSignin;

  const [messageApi, contextHolder] = message.useMessage();
  const emailSendSussess = () => {
    messageApi.open({
      type: "success",
      content: "Log in Success full",
    });
  };
  const emailSendError = (error: any) => {
    messageApi.open({
      type: "error",
      content: error,
    });
  };

  useEffect(() => {
    if (success) {
      message.success(resMessage);
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
    }
    if (error) {
      emailSendError(error);
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
      setEmail("");
    }
    if (successInGoogleSignin) {
      emailSendSussess();
      navigate("/");
    }
    if (errroInGoogleSignin) {
      emailSendError(error);
      dispatch({ type: USER_SIGNOUT });
      setEmail("");
    }
  }, [
    success,
    error,
    successInGoogleSignin,
    errroInGoogleSignin,
    navigate,
    resMessage,
  ]);

  const onFinish = (values: any) => {
    dispatch(
      sendEmailToUser({
        email,
        name: email.split("@")[0],
        signup: true,
        forgetPsssword: false,
      })
    );
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  const onFailure = (err: any) => {};

  const onSuccess = async (res: any) => {
    dispatch(
      googleSignupSignin({
        name: res?.profileObj?.name,
        email: res?.profileObj?.email,
        avatar: res?.profileObj?.imageUrl,
      })
    );
  };

  return (
    <Center pt={{ base: 10, lg: 100 }}>
      {contextHolder}
      <Box
        boxShadow="2xl"
        px={{ base: "5", lg: "10" }}
        pt="10"
        bgColor="#FBFBFB"
        width={{ base: "100%", lg: "540px" }}
      >
        <Text
          color="#333333"
          fontSize={{ base: "xl", lg: "2xl" }}
          fontWeight="semibold"
        >
          Create your account and step into a new realm of advertisement.{" "}
        </Text>
        {success ? (
          <Link
            color="teal.500"
            href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox"
          >
            click hear to inbox
          </Link>
        ) : null}

        <Form
          layout="vertical"
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          fields={[
            {
              name: ["email"],
              value: email,
            },
          ]}
        >
          <Form.Item
            label={
              <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                Enter email
              </Text>
            }
            name="email"
            rules={[
              { required: true, message: "Please input your email!" },
              { type: "email" },
            ]}
          >
            <Input
              placeholder="Enter your email"
              onChange={(e) => setEmail(e.target.value)}
              size="large"
            />
          </Form.Item>

          <Form.Item>
            <Button
              type="submit"
              width="100%"
              backgroundColor="#D7380E"
              py="2"
              fontSize={{ base: "xl", lg: "2xl" }}
              isLoading={loading}
              loadingText="Sending mail"
            >
              Verify email
            </Button>
          </Form.Item>
        </Form>
        <Stack py="5">
          <Box position="relative">
            <Divider />
            <AbsoluteCenter bg="white" px="4">
              or continue with
            </AbsoluteCenter>
          </Box>
        </Stack>

        <Stack direction="row" align="center" justifyContent="center" pt="3">
          <GoogleLogin
            clientId={
              process.env.REACT_APP_GOOGLE_CLIENT_ID ||
              "829203424949-dkctdksnijr38djuoa2mm3i7m1b979ih.apps.googleusercontent.com"
            }
            buttonText="Log in with Google"
            render={(renderProps) => (
              <Box
                py="2"
                width="100%"
                bgColor="#FFFFFF"
                color="#333333"
                // type="submit"
                onClick={renderProps.onClick}
                border="1px"
                rounded="md"
                textAlign="center"
              >
                <IconButton
                  bg="none"
                  pr="5"
                  icon={<FcGoogle size="25px" color="black" />}
                  aria-label="Close"
                />
                Log In with Google
              </Box>
            )}
            onSuccess={onSuccess}
            onFailure={onFailure}
            cookiePolicy={"single_host_origin"}
          />
        </Stack>
        <Stack pt="5">
          <Button
            py="3"
            width="100%"
            bgColor="#FFFFFF"
            color="#333333"
            type="submit"
            border="1px"
            onClick={() => navigate("/signin")}
          >
            Already have an account ? Log in
          </Button>
        </Stack>
        <Text fontSize="xs" textAlign="left" mt="4" pb="10">
          By logging in you agree to our{" "}
          <Link color="teal.500" href="#">
            Terms of Use{" "}
          </Link>{" "}
          and acknowledge that you have read our{" "}
          <Link color="teal.500" href="#">
            Privacy Policy
          </Link>{" "}
          .
        </Text>
      </Box>
    </Center>
  );
}
