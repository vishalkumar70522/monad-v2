import React, { useEffect, useState } from "react";
import { Box, Center, Text, Button } from "@chakra-ui/react";
import { Form, Input, message } from "antd";
import { signup } from "../../../Actions/userActions";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export function CreatePassword(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const name = window.location.pathname.split("/")[3];
  const [email, setEmail] = useState<any>("");
  const [password, setPassword] = useState<any>("");
  const [confirmPassword, setConfirmPassword] = useState<any>("");
  const userSignup = useSelector((state: any) => state.userSignup);
  const { loading, error, success } = userSignup;

  const [messageApi, contextHolder] = message.useMessage();
  const passwordCreateSuccess = () => {
    messageApi.open({
      type: "success",
      content: "Password created successfully",
    });
  };
  const passwordError = (error: any) => {
    messageApi.open({
      type: "error",
      content: error,
    });
  };

  useEffect(() => {
    if (success) {
      passwordCreateSuccess();
      setTimeout(() => {
        navigate("/");
      }, 1000);
    }
    if (error) {
      passwordError(error);
      setPassword("");
      setConfirmPassword("");
    }
  }, [success, error, navigate]);

  const onFinish = (values: any) => {
    if (password === confirmPassword) {
      if (password.length >= 8) {
        dispatch(signup(name, email, password));
      } else {
        passwordError("Password length atleast 6 charactors");
        setPassword("");
        setConfirmPassword("");
      }
    } else {
      passwordError("Password mismatched!, try again");
      setPassword("");
      setConfirmPassword("");
    }
  };

  const onFinishFailed = (errorInfo: any) => {};

  useEffect(() => {
    const email = window.location.pathname.split("/")[2];
    setEmail(email);
  }, [dispatch]);

  return (
    <Center pt={{ base: 5, lg: 100 }}>
      {contextHolder}
      <Box
        boxShadow="2xl"
        px={{ base: "5", lg: "10" }}
        py="10"
        bgColor="#FBFBFB"
        width={{ base: "100%", lg: "540px" }}
      >
        <Text
          color="#333333"
          fontSize={{ base: "xl", lg: "2xl" }}
          fontWeight="semibold"
        >
          Create new password
        </Text>
        <Text color="#5B5B5B" fontSize={{ base: "sm", lg: "md" }}>
          Password must be at least 8 characters long.
        </Text>
        <Form
          layout="vertical"
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          fields={[
            {
              name: ["password"],
              value: password,
            },
            {
              name: ["confirmPassword"],
              value: confirmPassword,
            },
          ]}
        >
          <Form.Item
            label={
              <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                New password
              </Text>
            }
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password
              placeholder="New password"
              onChange={(e) => setPassword(e.target.value)}
              size="large"
              minLength={8}
            />
          </Form.Item>
          <Form.Item
            label={
              <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                Confirm password
              </Text>
            }
            name="confirmPassword"
            rules={[
              {
                required: true,
                message: "Please input your confirm password!",
              },
            ]}
          >
            <Input.Password
              placeholder="Confirm password"
              onChange={(e) => setConfirmPassword(e.target.value)}
              size="large"
              minLength={8}
            />
          </Form.Item>

          <Form.Item>
            <Button
              type="submit"
              width="100%"
              backgroundColor="#D7380E"
              py="2"
              fontSize={{ base: "xl", lg: "2xl" }}
              isLoading={loading}
              loadingText="Saving password"
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Box>
    </Center>
  );
}
