import React, { useEffect, useState } from "react";
import {
  Box,
  Center,
  Stack,
  Text,
  Button,
  IconButton,
  Divider,
  AbsoluteCenter,
  Link,
} from "@chakra-ui/react";
import { Checkbox, Form, Input, message } from "antd";
import "./index.css";
import GoogleLogin from "react-google-login";
import { FcGoogle } from "react-icons/fc";
// import { gapi } from "gapi-script";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { googleSignupSignin, signin } from "../../../Actions/userActions";
import { USER_SIGNOUT } from "../../../Constants/userConstants";

export function Signin(props: any) {
  const location = useLocation();
  // console.log("location?.state?.path : ", location?.state);

  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [email, setEmail] = useState<any>(
    localStorage.getItem("myapp-email") || ""
  );
  const [password, setPassword] = useState<any>(
    localStorage.getItem("myapp-password") || ""
  );

  const userSignin = useSelector((state: any) => state.userSignin);
  const { error, success } = userSignin;

  const [messageApi, contextHolder] = message.useMessage();
  const LoginSuccess = () => {
    messageApi.open({
      type: "success",
      content: "Log in Success full",
    });
  };
  const LoginError = (error: any) => {
    messageApi.open({
      type: "error",
      content: error,
    });
  };

  const onFinish = (values: any) => {
    //console.log("Success:", values); {email : "" , password : ""}
    setIsLoading(true);
    dispatch(signin(email, password));
  };
  const onFailure = (err: any) => {};

  const onFinishFailed = (errorInfo: any) => {};

  const onSuccess = async (res: any) => {
    dispatch(
      googleSignupSignin({
        name: res?.profileObj?.name,
        email: res?.profileObj?.email,
        avatar: res?.profileObj?.imageUrl,
      })
    );
  };

  const remember = (e: any) => {
    console.log("remember called!");
    if (e.target.checked) {
      localStorage.setItem("myapp-email", email);
      localStorage.setItem("myapp-password", password);
    } else {
      localStorage.setItem("myapp-email", "");
      localStorage.setItem("myapp-password", "");
    }
  };

  useEffect(() => {
    if (success) {
      setIsLoading(false);
      LoginSuccess();
      navigate(location?.state?.path || "/", {
        state: {
          data: location?.state?.data,
        },
      });
    }
    if (error) {
      setIsLoading(false);
      LoginError(error);
      dispatch({ type: USER_SIGNOUT });
    }
  }, [success, error]);

  return (
    <Center pt={{ base: 10, lg: 100 }}>
      {contextHolder}
      <Box
        boxShadow="2xl"
        px={{ base: "5", lg: "10" }}
        pt="10"
        bgColor="#FBFBFB"
        width={{ base: "100%", lg: "540px" }}
      >
        <Text
          color="#333333"
          fontSize={{ base: "xl", lg: "lg" }}
          fontWeight="semibold"
        >
          Log in to see our top picks for you!
        </Text>
        <Text color="#5B5B5B" fontSize={{ base: "md", lg: "sm" }}>
          For security, please log in to access your information
        </Text>
        <Form
          layout="vertical"
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          fields={[
            {
              name: ["email"],
              value: email,
            },
            {
              name: ["password"],
              value: password,
            },
          ]}
        >
          <Form.Item
            label={
              <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                Enter email
              </Text>
            }
            name="email"
            style={{ fontSize: "20px" }}
            rules={[
              { required: true, message: "Please input your email!" },
              { type: "email" },
            ]}
          >
            <Input
              placeholder="Enter your email"
              onChange={(e) => setEmail(e.target.value)}
              size="large"
            />
          </Form.Item>

          <Form.Item
            label={
              <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                Password
              </Text>
            }
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password
              placeholder="Enter your password"
              onChange={(e) => setPassword(e.target.value)}
              size="large"
            />
          </Form.Item>

          <Form.Item>
            <Form.Item name="remember" noStyle>
              <Checkbox onClick={remember}>Remember me</Checkbox>
            </Form.Item>

            <a
              className="login-form-forgot"
              href=""
              onClick={() => navigate("/forgetPassword")}
            >
              Forgot password
            </a>
          </Form.Item>

          <Form.Item>
            <Button
              type="submit"
              isLoading={isLoading}
              loadingText="Log in"
              width="100%"
              backgroundColor="#D7380E"
              py="2"
              fontSize={{ base: "xl", lg: "2xl" }}
            >
              Log in
            </Button>
          </Form.Item>
        </Form>
        <Stack py="5">
          <Box position="relative">
            <Divider />
            <AbsoluteCenter bg="white" px="4">
              or log in with
            </AbsoluteCenter>
          </Box>
        </Stack>

        <Stack direction="row" align="center" justifyContent="center" pt="3">
          <GoogleLogin
            clientId={
              process.env.REACT_APP_GOOGLE_CLIENT_ID ||
              "829203424949-dkctdksnijr38djuoa2mm3i7m1b979ih.apps.googleusercontent.com"
            }
            buttonText="Log in with Google"
            render={(renderProps) => (
              <Box
                py="2"
                width="100%"
                bgColor="#FFFFFF"
                color="#333333"
                // type="submit"
                onClick={renderProps.onClick}
                border="1px"
                rounded="md"
                textAlign="center"
              >
                <IconButton
                  bg="none"
                  pr="5"
                  icon={<FcGoogle size="25px" color="black" />}
                  aria-label="Close"
                />
                Log In with Google
              </Box>
            )}
            onSuccess={onSuccess}
            onFailure={onFailure}
            cookiePolicy={"single_host_origin"}
          />
        </Stack>
        <Stack pt="5">
          <Button
            py="3"
            width="100%"
            bgColor="#FFFFFF"
            color="#333333"
            type="submit"
            border="1px"
            onClick={() => navigate("/signUp")}
          >
            Create a new account
          </Button>
        </Stack>
        <Text fontSize="xs" textAlign="left" mt="4" pb="10">
          By logging in you agree to our{" "}
          <Link color="teal.500" href="#">
            Terms of Use{" "}
          </Link>{" "}
          and acknowledge that you have read our{" "}
          <Link color="teal.500" href="#">
            Privacy Policy
          </Link>{" "}
          .
        </Text>
      </Box>
    </Center>
  );
}
