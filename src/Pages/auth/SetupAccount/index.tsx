import React, { useEffect, useState } from "react";
import { Box, Center, Stack, Text, Button } from "@chakra-ui/react";
import { Form, Input, Select } from "antd";
import { gapi } from "gapi-script";
const { Option } = Select;

const selectBefore = (
  <Select defaultValue="+91" style={{ width: 80 }}>
    <Option value="+91">+91</Option>
  </Select>
);

export function SetupAccount(props: any) {
  const [email, setEmail] = useState<any>("");
  const [password, setPassword] = useState<any>("");

  const clientId =
    "829203424949-dkctdksnijr38djuoa2mm3i7m1b979ih.apps.googleusercontent.com";

  const onFinish = (values: any) => {};
  const onFailure = (err: any) => {};

  const onFinishFailed = (errorInfo: any) => {};

  const onSuccess = async (res: any) => {
    setEmail(res?.profileObj?.email);
  };

  useEffect(() => {
    const initClient = () => {
      gapi.client.init({
        clientId: clientId,
        scope: "",
      });
    };
    gapi.load("client:auth2", initClient);
  }, []);

  return (
    <Center pt={{ base: 5, lg: 100 }}>
      <Box
        boxShadow="2xl"
        px={{ base: "5", lg: "10" }}
        pt="10"
        bgColor="#FBFBFB"
        width={{ base: "100%", lg: "540px" }}
      >
        <Text color="#333333" fontSize="2xl" fontWeight="semibold">
          Let's setup your account!
        </Text>
        <Text color="#5B5B5B" fontSize="md">
          Setup your account by providing some details{" "}
        </Text>
        <Form
          layout="vertical"
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label={
              <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                Name
              </Text>
            }
            name="name"
            style={{ fontSize: "20px" }}
            rules={[
              {
                required: true,
                message: "Please input Name of your organization!",
              },
            ]}
          >
            <Input
              placeholder="Name of your organization"
              onChange={(e) => setEmail(e.target.value)}
              size="large"
            />
          </Form.Item>

          <Form.Item
            label={
              <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                Contact number
              </Text>
            }
            name="contact"
            rules={[
              { required: true, message: "Please input your phone no.!" },
              { min: 10 },
              { max: 10 },
            ]}
          >
            <Input
              type="number"
              addonBefore={selectBefore}
              placeholder="Enter your phone no."
              onChange={(e) => setPassword(e.target.value)}
              size="large"
            />
          </Form.Item>
          <Stack pt="5">
            <Form.Item>
              <Button
                type="submit"
                width="100%"
                backgroundColor="#D7380E"
                py="2"
                fontSize={{ base: "xl", lg: "2xl" }}
              >
                Finish
              </Button>
            </Form.Item>
          </Stack>
        </Form>
      </Box>
    </Center>
  );
}
