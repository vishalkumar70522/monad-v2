import { Box, Flex, SimpleGrid, Text, Hide } from "@chakra-ui/react";
import { Radio, RadioChangeEvent, Skeleton } from "antd";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userCampaignsList } from "../../Actions/userActions";
import { MySingleCampaign, ResultNotFound } from "../../components/commans";

export function MyCampaigns(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [value, setValue] = useState<any>("All");
  const [campaigns, setCampaigns] = useState<any>([]);

  const filterCampaign = (value: any) => {
    if (value === "All") {
      setCampaigns(myVideos ? myVideos : []);
    } else {
      const data = myVideos?.filter((video: any) => video?.status === value);
      if (data.length > 0) setCampaigns([...data]);
      else setCampaigns([]);
    }
  };

  const onChange = (e: RadioChangeEvent) => {
    setValue(e.target.value);
    filterCampaign(e.target.value);
  };
  const userCampaign = useSelector((state: any) => state.userCampaign);
  const {
    loading: loadingMyVideos,
    error: errorMyVideos,
    campaign: myVideos,
  } = userCampaign;
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    if (errorMyVideos) {
      if (errorMyVideos === "Please Signin Again to continue") {
        navigate("/signin");
      }
    } else if (loadingMyVideos === false && myVideos) {
      setCampaigns(myVideos);
    }
  }, [navigate, errorMyVideos, myVideos]);

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    } else {
      dispatch(userCampaignsList(userInfo));
    }
  }, [navigate, dispatch]);

  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: 75, lg: 100 }}>
      <Box boxShadow="xl" p="5">
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={2}
          justifyContent="space-between"
        >
          <Text
            color="#333333"
            fontSize="2xl"
            fontWeight="665"
            m="0"
            align="left"
          >
            My campaigns
          </Text>
          <Flex gap={5}>
            <Hide below="md">
              <Text
                color="#333333"
                fontSize={{ base: "sm", lg: "md" }}
                fontWeight="665"
                align="center"
                m="0"
              >
                Filter
              </Text>
            </Hide>
            <Radio.Group onChange={onChange} value={value}>
              <Radio value={"All"}>All</Radio>
              <Radio value={"Active"}>Active</Radio>
              <Radio value={"Pending"}>Pending</Radio>
              <Radio value={"Deleted"}>Deleted</Radio>
            </Radio.Group>
          </Flex>
        </SimpleGrid>
      </Box>
      {loadingMyVideos ? (
        <Box pt="5">
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
        </Box>
      ) : campaigns?.length === 0 && loadingMyVideos === false ? (
        <ResultNotFound path="/" />
      ) : (
        campaigns
          .sort((c1: any, c2: any) => c1?.startDate - c2?.startDate)
          .reverse()
          .map((campaign: any, index: any) => (
            <MySingleCampaign campaign={campaign} key={index} />
          ))
      )}
    </Box>
  );
}
