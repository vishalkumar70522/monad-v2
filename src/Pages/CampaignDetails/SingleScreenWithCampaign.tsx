import {
  Box,
  Divider,
  Flex,
  Hide,
  Image,
  SimpleGrid,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import { BsDot } from "react-icons/bs";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";

export function SingleScreenWithCampaign(props: any) {
  const { campaign, screen, index, selectedIndex } = props;
  return (
    <Box
      bgColor="#F9F9F9"
      p={{ base: "2", lg: "5" }}
      mt="5"
      onClick={props?.onClick}
    >
      <Flex justifyContent="space-between">
        <Flex gap={{ base: "1", lg: "5" }}>
          <Image
            alt=""
            src={screen?.image}
            width={{ base: "84px", lg: "165px" }}
            height={{ base: "104px", lg: "104px" }}
            borderRadius="8px"
          />
          <Box width={{ base: "", lg: "320px" }}>
            <Text
              color="#222222"
              fontSize={{ base: "sm", lg: "xl" }}
              fontWeight="452"
              m="0"
            >
              {screen?.name}
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "12px", lg: "sm" }}
              fontWeight="600"
              m="0"
            >
              No of slots {campaign?.totalSlotBooked}
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "12px", lg: "sm" }}
              fontWeight="600"
              m="0"
            >
              Campaign start date {convertIntoDateAndTime(campaign?.startDate)}
            </Text>
          </Box>
        </Flex>
        <VStack align="flex-end">
          <Flex gap={1}>
            <BsDot
              color={
                campaign?.status === "Active"
                  ? "#00D615"
                  : campaign?.status === "Deleted"
                  ? "#E93A03"
                  : "yellow"
              }
              size="20px"
            />
            <Text color="#403F45" fontSize={{ base: "11px", lg: "sm" }} m="0">
              {campaign?.status}
            </Text>
          </Flex>

          <Hide below="md">
            {index != selectedIndex ? (
              <Flex
                justifyContent="space-between"
                gap={{ base: "10", lg: "20" }}
                pt={{ base: "5", lg: "10" }}
              >
                <Text
                  color="#222222"
                  fontSize={{ base: "12px", lg: "md" }}
                  fontWeight="600"
                  m="0"
                >
                  Amount spent
                </Text>
                <Text
                  color="#222222"
                  fontSize={{ base: "12px", lg: "md" }}
                  fontWeight="600"
                  m="0"
                >
                  ₹{campaign?.totalAmount}
                </Text>
              </Flex>
            ) : null}
          </Hide>
        </VStack>
      </Flex>
      {index === selectedIndex ? (
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "2", lg: "10" }}
          justifyContent="space-between"
          pt={{ base: "2", lg: "5" }}
        >
          <Stack fontWeight="427">
            <Text
              color="#4B4B4B"
              fontSize={{ base: "sm", lg: "md" }}
              align="left"
              m="0"
            >
              Average daily footfall:{"    "}
              {screen?.additionalData?.averageDailyFootfall}
            </Text>
            <Text
              color="#4B4B4B"
              fontSize={{ base: "sm", lg: "md" }}
              align="left"
              m="0"
            >
              Regular Audience Percentage : {"  "}{" "}
              {
                screen?.additionalData?.footfallClassification
                  ?.regularAudiencePercentage
              }
              {" %"}
            </Text>
            <Text
              color="#4B4B4B"
              fontSize={{ base: "sm", lg: "md" }}
              align="left"
              m="0"
            >
              Sex ration (male : female) : {"     "}
              {
                screen?.additionalData?.footfallClassification?.sexRatio?.male
              } :{" "}
              {screen?.additionalData?.footfallClassification?.sexRatio?.female}
            </Text>
            <Text
              color="#4B4B4B"
              fontSize={{ base: "sm", lg: "md" }}
              align="left"
              m="0"
            >
              Average purchase power (Start value - End value) : {"  ₹     "}
              {
                screen?.additionalData?.footfallClassification
                  ?.averagePurchasePower?.start
              }
              {"  "}-{"  ₹    "}
              {
                screen?.additionalData?.footfallClassification
                  ?.averagePurchasePower?.end
              }
            </Text>
            <Text
              color="#4B4B4B"
              fontSize={{ base: "sm", lg: "md" }}
              align="left"
              m="0"
            >
              average Age Group (Start age - End age) : {"    "}
              {
                screen?.additionalData?.footfallClassification?.averageAgeGroup
                  ?.averageStartAge
              }
              {" years  "}-{"   "}
              {
                screen?.additionalData?.footfallClassification?.averageAgeGroup
                  ?.averageEndAge
              }
              {" years"}
            </Text>
          </Stack>
          <Stack>
            <Text
              color="#000000"
              fontSize={{ base: "sm", lg: "md" }}
              align="left"
              m="0"
              fontWeight="638"
            >
              Screen split price
            </Text>
            <Flex justifyContent="space-between">
              <Text
                color="#222222"
                fontSize={{ base: "sm", lg: "md" }}
                align="left"
                m="0"
                fontWeight="400"
              >
                ₹{campaign?.rentPerSlot} x {campaign?.totalSlotBooked} Slots
              </Text>
              <Text
                color="#222222"
                fontSize={{ base: "sm", lg: "md" }}
                align="left"
                m="0"
                fontWeight="400"
              >
                ₹{campaign?.totalAmount}
              </Text>
            </Flex>
            <Flex justifyContent="space-between">
              <Text
                color="#222222"
                fontSize={{ base: "sm", lg: "md" }}
                align="left"
                m="0"
                fontWeight="400"
              >
                SGST
              </Text>
              <Text
                color="#222222"
                fontSize={{ base: "sm", lg: "md" }}
                align="left"
                m="0"
                fontWeight="400"
              >
                ₹123
              </Text>
            </Flex>
            <Flex justifyContent="space-between">
              <Text
                color="#222222"
                fontSize={{ base: "sm", lg: "md" }}
                align="left"
                m="0"
                fontWeight="400"
              >
                CGST
              </Text>
              <Text
                color="#222222"
                fontSize={{ base: "sm", lg: "md" }}
                align="left"
                m="0"
                fontWeight="400"
              >
                ₹123
              </Text>
            </Flex>
            <Divider color="#DDDDDD" />
            <Flex justifyContent="space-between">
              <Text
                color="#222222"
                fontSize={{ base: "sm", lg: "md" }}
                align="left"
                m="0"
                fontWeight="600"
              >
                Total
              </Text>
              <Text
                color="#222222"
                fontSize={{ base: "sm", lg: "md" }}
                align="left"
                m="0"
                fontWeight="600"
              >
                ₹{campaign?.totalAmount}
              </Text>
            </Flex>
          </Stack>
        </SimpleGrid>
      ) : null}
    </Box>
  );
}
