import { Box, Stack, Divider, SimpleGrid, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { getCampaignsAlongWithScreens } from "../../Actions/campaignAction";
import { useSelector } from "react-redux";
import { Skeleton, message } from "antd";
import { MediaContainer } from "../../components/commans";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { SingleScreenWithCampaign } from "./SingleScreenWithCampaign";
import { CAMPAIGNS_WITH_SCREENS_RESET } from "../../Constants/campaignConstants";

export function CampaignDetails(props: any) {
  let { cid, campaignName } = useParams();
  const dispatch = useDispatch<any>();
  const [campaign, setCampaign] = useState<any>(null);
  const [info, setInfo] = useState<any>(null);
  const [selectedIndex, setSelectedIndex] = useState<any>(0);

  const campaignsWithScreens = useSelector(
    (state: any) => state.campaignsWithScreens
  );
  const {
    loading: loadingCampaigns,
    error: errorCampaigns,
    campaigns,
  } = campaignsWithScreens;

  const getTotalNumberOfSlots = () => {
    const data = campaigns?.reduce(
      (accumulator: any, currentValue: any) => {
        const { campaign } = currentValue; // currentValue = {screen : {}, campaign : {}}
        accumulator.slots += campaign.totalSlotBooked;
        accumulator.amounts += campaign.totalAmount;
        switch (campaign.status) {
          case "Active":
            accumulator.active += 1;

            break;
          case "Pending":
            accumulator.pending += 1;

            break;
          case "Deleted":
            accumulator.deleted += 1;
            break;
          case "Pause":
            accumulator.pause += 1;

            break;
          default:
            accumulator.completed += 1;
        }
        return accumulator;
      },
      {
        slots: 0,
        amounts: 0,
        pending: 0,
        active: 0,
        deleted: 0,
        completed: 0,
        pause: 0,
      }
    );
    setInfo(data);
  };
  // console.log(campaign);
  useEffect(() => {
    if (errorCampaigns) {
      message.error(errorCampaigns);
      dispatch({ type: CAMPAIGNS_WITH_SCREENS_RESET });
    } else if (campaigns?.length > 0) {
      setCampaign(campaigns[0].campaign);
      getTotalNumberOfSlots();
    }
  }, [errorCampaigns, campaigns]);

  useEffect(() => {
    dispatch(getCampaignsAlongWithScreens(cid, campaignName));
  }, [cid, campaignName, dispatch]);
  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: 75, lg: 100 }}>
      {loadingCampaigns ? (
        <Box pt={{ base: "5", lg: "10" }}>
          <Skeleton avatar active paragraph={{ rows: 3 }} />
        </Box>
      ) : (
        <Stack>
          <Box boxShadow="2xl" p="5" borderRadius="4px">
            {campaign?.status === "pending" && (
              <Text
                color="#1C1C1C"
                fontSize={{ base: "12px", lg: "sm" }}
                fontWeight="479"
                m="0"
              >
                Your account is pending approval from the screen owner. This
                approval process is a one-time requirement for each screen.
                While you wait for approval, you can still post your ad. The
                approval process typically takes up to 2 hours
              </Text>
            )}
            {campaign?.status === "Completed" && (
              <Text
                color="#1C1C1C"
                fontSize={{ base: "12px", lg: "sm" }}
                fontWeight="479"
                m="0"
              >
                Your campaign has completed...
              </Text>
            )}
          </Box>
          <SimpleGrid
            columns={[1, null, 2]}
            spacing={{ base: "5", lg: "20" }}
            pt={{ base: "2", lg: "10" }}
          >
            <MediaContainer
              cid={campaign?.cid}
              // cid={`https://ipfs.io/ipfs/${campaign?.cid}`}
              width={{ base: "396px", lg: "337px" }}
              height={{ base: "237px", lg: "216px" }}
              autoPlay="false"
            />
            <Box>
              <SimpleGrid
                columns={[1, null, 2]}
                spacing={{ base: "1", lg: "20" }}
              >
                <Text
                  fontSize={{ base: "", lg: "sm" }}
                  fontWeight="600"
                  color="#222222"
                  m="0"
                >
                  Campaign name
                </Text>
                <Text
                  fontSize={{ base: "12px", lg: "sm" }}
                  fontWeight="600"
                  color="#2F2F2F"
                  m="0"
                >
                  {campaign?.campaignName}
                </Text>
              </SimpleGrid>
              <Divider color="#DDDDDD" />
              <SimpleGrid
                columns={[1, null, 2]}
                spacing={{ base: "1", lg: "20" }}
              >
                <Text
                  fontSize={{ base: "", lg: "sm" }}
                  fontWeight="600"
                  color="#222222"
                  m="0"
                >
                  Campaign creation date
                </Text>
                <Text
                  fontSize={{ base: "12px", lg: "sm" }}
                  fontWeight="600"
                  color="#2F2F2F"
                  m="0"
                >
                  {convertIntoDateAndTime(campaign?.createdAt)}
                </Text>
              </SimpleGrid>
              <Divider color="#DDDDDD" />
              <SimpleGrid
                columns={[1, null, 2]}
                spacing={{ base: "1", lg: "20" }}
              >
                <Text
                  fontSize={{ base: "", lg: "sm" }}
                  fontWeight="600"
                  color="#222222"
                  m="0"
                >
                  Total no. of slots
                </Text>
                <Text
                  fontSize={{ base: "12px", lg: "sm" }}
                  fontWeight="600"
                  color="#2F2F2F"
                  m="0"
                >
                  {info?.slots}
                </Text>
              </SimpleGrid>
              <Divider color="#DDDDDD" />
              <SimpleGrid
                columns={[1, null, 2]}
                spacing={{ base: "1", lg: "20" }}
              >
                <Text
                  fontSize={{ base: "", lg: "sm" }}
                  fontWeight="600"
                  color="#222222"
                  m="0"
                >
                  Total budget spent
                </Text>
                <Text
                  fontSize={{ base: "12px", lg: "sm" }}
                  fontWeight="600"
                  color="#2F2F2F"
                  m="0"
                >
                  ₹{info?.amounts}
                </Text>
              </SimpleGrid>
              <Divider color="#DDDDDD" />
            </Box>
          </SimpleGrid>
        </Stack>
      )}
      {info && (
        <SimpleGrid
          columns={[2, 3, 5]}
          spacing={{ base: "2", lg: "20" }}
          pt={{ base: "2", lg: "10" }}
        >
          <Box bgColor="#F7F7F7" p={{ base: "3", lg: "5" }} borderRadius="8px">
            <Text
              fontSize={{ base: "md", lg: "2xl" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              {info.pending}
            </Text>
            <Text
              fontSize={{ base: "10px", lg: "md" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              Pending screens
            </Text>
          </Box>
          <Box bgColor="#F7F7F7" p={{ base: "3", lg: "5" }} borderRadius="8px">
            <Text
              fontSize={{ base: "md", lg: "2xl" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              {info.active}
            </Text>
            <Text
              fontSize={{ base: "10px", lg: "md" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              Active screens
            </Text>
          </Box>
          <Box bgColor="#F7F7F7" p={{ base: "3", lg: "5" }} borderRadius="8px">
            <Text
              fontSize={{ base: "md", lg: "2xl" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              {info.pause}
            </Text>
            <Text
              fontSize={{ base: "10px", lg: "md" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              Pause screens
            </Text>
          </Box>
          <Box bgColor="#F7F7F7" p={{ base: "3", lg: "5" }} borderRadius="8px">
            <Text
              fontSize={{ base: "md", lg: "2xl" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              {info.deleted}
            </Text>
            <Text
              fontSize={{ base: "10px", lg: "md" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              Deleted screens
            </Text>
          </Box>
          <Box bgColor="#F7F7F7" p={{ base: "3", lg: "5" }} borderRadius="8px">
            <Text
              fontSize={{ base: "md", lg: "2xl" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              {info.completed}
            </Text>
            <Text
              fontSize={{ base: "10px", lg: "md" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              Completed screens
            </Text>
          </Box>
        </SimpleGrid>
      )}

      {loadingCampaigns ? (
        <Box pt={{ base: "5", lg: "10" }}>
          <Skeleton avatar active paragraph={{ rows: 3 }} />
        </Box>
      ) : (
        campaigns?.length > 0 &&
        campaigns?.map((data: any, index: any) => (
          <SingleScreenWithCampaign
            campaign={data.campaign}
            screen={data.screen}
            index={index}
            selectedIndex={selectedIndex}
            key={index}
            onClick={() =>
              setSelectedIndex(index === selectedIndex ? -1 : index)
            }
          />
        ))
      )}
    </Box>
  );
}
