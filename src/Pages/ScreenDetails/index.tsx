import {
  Box,
  Text,
  Flex,
  SimpleGrid,
  Image,
  Button,
  Show,
  Hide,
  Skeleton,
  Stack,
  // Stack,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCampaignListByScreenId } from "../../Actions/campaignAction";
import Axios from "axios";
import { LeftHandSide } from "./LeftHandSide";
import { CreateCampaign } from "./CreateCampaign";
import { AdvertiseBox, MediaContainer } from "../../components/commans";
import { ScreenReview } from "./ScreeenReview";
import { CreateCampaignName } from "../Models/CreateCampaignName";
import { UploadMedia } from "../Models/UploadMedia";
import { useNavigate } from "react-router-dom";
import { uploadMedia } from "../../Actions/mediaActions";
import { createVideoFromImage } from "../../Actions/videoFromImage";
import { Carousel, message } from "antd";
import { CreateCampaignPopup } from "./CreateCampaignPopup";
import { applyScreenAllyPlea } from "../../Actions/screenActions";
import { ImageView } from "../../components/commans/ImageView";

const dataSkeleton = [1, 2, 3, 4];

export function ScreenDetails(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const screenId = window.location.pathname.split("/")[2];
  const [openCreateCampaignName, setOpenCreateCampaignName] =
    useState<boolean>(false);
  const [openUploadMedia, setOpenUploadMedia] = useState<boolean>(false);
  const [openCreataCampaignPop, setOpenCreataCampaignPop] =
    useState<any>(false);

  const [campaignName, setCampaignName] = useState<any>("");
  const [fileUrl, setFileUrl] = useState<any>("");
  const [numberOfSlots, setNumberOfSlots] = useState<number>(30);
  const [startDateTime, setStartdateTime] = useState<any>(null);
  const [endDateTime, setEndDateTime] = useState<any>(null);

  const [screen, setScreen] = useState<any>(null);
  const [jsonData, setJsonData] = useState<any>({
    features: [],
  });
  const [loadingScreen, setLoadingScreen] = useState<any>(true);
  const [selectedImages, setSelectedImage] = useState<any>("");
  const [errorScreen, setErrorScreen] = useState<any>();
  const [geometry, setGeometry] = useState<any>();
  const [imageShow, setImageShow] = useState<any>(false);
  const [numberOfCampaignShow, setNumberOfCampaignShow] = useState<Number>(3);

  // hooks

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const activeCampaignListByScreenID = useSelector(
    (state: any) => state.activeCampaignListByScreenID
  );

  const {
    // loading: loadingCampaign,
    // error: errorCampaign,
    campaigns: videosList,
  } = activeCampaignListByScreenID;

  const handleCreateVideoFromImage = (filedata: any) => {
    dispatch(createVideoFromImage(filedata));
    navigate("/singleCampaign", {
      state: {
        campaignName,
        startDateTime,
        endDateTime,
        numberOfSlots,
        screenId,
      },
    });
    setCampaignName("");
    setFileUrl("");
  };

  const handleOpenCreateCampaignName = (value: any) => {
    if (!userInfo) {
      message.error("please sign in");
      navigate("/signin", {
        state: {
          path: `/screenDetails/${screenId}`,
        },
      });
    } else {
      setOpenCreateCampaignName(value);
    }
  };

  const videoUploadHandler = async (e: any) => {
    e.preventDefault();

    dispatch(
      uploadMedia({
        title: campaignName,
        thumbnail:
          "https://bafybeicduvlghzcrjtuxkro7foazucvuyej25rh3humeujbzt7bmio4hsa.ipfs.w3s.link/raily.png",
        fileUrl,
        media: "",
      })
    );
    navigate("/singleCampaign", {
      state: {
        campaignName,
        startDateTime,
        endDateTime,
        numberOfSlots,
        screenId,
      },
    });
    setCampaignName("");
    setFileUrl("");
  };

  const getScreenDetail = async (screenId: any) => {
    // var startTime = performance.now();
    try {
      // console.log("getScreenDetail called!");
      setLoadingScreen(true);
      // console.log("getScreenDetail : ", screenId);
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenId}`
      );
      // console.log("screen  : ", JSON.stringify(data));
      setScreen(data);
      setGeometry({
        coordinates: [data.lat, data.lng],
      });
      setJsonData({
        features: [
          {
            type: "Feature",
            properties: {
              pin: data?._id,
              screen: data?._id,
            },
            geometry: {
              coordinates: [data.lat, data.lng],
              type: "Point",
            },
          },
        ],
      });
      //var endTime = performance.now();
      // console.log(
      //   `Call to doSomething took ${endTime - startTime} milliseconds`
      // );
      setLoadingScreen(false);
    } catch (error: any) {
      setLoadingScreen(false);
      setErrorScreen(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      );
      //var endTime1 = performance.now();
      // console.log(
      //   `Call to doSomething took ${endTime1 - startTime} milliseconds`
      // );
    }
  };
  useEffect(() => {
    getScreenDetail(screenId);
    dispatch(getCampaignListByScreenId(screenId));
  }, [screenId, dispatch, navigate]);

  const handelCheckUser = () => {
    if (!userInfo || userInfo?._id === screen?.master?._id) {
      return true;
    } else {
      // now check user have access or not
      const value = screen?.allies?.find((user: any) => user === userInfo?._id);
      if (value) {
        return true;
      }
      return false;
    }
  };
  const handelPleaRequest = () => {
    if (!userInfo) {
      message.error("please sign in");
      navigate("/signin", {
        state: {
          path: `/screenDetails/${screen?._id}`,
        },
      });
    } else {
      dispatch(applyScreenAllyPlea(screen?._id));
    }
  };

  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: 75, lg: 100 }}>
      {/* screen images */}
      <ImageView
        imageShow={imageShow}
        setImageShow={(value: any) => setImageShow(value)}
        image={selectedImages}
      />
      <CreateCampaignName
        open={openCreateCampaignName}
        onCancel={() => setOpenCreateCampaignName(false)}
        openUploadMedia={() => setOpenUploadMedia(true)}
        setCampaignName={(value: any) => setCampaignName(value)}
        campaignName={campaignName}
      />
      <UploadMedia
        open={openUploadMedia}
        onCancel={() => setOpenUploadMedia(false)}
        createVideo={handleCreateVideoFromImage}
        videoUploadHandler={videoUploadHandler}
        setCampaignName={(value: any) => setCampaignName(value)}
        setFileUrl={(value: any) => setFileUrl(value)}
      />
      <CreateCampaignPopup
        open={openCreataCampaignPop}
        onCancel={() => setOpenCreataCampaignPop(false)}
        screen={screen}
        createCampaign={() => handleOpenCreateCampaignName(true)}
        setNumberOfSlots={(value: any) => setNumberOfSlots(value)}
        setStartdateTime={(value: any) => setStartdateTime(value)}
        setEndDateTime={(value: any) => setEndDateTime(value)}
      />
      {/* Desktop view of screen images */}
      <Hide below="md">
        {loadingScreen ? (
          <SimpleGrid columns={[1, 2, 2]} spacing="5">
            <Skeleton>
              <Box width="100%" height="340px"></Box>
            </Skeleton>
            <SimpleGrid columns={[1, 1, 2]} spacing="5">
              {dataSkeleton?.map((image: any, index: any) => (
                <Skeleton key={index}>
                  <Box width="100%" height="160px"></Box>
                </Skeleton>
              ))}
            </SimpleGrid>
          </SimpleGrid>
        ) : screen?.images?.length === 0 ? (
          <SimpleGrid columns={[1, 2, 2]} spacing="5">
            <Box
              onClick={() => {
                setSelectedImage(screen?.image);
                setImageShow(true);
              }}
            >
              <Image
                src={screen?.image}
                alt="screen image"
                width="100%"
                height="340px"
                borderRadius="8px"
              />
            </Box>
            <SimpleGrid columns={[1, 1, 2]} spacing="5">
              {dataSkeleton?.map((image: any, index: any) => (
                <Box
                  key={index}
                  onClick={() => {
                    setSelectedImage(screen?.image);
                    setImageShow(true);
                  }}
                >
                  <Image
                    src={screen?.image}
                    alt="screen image"
                    width="100%"
                    height="160px"
                    borderRadius="8px"
                  />
                </Box>
              ))}
            </SimpleGrid>
          </SimpleGrid>
        ) : (
          <SimpleGrid columns={[1, 2, 2]} spacing="5">
            <Box
              onClick={() => {
                setSelectedImage(`https://ipfs.io/ipfs/${screen?.images[0]}`);
                setImageShow(true);
              }}
            >
              <MediaContainer
                cid={screen?.images[0]}
                width="100%"
                height="340px"
                autoPlay="false"
              />
            </Box>
            <SimpleGrid columns={[1, 1, 2]} spacing="5">
              {screen?.images?.map((cid: any, index: any) => {
                if (index !== 0) {
                  return (
                    <Box
                      onClick={() => {
                        setSelectedImage(`https://ipfs.io/ipfs/${cid}`);
                        setImageShow(true);
                      }}
                      key={index}
                    >
                      <MediaContainer
                        cid={cid}
                        key={index}
                        width="100%"
                        height="160px"
                        autoPlay="false"
                      />
                    </Box>
                  );
                } else return null;
              })}
            </SimpleGrid>
          </SimpleGrid>
        )}
      </Hide>
      {/* Mobile view of screen image */}
      <Show below="md">
        {loadingScreen ? (
          <Skeleton>
            <Box width="100%" height="193px"></Box>
          </Skeleton>
        ) : screen?.images?.length === 0 ? (
          <Image
            onClick={() => {
              setSelectedImage(screen?.image);
              setImageShow(true);
            }}
            src={screen?.image}
            alt="screen image"
            width="100%"
            height="193px"
            borderRadius="8px"
          />
        ) : (
          <Carousel autoplay>
            {screen?.images?.map((cid: any, index: any) => (
              <Box
                key={index}
                onClick={() => {
                  setSelectedImage(`https://ipfs.io/ipfs/${cid}`);
                  setImageShow(true);
                }}
              >
                <MediaContainer
                  cid={cid}
                  key={index}
                  width="100%"
                  height="193px"
                  autoPlay="false"
                />
              </Box>
            ))}
          </Carousel>
        )}
      </Show>

      <Text
        color="#181818"
        fontSize={{ base: "2xl", lg: "3xl" }}
        fontWeight="semibold"
        m="0"
        pt="5"
      >
        {screen?.name}
      </Text>
      <Flex gap="16">
        <Box
          width={{ base: "100%", lg: "70%" }}
          justifyContent="start"
          pt={{ base: "2" }}
        >
          <LeftHandSide
            screen={screen}
            jsonData={jsonData}
            geometry={geometry}
          />
          <Show below="md">
            <Stack pt="2">
              {handelCheckUser() ? (
                <Button
                  color="#ffffff"
                  bgColor="#D7380E"
                  fontSize="md"
                  width="100%"
                  py="3"
                  onClick={() => setOpenCreataCampaignPop(true)}
                >
                  Create campaign
                </Button>
              ) : (
                <Button
                  color="#ffffff"
                  bgColor="#D7380E"
                  fontSize="md"
                  width="100%"
                  py="3"
                  onClick={handelPleaRequest}
                >
                  Send plea request
                </Button>
              )}
            </Stack>
          </Show>
        </Box>
        <Hide below="md">
          <Box
            width="30%"
            // height="736px"
            borderRadius="16px"
            boxShadow="2xl"
            p="5"
            pb="10"
          >
            <CreateCampaign
              screen={screen}
              createCampaign={() => handleOpenCreateCampaignName(true)}
              setNumberOfSlots={(value: any) => setNumberOfSlots(value)}
              setStartdateTime={(value: any) => setStartdateTime(value)}
              setEndDateTime={(value: any) => setEndDateTime(value)}
            />
          </Box>
        </Hide>
      </Flex>

      <Text
        color="#403F49"
        fontSize={{ base: "xl", lg: "3xl" }}
        align="left"
        fontWeight="semibold"
        pt="10"
      >
        Brands playing on screens {`(${videosList?.length})`}
      </Text>
      {videosList?.length === 0 ? (
        <Text
          color="#D7380E"
          fontSize="lg"
          align="left"
          fontWeight="semibold"
          pt="5"
        >
          No campaign is running on this screen now
        </Text>
      ) : (
        <div className="scrollmenu">
          {videosList &&
            videosList
              .slice(0, numberOfCampaignShow)
              .map((video: any, index: any) => (
                <AdvertiseBox video={video} key={index} />
              ))}
        </div>
      )}
      <Flex align="center" justifyContent="center">
        {videosList?.length > 3 && numberOfCampaignShow === 3 ? (
          <Button
            width="250px"
            p="3"
            variant="outline"
            borderColor="black"
            color="#D7380E"
            fontSize="xl"
            fontWeight="semibold"
            mt="20"
            onClick={() => setNumberOfCampaignShow(videosList?.length)}
          >
            See All
          </Button>
        ) : null}
        {numberOfCampaignShow !== 3 ? (
          <Button
            width="250px"
            p="3"
            variant="outline"
            borderColor="black"
            color="#D7380E"
            fontSize="xl"
            fontWeight="semibold"
            mt="20"
            onClick={() => setNumberOfCampaignShow(3)}
          >
            See Less
          </Button>
        ) : null}
      </Flex>
      <Box width="100%" pt="10">
        <ScreenReview screen={screen} />
      </Box>
    </Box>
  );
}
