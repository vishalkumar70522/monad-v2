import {
  Box,
  Text,
  Flex,
  HStack,
  VStack,
  Stack,
  Badge,
  SimpleGrid,
} from "@chakra-ui/react";
import { AiFillStar } from "react-icons/ai";
import { MdPhotoSizeSelectSmall } from "react-icons/md";
// import { beach, military, plain } from "../../assets/svg";
// import { MyMap } from "../MyMap";
import { Skeleton } from "antd";
export function LeftHandSide(props: any) {
  const { screen, jsonData } = props;
  return (
    <Box>
      {!screen ? (
        <Skeleton active paragraph={{ rows: 10 }} />
      ) : (
        <>
          <SimpleGrid columns={[1, 2, 3]}>
            <HStack spacing="-5" pr="10">
              <Flex align="center" bgColor="#00EB7A" p="2" m="0">
                <Text
                  pr="2"
                  color="#403F49"
                  fontSize="md"
                  align="left"
                  m="0"
                  justifyContent="center"
                >
                  {screen?.rating}
                </Text>
                <AiFillStar size="16px" color="#403F49" />
              </Flex>
              <Flex align="center" bgColor="#F1F1F1" p="3" m="0">
                <Text
                  pr="2"
                  color="#000000"
                  fontSize="12px"
                  align="left"
                  m="0"
                  justifyContent="center"
                >
                  5 ratings
                </Text>
              </Flex>
            </HStack>
            <Text
              color="#676767"
              fontSize={{ base: "12px", lg: "md" }}
              align="left"
              m="0"
              py="2"
              justifyContent="center"
            >
              {`${screen?.screenAddress}, ${screen?.districtCity}, ${screen?.stateUT}`}
            </Text>
            <Text
              color="#333333"
              fontSize="md"
              align="left"
              m="0"
              justifyContent="center"
            >
              Available slots: 2143
            </Text>
          </SimpleGrid>
          <Flex
            bgColor="#F0F0F0"
            borderRadius="4px"
            py="8px"
            px="16px"
            gap={5}
            width={{ base: "200px", lg: "200px" }}
            mt="5"
          >
            <VStack align="left">
              <Text fontSize="11px" color="#7D7D7D" m="0" fontWeight="400">
                Slot price
              </Text>
              <Flex gap="2">
                <Text fontSize="md" color="#222222" m="0" fontWeight="600">
                  ₹{screen?.rentPerSlot}
                </Text>
                <Text fontSize="15px" color="#222222" m="0" fontWeight="400">
                  slot
                </Text>
              </Flex>
            </VStack>
            <VStack align="left">
              <Text fontSize="11px" color="#7D7D7D" m="0" fontWeight="400">
                Slot time
              </Text>
              <Flex gap="2">
                <Text fontSize="md" color="#222222" m="0" fontWeight="600">
                  {screen?.slotsTimePeriod} sec
                </Text>
                <Text fontSize="15px" color="#222222" m="0" fontWeight="400">
                  slot
                </Text>
              </Flex>
            </VStack>
          </Flex>
          <Flex pt="5" gap="5">
            <Text fontSize="md" color="#403F49" pr="2">
              Screen size
            </Text>
            <MdPhotoSizeSelectSmall size="20px" color="#3D3D3D" />
            <Text fontSize="md" color="#403F49" pl="2">
              32 inches
            </Text>
          </Flex>
          <Stack pt="5" fontWeight="bold">
            <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
              Average daily footfall:{"    "}
              {screen?.additionalData?.averageDailyFootfall}
            </Text>
            <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
              Regular Audience Percentage : {"  "}{" "}
              {
                screen?.additionalData?.footfallClassification
                  ?.regularAudiencePercentage
              }
              {" %"}
            </Text>
            <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
              Sex ration (male : female) : {"     "}
              {
                screen?.additionalData?.footfallClassification?.sexRatio?.male
              } :{" "}
              {screen?.additionalData?.footfallClassification?.sexRatio?.female}
            </Text>
            <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
              Average purchase power (Start value - End value) : {"  ₹     "}
              {
                screen?.additionalData?.footfallClassification
                  ?.averagePurchasePower?.start
              }
              {"  "}-{"  ₹    "}
              {
                screen?.additionalData?.footfallClassification
                  ?.averagePurchasePower?.end
              }
            </Text>
            <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
              average Age Group (Start age - End age) : {"    "}
              {
                screen?.additionalData?.footfallClassification?.averageAgeGroup
                  ?.averageStartAge
              }
              {" years  "}-{"   "}
              {
                screen?.additionalData?.footfallClassification?.averageAgeGroup
                  ?.averageEndAge
              }
              {" years"}
            </Text>
            <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
              Employment status:{"    "}
              {screen?.additionalData?.footfallClassification?.employmentStatus?.map(
                (status: any, index: any) => (
                  <Badge colorScheme="green" ml="2" key={index}>
                    {status}
                  </Badge>
                )
              )}
            </Text>
            <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
              marital Status:{"    "}
              {screen?.additionalData?.footfallClassification?.maritalStatus?.map(
                (status: any, index: any) => (
                  <Badge colorScheme="cyan" ml="2" key={index}>
                    {status}
                  </Badge>
                )
              )}
            </Text>
            <Text color="#4B4B4B" fontSize="sm" align="left" m="0">
              Crowd mobility:{"    "}
              {screen?.additionalData?.footfallClassification?.crowdMobilityType?.map(
                (status: any, index: any) => (
                  <Badge colorScheme="green" ml="2" key={index}>
                    {status}
                  </Badge>
                )
              )}
            </Text>
          </Stack>
          {/* <Text
            fontSize={{ base: "md", lg: "3xl" }}
            color="#403F49"
            pt="10"
            fontWeight="bold"
          >
            Location highlights
          </Text>

          <Flex
            gap="10"
            border="1px"
            borderRadius="8px"
            borderColor="#9B9B9B"
            p="5"
          >
            <VStack>
              <Center
                border="1px"
                borderRadius="100%"
                borderColor="#403F49"
                p="3"
              >
                <Image src={plain} alt="plain" />
              </Center>
              <Text fontSize="13px" pt="3" color="#515151" align="center" m="0">
                Airport
              </Text>
            </VStack>
            <VStack>
              <Center
                border="1px"
                borderRadius="100%"
                borderColor="#403F49"
                p="3"
              >
                <Image src={beach} alt="beach" />
              </Center>
              <Text fontSize="13px" pt="3" color="#515151" align="center" m="0">
                Beach
              </Text>
            </VStack>
            <VStack>
              <Center
                border="1px"
                borderRadius="100%"
                borderColor="#403F49"
                p="3"
              >
                <Image src={military} alt="military" />
              </Center>
              <Text fontSize="13px" pt="3" color="#515151" align="center" m="0">
                Military
              </Text>
            </VStack>
          </Flex> */}

          {/* <Text
            fontSize={{ base: "md", lg: "3xl" }}
            color="#403F49"
            pt="10"
            fontWeight="bold"
          >
            Map view
          </Text>
          {props?.jsonData != undefined ? (
            <Box
              width={{ lg: "748px", base: "377px" }}
              height="397px"
              borderRadius="16px"
            >
              <MyMap
                data={props?.jsonData}
                geometry={props?.geometry}
                zoom="10"
              />
            </Box>
          ) : null} */}
        </>
      )}
    </Box>
  );
}
