import { Box, Button, Flex, Stack, Text, Textarea } from "@chakra-ui/react";
import { GiRoundStar } from "react-icons/gi";
import { Review } from "../../components/commans";
import { useEffect, useState } from "react";
import { Progress, message } from "antd";
import { useDispatch } from "react-redux";
import { createReview } from "../../Actions/screenActions";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export function ScreenReview(props: any) {
  const { screen } = props;
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [userScreenRating, setUserScreenRating] = useState<any>(0);
  const [userReview, setUserReview] = useState<any>("");

  const countEachRating = {
    5: 0,
    4: 0,
    3: 0,
    2: 0,
    1: 0,
  };
  if (screen) {
    screen?.reviews?.forEach((review: any) => {
      if (review.rating === 5) {
        countEachRating["5"] += 1;
      } else if (review.rating === 4) {
        countEachRating["4"] += 1;
      } else if (review.rating === 3) {
        countEachRating["3"] += 1;
      } else if (review.rating === 2) {
        countEachRating["2"] += 1;
      } else {
        countEachRating["1"] += 1;
      }
    });
  }

  const userSignin = useSelector((state: any) => state.userSignin);

  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  const screenReviewCreate = useSelector(
    (state: any) => state.screenReviewCreate
  );
  const {
    loading: loadingCommnet,
    success: screenReviewCreateSuccess,
    error: errorComment,
    review,
  } = screenReviewCreate;
  const handlePost = () => {
    //console.log("handlepost called!");
    if (userInfo) {
      if (userScreenRating > 0 && userReview.length > 10) {
        dispatch(
          createReview(screen?._id, {
            rating: userScreenRating,
            comment: userReview,
          })
        );
        setUserScreenRating(0);
        setUserReview("");
      } else {
        message.warning(
          "Please add your review and give start according to you"
        );
      }
    } else {
      navigate("/signin");
    }
  };
  useEffect(() => {
    if (screenReviewCreateSuccess) {
      message.success("Review Added Successfully");
    } else if (errorComment) {
      message.error(errorComment);
    }
  }, [screenReviewCreateSuccess, errorComment]);
  return (
    <Box>
      <Stack>
        <Text
          fontSize={{ base: "md", lg: "3xl" }}
          color="#403F49"
          align="left"
          fontWeight="655"
        >
          Review
        </Text>
        <Box
          width={{ base: "240px", lg: "25%" }}
          height={{ base: "173px", lg: "173px" }}
          bgColor="#F0F4FC"
          borderRadius="16px"
          p={{ base: "5", lg: "5" }}
        >
          <Flex gap={{ base: "2", lg: "5" }}>
            <Stack>
              <Flex align="center">
                <Text
                  fontSize={{ base: "2xl", lg: "4xl" }}
                  color="#403F49"
                  fontWeight="bold"
                  m="0"
                >
                  {screen?.rating}/
                </Text>
                <Text
                  fontSize={{ base: "sm", lg: "lg" }}
                  color="#403F49"
                  fontWeight="bold"
                  m="0"
                >
                  5
                </Text>
              </Flex>
              <Text
                fontSize={{ base: "12px", lg: "sm" }}
                color="#403F49"
                fontWeight="semibold"
                align="left"
                m="0"
              >
                {`Based on ${screen?.numReviews} reviews`}
              </Text>
              <Flex gap={1}>
                <GiRoundStar
                  size="20px"
                  color={screen?.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                />
                <GiRoundStar
                  size="20px"
                  color={screen?.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                />
                <GiRoundStar
                  size="20px"
                  color={screen?.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                />
                <GiRoundStar
                  size="20px"
                  color={screen?.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                />
                <GiRoundStar
                  size="20px"
                  color={screen?.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                />
              </Flex>
            </Stack>
            <Flex direction="column">
              <Flex align="center">
                <Text
                  fontSize={{ base: "9px", lg: "sm" }}
                  color="#403F49"
                  m="0"
                >
                  5
                </Text>
                <Progress
                  percent={(100 * countEachRating["5"]) / screen?.numReviews}
                  showInfo={false}
                />
              </Flex>
              <Flex align="center">
                <Text
                  fontSize={{ base: "9px", lg: "sm" }}
                  color="#403F49"
                  m="0"
                >
                  4
                </Text>
                <Progress
                  percent={(100 * countEachRating["4"]) / screen?.numReviews}
                  showInfo={false}
                />
              </Flex>
              <Flex align="center">
                <Text
                  fontSize={{ base: "9px", lg: "sm" }}
                  color="#403F49"
                  m="0"
                >
                  3
                </Text>
                <Progress
                  percent={(100 * countEachRating["3"]) / screen?.numReviews}
                  showInfo={false}
                />
              </Flex>
              <Flex align="center">
                <Text
                  fontSize={{ base: "9px", lg: "sm" }}
                  color="#403F49"
                  m="0"
                >
                  2
                </Text>
                <Progress
                  percent={(100 * countEachRating["2"]) / screen?.numReviews}
                  showInfo={false}
                />
              </Flex>
              <Flex align="center">
                <Text
                  fontSize={{ base: "9px", lg: "sm" }}
                  color="#403F49"
                  m="0"
                >
                  1
                </Text>
                <Progress
                  percent={(100 * countEachRating["1"]) / screen?.numReviews}
                  showInfo={false}
                />
              </Flex>
            </Flex>
          </Flex>
        </Box>
        <Text
          fontSize="2xl"
          color="#403F49"
          fontWeight="semibold"
          align="left"
          pt="5"
          m="0"
        >
          Rate your screen
        </Text>
        <Flex py="2">
          <GiRoundStar
            size="30px"
            color={userScreenRating >= 1 ? "#0EBCF5" : "#E2E2E2"}
            onClick={() => setUserScreenRating(1)}
          />
          <GiRoundStar
            size="30px"
            color={userScreenRating >= 2 ? "#0EBCF5" : "#E2E2E2"}
            onClick={() => setUserScreenRating(2)}
          />
          <GiRoundStar
            size="30px"
            color={userScreenRating >= 3 ? "#0EBCF5" : "#E2E2E2"}
            onClick={() => setUserScreenRating(3)}
          />
          <GiRoundStar
            size="30px"
            color={userScreenRating >= 4 ? "#0EBCF5" : "#E2E2E2"}
            onClick={() => setUserScreenRating(4)}
          />
          <GiRoundStar
            size="30px"
            color={userScreenRating >= 5 ? "#0EBCF5" : "#E2E2E2"}
            onClick={() => setUserScreenRating(5)}
          />
        </Flex>
        <Text
          fontSize={{ base: "sm", lg: "xl" }}
          color="#403F49"
          fontWeight="semibold"
          align="left"
          m="0"
        >
          Write a review
        </Text>
        <Textarea
          placeholder="Enter your review here"
          width={{ base: "100%", lg: "30%" }}
          size="md"
          color="#000000"
          value={userReview}
          onChange={(e) => setUserReview(e.target.value)}
        />
        <Stack pt="5">
          <Button
            width="197px"
            py="3"
            bgColor="#D7380E"
            color="#FFFFFF"
            onClick={handlePost}
          >
            Post
          </Button>
        </Stack>

        <Stack pt="10" pb="5">
          {screen?.reviews?.length > 0
            ? screen?.reviews?.map((review: any, index: any) => (
                <Review review={review} key={index} />
              ))
            : null}
        </Stack>
      </Stack>
    </Box>
  );
}
