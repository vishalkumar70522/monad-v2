import {
  Box,
  Text,
  Flex,
  HStack,
  Stack,
  Button,
  Divider,
  Skeleton,
  SimpleGrid,
  InputGroup,
  FormControl,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { Slider, message } from "antd";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { applyScreenAllyPlea } from "../../Actions/screenActions";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { BsCalendar2Date } from "react-icons/bs";
import {
  IconButton as MiuiIconButton,
  InputAdornment,
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";

export function CreateCampaign(props: any) {
  const { screen } = props;
  // console.log("screen : ", JSON.stringify(screen));
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [numberOfSlots, setNumberOfSlots] = useState<number>(30);
  const [startDateTime, setStartdateTime] = useState<any>(null);
  const [endDateTime, setEndDateTime] = useState<any>(null);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const handleEndDate = (value: any) => {
    setEndDateTime(value);
  };
  const handleStartDate = (value: any) => {
    setStartdateTime(value);
  };

  const handleCreateCampaign = () => {
    // console.log(endDateTime);
    if (!startDateTime) message.error("Please enter start date");
    else if (!endDateTime) message.error("Please enter end date");
    else if (numberOfSlots === 0)
      message.error("Please select no. of slots > 0");
    else {
      props?.setNumberOfSlots(numberOfSlots);
      props?.setStartdateTime(startDateTime);
      props?.setEndDateTime(endDateTime);
      props?.createCampaign();
    }
  };

  const handelCheckUser = () => {
    if (!userInfo || userInfo?._id === screen?.master?._id) {
      return true;
    } else {
      // now check user have access or not
      const value = screen?.allies?.find((user: any) => user == userInfo?._id);
      if (value) {
        return true;
      }
      return false;
    }
  };

  //screenAllyPleaRequest

  const screenAllyPleaRequest = useSelector(
    (state: any) => state.screenAllyPleaRequest
  );
  const { success, error } = screenAllyPleaRequest;

  useEffect(() => {
    if (error) {
      message.error(error);
    } else if (success) {
      message.success(
        `Plea has been sent to the screen owner. \n It will take some time to approve your request. \nAfter that you can create campaign on this screan`
      );
    }
  }, [success, error]);

  const handelPleaRequest = () => {
    if (!userInfo) {
      message.error("please sign in");
      navigate("/signin", {
        state: {
          path: `/screenDetails/${screen?._id}`,
        },
      });
    } else {
      dispatch(applyScreenAllyPlea(screen?._id));
    }
  };

  return (
    <Box>
      {!screen ? (
        <Skeleton>
          <Box width="30%" height="736px"></Box>
        </Skeleton>
      ) : (
        <>
          <HStack justifyContent="space-between">
            <Flex align="baseline" gap={1}>
              <Text
                color="#222222"
                fontSize={{ base: "16px", lg: "xl" }}
                align="left"
              >
                {`₹${screen?.rentPerSlot}/`}
              </Text>
              <Text color="#222222" align="left" fontSize={{ base: "15px" }}>
                slot
              </Text>
            </Flex>
            <Stack pr="5">
              <Text
                fontSize={{ base: "11px", lg: "md" }}
                color="#000000"
                align="left"
                px="5"
                py="2"
                bg="#FFD700"
                borderRadius="8px"
              >
                {`${screen?.slotsTimePeriod} sec / slot`}
              </Text>
            </Stack>
          </HStack>
          <Divider color="#DDDDDD" />
          <Text
            align="center"
            color="#222222"
            fontSize="xl"
            fontWeight="semibold"
          >
            Calculate budget
          </Text>
          <SimpleGrid columns={[1, 1, 1]} spacing={{ base: "5", lg: "2" }}>
            <InputGroup width="100%">
              <FormControl id="startDateHere" width="100%">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    inputVariant="outlined"
                    placeholder="Enter start date"
                    value={startDateTime}
                    onChange={handleStartDate}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <MiuiIconButton>
                            <BsCalendar2Date />
                          </MiuiIconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </MuiPickersUtilsProvider>
              </FormControl>
            </InputGroup>
            <InputGroup width="100%">
              <FormControl id="startDateHere">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    placeholder="Enter end date"
                    inputVariant="outlined"
                    value={endDateTime}
                    onChange={handleEndDate}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <MiuiIconButton>
                            <BsCalendar2Date />
                          </MiuiIconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </MuiPickersUtilsProvider>
              </FormControl>
            </InputGroup>
          </SimpleGrid>
          <Text align="left" color="#222222" fontSize="md" pt="5">
            Total number of slots
          </Text>

          <HStack>
            <Stack width="80%">
              <Slider
                defaultValue={numberOfSlots}
                trackStyle={{ height: "10px", borderRadius: "8px" }}
                onChange={(value: any) => setNumberOfSlots(value)}
              />
            </Stack>
            <Text
              color="#000000"
              fontSize="md"
              border="1px"
              borderRadius="8px"
              py="2"
              px="10"
              borderColor="#DDDDDD"
            >
              {numberOfSlots}
            </Text>
          </HStack>
          {handelCheckUser() ? (
            <Button
              color="#ffffff"
              bgColor="#D7380E"
              fontSize="md"
              width="100%"
              py="3"
              onClick={handleCreateCampaign}
            >
              Create campaign
            </Button>
          ) : (
            <Button
              color="#ffffff"
              bgColor="#D7380E"
              fontSize="md"
              width="100%"
              py="3"
              onClick={handelPleaRequest}
            >
              Send plea request
            </Button>
          )}
          <HStack
            justifyContent="space-between"
            color="#222222"
            fontSize="md"
            pt="5"
          >
            <Text align="left">{`₹${screen?.rentPerSlot} x ${numberOfSlots} slots`}</Text>
            <Text align="left">{`₹${
              screen?.rentPerSlot * numberOfSlots
            }`}</Text>
          </HStack>
          <HStack justifyContent="space-between" color="#222222" fontSize="md">
            <Text align="left" m="0">
              GST 5 %
            </Text>
            <Text align="left" m="0">{`₹${
              (screen?.rentPerSlot * numberOfSlots * 5) / 100
            }`}</Text>
          </HStack>
          <Divider color="#DDDDDD" />
          <HStack
            justifyContent="space-between"
            color="#222222"
            fontSize="md"
            fontWeight="semibold"
          >
            <Text align="left" m="0">
              Total
            </Text>
            <Text align="left" m="0">{`₹${
              screen?.rentPerSlot * numberOfSlots +
              (screen?.rentPerSlot * numberOfSlots * 5) / 100
            }`}</Text>
          </HStack>
        </>
      )}
    </Box>
  );
}
