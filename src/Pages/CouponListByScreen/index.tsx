import {
  Box,
  Button,
  Center,
  Flex,
  Image,
  Stack,
  Text,
  Tooltip,
} from "@chakra-ui/react";
import "./index.css";
import { useEffect, useState } from "react";
import { BiArrowBack } from "react-icons/bi";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Skeleton, message } from "antd";
import { addUserToCoupon } from "../../Actions/couponAction";
import { useSelector } from "react-redux";
import { ADD_USER_TO_COUPON_RESET } from "../../Constants/couponConstants";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { BsInfoCircle } from "react-icons/bs";
import {
  detailsScreen,
  getCouponListByScreenId,
} from "../../Actions/screenActions";
import { sendScanDataInLogs } from "../../Actions/qrcodeAction";
import { SCREEN_COUPONS_RESET } from "../../Constants/screenConstants";
import { ResultNotFound } from "../../components/commans";
// import { AiOutlineLeft, AiOutlineRight } from "react-icons/ai";

export function CouponListByScreen(props: any) {
  // const screenId = props.screenId;
  const screenId = window.location.pathname.split("/")[3];
  // const location = useLocation();

  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [index, setIndex] = useState<any>(0);
  const [showDetails, setShowDetails] = useState<any>(-1);
  const [userLat, setUserLat] = useState<any>();
  const [userLng, setUserLng] = useState<any>();

  const redeemCoupon = useSelector((state: any) => state.redeemCoupon);
  const {
    loading: redeemCouponLoading,
    error: redeemCouponError,
    success,
  } = redeemCoupon;

  // console.log(coupons);
  // console.log(screenId);
  const screenDetails = useSelector((state: any) => state.screenDetails);
  const { loading: loadingScreen, error: errorScreen, screen } = screenDetails;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const screenCoupons = useSelector((state: any) => state.screenCoupons);
  const {
    loading: loadingCouponList,
    error: errorCouponList,
    coupons,
  } = screenCoupons;

  useEffect(() => {
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
    };
    function successGeo(pos: any) {
      var crd = pos.coords;
      setUserLat(crd.latitude);
      setUserLng(crd.longitude);
      // console.log("Your current position is:");
      // console.log(`Latitude : ${crd.latitude}`);
      // console.log(`Longitude: ${crd.longitude}`);
      // console.log(`More or less ${crd.accuracy} meters.`);
    }

    function errors(err: any) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    }

    if (navigator.geolocation) {
      navigator.permissions
        .query({ name: "geolocation" })
        .then(function (result) {
          // console.log(result);
          if (result.state === "granted") {
            //
            navigator.geolocation.getCurrentPosition(
              successGeo,
              errors,
              options
            );
          } else if (result.state === "prompt") {
            //
            navigator.geolocation.getCurrentPosition(
              successGeo,
              errors,
              options
            );
          } else if (result.state === "denied") {
            //
          }
        });
    } else {
      console.log("not possible");
    }
    // console.log(userInfo ? userInfo._id : null);
    // console.log(document.referrer);
    if (document.referrer !== "") {
      dispatch(
        sendScanDataInLogs({
          scanResult: {
            text: window.location.href,
            timestamp: new Date(),
            userLat: userLat,
            userLng: userLng,
          },
          // scanUser: userInfo?._id || null,
          scanUser: userInfo?.email || null,
          screenId: screenId,
        })
      );
    }
  }, []);
  useEffect(() => {
    if (errorCouponList) {
      message.error(errorCouponList);
      dispatch({ type: SCREEN_COUPONS_RESET });
    }
    if (success) {
      message.success("This coupon added in coupon list");
      dispatch({ type: ADD_USER_TO_COUPON_RESET });
    }
    if (redeemCouponError) {
      message.warning(redeemCouponError);
      dispatch({ type: ADD_USER_TO_COUPON_RESET });
    }
    dispatch(detailsScreen(screenId));
    dispatch(getCouponListByScreenId(screenId));
  }, [
    redeemCouponError,
    success,
    navigate,
    dispatch,
    screenId,
    errorCouponList,
    index,
  ]);

  const handelClaim = () => {
    if (!userInfo) {
      navigate("/signin", { state: { path: window.location.pathname } });
    } else {
      dispatch(
        addUserToCoupon({
          couponId: coupons?.filter((coup: any) => {
            if (
              !coup?.rewardCoupons
                ?.map((cp: any) => cp.redeemer)
                .includes(userInfo?._id)
            ) {
              return coup;
            }
          })[index]?._id,
          screenId: screenId,
        })
      );
      dispatch(getCouponListByScreenId(screenId));
    }
  };
  return (
    <Box px={{ base: "", lg: "" }} py={{ base: "", lg: "" }}>
      {coupons?.length !== 0 ? (
        <Stack className="top-circle">
          {index <
          coupons?.filter((coup: any) => {
            if (
              !coup?.rewardCoupons
                ?.map((cp: any) => cp.redeemer)
                .includes(userInfo?._id)
            ) {
              return coup;
            } else {
              return null;
            }
          })?.length ? (
            <Stack
              pt={{ base: "75", lg: "100" }}
              px="2"
              alignItems="center"
              justifyContent="center"
              alignContent="center"
            >
              <Box alignItems="center">
                <Text
                  className="text"
                  color="#D30B0B"
                  fontSize="28px"
                  fontWeight="1000"
                  // align="center"
                >
                  CONGRATULATIONS!
                </Text>
                {/* <text>Claim your favorite offers</text> */}
                <Text
                  mt="0"
                  color="#000000"
                  fontSize="lg"
                  fontWeight="700"
                  align="center"
                >
                  YOU WON
                </Text>
              </Box>
              {coupons?.filter((coup: any) => {
                if (
                  !coup?.rewardCoupons
                    ?.map((cp: any) => cp.redeemer)
                    .includes(userInfo?._id)
                ) {
                  return coup;
                } else {
                  return null;
                }
              }).length === 0 ? (
                <>
                  <Text
                    align="center"
                    color="#161616"
                    fontSize="md"
                    fontWeight="530"
                    pt="3"
                    onClick={() => {
                      setIndex(
                        coupons?.filter((coup: any) => {
                          if (
                            !coup?.rewardCoupons
                              ?.map((cp: any) => cp.redeemer)
                              .includes(userInfo?._id)
                          ) {
                            return coup;
                          } else {
                            return null;
                          }
                        })?.length + 1
                      );
                    }}
                  >
                    Next
                  </Text>
                </>
              ) : (
                <>
                  <Flex>
                    {/* <Stack justify="center" pr="10">
                      <AiOutlineLeft
                        color="#00000070"
                        fontSize="20px"
                        // onClick={() => setIndex(index - 1)}
                      />
                    </Stack> */}
                    {showDetails === index ? (
                      <Stack onClick={() => setShowDetails(-1)}>
                        <DetailsPopup
                          userInfo={userInfo}
                          coupons={coupons?.filter((coup: any) => {
                            if (
                              !coup?.rewardCoupons
                                ?.map((cp: any) => cp.redeemer)
                                .includes(userInfo?._id)
                            ) {
                              return coup;
                            }
                          })}
                          index={index}
                          // setShowDetails={() => setShowDetails}
                        />
                      </Stack>
                    ) : (
                      <Stack onClick={() => setShowDetails(index)}>
                        <ImagePopup
                          coupons={coupons?.filter((coup: any) => {
                            if (
                              !coup?.rewardCoupons
                                ?.map((cp: any) => cp.redeemer)
                                .includes(userInfo?._id)
                            ) {
                              return coup;
                            }
                          })}
                          index={index}
                        />
                      </Stack>
                    )}
                    {/* <Stack justify="center" pl="10">
                      <AiOutlineRight
                        color="#00000070"
                        fontSize="20px"
                        // onClick={() => setIndex(index + 1)}
                      />
                    </Stack> */}
                  </Flex>

                  <Text
                    align="center"
                    color="#4E4E4E"
                    fontSize="md"
                    fontWeight="656"
                    py="2"
                    m="0"
                  >{`${index + 1}/${
                    coupons?.filter((coup: any) => {
                      if (
                        !coup?.rewardCoupons
                          ?.map((cp: any) => cp.redeemer)
                          .includes(userInfo?._id)
                      ) {
                        return coup;
                      }
                    })?.length
                  }`}</Text>
                  <Button
                    borderRadius="57px"
                    color="#FFFFFF"
                    bgColor="#2F9400"
                    py="5"
                    mx="5"
                    fontWeight="630"
                    fontSize="md"
                    width="100%"
                    isLoading={redeemCouponLoading}
                    loadingText="Adding coupon in your bucket"
                    onClick={handelClaim}
                  >
                    Collect
                  </Button>
                  <Text
                    align="center"
                    color="#161616"
                    fontSize="md"
                    fontWeight="530"
                    pt="3"
                    onClick={() => {
                      setIndex(index + 1);
                    }}
                  >
                    Next
                  </Text>
                </>
              )}
            </Stack>
          ) : (
            <Box
              pt={{ base: "75", lg: "100" }}
              px="2"
              alignItems="center"
              justifyContent="center"
              alignContent="center"
            >
              <Text
                // className="text"
                color="#D30B0B"
                fontSize="28px"
                fontWeight="700"
                align="center"
                // width="352px"
              >
                CONGRATULATIONS!
              </Text>
              {coupons && (
                <Text
                  color="#000000"
                  fontSize="md"
                  fontWeight="700"
                  align="center"
                  // width="352px"
                >
                  {` YOU HAVE CLAIMED ${
                    coupons?.filter((coup: any) => {
                      if (
                        coup?.rewardCoupons
                          ?.map((cp: any) => cp.redeemer)
                          .includes(userInfo?._id)
                      ) {
                        return coup;
                      }
                    })?.length
                  } of ${coupons?.length || 0} COUPONS`}
                </Text>
              )}

              {loadingScreen ? (
                <Skeleton />
              ) : (
                <Stack>
                  <Text
                    color="#00AE46"
                    fontSize="lg"
                    fontWeight="700"
                    align="center"
                    // width="352px"
                  >
                    {screen.name}, <br />
                    {screen.screenAddress}
                  </Text>
                </Stack>
              )}

              <div className="scrollmenu">
                {/* <index  */}
                {coupons?.filter((coup: any) => {
                  if (
                    coup?.rewardCoupons
                      ?.map((cp: any) => cp.redeemer)
                      .includes(userInfo?._id)
                  ) {
                    return coup;
                  }
                }).length > 0 &&
                  coupons
                    ?.filter((coup: any) => {
                      if (
                        coup?.rewardCoupons
                          ?.map((cp: any) => cp.redeemer)
                          .includes(userInfo?._id)
                      ) {
                        return coup;
                      }
                    })
                    ?.map((coupon: any, index: any) => {
                      if (showDetails === index)
                        return (
                          <Stack
                            align="center"
                            p="5"
                            key={index}
                            onClick={() => setShowDetails(-1)}
                          >
                            <DetailsPopup
                              userInfo={userInfo}
                              coupons={coupons?.filter((coup: any) => {
                                if (
                                  coup?.rewardCoupons
                                    ?.map((cp: any) => cp.redeemer)
                                    .includes(userInfo?._id)
                                ) {
                                  return coup;
                                }
                              })}
                              index={index}
                            />
                          </Stack>
                        );
                      else
                        return (
                          <Stack
                            align="center"
                            key={index}
                            p="5"
                            onClick={() => setShowDetails(index)}
                          >
                            <ImagePopup
                              coupons={coupons?.filter((coup: any) => {
                                if (
                                  coup?.rewardCoupons
                                    ?.map((cp: any) => cp.redeemer)
                                    .includes(userInfo?._id)
                                ) {
                                  return coup;
                                }
                              })}
                              index={index}
                            />
                          </Stack>
                        );
                    })}
              </div>
              <Stack width="100%">
                <Button
                  borderRadius="57px"
                  color="#FFFFFF"
                  bgColor="#2F9400"
                  py="5"
                  fontWeight="630"
                  fontSize="md"
                  width="100%"
                  onClick={() => {
                    setIndex(0);
                    navigate("/");
                  }}
                >
                  Done
                </Button>
                <Button
                  borderRadius="57px"
                  color="#FFFFFF"
                  bgColor="#2F9400"
                  py="5"
                  fontWeight="630"
                  fontSize="md"
                  width="100%"
                  onClick={() => setIndex(0)}
                >
                  See coupons again
                </Button>
              </Stack>
            </Box>
          )}
        </Stack>
      ) : (
        <ResultNotFound />
      )}
    </Box>
  );
}

const ImagePopup = ({ coupons, index }: any) => {
  return (
    <Box
      borderRadius="16px"
      border="1px"
      borderColor="#000000"
      bg="white"
      p="5"
      width="311px"
      height="475px"
      // className="circle-right"
    >
      <Box>
        <Image
          src={coupons[index]?.couponRewardInfo?.images[0]}
          alt="image"
          borderRadius="8px"
          height="261px"
          width="100%"
        />
      </Box>
      <Center pt="5" flexDirection="column">
        <Image src={coupons[index]?.brandLogo} alt="image" height="40px" />
        <Box height="90px" sx={{ "& > p": { noOfLines: 2 } }}>
          <Tooltip
            // width="400px"
            // isOpen={true}
            label={
              <Text align="center" width="300px">
                {coupons[index]?.offerName}
              </Text>
            }
          >
            <Text
              pt="5"
              color="#262626"
              fontSize="lg"
              fontWeight="700"
              width="242px"
              align="center"
              className="text"
              // isTruncated={true}
              noOfLines={2}
            >
              {coupons[index]?.offerName}
            </Text>
          </Tooltip>
        </Box>
        <Button
          color="#000000"
          leftIcon={<BsInfoCircle size="20px" color="#000000" />}
          fontWeight="600"
          fontSize="13px"
          variant="null"
          // onClick={() => setShowDetails(index)}
        >
          know more
        </Button>
      </Center>
    </Box>
  );
};

const DetailsPopup = ({ userInfo, coupons, index }: any) => {
  const navigate = useNavigate();
  return (
    <Box
      className=""
      width="311px"
      height="475px"
      borderRadius="16px"
      border="1px"
      borderColor="#000000"
      bgColor="#F7F7F7"
    >
      <Box height="65%" px="5" py="5">
        <Image src={coupons[index]?.brandLogo} alt="image" height="46px" />
        <Tooltip
          // width="400px"
          label={
            <Text align="center" width="300px">
              {coupons[index]?.offerName}
            </Text>
          }
        >
          <Text
            color="#000000"
            fontSize="lg"
            fontWeight="700"
            py="5"
            m="0"
            isTruncated={true}
            noOfLines={2}
          >
            {coupons[index]?.offerName}
          </Text>
        </Tooltip>
        <Tooltip
          width="500px"
          // height="500px"
          label={<Text align="center">{coupons[index]?.offerDetails}</Text>}
        >
          <Text
            className="text"
            color="#000000"
            fontSize="sm"
            // fontWeight="700"
            pb="5"
            m="0"
            isTruncated={true}
            noOfLines={5}
            height="40%"
          >
            {coupons[index]?.offerDetails}
          </Text>
        </Tooltip>
        {/* <Text color="#000000" fontSize="15px" fontWeight="400" m="0">
        Redeemable at all Funkfeets store at Delhi, Gurgoan, Noida
        outlets
      </Text> */}
        <Text
          className="text"
          color="#000000"
          fontSize="15px"
          fontWeight="400"
          py="5"
          m="0"
          onClick={() =>
            navigate("/couponDetils", {
              state: {
                data: coupons[index],
                path: "/",
                showCouponCode:
                  coupons[index]?.rewardCoupons.filter(
                    (cp: any) => cp.email === userInfo?.email
                  ).length !== 0,
              },
            })
          }
        >
          Click here for more details
        </Text>
        {/* <Text color="#000000" fontSize="15px" fontWeight="400" m="0" pb="2">
          No cash value.
        </Text> */}
      </Box>
      <Button
        color="#F5F5F5"
        bgColor={"#252525"}
        fontWeight="600"
        fontSize="24px"
        py="3"
        width="100%"
        borderRadius="0px"
        // onClick={() => setShowDetails(index)}
      >
        Code : {coupons[index]?.couponCode}
      </Button>
      <Text
        py="5"
        color="#727272"
        fontSize="12px"
        fontWeight={"521"}
        align="center"
        m="0"
      >
        Valid until{" "}
        {coupons[index]?.couponRewardInfo?.validity?.to
          ? convertIntoDateAndTime(
              coupons[index]?.couponRewardInfo?.validity?.to
            )
          : "Life time"}
      </Text>
      <Center pb="2">
        <BiArrowBack
          size="30px"
          color="#292929"
          // onClick={() => setShowDetails(-1)}
        />
      </Center>
    </Box>
  );
};
