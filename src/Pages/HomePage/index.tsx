import { Box, Button, SimpleGrid, Stack, Text } from "@chakra-ui/react";
import {
  AdvertiseBox,
  ContactUs,
  PopulerScreens,
  SearchWithCreateCampaign,
} from "../../components/commans";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCampaignList } from "../../Actions/campaignAction";
import { listScreens } from "../../Actions/screenActions";

import { useNavigate } from "react-router-dom";

import { Skeleton } from "antd";

export function HomePage(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [screensList, setScreensList] = useState<any>(null);
  const [jsonData, setJsonData] = useState<any>(null);

  //screen list
  const screenList = useSelector((state: any) => state.screenList);
  const { loading: loadingScreens, error: errorScreens, screens } = screenList;

  const filterScreenListByAudiance = useSelector(
    (state: any) => state.filterScreenListByAudiance
  );
  const {
    loading: loadingfilterScreenListByAudiance,
    error: errorfilterScreenListByAudiance,
    screens: filterScreens,
    filterOption,
  } = filterScreenListByAudiance;

  // campaign list
  const campaignListAll = useSelector((state: any) => state.campaignListAll);
  const {
    loading: loadingVideos,
    error: errorVideos,
    allCampaign,
  } = campaignListAll;
  // console.log("allCampaign : ", new Set(allCampaign));

  useEffect(() => {
    if (screens?.length > 0) {
      setScreensList(screens);
      const data = screens?.slice(0, 3)?.map((screen: any) => {
        return {
          type: "Feature",
          properties: {
            screen: screen?._id,
          },
          geometry: {
            coordinates: [screen?.lat, screen?.lng],
            type: "Point",
          },
        };
      });
      setJsonData({
        features: data,
      });
    }
  }, [screens]);

  useEffect(() => {
    if (filterScreens?.length > 0) {
      setScreensList(filterScreens);
      const data = filterScreens?.slice(0, 3)?.map((screen: any) => {
        return {
          type: "Feature",
          properties: {
            screen: screen?._id,
          },
          geometry: {
            coordinates: [screen?.lat, screen?.lng],
            type: "Point",
          },
        };
      });
      setJsonData({
        features: data,
      });
    }
  }, [filterScreens]);

  useEffect(() => {
    // console.log("userInfo : ", userInfo);

    dispatch(getCampaignList());
    dispatch(listScreens({}));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);
  return (
    <Box px={{ base: 2, lg: 20 }} pt={{ base: 75, lg: 100 }}>
      <SearchWithCreateCampaign />
      {loadingScreens || loadingfilterScreenListByAudiance ? (
        <Box pt="5">
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
        </Box>
      ) : loadingfilterScreenListByAudiance === false &&
        filterScreens?.length > 0 ? (
        <PopulerScreens
          screensList={filterScreens}
          jsonData={jsonData}
          filtered={true}
          filterOption={filterOption}
        />
      ) : loadingfilterScreenListByAudiance === false &&
        filterScreens?.length === 0 ? (
        <Box pt="10" justifyContent="center">
          {" "}
          <Text color="red" fontSize="lg" fontWeight="600">
            No result found!
          </Text>
          <Button
            variant="outline"
            color="#D7380E"
            fontSize={{ base: "10px", lg: "md" }}
            borderRadius={{ base: "lg", lg: "2xl" }}
            py="2"
            onClick={() => window.location.reload()}
          >
            Reset filter
          </Button>
        </Box>
      ) : (
        <PopulerScreens
          screensList={screensList}
          jsonData={jsonData}
          filtered={false}
        />
      )}

      <Stack pt="10">
        {loadingfilterScreenListByAudiance === false &&
        filterScreens?.length > 0 ? null : (
          <Button
            variant="outline"
            color="#D7380E"
            fontSize="md"
            py="3"
            px="10"
            width={{ base: "100px", lg: "200px" }}
            onClick={() => navigate("/allScreen")}
          >
            See all
          </Button>
        )}
        <Text
          color="#403F49"
          pt={{ base: "5", lg: "10" }}
          pb={{ base: "5", lg: "10" }}
          fontSize={{ base: "lg", lg: "3xl" }}
          fontWeight="bold"
          align="left"
        >
          Ads playing
        </Text>
        <SimpleGrid columns={[1, 3]} spacing={{ base: "4", lg: "10" }}>
          {allCampaign &&
            allCampaign
              ?.filter((camp: any) => camp.status !== "Deleted")
              ?.slice(0, 3)
              ?.map(() => {
                return allCampaign.splice(
                  Math.floor(Math.random() * allCampaign.length),
                  1
                )[0];
              }, allCampaign.slice())
              ?.map((campaign: any) => (
                <AdvertiseBox video={campaign} key={campaign?._id} />
              ))}
        </SimpleGrid>
        {/* contact us page */}
        <Stack pt="5">
          <ContactUs />
        </Stack>
      </Stack>
    </Box>
  );
}
