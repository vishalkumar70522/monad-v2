import { Box } from "@chakra-ui/react";
import { SearchScreen } from "../../components/commans/SearchScreen";

export function FilterScreenByLocationAndLocality(props: any) {
  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: "75", lg: "100" }}>
      <SearchScreen />
    </Box>
  );
}
