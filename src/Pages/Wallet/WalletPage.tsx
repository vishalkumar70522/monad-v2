import { Button, Form, Input, Modal } from "antd";
import { Box } from "@chakra-ui/react";
import React from "react";
import Axios from "axios";
import { Buffer } from "buffer";
import { SHA256 } from "crypto-js";
import { v4 as uuidv4 } from "uuid";

import { useSelector } from "react-redux";

export function WalletPage(props: any) {
  const [form] = Form.useForm();
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

  const handleOpenPhonePayGetway = async (values: any) => {
    console.log("paymentHandler : called!");
    const PROD_HOST_URL = `https://api.phonepe.com/apis/hermes/pg/v1/pay`;
    // const UAT_PAY_API_URL = `https://api-preprod.phonepe.com/apis/pg-sandbox/pg/v1/pay`;

    const amount = values.amount;
    const transactionId = uuidv4();
    console.log("transaction id : ", transactionId);
    const callbackUrl = `${process.env.REACT_APP_BLINDS_SERVER}/api/userWallet/callbackHandler?userId=${userInfo?._id}&amount=${amount}&transactionId=${transactionId}`;
    console.log(
      "amount, userId, callbackUrl : ",
      amount,
      userInfo?._id,
      callbackUrl
    );
    console.log("process.REACT_MERCHANT_ID : MONADONLINE");

    const payloadForFetch = {
      merchantId: "MONADONLINE",
      merchantTransactionId: transactionId,
      merchantUserId: "MUID123",
      amount: amount,
      redirectUrl: callbackUrl,
      redirectMode: "POST",
      callbackUrl: callbackUrl,
      mobileNumber: "9999999999",
      paymentInstrument: {
        type: "PAY_PAGE",
      },
    };
    let objJsonStr = JSON.stringify(payloadForFetch);
    console.log("payloadForFetch : ", objJsonStr);
    let payload = Buffer.from(objJsonStr).toString("base64");
    // console.log("payload : ", payloadForFetch);
    // console.log("payload : ", objJsonStr);
    console.log("payload : ", payload);
    const salt = "12d82564-34a0-4c7a-938b-24916449add4"; // process.env.REACT_PHONE_PAY_SALY;
    console.log("Salt : ", salt);
    const saltIndex = 1;

    /**
     * SHA256(base64 encoded payload + "/pg/v1/refund" + salt key) + ### + salt index
     */

    let url = SHA256(`${payload}/pg/v1/pay${salt}`).toString();
    url = url + "###" + saltIndex;
    console.log("X-VERIFY : ", url);

    const { data } = await Axios.post(
      PROD_HOST_URL,
      {
        request: payload, // body
      },
      {
        headers: {
          accept: "application/json",
          "Content-Type": "application/json",
          "X-VERIFY": url,
        },
      }
    );
    console.log("response data : ", data);
    if (data.success) {
      console.log(
        "response phonepay url : ",
        data?.data?.instrumentResponse?.redirectInfo?.url
      );
      window.open(data?.data?.instrumentResponse?.redirectInfo?.url);
    }
  };

  const openPhonePayPage = async (values: any) => {
    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/userWallet/paymentHandler?amount=${values.amount}&userId=${userInfo?._id}`
      );
      console.log("data : ", JSON.stringify(data));
      window.open(data.url);
    } catch (error) {
      console.log("Error : ", JSON.stringify(error));
    }
  };
  const onFinish = (values: any) => {
    console.log("values: ", values);
    openPhonePayPage(values);
  };
  return (
    <Modal
      title="Wallet recharge"
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      // width={1000}
      // width="auto"
      style={{ top: 5 }}
    >
      <Box p="10" bgColor="gray">
        <Form
          {...layout}
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          style={{ maxWidth: 600 }}
        >
          <Form.Item
            name="amount"
            label="Enter amount"
            rules={[{ required: true }]}
          >
            <Input type="number" placeholder="Enter amount" />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Box>
    </Modal>
  );
}
