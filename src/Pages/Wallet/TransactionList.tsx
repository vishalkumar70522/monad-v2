import { Box, Text } from "@chakra-ui/react";
import { Tag } from "antd";
import Table, { ColumnsType } from "antd/es/table";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";

export function TransactionList(props: any) {
  const columns: ColumnsType<any> = [
    {
      title: "Transaction Id",
      dataIndex: "txId",
      key: "txId",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Date",
      dataIndex: "txDate",
      key: "txDate",
      render: (text) => (
        <Text width="110px">{convertIntoDateAndTime(text)}</Text>
      ),
    },
    {
      title: "Status",
      dataIndex: "success",
      key: "success",
      render: (value) => <Tag color={value ? "green" : "red"}>{value}</Tag>,
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
      render: (value) => <Tag color={"green"}>{value}</Tag>,
    },
    {
      title: "Aditional info",
      dataIndex: "aditionalInfo",
      key: "aditionalInfo",
      render: (value) => (
        <Text _hover={{ color: "blue" }}>{JSON.stringify(value)}</Text>
      ),
    },
    {
      title: "Type",
      dataIndex: "txType",
      key: "txType",
      render: (value) => (
        <Tag color={value === "CRADIT" ? "green" : "red"}>
          {value ? value : "CRADIT"}
        </Tag>
      ),
    },
  ];

  return (
    <Box>
      <Table
        columns={columns}
        loading={props?.loading}
        dataSource={props?.data}
        pagination={{ pageSize: 10 }}
        scroll={{ x: "100%" }}
      />
    </Box>
  );
}
