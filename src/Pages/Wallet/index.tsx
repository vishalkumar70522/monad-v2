import { Box, Button, Flex, SimpleGrid, Text, Center } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { WalletPage } from "./WalletPage";
import { TransactionList } from "./TransactionList";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  createUserWallet,
  getUserWalletBalance,
  getUserWalletTransaction,
} from "../../Actions/walletAction";
import {
  CREATE_USER_WALLET_RESET,
  GET_TRANSACTION_LIST_RESET,
  GET_WALLET_BALANCE_RESET,
} from "../../Constants/walletConstants";
import { message } from "antd";
import { useNavigate } from "react-router-dom";

export function Wallet(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [openModel, setOpenModel] = useState<any>(false);

  const createWallet = useSelector((state: any) => state.createWallet);

  const {
    loading: loadingCretaeWallet,
    success: successCretaeWallet,
    error: errorCreateWallet,
  } = createWallet;

  const walletTransaction = useSelector(
    (state: any) => state.walletTransaction
  );

  const {
    loading: loadingWalletTrasaction,
    error: errroWalletTransaction,
    success: successWalletTransaction,
    transaction,
  } = walletTransaction;

  //   console.log("transaction : ", transaction);

  const walletBalance = useSelector((state: any) => state.walletBalance);

  const {
    loading: loadingWalletBalance,
    error: errorWalletBalance,
    success: successWalletBalance,
    wallet,
  } = walletBalance;

  //   console.log("wallet : ", wallet);
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const handleCreateWallet = () => {
    dispatch(createUserWallet());
  };

  useEffect(() => {
    if (!userInfo) {
      navigate("/signIn", { state: { path: "/wallet" } });
    }
    if (userInfo?.userWallet) {
      dispatch(getUserWalletBalance());
      dispatch(getUserWalletTransaction());
    }
  }, [userInfo, dispatch]);

  useEffect(() => {
    if (successCretaeWallet) {
      dispatch({ type: CREATE_USER_WALLET_RESET });
      dispatch(getUserWalletBalance());
      message.success("User wallet cretaed successfull");
    }
    if (errorCreateWallet) {
      dispatch({ type: CREATE_USER_WALLET_RESET });
      message.error(errorCreateWallet);
    }
    if (errorWalletBalance) {
      message.error(errorWalletBalance);
      dispatch({ type: GET_WALLET_BALANCE_RESET });
    }

    if (errroWalletTransaction) {
      message.error(errroWalletTransaction);
      dispatch({ type: GET_TRANSACTION_LIST_RESET });
    }
  }, [
    successCretaeWallet,
    errorCreateWallet,
    errroWalletTransaction,
    errorWalletBalance,
  ]);

  return (
    <Box py={{ base: 70, lg: 100 }} px={{ base: 5, lg: 20 }}>
      <WalletPage open={openModel} onCancel={() => setOpenModel(false)} />
      {userInfo?.userWallet ? (
        <Box>
          <SimpleGrid columns={[1, 1, 2]}>
            <Flex gap="5">
              <Text
                fontSize={{ base: "lg", lg: "4xl" }}
                color="#000000"
                fontWeight="600"
              >
                Current Balance:
              </Text>

              <Text
                fontSize={{ base: "lg", lg: "4xl" }}
                color="#0EBCF5"
                fontWeight="600"
              >
                Rs {wallet?.balance}
              </Text>
            </Flex>

            <Button
              py="3"
              px="5"
              bgColor="red"
              width="250px"
              fontWeight="600"
              fontSize={{ base: "lg", lg: "2xl" }}
              onClick={() => setOpenModel(true)}
            >
              Recharge your wallet
            </Button>
          </SimpleGrid>

          <Box py="10">
            <TransactionList
              data={transaction}
              loading={loadingWalletTrasaction}
            />
          </Box>
        </Box>
      ) : (
        <Box>
          <Center flexDir="column">
            <Text
              fontSize={{ base: "lg", lg: "4xl" }}
              fontWeight={"600"}
              color="#000000"
            >
              You have no wallet
            </Text>
            <Button
              bgColor="red"
              py="3"
              px="5"
              fontWeight="600"
              fontSize={{ base: "lg", lg: "4xl" }}
              onClick={handleCreateWallet}
            >
              Create wallet
            </Button>
          </Center>
        </Box>
      )}
    </Box>
  );
}
