import {
  Box,
  HStack,
  SimpleGrid,
  Text,
  Button,
  Divider,
  Image,
  Flex,
  Stack,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { AiFillStar } from "react-icons/ai";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { detailsScreen } from "../../Actions/screenActions";
import { useSelector } from "react-redux";
import { CREATE_CAMPAIGN_RESET } from "../../Constants/campaignConstants";
import { createCamapaign } from "../../Actions/campaignAction";
import { message } from "antd";
import { MEDIA_UPLOAD_RESET } from "../../Constants/mediaConstants";

export function SingleCampaign(props: any) {
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const createCampaign = useSelector((state: any) => state.createCampaign);
  const {
    loading: loadingSlotBooking,
    error: errorSlotBooking,
    success: successSlotBooking,
  } = createCampaign;

  const screenDetails = useSelector((state: any) => state.screenDetails);
  const { loading: loadingScreen, error: errorScreen, screen } = screenDetails;
  const mediaUpload = useSelector((state: any) => state.mediaUpload);
  const {
    loading: loadingMedia,
    media: mediaData,
    success,
    error: errorMedia,
  } = mediaUpload;

  useEffect(() => {
    if (errorMedia) {
      message.error(errorMedia);
      dispatch({ type: MEDIA_UPLOAD_RESET });
    }
  }, [errorMedia]);

  useEffect(() => {
    if (errorSlotBooking) {
      message.error(errorSlotBooking);
      dispatch({ type: CREATE_CAMPAIGN_RESET });
    }
    if (successSlotBooking) {
      message.success("Campaign created successFull");
      dispatch({ type: CREATE_CAMPAIGN_RESET });
      dispatch({ type: MEDIA_UPLOAD_RESET });
      navigate("/");
    }
  }, [errorSlotBooking, successSlotBooking]);

  const slotBookingHandler = () => {
    dispatch(
      createCamapaign({
        screenId: location?.state?.screenId,
        mediaId: mediaData?._id,
        campaignName: location?.state?.campaignName,
        totalSlotBooked: location?.state?.numberOfSlots,
        startDate: location?.state?.startDateTime,
        endDate: location?.state?.endDateTime,
        isDefaultCampaign: location?.state?.defaultCampaign || false,
      })
    );
  };
  useEffect(() => {
    dispatch(detailsScreen(location?.state?.screenId));
  }, [location, dispatch]);
  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: 75, lg: 100 }}>
      <SimpleGrid columns={[1, 1, 2]} spacing="10">
        <Box>
          <Box bgColor="#F7F7F7" p="5" borderRadius="8px">
            <Text
              color="#000000"
              fontSize="sm"
            >{`Your account is pending approval from the screen owner. This approval process is a one-time requirement for each screen. While you wait for approval, you can still post your ad. The approval process typically takes up to 2 hours`}</Text>
          </Box>
          <Box width="638px">
            <HStack pt="5" justifyContent="space-between">
              <Box
                as="video"
                src={mediaData?.media}
                autoPlay
                loop
                muted
                display="inline-block"
                borderRadius="4px"
                height="118px"
                width="197px"
              ></Box>
            </HStack>
            <Text
              color="#222222"
              fontSize={{ base: "xl", lg: "2xl" }}
              pt="5"
              fontWeight="600"
              m="0"
            >
              Campaign name
            </Text>
            <HStack justifyContent="space-between" m="0">
              <Text
                color="#2F2F2F"
                fontSize={{ base: "lg", lg: "xl" }}
                fontWeight="400"
                m="0"
              >
                {location?.state?.campaignName}
              </Text>
              <Button
                fontSize={{ base: "md", lg: "md" }}
                variant="null"
                color="#353535"
                fontWeight="638"
              >
                Edit
              </Button>
            </HStack>
            <Divider color="#DDDDDD" />
            <Text
              color="#222222"
              fontSize={{ base: "xl", lg: "2xl" }}
              pt="5"
              fontWeight="600"
              m="0"
            >
              Dates
            </Text>
            <HStack justifyContent="space-between" m="0">
              <Text
                color="#2F2F2F"
                fontSize={{ base: "lg", lg: "xl" }}
                fontWeight="400"
                m="0"
              >
                {location?.state?.startDateTime?.toDateString()} -{" "}
                {location?.state?.endDateTime?.toDateString()}
              </Text>
              <Button
                fontSize={{ base: "md", lg: "md" }}
                variant="null"
                color="#353535"
                fontWeight="638"
              >
                Edit
              </Button>
            </HStack>
            <Divider color="#DDDDDD" />
            <Text
              color="#222222"
              fontSize={{ base: "xl", lg: "2xl" }}
              pt="5"
              fontWeight="600"
              m="0"
            >
              No of slots
            </Text>
            <HStack justifyContent="space-between" m="0">
              <Text
                color="#2F2F2F"
                fontSize={{ base: "lg", lg: "xl" }}
                fontWeight="400"
                m="0"
              >
                {location?.state?.numberOfSlots}
              </Text>
              <Button
                fontSize={{ base: "md", lg: "md" }}
                variant="null"
                color="#353535"
                fontWeight="638"
              >
                Edit
              </Button>
            </HStack>
            <Divider color="#DDDDDD" />
          </Box>
        </Box>
        <Box
          border="1px"
          borderRadius="12px"
          borderColor="#DDDDDD"
          p="5"
          width={{ base: "100%", lg: "60%" }}
          height="426px"
        >
          <Flex gap="10">
            <Image
              src={screen?.image}
              alt="screen"
              width="124px"
              height="110px"
              borderRadius="8px"
            />
            <Box alignContent="top">
              <Text color="#222222" fontSize="sm" fontWeight="432" m="0">
                {screen?.name}
              </Text>
              <Text color="#676767" fontSize="12px" fontWeight="600" m="0">
                {screen?.screenAddress}, {screen?.districtCity},{" "}
                {screen?.stateUT}
              </Text>
              <HStack>
                <Flex align="center">
                  <AiFillStar size="12px" color="#222222" />
                  <Text
                    pl="1"
                    color="#222222"
                    fontSize="12px"
                    align="left"
                    m="0"
                    fontWeight="600"
                    justifyContent="center"
                  >
                    {screen?.rating}
                  </Text>
                  <Text m="0" fontSize="12px" pl="1">{`(14 reviews)`}</Text>
                </Flex>
              </HStack>
            </Box>
          </Flex>
          <Divider color="#DDDDDD" />
          <Text
            color="#222222"
            fontSize={{ base: "xl", lg: "2xl" }}
            pt="2"
            fontWeight="600"
            m="0"
          >
            Price details
          </Text>
          <HStack justifyContent="space-between" m="0" pt="2">
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              ₹{screen?.rentPerSlot} x {location?.state?.numberOfSlots} Slots
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              ₹{screen?.rentPerSlot * location?.state?.numberOfSlots}
            </Text>
          </HStack>
          <HStack justifyContent="space-between" m="0">
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              SGST
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              ₹123
            </Text>
          </HStack>
          <HStack justifyContent="space-between" m="0">
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              CGST
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              ₹123
            </Text>
          </HStack>
          <Divider color="#DDDDDD" />
          <HStack justifyContent="space-between" m="0">
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="600"
              m="0"
            >
              Total
            </Text>
            <Text
              color="#222222"
              fontSize={{ base: "md", lg: "md" }}
              fontWeight="600"
              m="0"
            >
              ₹3,274
            </Text>
          </HStack>
        </Box>
      </SimpleGrid>
      <Stack pt="5">
        <Button
          bgColor="#D7380E"
          fontSize="lg"
          fontWeight="600"
          py="2"
          width="307px"
          isLoading={!(!loadingMedia && success) || loadingSlotBooking}
          loadingText="Creating Media"
          onClick={slotBookingHandler}
        >
          Proceed to payment
        </Button>
      </Stack>
    </Box>
  );
}
