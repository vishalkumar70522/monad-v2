// // import React, { useEffect, useRef, useState } from "react";
// // import "./videoStyle.css";
// import Axios from "axios";
// // import ReactPlayer from "react-player";
// import { Stack } from "@chakra-ui/react";

// import React, { useState, useEffect, useRef } from "react";
// import ReactPlayer from "react-player";
// export function FullScreenPlaylist(props) {
//   const playerRef = useRef(null);

//   const screenId = window.location.pathname.split("/")[2];
//   const [videos, setVideos] = useState([]);
//   const [index, setIndex] = useState();
//   const [isPlayed, setIsPlayed] = useState(true);
//   // const [isPause, setIsPause] = useState(false);
//   const [data, setDate] = useState({
//     url: "https://d2l701j0qa6ok6.cloudfront.net/video.mkv",
//     playing: true,
//     played: 0,
//   });
//   const url = "https://d2l701j0qa6ok6.cloudfront.net/video.mkv";

//   const style = {
//     zIndex: "1",
//     backgroundColor: "#000000",
//   };

//   const setPlayStatus = (status) => {
//     window?.localStorage?.setItem("isPlay", status);
//   };

//   const getCampaignList = async () => {
//     try {
//       const { data } = await Axios.get(
//         `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${screenId}/screen`
//       );
//       setVideos(
//         data
//           .filter((data) => data.status === "Active")
//           .map((video) => video.video)
//       );
//       setIndex(Math.floor(Math.random() * (videos.length - 0) + 0));
//       console.log("getCampaignList : ", videos, index);
//     } catch (error) {
//       console.log("error", error);
//     }
//   };

//   const onReady = React.useCallback(() => {
//     const timeToStart = 7 * 60 + 12.6;
//     playerRef?.current?.seekTo(timeToStart, "seconds");
//   }, [playerRef.current]);

//   useEffect(() => {
//     getCampaignList();
//     if (!index && videos.length > 0) {
//       setIndex(Math.floor(Math.random() * (videos.length - 0) + 0));
//     }
//   }, [props]);

//   const videoRef = useRef(null);
//   // Create a state variable to store the current video index
//   const [currentVideo, setCurrentVideo] = useState(0);

//   // Create a state variable to store the video status
//   const [playing, setPlaying] = useState(true);

//   // Create a state variable to store the current played seconds
//   const [playedSeconds, setPlayedSeconds] = useState(0);

//   // Create a state variable to store the elapsed time
//   const [elapsedTime, setElapsedTime] = useState(0);

//   // Create a state variable to store the seek time
//   const [seekTime, setSeekTime] = useState([0, 0]);

//   // Define an array of video urls
//   const videos1 = [
//     // "https://youtu.be/4-TZLMisluk",
//     "https://d2l701j0qa6ok6.cloudfront.net/video.mkv",
//     "https://ipfs.io/ipfs/bafybeibmb5mgqavi3jgwclowousfdrjghiicoyhr2sfwuahgmanfgcjchi",
//     // "https://www.youtube.com/watch?v=LXb3EKWsInQ",
//   ];

//   // Define a function to handle the video progress event
//   const handleVideoProgress = (state) => {
//     // Update the elapsed time with the current playedSeconds
//     setElapsedTime(state.playedSeconds);
//   };

//   // Define a function to handle the video ready event
//   const handleVideoReady = () => {
//     // videoRef.current.autoPlay();
//     // check if the video duration is available
//     if (videoRef.current.getInternalPlayer().readyState === 4) {
//       videoRef.current.seekTo(seekTime[currentVideo], "seconds");
//       // Update the played seconds with the seek time
//       setPlayedSeconds(seekTime[currentVideo]);
//     }
//     // Seek to the stored seek time for the current video
//   };

//   // Define a function to handle the video play event
//   const handleVideoPlay = () => {
//     // Set the playing state to true
//     setPlaying(true);
//   };

//   // Define a function to handle the video pause event
//   const handleVideoPause = () => {
//     // Set the playing state to false
//     setPlaying(false);
//   };

//   // Define a function to handle the video end event
//   const handleVideoEnd = () => {
//     // Reset the video status to paused
//     setPlaying(false);
//     // Switch to the next video in the array
//     setCurrentVideo((currentVideo + 1) % videos1.length);
//     // Reset the elapsed time to zero
//     setElapsedTime(0);
//   };

//   // Use an effect hook to check the elapsed time and pause the video if it reaches 30 seconds
//   useEffect(() => {
//     if (elapsedTime >= 10 && elapsedTime !== playedSeconds) {
//       // Store the current seek time for the current video
//       setSeekTime((prevSeekTime) => {
//         const newSeekTime = [...prevSeekTime];
//         newSeekTime[currentVideo] = elapsedTime;
//         return newSeekTime;
//       });
//       // Reset the video status to paused
//       setPlaying(false);
//       // Switch to the next video in the array
//       setCurrentVideo((currentVideo + 1) % videos1.length);
//       // Reset the elapsed time to zero
//       setElapsedTime(0);
//       // Update the played seconds with zero
//       setPlayedSeconds(0);
//     }
//   }, [elapsedTime]);

//   return (
//     <Stack
//       allowFullScreen
//       id="myVideo"
//       height="100%"
//       align="center"
//       __css={style}
//     >
//       {videos.length > 0 && index < videos?.length ? (
//         <ReactPlayer
//           playing={true}
//           url={videos[index]}
//           width="100%"
//           controls
//           // loop={isPlayed}
//           autoPlay={true}
//           // muted={true}
//           volume={0.5}
//           height="100%"
//           onProgress={handleVideoProgress}
//           onPlay={handleVideoPlay}
//           onPause={handleVideoPause}
//           // onReady={handleVideoReady}
//           // onEnded={handleVideoEnd}
//           onEnded={() => {
//             setIndex(index + 1);
//             if (index + 1 >= videos.length) {
//               console.log("index + 1 >= videos.length ");
//               setPlayStatus(true);
//               setTimeout(() => {
//                 setPlayStatus(false);
//                 setIndex(0);
//               }, 1000 * 60 * 2);
//               handleVideoEnd();
//             }
//             // getCampaignList();
//           }}
//         />
//       ) : (
//         <ReactPlayer
//           ref={videoRef}
//           playing={window?.localStorage?.getItem("isPlay")}
//           // playing={true}
//           url={data.url}
//           width="100%"
//           controls
//           autoPlay
//           volume={0.5}
//           height="100%"
//           // onReady={onReady}
//           onProgress={handleVideoProgress}
//           onPlay={handleVideoPlay}
//           onPause={handleVideoPause}
//           // onReady={handleVideoReady}
//           onEnded={handleVideoEnd}
//         />
//       )}
//       {/* <div className="">
//         <ReactPlayer
//           ref={videoRef}
//           url={videos1[currentVideo]}
//           playing={true}
//           controls
//           autoPlay={true}
//           muted={true}
//           onProgress={handleVideoProgress}
//           onPlay={handleVideoPlay}
//           onPause={handleVideoPause}
//           onReady={handleVideoReady}
//           onEnded={handleVideoEnd}
//         />
//       </div> */}
//     </Stack>
//   );
// }

import React, { useState, useEffect, useRef } from "react";
import "./videoStyle.css";

// A custom hook to get the current time of a video element
const useCurrentTime = (videoRef) => {
  const [currentTime, setCurrentTime] = useState(0);

  useEffect(() => {
    // Update the current time every 100 milliseconds
    const interval = setInterval(() => {
      if (videoRef.current) {
        setCurrentTime(videoRef.current.currentTime);
      }
    }, 100);

    // Clear the interval when the component unmounts
    return () => {
      clearInterval(interval);
    };
  }, [videoRef]);

  return currentTime;
};

// A custom hook to get the duration of a video element
const useDuration = (videoRef) => {
  const [duration, setDuration] = useState(0);

  useEffect(() => {
    // Set the duration when the video is loaded
    const handleLoadedMetadata = () => {
      if (videoRef.current) {
        setDuration(videoRef.current.duration);
      }
    };

    // Add an event listener to the video element
    if (videoRef.current) {
      videoRef.current.addEventListener("loadedmetadata", handleLoadedMetadata);
    }

    // Remove the event listener when the component unmounts
    return () => {
      if (videoRef.current) {
        videoRef.current.removeEventListener(
          "loadedmetadata",
          handleLoadedMetadata
        );
      }
    };
  }, [videoRef]);

  return duration;
};

// A custom hook to check if a video element is playing
const useIsPlaying = (videoRef) => {
  const [isPlaying, setIsPlaying] = useState(false);

  useEffect(() => {
    // Set the isPlaying state to true when the video is playing
    const handlePlay = () => {
      setIsPlaying(true);
    };

    // Set the isPlaying state to false when the video is paused
    const handlePause = () => {
      setIsPlaying(false);
    };

    // Add event listeners to the video element
    if (videoRef.current) {
      videoRef.current.addEventListener("play", handlePlay);
      videoRef.current.addEventListener("pause", handlePause);
    }

    // Remove the event listeners when the component unmounts
    return () => {
      if (videoRef.current) {
        videoRef.current.removeEventListener("play", handlePlay);
        videoRef.current.removeEventListener("pause", handlePause);
      }
    };
  }, [videoRef]);

  return isPlaying;
};

// A custom hook to toggle the playback of a video element
const useTogglePlayback = (videoRef) => {
  const isPlaying = useIsPlaying(videoRef);

  // A function to toggle the playback of the video element
  const togglePlayback = () => {
    if (videoRef.current) {
      if (isPlaying) {
        videoRef.current.pause();
      } else {
        videoRef.current.play();
      }
    }
  };

  return togglePlayback;
};

// A component that renders a short video overlay on top of a long video
export function FullScreenPlaylist() {
  // The URL of the long video
  const longVideoUrl = "https://d2l701j0qa6ok6.cloudfront.net/video.mkv";

  // The URL of the short video
  const shortVideoUrl =
    "https://ipfs.io/ipfs/bafybeibmb5mgqavi3jgwclowousfdrjghiicoyhr2sfwuahgmanfgcjchi";

  // A ref to access the long video element
  const longVideoRef = useRef(null);

  // A ref to access the short video element
  const shortVideoRef = useRef(null);

  // The current time of the long video in seconds
  const currentTime = useCurrentTime(longVideoRef);

  // The duration of the long video in seconds
  const duration = useDuration(longVideoRef);

  // A state to track if the short video is visible or not
  const [isVisible, setIsVisible] = useState(false);

  // A function to toggle the playback of the long video
  const toggleLongVideoPlayback = useTogglePlayback(longVideoRef);

  // A function to toggle the playback of the short video
  const toggleShortVideoPlayback = useTogglePlayback(shortVideoRef);

  useEffect(() => {
    // A function to show and play the short video at certain intervals of time
    const showShortVideo = () => {
      // The intervals of time in seconds when the short video should be shown
      const intervals = [10, 20, 30];

      // Check if the current time matches any of the intervals
      for (let interval of intervals) {
        if (Math.floor(currentTime) === interval) {
          // Pause the long video and show the short video
          toggleLongVideoPlayback();
          setIsVisible(true);

          // Play the short video from the beginning
          if (shortVideoRef.current) {
            shortVideoRef.current.currentTime = 0;
            toggleShortVideoPlayback();
          }

          break;
        }
      }
    };

    showShortVideo();
  }, [currentTime, toggleLongVideoPlayback, toggleShortVideoPlayback]);

  useEffect(() => {
    // A function to hide and pause the short video when it ends
    const hideShortVideo = () => {
      // Check if the short video has ended
      if (shortVideoRef.current && shortVideoRef.current.ended) {
        // Hide the short video and resume the long video
        setIsVisible(false);
        toggleLongVideoPlayback();
      }
    };

    // Add an event listener to the short video element
    if (shortVideoRef.current) {
      shortVideoRef.current.addEventListener("ended", hideShortVideo);
    }

    // Remove the event listener when the component unmounts
    return () => {
      if (shortVideoRef.current) {
        shortVideoRef.current.removeEventListener("ended", hideShortVideo);
      }
    };
  }, [toggleLongVideoPlayback]);

  return (
    <div className="video-overlay">
      {/* The long video element */}
      <video
        ref={longVideoRef}
        src={longVideoUrl}
        controls
        autoPlay
        muted
        width="800"
        height="450"
      />

      {/* The short video element */}
      {isVisible && (
        <video
          ref={shortVideoRef}
          src={shortVideoUrl}
          width="200"
          height="150"
          autoPlay
          muted
          style={{ position: "absolute", top: "0", right: "0" }}
        />
      )}

      {/* A progress bar to show the current time and duration of the long video */}
      <div className="progress-bar">
        <div
          className="progress"
          style={{ width: `${(currentTime / duration) * 100}%` }}
        />
        <span className="time">
          {Math.floor(currentTime)} / {Math.floor(duration)} seconds
        </span>
      </div>
    </div>
  );
}
