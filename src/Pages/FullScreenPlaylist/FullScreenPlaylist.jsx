import React, { useState, useEffect, useRef } from "react";
import Axios from "axios";
import { Stack } from "@chakra-ui/react";
import "./videoStyle.css";
import { useDispatch } from "react-redux";
import { checkPlaylist, detailsScreen } from "../../Actions/screenActions";
import { useSelector } from "react-redux";
// import ReactPlayer from "react-player";

// Define the video sources
// const longVideo = "https://d2l701j0qa6ok6.cloudfront.net/video.mkv";
// const longVideo = "https://d2l701j0qa6ok6.cloudfront.net/Steve.Jobs.2015.720p.BRRip.x264.AAC-ETRG.mp4"
// const longVideo = "https://youtu.be/4-TZLMisluk";
// const shortVideoUrls = [
//   "https://ipfs.io/ipfs/bafybeibmb5mgqavi3jgwclowousfdrjghiicoyhr2sfwuahgmanfgcjchi",
//   "https://ipfs.io/ipfs/bafybeibmb5mgqavi3jgwclowousfdrjghiicoyhr2sfwuahgmanfgcjchi",
// ];

const URL = "https://ip.nf/me.json";

// Define a custom hook to get the current time of a video element
const useCurrentTime = (videoRef) => {
  const [currentTime, setCurrentTime] = useState(0);

  useEffect(() => {
    // Update the current time every 100 ms
    const interval = setInterval(() => {
      if (videoRef.current) {
        setCurrentTime(videoRef.current.currentTime);
      }
    }, 50);

    // Clear the interval when the component unmounts
    return () => {
      clearInterval(interval);
    };
  }, [videoRef]);

  return currentTime;
};

// Define a custom hook to get the duration of a video element
const useDuration = (videoRef) => {
  const [duration, setDuration] = useState(0);

  useEffect(() => {
    // Set the duration when the video is loaded
    const handleLoadedMetadata = () => {
      if (videoRef.current) {
        setDuration(videoRef.current.duration);
      }
    };

    // Add an event listener to the video element
    if (videoRef.current) {
      videoRef.current.addEventListener("loadedmetadata", handleLoadedMetadata);
    }

    // Remove the event listener when the component unmounts
    return () => {
      if (videoRef.current) {
        videoRef.current.removeEventListener(
          "loadedmetadata",
          handleLoadedMetadata
        );
      }
    };
  }, [videoRef]);

  return duration;
};

// Define a custom hook to seek a video element to a given time
const useSeek = (videoRef, time) => {
  useEffect(() => {
    // Seek the video to the given time
    if (videoRef.current) {
      videoRef.current.currentTime = time;
    }
  }, [videoRef, time]);
};

// Define the main component
export const FullScreenPlaylist = () => {
  const screenId = window.location.pathname.split("/")[2];
  const dispatch = useDispatch();
  // Create refs for the video elements
  const longVideoRef = useRef(null);
  const shortVideoRef = useRef(null);

  const [ad, setAd] = useState(false);
  const [intervalTime, setIntervalTime] = useState([60]);
  const [pauseTime, setPauseTime] = useState([]);
  const [currentShortVideoIndex, setCurrentShortVideoIndex] = useState(0);
  const [shortVideoUrls, setShortVideoUrls] = useState([]);
  const [longVideo, setLongVideo] = useState("");
  const [info, setInfo] = useState({ ip: "" });

  const style = {
    zIndex: "1",
    backgroundColor: "#000000",
  };

  // Get the current time and duration of each video element
  const longVideoCurrentTime = useCurrentTime(longVideoRef);
  const longVideoDuration = useDuration(longVideoRef);

  // Create state variables for the playing status of each video element
  const [longVideoPlaying, setLongVideoPlaying] = useState(true);
  const [shortVideoPlaying, setShortVideoPlaying] = useState(false);

  // Create a state variable for the seek time of the long video element
  const [longVideoSeekTime, setLongVideoSeekTime] = useState(0);

  // Use the custom hook to seek the long video element to the seek time
  useSeek(longVideoRef, longVideoSeekTime);

  const screenDetails = useSelector((state) => state.screenDetails);
  const { loading: loadingScreen, error: errorScreen, screen } = screenDetails;

  var elem = document.getElementById("myvideo");

  /* When the openFullscreen() function is executed, open the video in fullscreen.
  Note that we must include prefixes for different browsers, as they don't support the requestFullscreen method yet */
  useEffect(() => {
    const video = ad ? shortVideoRef.current : longVideoRef.current;

    const handlePlay = () => {
      // Check if the video is paused and can be played
      if (!ad && video.paused && !video.ended) {
        video.play();
      }
      // if (elem) {
      //   elem.addEventListener("play", () => {
      //     // elem.fullscreenEnabled();
      //     if (elem.requestFullscreen) {
      //       elem.requestFullscreen();
      //     } else if (elem.webkitRequestFullscreen) {
      //       elem.webkitRequestFullscreen();
      //     } else if (elem.msRequestFullscreen) {
      //       elem.msRequestFullscreen();
      //     }
      //   });
      // }
    };

    const fetchInfo = async () => {
      const { data } = await Axios.get(URL);
      setInfo({ ...data });
      // console.log(info);
      // console.log(data.ip);
    };
    const getScreenData = async (screenId) => {
      try {
        const { data } = await Axios.get(
          `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/screenData/${screenId}`
        );
        const interval = parseInt(data.screenData.erickshawData.adIntervals);
        setLongVideo(data.screenData.erickshawData.defaultContents);
        setIntervalTime([interval]);
        console.log([interval]);
        // console.log("getCampaignList : ", shortVideoUrls, currentShortVideoIndex);
      } catch (error) {
        console.log("error", error);
      }
    };
    const getCampaignList = async (screenId) => {
      try {
        const { data } = await Axios.get(
          `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${screenId}/screen`
        );
        setShortVideoUrls(
          data
            .filter((data) => data.status === "Active")
            .map((video) => video.video)
        );
        // console.log(data);
        setCurrentShortVideoIndex(
          Math.round(Math.random() * (shortVideoUrls.length - 0) + 0)
        );
        // console.log("getCampaignList : ", shortVideoUrls, currentShortVideoIndex);
      } catch (error) {
        console.log("error", error);
      }
    };
    if (longVideoRef.current.pause() && !ad) {
      longVideoRef.current.play();
    }
    getCampaignList(screenId);
    getScreenData(screenId);
    fetchInfo();
    dispatch(detailsScreen(screenId));
    video.addEventListener("play", handlePlay);

    return () => {
      video.removeEventListener("play", handlePlay);
    };
  }, [dispatch, screenId, shortVideoUrls.length]);

  const handleShortVideoLoadedData = (state) => {
    shortVideoRef?.current?.play();
  };

  const handlePlay = () => {};
  // Handle the ended event of the first short video element
  const handleShortVideoEnded = () => {
    // Pause the first short video element and play the second short video element
    // shortVideo1Ref.current.addEventListener("ended", console.log("ended"));

    setCurrentShortVideoIndex((prevIndex) =>
      prevIndex + 1 < shortVideoUrls.length ? prevIndex + 1 : 0
    );
    longVideoRef.current.play();
    setAd(!ad);
    const deviceInfo = {
      // deviceId: deviceId,
      deviceIp: info.ip.ip,
      deviceLongi: info.ip.longitude,
      deviceLati: info.ip.latitude,
      // deviceMaac: deviceMaac,
      // deviceDisplay: deviceDisplay,
    };
    if (shortVideoUrls[currentShortVideoIndex]) {
      dispatch(
        checkPlaylist({
          screenName: screen.name,
          timeNow: new Date(),
          currentVid: shortVideoUrls[currentShortVideoIndex]
            .split("/")
            .slice(-1),
          deviceInfo: deviceInfo,
        })
      );
    }
  };

  const handleLoadData = () => {
    const pausingIntervals = longVideoDuration / intervalTime[0];
    const pausingTimes = [];
    for (let i = 0; i < pausingIntervals; i++) {
      const pt = 0 + i * intervalTime[0];
      pausingTimes.push(pt);
    }
    setPauseTime(pausingTimes);
  };

  // Handle the pause event of the long video element
  const handleLongVideoPause = () => {
    // Set the seek time to the current time of the long video element
    setLongVideoSeekTime(longVideoCurrentTime);

    // Pause the long video element and play the first short video element
    setLongVideoPlaying(false);
    setAd(true);
    setShortVideoPlaying(true);
  };

  const handleLongVideoProgress = (state) => {
    // console.log(Math.round(longVideoCurrentTime));

    if (pauseTime.length > 0) {
      const pauseNowTime = pauseTime.reduce((prev, curr) => {
        return Math.abs(curr - longVideoCurrentTime) <
          Math.abs(prev - longVideoCurrentTime)
          ? curr
          : prev;
      });
      if (Math.abs(Math.round(longVideoCurrentTime - pauseNowTime)) < 5) {
        longVideoRef.current.pause();
        setAd(true);
      }

      // console.log(Math.abs(Math.round(longVideoCurrentTime - pauseNowTime)));
    }
    // console.log(pausingTimes);
  };

  return (
    <Stack
      allowFullScreen
      id="myVideo"
      height="100%"
      align="center"
      __css={style}
    >
      {/* <iframe
              title="sa"
              ref={longVideoRef}
              src={`${longVideo}`}
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
              muted
              width="100%"
              height="100%"
            ></iframe> */}

      {ad ? (
        <video
          id="myvideo"
          ref={shortVideoRef}
          src={shortVideoUrls[currentShortVideoIndex]}
          muted
          autoPlay={true}
          controls
          // width="1920px"
          // height="1080px"
          allow="fullscreen"
          poster="https://bafybeieoz24a5q4g6tcqfbxmtzba3zpyehsoissxmjvn5w2g7vdedw2wsu.ipfs.w3s.link/19201080.png"
          objectFit="cover"
          onPlay={handlePlay}
          onLoadedData={handleShortVideoLoadedData}
          onEnded={handleShortVideoEnded}
          style={{ position: "absolute", top: 0, zIndex: 10 }}
        />
      ) : null}
      <video
        id="myvideo"
        ref={longVideoRef}
        src={longVideo}
        autoPlay={!ad ? true : false}
        // isPlaying
        // playing
        muted
        controls
        // width="1920px"
        // height="1080px"
        objectFit="cover"
        poster="https://bafybeieoz24a5q4g6tcqfbxmtzba3zpyehsoissxmjvn5w2g7vdedw2wsu.ipfs.w3s.link/19201080.png"
        allow={!ad && "fullscreen"}
        onPlay={handlePlay}
        onLoadedData={handleLoadData}
        onPause={handleLongVideoPause}
        onProgress={handleLongVideoProgress}
        style={{ position: "absolute", top: 0, zIndex: 1 }}
      />

      {/* <div className="video-info">
        <p>
          Long video: {longVideoCurrentTime.toFixed(2)} /{" "}
          {longVideoDuration.toFixed(2)}
        </p>
        <p>
          Short video 1: {shortVideo1CurrentTime.toFixed(2)} /{" "}
          {shortVideo1Duration.toFixed(2)}
        </p>
        <p>
          Short video 2: {shortVideo2CurrentTime.toFixed(2)} /{" "}
          {shortVideo2Duration.toFixed(2)}
        </p>
      </div> */}
    </Stack>
  );
};
