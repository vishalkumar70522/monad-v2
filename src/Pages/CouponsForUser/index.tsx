import {
  Box,
  Button,
  Center,
  Stack,
  Image,
  Text,
  SimpleGrid,
  Show,
  Flex,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getCouponListForUser } from "../../Actions/couponAction";
import { useDispatch } from "react-redux";
import { Skeleton, message } from "antd";
import { ResultNotFound } from "../../components/commans";
import { ScanQRCode } from "../ScanQRCode";
import { FiUser } from "react-icons/fi";
import { scanner } from "../../assets/svg";
import { AiOutlineHome } from "react-icons/ai";

export function CouponsForUser() {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [openQRScanner, setOpenQRScanner] = useState<any>(false);

  const couponListForUser = useSelector(
    (state: any) => state.couponListForUser
  );
  const {
    loading: loadingCouponList,
    error: errorCouponList,
    coupons,
  } = couponListForUser;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const handelClickBuuton = (path: string) => {
    if (!userInfo) {
      navigate("/signin", { state: { path } });
    } else {
      navigate(path);
    }
  };

  useEffect(() => {
    if (errorCouponList) {
      message.error(errorCouponList);
    }
  }, [errorCouponList]);

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/userCoupons" } });
    }
    dispatch(getCouponListForUser(userInfo?._id));
  }, [dispatch, userInfo, navigate]);
  return (
    <Box px={{ base: 5, lg: 20 }} py={{ base: 75, lg: 100 }}>
      <ScanQRCode
        open={openQRScanner}
        onCancel={() => setOpenQRScanner(false)}
      />
      <Text
        fontSize={{ base: "lg", lg: "4xl" }}
        fontWeight="600"
        color="#000000"
      >
        My Coupons
      </Text>
      {loadingCouponList ? (
        <Box>
          <Skeleton avatar paragraph={{ rows: 4 }} />
          <Skeleton avatar paragraph={{ rows: 4 }} />
        </Box>
      ) : loadingCouponList === false && coupons?.length === 0 ? (
        <ResultNotFound path="/" />
      ) : (
        <SimpleGrid columns={[2, 2, 3]} spacing={{ base: "3", lg: "10" }}>
          {coupons?.map((coupon: any, index: any) => (
            <Stack
              width={{ base: "", lg: "300px" }}
              border="1px"
              borderColor={"#E7E7E7"}
              borderRadius="4px"
              key={index}
              onClick={() =>
                navigate("/couponDetils", {
                  state: {
                    data: coupon,
                    path: "/userCoupons",
                    showCouponCode: true,
                  },
                })
              }
              justifyContent="space-between"
            >
              <Center p={{ base: "2", lg: "5" }} flexDirection="column">
                <Image
                  src={coupon?.brandLogo}
                  alt="logo"
                  borderRadius="full"
                  boxSize={{ base: "70px", lg: "150px" }}
                />
                <Text
                  fontSize={{ base: "12px", lg: "lg" }}
                  fontWeight="533"
                  color="#000000"
                  align="center"
                >
                  {coupon?.brandName}
                </Text>
                <Text
                  className="text"
                  fontSize={{ base: "sm", lg: "lg" }}
                  fontWeight="533"
                  color="#000000"
                  m="0"
                  align="center"
                  // isTruncated={true}
                  noOfLines={2}
                >
                  {coupon?.offerName}
                </Text>
                <Text
                  className="text"
                  pt="2"
                  fontSize={{ base: "12px", lg: "lg" }}
                  fontWeight="533"
                  color="#000000"
                  m="0"
                  align="center"
                  // isTruncated={true}
                  noOfLines={2}
                >
                  {coupon?.offerDetails}
                </Text>
              </Center>
              <Button
                color="#FFFFFF"
                bgColor="#0EBCF5"
                py="3"
                px="2"
                alignContent="center"
                // alignContent="center"
                borderRadius="0px"
                borderBottomLeftRadius="4px"
                borderBottomRightRadius="4px"
                fontSize={{ base: "md", lg: "lg" }}
                fontWeight="600"
              >
                View details
              </Button>
            </Stack>
          ))}
        </SimpleGrid>
      )}
      <Show below="md">
        <div className="footer">
          <Flex
            justifyContent="space-between"
            px="10"
            bgColor="#E2E2E2"
            py="1"
            borderTopEndRadius="40px"
            borderTopLeftRadius="40px"
          >
            <Stack onClick={() => handelClickBuuton("/")} align="center">
              <AiOutlineHome color="#494949" size="30px" />
              {/* <Image src={couponicon} alt="couponIcon" width="30px" /> */}
              <Text color="" fontSize="12px" fontWeight="400" m="0">
                Home
              </Text>
            </Stack>
            <Stack mt="-10" onClick={() => setOpenQRScanner(true)}>
              <Image
                src={scanner}
                width="80px"
                height="80px"
                alt="scanner"
                onClick={() => setOpenQRScanner(true)}
              />
            </Stack>
            <Stack
              onClick={() => handelClickBuuton("/userProfile")}
              align="center"
            >
              <FiUser color="#494949" size="30px" />
              <Text color="#494949" fontSize="12px" fontWeight="400" m="0">
                Account
              </Text>
            </Stack>
          </Flex>
        </div>
      </Show>
    </Box>
  );
}
