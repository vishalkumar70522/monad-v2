import {
  Box,
  Button,
  Text,
  HStack,
  SimpleGrid,
  Skeleton,
  Center,
} from "@chakra-ui/react";
import { message } from "antd";
import { useEffect } from "react";
import { SingleScreen } from "../../components/commans";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { userScreensList } from "../../Actions/userActions";
import { createScreen } from "../../Actions/screenActions";
import { SCREEN_CREATE_RESET } from "../../Constants/screenConstants";
import { USER_SCREENS_RESET } from "../../Constants/userConstants";

const data = [1, 2, 3, 4, 5, 6];

export function MyScreenList(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const userScreens = useSelector((state: any) => state.userScreens);
  const { loading: loadingScreens, error: errorScreens, screens } = userScreens;
  const screenCreate = useSelector((state: any) => state.screenCreate);
  const {
    loading: loadingCreate,
    error: errorCreate,
    success: successCreate,
    screen: createdScreen,
  } = screenCreate;

  const handleCreateScree = () => {
    dispatch(createScreen());
  };

  useEffect(() => {
    if (errorCreate) {
      message.error(errorCreate);
    } else if (successCreate) {
      // message.success(
      //   "Ad space created successfully! Now edit your details"
      // );
      dispatch({ type: SCREEN_CREATE_RESET });
      navigate(`/edit-screen/${createdScreen?._id}`);
    }
    if (errorScreens) {
      message.error(errorScreens);
      dispatch({ type: USER_SCREENS_RESET });
      if (errorScreens === "Please Signin Again to continue") {
        navigate("/signin");
      }
    }
  }, [successCreate, errorCreate, errorScreens]);

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }

    dispatch(userScreensList(userInfo));
  }, [userInfo]);

  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: 75, lg: 100 }}>
      <HStack
        justifyContent="space-between"
        boxShadow="2xl"
        align="center"
        p="5"
        pl="5"
        pr="10"
      >
        <Text
          color="#333333"
          fontSize={{ base: "lg", lg: "2xl" }}
          fontWeight="665"
          align="center"
          m="0"
        >
          My Adspaces
        </Text>
        <Button
          color="#82CD47"
          _hover={{ bg: "#82CD47", color: "#FFFFFF" }}
          fontSize={{ base: "lg", lg: "xl" }}
          boxShadow="xl"
          alignContent="center"
          variant="outline"
          px="5"
          py="3"
          bgColor="#FFFFFF"
          isLoading={loadingCreate}
          loadingText="Screen creating..."
          onClick={handleCreateScree}
        >
          + Adspaces
        </Button>
      </HStack>
      <Box pt="10">
        {loadingScreens ? (
          data?.map((value: any) => (
            <Skeleton key={value}>
              <Box
                width="417px"
                height="251px"
                bgPosition="center"
                bgSize="cover"
                borderRadius="8px"
              ></Box>
            </Skeleton>
          ))
        ) : (
          <SimpleGrid
            columns={[1, null, 3]}
            // spacing={{ base: "5", lg: "10" }}
            spacing={{ base: "5", lg: "10" }}
          >
            {screens?.length > 0 &&
              screens?.map((singleScreen: any, index: any) => (
                <SingleScreen screen={singleScreen} key={index} />
              ))}
          </SimpleGrid>
        )}
        {screens?.length === 0 && (
          <Center flexDirection="column">
            <Text
              color="#000000"
              fontSize={{ base: "lg", lg: "3xl" }}
              fontWeight="600"
            >
              Get more sales with your ad space
            </Text>
            <Text
              color="#535353"
              fontSize={{ base: "12px", lg: "md" }}
              fontWeight="600"
            >
              Now you can create coupons to get attention from customers
            </Text>
            <Button
              color="#82CD47"
              _hover={{ bg: "#82CD47", color: "#FFFFFF" }}
              fontSize={{ base: "lg", lg: "2xl" }}
              boxShadow="xl"
              alignContent="center"
              variant="outline"
              px="5"
              py="3"
              bgColor="#FFFFFF"
              isLoading={loadingCreate}
              loadingText="Screen creating..."
              onClick={handleCreateScree}
            >
              Create your Ad space
            </Button>
          </Center>
        )}
      </Box>
    </Box>
  );
}
