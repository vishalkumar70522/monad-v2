import { Box, Button, Flex, SimpleGrid, Stack, Text } from "@chakra-ui/react";

import {
  PopulerScreens,
  Screen,
  SearchWithCreateCampaign,
} from "../../components/commans";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllScreens, listScreens } from "../../Actions/screenActions";

import { CalculateBudgetPopup } from "../Models/CalculateBudgetPopup";
import { getPinJson } from "../../Actions/pinActions";
import { Checkbox, Divider, Skeleton } from "antd";

export function AllScreenPage(props: any) {
  const [pageNumber, setPageNumber] = useState<any>(1);
  const dispatch = useDispatch<any>();

  const [screensList, setScreensList] = useState<any>([]);

  const [showCalculateBudget, setShowCalculateBudgetPopup] =
    useState<boolean>(false);
  const [selectedScreen, setSelectedScreen] = useState<any>(null);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const screenList = useSelector((state: any) => state.screenList);
  const {
    loading: loadingScreens,
    error: errorScreens,
    screens: listOfScreens,
  } = screenList;

  const jsonPins = useSelector((state: any) => state.jsonPins);
  const { loading: loadingAllPins, error: errorAllPins, jsonData } = jsonPins;
  // console.log("json Data : ", jsonData);

  const allScreensGet = useSelector((state: any) => state.allScreensGet);
  const {
    loading: loadingAllScreens,
    error: errorAllScreens,
    screens: allScreens,
  } = allScreensGet;

  const filterScreenListByAudiance = useSelector(
    (state: any) => state.filterScreenListByAudiance
  );
  const {
    loading: loadingfilterScreenListByAudiance,
    error: errorfilterScreenListByAudiance,
    screens: filterScreens,
    filterOption,
  } = filterScreenListByAudiance;

  // const columns: ColumnsType<any> = [
  //   {
  //     title: (
  //       <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
  //         Preview
  //       </Text>
  //     ),
  //     dataIndex: "logo",
  //     key: "logo",
  //     render: () => (
  //       // <Image src={value} alt={value} width="70px" height="70px" />
  //       // <Box as="video" src={value} width="70px" height="70px" />
  //     ),
  //   },
  // ]
  const handleSelectedScreen = (screen: any) => {
    setSelectedScreen(screen);
    setShowCalculateBudgetPopup(true);
  };

  const loadMore = () => {
    setPageNumber(Number(pageNumber) + 1);
    dispatch(listScreens({ pageNumber: String(Number(pageNumber) + 1) }));
  };
  useEffect(() => {
    if (
      listOfScreens?.length > 0 &&
      screensList.length < Number(pageNumber) * 6
    ) {
      setScreensList([...listOfScreens, ...screensList]);
    }
    // if (allScreens) {
    // }
  }, [listOfScreens]);

  useEffect(() => {
    dispatch(listScreens({ pageNumber }));
    dispatch(getPinJson());
    dispatch(getAllScreens());
  }, [dispatch, pageNumber]);

  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: 75, lg: 100 }}>
      {/* calculate budget model */}
      <CalculateBudgetPopup
        showCalculateBudget={showCalculateBudget}
        screen={selectedScreen}
        setShowCalculateBudgetPopup={() => setShowCalculateBudgetPopup(false)}
      />
      {/* Search Button */}

      <SearchWithCreateCampaign />
      {loadingScreens || loadingfilterScreenListByAudiance ? (
        <Box pt="5">
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
        </Box>
      ) : loadingfilterScreenListByAudiance === false &&
        filterScreens?.length > 0 ? (
        <PopulerScreens
          screensList={filterScreens}
          jsonData={jsonData}
          filtered={true}
          filterOption={filterOption}
        />
      ) : loadingfilterScreenListByAudiance === false &&
        filterScreens?.length === 0 ? (
        <Box pt="10" justifyContent="center">
          {" "}
          <Text color="red" fontSize="lg" fontWeight="600">
            No result found!
          </Text>
          <Button
            variant="outline"
            color="#D7380E"
            fontSize={{ base: "10px", lg: "md" }}
            borderRadius={{ base: "lg", lg: "2xl" }}
            py="2"
            onClick={() => window.location.reload()}
          >
            Reset filter
          </Button>
        </Box>
      ) : (
        <PopulerScreens
          screensList={screensList}
          jsonData={jsonData}
          filtered={false}
        />
      )}
      <Stack pt="10">
        <Button
          variant="outline"
          color="#D7380E"
          fontSize="md"
          py="3"
          px="10"
          width={{ base: "100px", lg: "200px" }}
          onClick={loadMore}
        >
          See more
        </Button>
      </Stack>
      <Divider />
      <Stack>
        <Text
          fontSize={{ base: "20px", lg: "3xl" }}
          color="#403F49"
          align="left"
          fontWeight="semibold"
        >
          All Adspaces
        </Text>
        {loadingAllScreens ? (
          <Box pt="5">
            <Skeleton avatar active paragraph={{ rows: 4 }} />{" "}
            <Skeleton avatar active paragraph={{ rows: 4 }} />{" "}
            <Skeleton avatar active paragraph={{ rows: 4 }} />{" "}
          </Box>
        ) : errorAllScreens ? (
          <Text>{errorAllScreens}</Text>
        ) : allScreens ? (
          <>
            <Text>Total {allScreens.length} Adspaces</Text>
            {allScreens?.map((screen: any, index: any) => (
              <Flex key={index}>
                <Stack px="2" align="center">
                  <Checkbox
                    onChange={(e) => handleSelectedScreen(screen)}
                  ></Checkbox>
                </Stack>
                <Screen
                  key={index}
                  screen={screen}
                  selectedScreen={handleSelectedScreen}
                />
              </Flex>
            ))}
          </>
        ) : null}
        <SimpleGrid columns={[1, 2]} gap={2}>
          {/* <Table
            columns={columns}
            dataSource={props?.videosList || []}
            pagination={{ pageSize: 10 }}
            scroll={{ y: 300 }}
          /> */}
        </SimpleGrid>
      </Stack>
    </Box>
  );
}
