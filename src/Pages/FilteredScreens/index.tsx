import { Box } from "@chakra-ui/react";

import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getCampaignList } from "../../Actions/campaignAction";
import { listScreens } from "../../Actions/screenActions";
import {
  PopulerScreens,
  ResultNotFound,
  SearchWithCreateCampaign,
} from "../../components/commans";
import { useSelector } from "react-redux";
import { Skeleton } from "antd";

export function FilteredScreens(props: any) {
  const dispatch = useDispatch<any>();
  const [jsonData, setJsonData] = useState<any>([]);

  const filterScreenList = useSelector((state: any) => state.filterScreenList);
  const {
    loading: loadingScreens,
    error: errorScreens,
    screens,
  } = filterScreenList;

  // console.log("allCampaign : ", JSON.stringify(allCampaign));

  useEffect(() => {
    if (screens?.length > 0) {
      const data = screens?.slice(0, 3)?.map((screen: any) => {
        return {
          type: "Feature",
          properties: {
            screen: screen?._id,
          },
          geometry: {
            coordinates: [screen.lat, screen.lng],
            type: "Point",
          },
        };
      });
      setJsonData({
        features: data,
      });
    }
  }, [screens]);

  useEffect(() => {
    // console.log("userInfo : ", userInfo);

    dispatch(getCampaignList());
    dispatch(listScreens({}));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);
  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: "75", lg: "100" }}>
      {/* calculate budget model */}
      <SearchWithCreateCampaign />
      {loadingScreens ? (
        <Box pt="5">
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
        </Box>
      ) : screens?.length === 0 ? (
        <ResultNotFound path="/searchScreenBasedOnLocationAndLocality" />
      ) : (
        <PopulerScreens screensList={screens} jsonData={jsonData} />
      )}
    </Box>
  );
}
