import { Box, Hide, Show } from "@chakra-ui/react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { DesktopViewUserProfile } from "./DesktopViewUserProfile";
import { MobileViewUserProfile } from "./MobileViewUserProfile";

export function UserProfile(props: any) {
  const navigate = useNavigate();
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/userProfile" } });
    }
  }, [props, userInfo]);

  return (
    <Box py={{ base: "75", lg: "100" }}>
      <Hide below="md">
        <DesktopViewUserProfile />
      </Hide>
      <Show below="md">
        <MobileViewUserProfile />
      </Show>
    </Box>
  );
}
