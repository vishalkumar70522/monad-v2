import React from "react";
import {
  Box,
  Button,
  Flex,
  Text,
  Image,
  // Center,
  // Divider,
  SimpleGrid,
  HStack,
} from "@chakra-ui/react";

export function CreatorProfileView() {
  return (
    <Box
      mt="-70px"
      height="427.3px"
      background="linear-gradient(to bottom, #93BFD8, #A1B6EB28)"
    >
      <Flex pt="111px">
        <Box
          py="25px"
          px="34px"
          ml="70px"
          width="347px"
          height="370px"
          background="#ffffff"
          boxShadow="base"
          rounded="8px"
        >
          <Box height="137px" width="279px" rounded="8px">
            <Image />
          </Box>
          <HStack>
            <Box>Ratings</Box>
            <Box>
              <Text>Rate Now</Text>
            </Box>
          </HStack>
          <Text>8 Ratings & 6 Reviews</Text>
          <Button>Visit Site</Button>
          <Flex>
            <Text>Share</Text>
          </Flex>
        </Box>
        <Box m="20px">
          <SimpleGrid columns={[2]} gap="10px">
            <Box></Box>
            <Box></Box>
          </SimpleGrid>
        </Box>
      </Flex>
    </Box>
  );
}
