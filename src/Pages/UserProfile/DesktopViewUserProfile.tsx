import {
  Box,
  Button,
  Flex,
  Text,
  Image,
  Center,
  Divider,
  SimpleGrid,
  Stack,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { EditBrandProfileData } from "./EditBrandProfileData";
import { EditCreatorProfileData } from "./EditCreatorProfileData";

import { AiOutlineEdit } from "react-icons/ai";
import { GrFormEdit } from "react-icons/gr";
import { EditUserProfilePopup } from "./EditUserProfilePopup";
import { useNavigate } from "react-router-dom";
import { Skeleton } from "antd";

export function DesktopViewUserProfile(props: any) {
  const navigate = useNavigate();

  const [openEditUserProfile, setOpenEditUserProfile] = useState<any>(false);
  const [activity, setActivity] = useState<any>("Profile");
  const [editPublicProfile, setEditPublicProfile] = useState<any>(false);
  const [editCreatorProfile, setEditCreatorProfile] = useState<any>(false);

  const [creatorView, setCreatorView] = useState<any>(false);
  const [brandView, setBrandView] = useState<any>(false);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { loading: loadingUserInfo, userInfo } = userSignin;

  const handleBrandView = () => {
    setCreatorView(false);
    setBrandView(true);
  };

  const handleCreatorView = () => {
    setBrandView(false);
    setCreatorView(true);
  };

  const handleMasterView = () => {
    setBrandView(false);
    setCreatorView(false);
    setEditPublicProfile(false);
    setEditCreatorProfile(false);
  };

  useEffect(() => {}, []);
  return (
    <Box>
      <EditUserProfilePopup
        open={openEditUserProfile}
        onCancel={() => setOpenEditUserProfile(false)}
      />
      {loadingUserInfo ? (
        <Skeleton />
      ) : (
        <Box px={{ base: 2, lg: 20 }} py={{ base: 5, lg: 5 }}>
          <Text
            fontSize={{ base: "", lg: "36px" }}
            fontWeight="600"
            color="#000000"
            m="0"
          >
            Account
          </Text>
          <Text
            fontSize={{ base: "sm", lg: "md" }}
            fontWeight="400"
            color="#4E4E4E"
          >
            Set your account details from below
          </Text>
          <Flex
            color="#000000"
            fontSize="xl"
            fontWeight="400"
            mt={{ base: "3", lg: "10" }}
          >
            <Button
              py="3"
              px="14"
              bg={activity === "Profile" ? "#FFFFFF" : "#F2F2F2"}
              color="#000000"
              boxShadow={activity === "Profile" ? "xl" : ""}
              onClick={() => setActivity("Profile")}
              borderRadius={activity === "Profile" ? "4px" : "0px"}
            >
              Profile
            </Button>
            <Button
              py="3"
              px="14"
              bg={activity === "Business" ? "#FFFFFF" : "#F2F2F2"}
              color="#000000"
              boxShadow={activity === "Business" ? "xl" : ""}
              onClick={() => setActivity("Business")}
              borderRadius={activity === "Profile" ? "4px" : "0px"}
            >
              Business
            </Button>
            <Button
              py="3"
              px="14"
              bg={activity === "Setting" ? "#FFFFFF" : "#F2F2F2"}
              color="#000000"
              boxShadow={activity === "Setting" ? "xl" : ""}
              onClick={() => setActivity("Setting")}
              borderRadius={activity === "Profile" ? "4px" : "0px"}
            >
              Setting
            </Button>
            <Button
              py="3"
              px="14"
              bg={activity === "Help" ? "#FFFFFF" : "#F2F2F2"}
              color="#000000"
              boxShadow={activity === "Help" ? "xl" : ""}
              onClick={() => setActivity("Help")}
              borderRadius={activity === "Profile" ? "4px" : "0px"}
            >
              Help
            </Button>
          </Flex>
          {activity === "Profile" && (
            <Flex mt={{ base: "", lg: "10" }} justifyContent="space-between">
              <Flex gap={{ base: "5", lg: "10" }}>
                <Image
                  p="5"
                  borderRadius="100%"
                  height={{ base: "153", lg: "159px" }}
                  width={{ base: "153", lg: "159px" }}
                  src={userInfo?.avatar}
                  alt=""
                />
                <Stack
                  align="start"
                  justify="center"
                  p={{ base: "", lg: "10" }}
                >
                  <Text
                    fontSize={{ base: "md", lg: "lg" }}
                    fontWeight="600"
                    color="#333333"
                    m="0"
                  >
                    {userInfo?.name}
                  </Text>
                  <Text
                    fontSize={{ base: "sm", lg: "md" }}
                    fontWeight="600"
                    color="#5C5C5C"
                    m="0"
                  >
                    {userInfo?.email}
                  </Text>
                  <Text
                    fontSize={{ base: "sm", lg: "md" }}
                    fontWeight="600"
                    color="#5C5C5C"
                    m="0"
                  >
                    {userInfo?.phone}
                  </Text>
                </Stack>
              </Flex>
              <Center pl={{ base: "5", lg: "20" }}>
                <Button
                  py="3"
                  px="5"
                  variant="outline"
                  color="#0EBCF5"
                  bgColor="#FFFFFF"
                  fontWeight="600"
                  fontSize="md"
                  _hover={{ color: "#FFFFFF", bgColor: "#0EBCF5" }}
                  rightIcon={<AiOutlineEdit size="20px" />}
                  onClick={() => setOpenEditUserProfile(true)}
                >
                  {" "}
                  Edit
                </Button>
              </Center>
            </Flex>
          )}
          {activity === "Business" && (
            <>
              <SimpleGrid columns={[1, 2, 3]}>
                <Stack align="center" onClick={handleMasterView}>
                  <Image
                    align="center"
                    // border="1px"
                    height="250px"
                    src="https://retailsphere.com/wp-content/uploads/2022/05/AdobeStock_340642544-1024x683.jpeg"
                  />
                  <Stack align="center" py="">
                    <Text fontSize="lg" fontWeight="600">
                      Adspaces
                    </Text>
                    <Text textAlign="center" px="10" fontSize="sm">
                      Turn your store, restaurant, cafe or building into an
                      adspace and monetise your walk-in customers using open
                      ads...
                    </Text>
                    {!userInfo?.isMaster && (
                      // <Center pl={{ base: "5", lg: "20" }}>
                      <Button
                        py="2"
                        px="5"
                        variant="outline"
                        color="#020202"
                        bgColor="#FFFFFF"
                        border="transparent"
                        fontWeight="600"
                        fontSize="sm"
                        _hover={{ color: "#FFFFFF", bgColor: "#02020250" }}
                        // rightIcon={<AiOutlineEdit size="20px" />}
                        onClick={() => setOpenEditUserProfile(true)}
                      >
                        {" "}
                        Get Adspace Business Access
                      </Button>
                      // </Center>
                    )}
                  </Stack>
                </Stack>
                <Stack align="center" onClick={handleBrandView}>
                  <Image
                    height="250px"
                    src="https://media.gq.com/photos/60f82bad1b7faefafbaa3db1/master/w_6000,h_4000,c_limit/CK%20NY%20M-891%205.26.21%20(3).JPG"
                  />
                  <Stack align="center" py="">
                    <Text fontSize="lg" fontWeight="600">
                      Advertisers
                    </Text>
                    <Text textAlign="center" px="10" fontSize="sm">
                      Innovate the way you advertise and extend your ad
                      campaigns' reach with audience engagement using open
                      ads...
                    </Text>
                    {userInfo && !userInfo?.isBrand && (
                      // <Center pl={{ base: "5", lg: "20" }}>
                      <Button
                        py="2"
                        px="5"
                        variant="outline"
                        color="#020202"
                        bgColor="#FFFFFF"
                        border="transparent"
                        fontWeight="600"
                        fontSize="sm"
                        _hover={{ color: "#FFFFFF", bgColor: "#02020250" }}
                        // rightIcon={<AiOutlineEdit size="20px" />}
                        onClick={() => setOpenEditUserProfile(true)}
                      >
                        {" "}
                        Get Professional Advertiser Access
                      </Button>
                      // </Center>
                    )}
                    {userInfo?.isBrand && brandView ? (
                      <>
                        <Text
                          onClick={() => {
                            if (userInfo?.brand?.length === 0) {
                              setEditPublicProfile(true);
                              setEditCreatorProfile(false);
                            } else {
                              navigate(`/brandProfile/${userInfo?.brand[0]}`);
                            }
                          }}
                        >
                          Click here for public profile
                        </Text>
                        {!editPublicProfile ? (
                          <Button
                            color="#3E3E3E"
                            py="3"
                            fontSize="sm"
                            fontWeight="400"
                            bgColor="#C4C4C4"
                            rightIcon={
                              <GrFormEdit size="20px" color="#3E3E3E" />
                            }
                            onClick={() => {
                              setEditPublicProfile(true);
                              setEditCreatorProfile(false);
                            }}
                          >
                            Edit public profile details
                          </Button>
                        ) : null}
                      </>
                    ) : null}
                  </Stack>
                </Stack>
                {/* <Stack align="center" onClick={handleCreatorView}>
                  <Image
                    height="250px"
                    src="https://wallpaperaccess.com/full/9488927.jpg"
                  />
                  <Stack align="center" py="">
                    <Text fontSize="lg" fontWeight="600">
                      Influencers
                    </Text>
                    <Text textAlign="center" px="10" fontSize="sm">
                      Find your gigs from the local market and build a social and
                      local reputation for yourself and the brand using open
                      ads...
                    </Text>
                    {!userInfo?.isCreator && (
                      <Center pl={{ base: "5", lg: "20" }}>
                        <Button
                          py="2"
                          px="5"
                          variant="outline"
                          color="#020202"
                          bgColor="#FFFFFF"
                          border="transparent"
                          fontWeight="600"
                          fontSize="sm"
                          _hover={{ color: "#FFFFFF", bgColor: "#02020250" }}
                          // rightIcon={<AiOutlineEdit size="20px" />}
                          onClick={() => setOpenEditUserProfile(true)}
                        >
                          {" "}
                          Get Influencer Business Access
                        </Button>
                      </Center>
                    )}
                    {userInfo?.isCreator && creatorView ? (
                      <>
                        <Text
                          onClick={() =>
                            navigate(`/creatorProfile/${userInfo?.creator[0]}`)
                          }
                        >
                          Click here for public profile
                        </Text>
                        {!editCreatorProfile && (
                          <Button
                            color="#3E3E3E"
                            py="3"
                            fontSize="sm"
                            fontWeight="400"
                            bgColor="#C4C4C4"
                            onClick={() => {
                              setEditCreatorProfile(true);
                              setEditPublicProfile(false);
                            }}
                            rightIcon={<GrFormEdit size="20px" color="#3E3E3E" />}
                          >
                            Edit creator profile details
                          </Button>
                        )}
                      </>
                    ) : null}
                  </Stack>
                </Stack> */}
              </SimpleGrid>
              <Divider color="#E6E6E6" />
              {editCreatorProfile && (
                <EditCreatorProfileData
                  setEditCreatorProfile={() => setEditCreatorProfile(false)}
                />
              )}
              {editPublicProfile && (
                <EditBrandProfileData
                  setEditPublicProfile={() => setEditPublicProfile(false)}
                />
              )}
            </>
          )}
          {activity === "Setting" && <>Coming soon...</>}
        </Box>
      )}
    </Box>
  );
}
