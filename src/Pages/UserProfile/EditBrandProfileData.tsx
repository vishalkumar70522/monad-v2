import {
  Box,
  Button,
  Divider,
  Flex,
  HStack,
  Image,
  Input,
  InputGroup,
  InputLeftAddon,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import TextArea from "antd/es/input/TextArea";
import { useEffect, useMemo, useRef, useState } from "react";
import { AiOutlinePlusCircle } from "react-icons/ai";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {
  changeUserAsBrand,
  getBrandDetailsById,
  updateBrandDetails,
} from "../../Actions/brandAction";
import { Space, message } from "antd";
import { addFileOnWeb3 } from "../../utils/web3";
import {
  CREATE_BRAND_RESET,
  GET_BRAND_DETAILS_RESET,
  UPDATE_BRAND_RESET,
} from "../../Constants/brandConstants";
import { useNavigate } from "react-router";
import { signout } from "../../Actions/userActions";
import Axios from "axios";

import debounce from "lodash/debounce";
import { Select, Spin } from "antd";

import type { SelectProps } from "antd/es/select";

const { Option } = Select;
const brandTypeOption = [
  {
    label: "ONLINE",
    value: "ONLINE",
  },
  {
    label: "OFFLINE",
    value: "OFFLINE",
  },
  {
    label: "HYBRID",
    value: "HYBRID",
  },
];

export interface DebounceSelectProps<ValueType = any>
  extends Omit<SelectProps<ValueType | ValueType[]>, "options" | "children"> {
  fetchOptions: (search: string) => Promise<ValueType[]>;
  debounceTimeout?: number;
}

function DebounceSelect<
  ValueType extends {
    key?: string;
    label: React.ReactNode;
    value: string | number;
  } = any
>({
  fetchOptions,
  debounceTimeout = 2000,
  ...props
}: DebounceSelectProps<ValueType>) {
  const [fetching, setFetching] = useState(false);
  const [options, setOptions] = useState<any[]>([]);
  const fetchRef = useRef(0);

  const debounceFetcher = useMemo(() => {
    const loadOptions = (value: string) => {
      fetchRef.current += 1;
      const fetchId = fetchRef.current;
      setOptions([]);
      setFetching(true);

      fetchOptions(value).then((newOptions) => {
        if (fetchId !== fetchRef.current) {
          // for fetch callback order
          return;
        }

        setOptions(newOptions);
        setFetching(false);
      });
    };

    return debounce(loadOptions, debounceTimeout);
  }, [fetchOptions, debounceTimeout]);

  return (
    <Select
      filterOption={false}
      onSearch={debounceFetcher}
      notFoundContent={fetching ? <Spin size="small" /> : null}
      {...props}
      // optionLabelProp="label"
      maxTagCount="responsive"
    >
      {options}
    </Select>
  );
}

// Usage of DebounceSelect
interface UserValue {
  label: any;
  value: string;
}

async function fetchUserList(text: string): Promise<any[]> {
  // return Axios.get(
  //   `${process.env.REACT_APP_BLINDS_SERVER}/api/users/filterUser/${text}`
  // ).then((response: any) => {
  //   console.log(response);
  //   return response?.data?.map((user: any) => ({
  //     label: `${user.name}`,
  //     value: user?._id,
  //   }));
  // });

  return Axios.get(
    `${process.env.REACT_APP_BLINDS_SERVER}/api/users/filterUser/${text}`
  ).then((response: any) => {
    return response?.data?.map((user: any) => (
      <Option value={user?._id} label={`${user?.name}`}>
        <Space>
          <Image
            src={user?.avatar}
            alt="profile image"
            height="30px"
            width="30px"
          />
          {`${user?.name}, ${user?.email}`}
        </Space>
      </Option>
    ));
  });
}

export function EditBrandProfileData(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [brandName, setBrandName] = useState<any>("");
  const [tagline, setTagline] = useState<any>("");
  const [address, setAddress] = useState<any>("");
  const [brandUrl, setBrandUrl] = useState<any>("");
  const [aboutBrand, setAboutBrand] = useState<any>("");
  const [brandLogo, setBrandLogo] = useState<any>(null);
  const [file, setFile] = useState<any>(null);
  const [images, setImages] = useState<any>([]);
  const [files, setFiles] = useState<any>([]);
  const [contactNumber, setContactNumber] = useState<any>("");
  const [email, setEmail] = useState<any>("");
  const [instagramId, setInstagramId] = useState<any>("");
  const [facebookId, setFacebookId] = useState<any>("");
  const [snapchat, setSnapchat] = useState<any>("");
  const [linkedin, setLinkedin] = useState<any>("");
  const [youtube, setYoutube] = useState<any>("");
  const [twitter, setTwitter] = useState<any>("");
  const [brandCategory, setBrandCategory] = useState<any>("");
  const [brandType, setBrandType] = useState<any>("");
  const [loading, setLoading] = useState<any>(false);
  const [oldImages, setOldImages] = useState<any>([]);
  const [affiliatedUsers, setAfiliatedUsers] = useState<any[]>([]);

  let hiddenInput: any = null;
  let hiddenInput1: any = null;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const createNewBrand = useSelector((state: any) => state.createNewBrand);
  const { loading: loadingBrnad, brand, success, error } = createNewBrand;

  const updateBrand = useSelector((state: any) => state.updateBrand);
  const {
    loading: loadingUpdateBrnad,
    brand: updatedBrand,
    success: successUpdateBrand,
    error: errorUpdateBrand,
  } = updateBrand;

  const brandDetails = useSelector((state: any) => state.brandDetails);
  const {
    loading: loadingBrandDetails,
    brand: brandData,
    error: errorBrandDetails,
  } = brandDetails;

  useEffect(() => {
    if (brandType === "OFFLINE" || brandType === "HYBRID") {
      if (!affiliatedUsers.includes(userInfo?._id)) {
        setAfiliatedUsers([...affiliatedUsers, userInfo?._id]);
      }
    }
  }, [brandType]);

  useEffect(() => {
    if (error) {
      message.error(error);
      dispatch({ type: CREATE_BRAND_RESET });
    }
    if (success) {
      dispatch({ type: CREATE_BRAND_RESET });
      message.success("Brand created successfully, please login again!");
      dispatch(signout());
    }
    if (updatedBrand && successUpdateBrand) {
      dispatch({ type: UPDATE_BRAND_RESET });
      message.success("Brand updated succesfully, please login again!");
      dispatch(signout());
    }
    if (errorUpdateBrand) {
      message.success(errorUpdateBrand);
      dispatch({ type: UPDATE_BRAND_RESET });
    }
  }, [
    dispatch,
    error,
    success,
    errorUpdateBrand,
    successUpdateBrand,
    updatedBrand,
    navigate,
  ]);

  function handleLogoSelect(file: any) {
    const fileThumbnail = URL.createObjectURL(file);
    setBrandLogo(fileThumbnail);
    setFile(file);
  }
  async function handlePhotoSelect(fileArray: any) {
    const thumbnails = [];
    for (let file of fileArray) {
      if (file?.type.split("/")[0] === "image") {
        const fileThumbnail = URL.createObjectURL(file);
        thumbnails.push({ fileThumbnail, type: "image", cid: "" });
      }
      // else if (
      //   file?.type.split("/")[0] === "video" &&
      //   file?.type.split("/")[1] === "mp4"
      // ) {
      //   const fileThumbnail = URL.createObjectURL(file);

      //   setImages([...images, { fileThumbnail, type: "video", cid: "" }]);
      //   setFiles([...files, file]);
      // }
    }
    setImages([...images, ...thumbnails]);
    setFiles([...files, ...fileArray]);
  }
  const deleteImages = (image: any, index: any) => {
    setImages([...images.filter((data: any) => data.fileThumbnail !== image)]);
    setFiles([...files.filter((file: any, i: any) => i !== index)]);
  };

  const handelValidateUpdateBrand = () => {
    if (!brandName) {
      message.error("Please Enter brand Name");
      return false;
    } else {
      return true;
    }
  };

  const handelValidateCreateBrand = () => {
    if (!brandName) {
      message.error("Please Enter brand Name");
      return false;
      // } else if (!brandUrl) {
      //   message.error("Please enter brand website URL");
      //   return false;
    } else if (!brandLogo) {
      message.error("Please upload brand logo");
      return false;
      // } else if (images?.length === 0 && !brandData?.brandDetails?.images) {
      //   message.error("Please upload product images");
      //   return false;
    } else {
      return true;
    }
  };

  const handleSave = async () => {
    // check all data present or not
    if (handelValidateCreateBrand()) {
      const cids = [];
      let brandLogoCid = null;
      setLoading(true);

      if (files?.length > 0) {
        for (let file of files) {
          try {
            const cid = await addFileOnWeb3(file);
            cids.push(cid);
          } catch (error) {
            message.error("Error in uploading product images");
          }
        }
      }
      if (file) {
        const cid = await addFileOnWeb3(file);
        brandLogoCid = cid;
      }
      if (userInfo?.brand?.length < 1) {
        dispatch(
          changeUserAsBrand({
            brandName,
            website: brandUrl,
            tagline,
            address,
            aboutBrand,
            logo: `https://ipfs.io/ipfs/${brandLogoCid}`,
            images: cids,
            phone: contactNumber,
            email,
            instagramId,
            facebookId,
            snapchat,
            linkedin,
            youtube,
            twitter,
            brandCategory,
            brandType,
            affiliatedUsers,
          })
        );
      }
      setLoading(false);
    }
  };

  const updateBrandData = async () => {
    // check all data present or not
    if (handelValidateUpdateBrand()) {
      const cids = [];
      let brandLogoCid = null;
      setLoading(true);

      if (files?.length > 0) {
        for (let file of files) {
          try {
            const cid = await addFileOnWeb3(file);
            cids.push(cid);
          } catch (error) {
            message.error("Error in uploading product images");
          }
        }
      }

      if (file) {
        const cid = await addFileOnWeb3(file);
        brandLogoCid = cid;
      }

      dispatch(
        updateBrandDetails({
          brandName,
          website: brandUrl,
          aboutBrand,
          tagline,
          address,
          logo: brandLogoCid
            ? `https://ipfs.io/ipfs/${brandLogoCid}`
            : brandData?.brandDetails?.logo,

          images: [...oldImages, ...cids],
          phone: contactNumber,
          email,
          instagramId,
          facebookId,
          snapchat,
          linkedin,
          youtube,
          twitter,
          brandCategory,
          brandType,
          affiliatedUsers,
        })
      );

      setLoading(false);
    }
  };
  useEffect(() => {
    if (!userInfo) {
      navigate("/");
    }
    dispatch({ type: GET_BRAND_DETAILS_RESET });
  }, [navigate, dispatch]);

  useEffect(() => {
    if (userInfo?.isBrand && userInfo?.brand?.length > 0 && !brandData) {
      dispatch(getBrandDetailsById(userInfo.brand[0]));
    }
    if (
      brandData &&
      userInfo?.isBrand &&
      brandData?._id === userInfo?.brand[0]
    ) {
      // console.log(
      //   "brandData?.brandDetails?.brandType : ",
      //   brandData?.brandDetails?.brandType
      // );
      setBrandName(brandData?.brandName);
      setBrandLogo(brandData?.brandDetails?.logo);
      // if (brandData?.brandDetails?.images.length > 0) {
      //   const x =
      //     brandData?.brandDetails?.images?.map(
      //       (cid: any) => `https://ipfs.io/ipfs/${cid}`
      //     ) || [];
      //   setImages(x);
      // }
      setContactNumber(brandData?.brandDetails?.phone);
      setEmail(brandData?.brandDetails?.email);
      setFacebookId(brandData?.brandDetails?.facebookId);
      setInstagramId(brandData?.brandDetails?.instagramId);
      setBrandUrl(brandData?.brandDetails?.website);
      setAboutBrand(brandData?.brandDetails?.aboutBrand);
      setTagline(brandData?.tagline);
      setAddress(brandData?.address);
      setSnapchat(brandData?.brandDetails?.snapchat);
      setLinkedin(brandData?.brandDetails?.linkedin);
      setYoutube(brandData?.brandDetails?.youtube);
      setTwitter(brandData?.brandDetails?.twitter);
      setBrandCategory(brandData?.brandCategory);
      setBrandType(brandData?.brandType);
      setOldImages(brandData?.brandDetails?.images);
      setAfiliatedUsers(brandData?.affiliatedUsers);
    }
  }, [dispatch, brandData, userInfo, navigate]);

  return (
    <Box px={{ base: "3", lg: "20" }} py={{ base: "75", lg: "75" }}>
      <Text
        fontSize={{ base: "sm", lg: "lg" }}
        fontWeight="600"
        color="#333333"
        m="0"
      >
        Public profile details
      </Text>
      <Text
        fontSize={{ base: "sm", lg: "md" }}
        fontWeight="400"
        color="#4E4E4E"
        m="0"
      >
        These details will be visible to public
      </Text>

      <Box width={{ base: "100%", lg: "80%" }}>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Flex align="center" gap="1">
              <Text
                fontSize={{ base: "sm", lg: "lg" }}
                fontWeight="600"
                color="red"
                m="0"
              >
                *
              </Text>
              <Text
                fontSize={{ base: "sm", lg: "lg" }}
                fontWeight="600"
                color="#333333"
                m="0"
              >
                Brand Name
              </Text>
            </Flex>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your brand name that will be visible to public
            </Text>
          </Flex>
          <Input
            borderColor="#939393"
            onChange={(e: any) => setBrandName(e.target.value)}
            placeholder="Enter brand name"
            py="2"
            value={brandName}
            borderRadius="4px"
          ></Input>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Website
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your website for public to visit
            </Text>
          </Flex>
          <InputGroup size="sm">
            {/* <InputLeftAddon children="https://" borderColor="#939393" /> */}
            <Input
              borderColor="#939393"
              value={brandUrl}
              py="2"
              placeholder="mysite"
              onChange={(e: any) => setBrandUrl(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>

        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>
        <SimpleGrid columns={[1, 1, 2]} spacing={{ base: "1", lg: "20" }}>
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Tagline
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Choose a quirky one-liner to explain about your brand{" "}
            </Text>
          </Flex>
          <Input
            borderColor="#939393"
            onChange={(e: any) => setTagline(e.target.value)}
            placeholder="Enter tagline"
            py="2"
            value={tagline}
            borderRadius="4px"
          ></Input>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt="5"
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Brand Category
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              What is the category of service for the brand{" "}
            </Text>
          </Flex>
          <Input
            borderColor="#939393"
            onChange={(e: any) => setBrandCategory(e.target.value)}
            placeholder="Enter brand category"
            py="2"
            value={brandCategory}
            borderRadius="4px"
          ></Input>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt="5"
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Brand Type
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              How does your brand operates{" "}
            </Text>
          </Flex>
          <Select
            size={"large"}
            placeholder="select brand type"
            value={brandType}
            onChange={setBrandType}
            style={{ width: "100%" }}
            options={brandTypeOption}
          />
        </SimpleGrid>
        {brandType === "OFFLINE" || brandType === "HYBRID" ? (
          <SimpleGrid
            columns={[1, 1, 2]}
            spacing={{ base: "1", lg: "20" }}
            pt={{ base: "5", lg: "5" }}
          >
            <Flex direction="column">
              <Text
                fontSize={{ base: "sm", lg: "lg" }}
                fontWeight="600"
                color="#333333"
                m="0"
              >
                affiliated users
              </Text>
              <Text
                fontSize={{ base: "sm", lg: "md" }}
                fontWeight="400"
                color="#4E4E4E"
                m="0"
              >
                Select users
              </Text>
            </Flex>
            <DebounceSelect
              mode="multiple"
              value={affiliatedUsers}
              placeholder="Select users"
              fetchOptions={fetchUserList}
              onChange={(newValue) => {
                setAfiliatedUsers(newValue);
              }}
              style={{ width: "100%" }}
              size="large"
            />
          </SimpleGrid>
        ) : null}
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt="5"
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              About
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              A quick description of your brand/company
            </Text>
          </Flex>
          <TextArea
            showCount
            maxLength={500}
            value={aboutBrand}
            style={{ height: 120, marginBottom: 24 }}
            onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
              setAboutBrand(e.target.value);
            }}
            placeholder="Write about your brand"
          />
        </SimpleGrid>
        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>

        <Text
          fontSize={{ base: "sm", lg: "lg" }}
          fontWeight="600"
          color="#333333"
          m="0"
        >
          Photos{" "}
        </Text>
        <Text
          fontSize={{ base: "sm", lg: "md" }}
          fontWeight="400"
          color="#4E4E4E"
          m="0"
        >
          The brand all details will be shown here{" "}
        </Text>

        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "3", lg: "5" }}
        >
          <Flex direction="column">
            <Flex align="center" gap="1">
              <Text
                fontSize={{ base: "sm", lg: "lg" }}
                fontWeight="600"
                color="red"
                m="0"
              >
                *
              </Text>
              <Text
                fontSize={{ base: "sm", lg: "lg" }}
                fontWeight="600"
                color="#333333"
                m="0"
              >
                Brand Logo
              </Text>
            </Flex>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Change or add new brand logo that will be visible to public
            </Text>
          </Flex>
          <Flex gap="5">
            {brandLogo && (
              <Box>
                <Image
                  src={brandLogo}
                  width="76px"
                  height="86px"
                  borderRadius="100%"
                />
              </Box>
            )}
            <Button
              variant="outline"
              width="76px"
              height="86px"
              onClick={() => hiddenInput.click()}
              borderColor="#8B8B8B"
              color="#8B8B8B"
            >
              <AiOutlinePlusCircle size="30px" />
            </Button>
            <Input
              hidden
              type="file"
              ref={(el) => (hiddenInput = el)}
              accept="image/png, image/jpeg"
              onChange={(e: any) => handleLogoSelect(e.target.files[0])}
            />
          </Flex>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "2", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Brand product images/videos
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Change or add new brand images and videos that will be visible to
              public
            </Text>
          </Flex>
          <HStack spacing={{ base: "1", lg: "3" }}>
            <HStack spacing={{ base: "1", lg: "3" }}>
              {images?.map((data: any, index: any) =>
                data.type === "image" ? (
                  <Image
                    key={index}
                    src={data.fileThumbnail}
                    width="76px"
                    height="86px"
                    onClick={() => deleteImages(data.fileThumbnail, index)}
                  />
                ) : (
                  <Box
                    key={index}
                    as="video"
                    src={data.fileThumbnail}
                    autoPlay
                    loop
                    muted
                    display="inline-block"
                    width="100px"
                    height="100px"
                    onClick={() => deleteImages(data.fileThumbnail, index)}
                  ></Box>
                )
              )}
            </HStack>

            <Button
              variant="outline"
              width="76px"
              height="86px"
              onClick={() => hiddenInput1.click()}
              borderColor="#8B8B8B"
              color="#8B8B8B"
            >
              <AiOutlinePlusCircle size="30px" />
            </Button>

            <Input
              hidden
              multiple
              type="file"
              ref={(el) => (hiddenInput1 = el)}
              accept="image/png, image/jpeg, image/jpg"
              onChange={(e: any) => handlePhotoSelect(e.target.files)}
            />
          </HStack>
        </SimpleGrid>
        <Text
          fontSize={{ base: "sm", lg: "md" }}
          fontWeight="400"
          color="#4E4E4E"
          m="0"
        >
          Previously uploaded
        </Text>
        <SimpleGrid columns={[3, 3, 6]} spacing="1px">
          {brandData?.brandDetails?.images.map((img: any, index: any) => (
            <Image
              key={index}
              p="1"
              src={`https://ipfs.io/ipfs/${img}`}
              width="76px"
              height="86px"
              // onClick={() => deleteImages(img, index)}
            />
          ))}
        </SimpleGrid>
        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>
        <Text
          fontSize={{ base: "sm", lg: "lg" }}
          fontWeight="600"
          color="#333333"
          m="0"
        >
          Contact details
        </Text>
        <Text
          fontSize={{ base: "sm", lg: "md" }}
          fontWeight="400"
          color="#4E4E4E"
          m="0"
        >
          Enter your brand name that will be visible to public{" "}
        </Text>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Helpline number
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your brand helpline number
            </Text>
          </Flex>
          <InputGroup>
            <InputLeftAddon children="+91" borderColor="#939393" py="3" />
            <Input
              type="tel"
              placeholder="phone number"
              borderColor="#939393"
              value={contactNumber}
              onChange={(e: any) => setContactNumber(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Helpline email
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your brand helpline email
            </Text>
          </Flex>
          <InputGroup>
            <Input
              type="email"
              py="3"
              placeholder="Enter brand email"
              borderColor="#939393"
              value={email}
              onChange={(e: any) => setEmail(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Registered Address
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Please enter your office's registered address
            </Text>
          </Flex>
          <InputGroup>
            <Input
              // type="string"
              py="3"
              placeholder="Enter brand address"
              borderColor="#939393"
              value={address}
              onChange={(e: any) => setAddress(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>
        <Text
          fontSize={{ base: "sm", lg: "lg" }}
          fontWeight="600"
          color="#333333"
          m="0"
        >
          Social profiles{" "}
        </Text>
        <Text
          fontSize={{ base: "sm", lg: "md" }}
          fontWeight="400"
          color="#4E4E4E"
          m="0"
        >
          Enter your social profiles here{" "}
        </Text>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Instagram
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your brand's instagram username
            </Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="instagram.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={instagramId}
              onChange={(e: any) => setInstagramId(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Facebook
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your brand's facebook username
            </Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="facebook.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={facebookId}
              onChange={(e: any) => setFacebookId(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Snapchat
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your brand's snapchat username
            </Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="snapchat.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={snapchat}
              onChange={(e: any) => setSnapchat(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Linkedin
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your brand's linkedin username
            </Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="linkedin.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={linkedin}
              onChange={(e: any) => setLinkedin(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Youtube
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your brand's youtube username
            </Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="youtube.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={youtube}
              onChange={(e: any) => setYoutube(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              X
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your brand's x.com username
            </Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="x.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={twitter}
              onChange={(e: any) => setTwitter(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>
        <Flex justifyContent="flex-end" gap="5">
          {userInfo?.isBrand && userInfo?.brand[0] ? (
            <Button
              py="3"
              px="5"
              variant="outline"
              color="#0EBCF5"
              bgColor="#FFFFFF"
              _hover={{ color: "#FFFFFF", bgColor: "#0EBCF5" }}
              onClick={updateBrandData}
              isLoading={loadingUpdateBrnad || loading || loadingBrnad}
              loadingText="updating..."
            >
              Update
            </Button>
          ) : (
            <Button
              py="3"
              px="5"
              variant="outline"
              color="#0EBCF5"
              bgColor="#FFFFFF"
              _hover={{ color: "#FFFFFF", bgColor: "#0EBCF5" }}
              onClick={handleSave}
              isLoading={loadingBrnad || loading || loadingUpdateBrnad}
              loadingText="Saving..."
            >
              Save
            </Button>
          )}
        </Flex>
      </Box>
    </Box>
  );
}
