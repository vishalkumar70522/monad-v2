import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Flex,
  Text,
  Image,
  // Center,
  // Divider,
  SimpleGrid,
  Stack,
  Show,
  Hide,
  UnorderedList,
  ListItem,
} from "@chakra-ui/react";
import { Skeleton } from "antd";

import { useDispatch, useSelector } from "react-redux";
import { getBrandDetailsById } from "../../Actions/brandAction";
import { getCouponListForBrand } from "../../Actions/couponAction";
import { GiRoundStar } from "react-icons/gi";
import { FiExternalLink } from "react-icons/fi";
import { TbShare3 } from "react-icons/tb";
import { FaStar } from "react-icons/fa";
import { useNavigate } from "react-router";
import { BsCheckCircleFill } from "react-icons/bs";
import { MdPeopleAlt } from "react-icons/md";
import { SlArrowDown, SlArrowUp } from "react-icons/sl";
import { getCampaignListForBrand } from "../../Actions/campaignAction";
import { ImageSliderPopup } from "../../components/commans";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { couponicon } from "../../assets/funkfeets";
export function BrandProfileView() {
  const brandId = window.location.pathname.split("/")[2];
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();

  const [aboutView, setAboutView] = useState<Boolean>(false);
  const [dealsView, setDealsView] = useState<Boolean>(true);
  const [detailsView, setDetailsView] = useState<Boolean>(false);
  const [imageShow, setImageShow] = useState<any>(false);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const campaignListForBrand = useSelector(
    (state: any) => state.campaignListForBrand
  );
  const {
    loading: loadingMyVideos,
    error: errorMyVideos,
    campaigns: myVideos,
  } = campaignListForBrand;

  const brandDetails = useSelector((state: any) => state.brandDetails);
  const {
    loading: loadingBrandData,
    brand: brandData,
    error: errorBrandData,
  } = brandDetails;
  const couponListForBrand = useSelector(
    (state: any) => state.couponListForBrand
  );
  const {
    loading: loadingCouponList,
    error: errorCouponList,
    coupons,
  } = couponListForBrand;

  useEffect(() => {
    dispatch(getBrandDetailsById(brandId));
    dispatch(getCampaignListForBrand(brandId));
    dispatch(getCouponListForBrand(brandId));
  }, [dispatch, brandId]);

  // console.log(
  //   Array.from(new Set(myVideos?.map((vid: any) => vid.video)))
  // );
  return (
    <Box>
      {brandData && (
        <ImageSliderPopup
          imageShow={imageShow}
          setImageShow={(value: any) => setImageShow(value)}
          images={brandData?.brandDetails?.images}
        />
      )}
      <Show below="md">
        {loadingBrandData ? (
          <Box>
            <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
          </Box>
        ) : (
          <>
            <Box
              pt="75px"
              // height="400px"
              background="linear-gradient(to bottom, #93BFD8, #A1B6EB28)"
            >
              <SimpleGrid columns={[2]}>
                <Stack pt="15px" px="10px" align="center">
                  <Image
                    // bg="linear-gradient(to bottom, #A1B6EB28, #93BFD8)"
                    rounded="md"
                    width="100%"
                    height="200px"
                    src={brandData?.brandDetails?.logo}
                  />
                </Stack>
                <Stack pt="15px" px="10px">
                  <Image
                    bg="#ffffff"
                    rounded="md"
                    height="200px"
                    width="100%"
                    onClick={() => setImageShow(true)}
                    src={`https://ipfs.io/ipfs/${brandData?.brandDetails?.images[0]}`}
                  />
                  <Stack align="end">
                    <Button
                      color="#02020280"
                      py="6px"
                      // px="2px"
                      // mr="2px"
                      width="40%"
                      fontSize="10px"
                      bgColor="#ffffff"
                      onClick={() => setImageShow(true)}
                    >
                      See all photos
                    </Button>
                  </Stack>
                </Stack>
              </SimpleGrid>
              <Box px="15px" pb="10px">
                <Text fontSize="24px" fontWeight="600">
                  {brandData?.brandName}
                </Text>
                <Text fontSize="14px">{brandData?.tagline}</Text>
                <Text fontSize="12px">{brandData?.address}</Text>
              </Box>
            </Box>
            <Box py="10px" px="10px" bg="#f2f2f2">
              <Flex justify="space-between">
                <Flex>
                  <Button
                    color="#ffffff"
                    bgColor="#D7380E"
                    fontSize="sm"
                    width="50%"
                    px="40px"
                    py="5px"
                    // mb="10px"
                    rightIcon={<FiExternalLink color="white" fontSize="20px" />}
                    onClick={() =>
                      brandData?.brandDetails?.website.split(":")[0] !== "https"
                        ? window.open(
                            `https://${brandData?.brandDetails?.website}`
                          )
                        : window.open(brandData?.brandDetails?.website)
                    }
                  >
                    Visit site
                  </Button>
                  <Flex px="5" align="center">
                    <Text
                      my="2"
                      mx="1"
                      fontWeight="600"
                      textAlign="center"
                      fontSize="md"
                    >
                      Share
                    </Text>
                    <TbShare3 color="#02020290" fontSize="20px" />
                  </Flex>
                </Flex>
                <Flex align="center">
                  <FaStar color="#FCA902" fontSize="30px" />
                  <Box mx="2" px="2" bg="#ffffff">
                    <Text fontWeight="600" fontSize="md" my="1">
                      {brandData?.ratings || 0} / 5
                    </Text>
                  </Box>
                </Flex>
              </Flex>
            </Box>
            <Box
              mt="5px"
              // mx="15px"
              mb="20px"
              p="5px"
              boxShadow="md"
              background="#ffffff"
              rounded="sm"
            >
              <Flex>
                <Button
                  background={dealsView ? "#0EBCF5" : "#FFFFFF"}
                  color={dealsView ? "#FFFFFF" : "#020202"}
                  _hover={{ color: "#0EBCF5", bgColor: "#ffffff" }}
                  mx="5px"
                  px="20px"
                  py="10px"
                  onClick={() => {
                    setDealsView(true);
                    setAboutView(false);
                  }}
                >
                  All Deals ({coupons?.length})
                </Button>
                <Button
                  background={aboutView ? "#0EBCF5" : "#FFFFFF"}
                  color={aboutView ? "#FFFFFF" : "#020202f"}
                  _hover={{ color: "#0EBCF5", bgColor: "#ffffff" }}
                  mx="5px"
                  px="20px"
                  py="10px"
                  onClick={() => {
                    setAboutView(true);
                    setDealsView(false);
                  }}
                >
                  About
                </Button>
              </Flex>
            </Box>
            {aboutView && (
              <>
                <Box mx="15px" rounded="sm" boxShadow="md">
                  <Box pt="5" px="5">
                    <Text fontSize="12px" fontWeight="600">
                      About
                    </Text>
                    <Text fontSize="14px" fontWeight="">
                      {brandData?.brandDetails?.aboutBrand}
                    </Text>
                  </Box>
                  <Box pt="3" px="5">
                    <Text fontSize="12px" fontWeight="1000">
                      Phone
                    </Text>
                    <Text fontSize="14px" fontWeight="">
                      {brandData?.brandDetails?.phone}
                    </Text>
                  </Box>
                  <Box pt="3" px="5">
                    <Text fontSize="12px" fontWeight="1000">
                      Email
                    </Text>
                    <Text fontSize="14px" fontWeight="">
                      {brandData?.brandDetails?.email}
                    </Text>
                  </Box>
                  <Box py="3" px="5">
                    <Text fontSize="12px" fontWeight="1000">
                      Website
                    </Text>
                    <Text fontSize="14px" fontWeight="">
                      {brandData?.brandDetails?.website}
                    </Text>
                  </Box>
                </Box>
                <Box mx="15px" pt="2" rounded="sm" boxShadow="md">
                  <Box py="3" px="5">
                    <Text fontSize="12px" fontWeight="1000">
                      Address
                    </Text>
                    <Text fontSize="14px" fontWeight="">
                      {brandData?.address}
                    </Text>
                  </Box>
                </Box>
              </>
            )}
            {loadingCouponList ? (
              <Box>
                <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
              </Box>
            ) : (
              <>
                {dealsView && (
                  <>
                    <Box
                      mx="15px"
                      // pt="2"
                      rounded="sm"
                    >
                      {coupons?.map((coupon: any, index: any) => (
                        <Box pb="3" key={index}>
                          <Box
                            px="5"
                            pt="3"
                            bg="#ffffff"
                            rounded="sm"
                            boxShadow="md"
                          >
                            <Flex>
                              <Flex p="1" align="center">
                                <BsCheckCircleFill color="green" />
                                <Text fontSize="12px" m="1">
                                  Verified
                                </Text>
                              </Flex>
                              <Flex p="1" align="center">
                                <MdPeopleAlt color="#02020250" />
                                <Text fontSize="12px" m="1">
                                  {coupon.rewardCoupons.length} people used
                                </Text>
                              </Flex>
                            </Flex>
                            <Flex direction="column">
                              <Text fontSize="20px" fontWeight="600">
                                {coupon?.offerName}
                              </Text>
                              <Text fontSize="14px" fontWeight="">
                                {coupon?.offerDetails}
                              </Text>
                              <Button
                                color="#ffffff"
                                bgColor="#D7380E"
                                fontSize="md"
                                width="50%"
                                px="20px"
                                py="10px"
                                mb="10px"
                                onClick={() =>
                                  navigate("/couponDetils", {
                                    state: {
                                      data: coupon,
                                      path: "/",
                                      showCouponCode: false,
                                    },
                                  })
                                }
                              >
                                Grab Deal
                              </Button>
                            </Flex>
                          </Box>
                          <Box
                            bg="#F6F6F6"
                            onClick={() => setDetailsView(!detailsView)}
                          >
                            <Flex
                              // p="1"
                              align="center"
                            >
                              <Text
                                color="#0EBCF5"
                                pl="20px"
                                pr="10px"
                                mt="13px"
                              >
                                {detailsView ? "Hide Details" : "Show Details"}
                              </Text>
                              {detailsView ? (
                                <SlArrowUp color="#0EBCF5" fontSize="" />
                              ) : (
                                <SlArrowDown color="#0EBCF5" fontSize="" />
                              )}
                            </Flex>
                            {detailsView ? (
                              <Stack
                                px="5"
                                onClick={() => setDetailsView(!detailsView)}
                              >
                                {/* <Text fontSize="12px" fontWeight="600">
                                  {coupon?.offerDetails}
                                </Text> */}
                                <UnorderedList>
                                  <ListItem fontSize="12px">
                                    {coupon?.couponRewardInfo?.validity?.to
                                      ? `Coupon valid till ${convertIntoDateAndTime(
                                          coupon?.couponRewardInfo?.validity?.to
                                        )} `
                                      : `This coupon has no expiry date, you can use when ever you want.`}{" "}
                                  </ListItem>
                                  <ListItem fontSize="12px">{`This is a ${coupon?.couponRewardInfo?.couponType} coupon.`}</ListItem>
                                  <ListItem fontSize="12px">{`You can use this coupon only ${coupon?.couponRewardInfo?.redeemFrequency} times.`}</ListItem>
                                </UnorderedList>
                              </Stack>
                            ) : null}
                          </Box>
                        </Box>
                      ))}
                    </Box>
                  </>
                )}
              </>
            )}
            <Box
              my="5px"
              px="3"
              py="6"
              // width="347px"
              // height="370px"
              background="#ffffff"
              rounded="8px"
              boxShadow="md"
            >
              <Text fontSize="20px" fontWeight="600">
                Brand Campaigns
              </Text>
              {loadingMyVideos ? (
                <>Loading...</>
              ) : (
                <SimpleGrid columns={[2]} spacingY="20px" spacingX="25px">
                  {Array.from(
                    new Set(myVideos?.map((vid: any) => vid.video))
                  ).map((video: any, index: any) => (
                    <Stack
                      // width="150px"
                      // height="57px"
                      // border="1px solid black"
                      key={index}
                    >
                      <Box
                        //
                        as="video"
                        src={video}
                        width="100%"
                        height="100%"
                        objectFit="cover"
                      />
                    </Stack>
                  ))}
                </SimpleGrid>
              )}
            </Box>
          </>
        )}
      </Show>
      <Hide below="md">
        {loadingBrandData ? (
          <>Loading</>
        ) : (
          <Box
            height="400px"
            background="linear-gradient(to bottom, #93BFD8, #A1B6EB28)"
          >
            <Flex pt="111px" pl="70px">
              <Stack>
                <Box
                  my="5px"
                  px="10"
                  py="8"
                  width="347px"
                  height="370px"
                  background="#ffffff"
                  rounded="8px"
                  boxShadow="md"
                >
                  <Stack alignSelf="center" background="#222222" rounded="8px">
                    <Image
                      align="center"
                      width="279px"
                      height="137px"
                      src={brandData?.brandDetails?.logo}
                      alt=""
                    />
                  </Stack>
                  <Stack>
                    <Flex justify="space-between">
                      <Flex pt="5">
                        <GiRoundStar
                          size="30px"
                          color={brandData?.rating >= 1 ? "#0EBCF5" : "#E2E2E2"}
                          // onClick={() => setUserScreenRating(1)}
                        />
                        <GiRoundStar
                          size="30px"
                          color={brandData?.rating >= 2 ? "#0EBCF5" : "#E2E2E2"}
                          // onClick={() => setUserScreenRating(2)}
                        />
                        <GiRoundStar
                          size="30px"
                          color={brandData?.rating >= 3 ? "#0EBCF5" : "#E2E2E2"}
                          // onClick={() => setUserScreenRating(3)}
                        />
                        <GiRoundStar
                          size="30px"
                          color={brandData?.rating >= 4 ? "#0EBCF5" : "#E2E2E2"}
                          // onClick={() => setUserScreenRating(4)}
                        />
                        <GiRoundStar
                          size="30px"
                          color={brandData?.rating >= 5 ? "#0EBCF5" : "#E2E2E2"}
                          // onClick={() => setUserScreenRating(5)}
                        />
                      </Flex>
                      <Flex pt="30px">
                        <Text fontSize="13px" fontWeight="600">
                          Rate now
                        </Text>
                      </Flex>
                    </Flex>
                    <Text fontSize="13px">
                      {brandData?.rating || 0} Ratings &{" "}
                      {brandData?.reviews.length} Reviews
                    </Text>
                  </Stack>
                  <Stack align="center" py="3">
                    <Button
                      color="#ffffff"
                      bgColor="#D7380E"
                      fontSize="sm"
                      width="80%"
                      p="2"
                      rightIcon={
                        <FiExternalLink color="white" fontSize="20px" />
                      }
                      onClick={() =>
                        brandData?.brandDetails?.website.split(":")[0] !==
                        "https"
                          ? window.open(
                              `https://${brandData?.brandDetails?.website}`
                            )
                          : window.open(brandData?.brandDetails?.website)
                      }
                    >
                      Visit Site
                    </Button>
                    <Flex px="5" align="center">
                      <Text
                        my="1"
                        mx="1"
                        fontWeight="600"
                        textAlign="center"
                        fontSize="md"
                      >
                        Share
                      </Text>
                      <TbShare3 color="#02020290" fontSize="20px" />
                    </Flex>
                  </Stack>
                </Box>
                <Box
                  my="5px"
                  px="3"
                  py="6"
                  width="347px"
                  // height="370px"
                  background="#ffffff"
                  rounded="8px"
                  boxShadow="md"
                >
                  <Text fontSize="20px" fontWeight="600">
                    Brand Campaigns
                  </Text>
                  {loadingMyVideos ? (
                    <Box>
                      <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
                    </Box>
                  ) : (
                    <SimpleGrid columns={[2]} spacingY="20px" spacingX="25px">
                      {Array.from(
                        new Set(myVideos?.map((vid: any) => vid.video))
                      ).map((video: any, index: any) => (
                        <Stack
                          width="150px"
                          height="57px"
                          // border="1px solid black"
                          key={index}
                        >
                          <Box
                            //
                            as="video"
                            src={video}
                            width="100%"
                            height="100%"
                            objectFit="cover"
                          />
                        </Stack>
                      ))}
                    </SimpleGrid>
                  )}
                </Box>
              </Stack>
              <Stack width="100%" p="10px">
                <Flex justify="space-between">
                  <Box px="60px" pt="20px">
                    <Text fontSize="32px" fontWeight="600">
                      {brandData?.brandName}
                    </Text>
                    <Text fontSize="16px">
                      {/* Tagline */}
                      {brandData?.tagline}
                    </Text>
                    <Flex>
                      <Text fontSize="13px">{brandData?.address}</Text>
                    </Flex>
                    <Flex py="10px" width="100%">
                      <Image
                        px="1"
                        pb="1"
                        src={couponicon}
                        alt="couponIcon"
                        width="40px"
                        height="40px"
                      />
                      <Text
                        align="center"
                        color="#00A707"
                        fontSize="24px"
                        fontWeight="600"
                      >
                        Offers available
                      </Text>
                    </Flex>
                  </Box>
                  <Stack
                    align="end"
                    mt="10px"
                    mr={{ base: "10px", lg: "200px" }}
                    onClick={() => setImageShow(true)}
                  >
                    <Image
                      width="250px"
                      height="200px"
                      src={`https://ipfs.io/ipfs/${brandData?.brandDetails?.images[0]}`}
                    />
                    <Button
                      color="#02020250"
                      py="5px"
                      px="10px"
                      mr="2px"
                      bgColor="#ffffff80"
                      // onClick={() => setImageShow(true)}
                    >
                      See all photos
                    </Button>
                  </Stack>
                </Flex>
                <Box
                  mt="5px"
                  mx="15px"
                  mb="20px"
                  p="5px"
                  // width="95%"
                  boxShadow="md"
                  background="#ffffff"
                  rounded="sm"
                >
                  <Flex>
                    <Button
                      background={dealsView ? "#ffffff" : "#0EBCF5"}
                      color={dealsView ? "#020202" : "#ffffff"}
                      _hover={{ color: "#0EBCF5", bgColor: "#ffffff" }}
                      mx="5px"
                      px="20px"
                      py="10px"
                      onClick={() => {
                        setDealsView(true);
                        setAboutView(false);
                      }}
                    >
                      All Deals
                    </Button>
                    <Button
                      background={aboutView ? "#ffffff" : "#0EBCF5"}
                      color={aboutView ? "#020202" : "#ffffff"}
                      _hover={{ color: "#0EBCF5", bgColor: "#ffffff" }}
                      mx="5px"
                      px="20px"
                      py="10px"
                      onClick={() => {
                        setAboutView(true);
                        setDealsView(false);
                      }}
                    >
                      About
                    </Button>
                  </Flex>
                </Box>
                {aboutView && (
                  <>
                    <Box mx="15px" rounded="sm" bg="#F6F6F6">
                      <Box pt="5" px="5">
                        <Text fontSize="14px" fontWeight="600">
                          About
                        </Text>
                        <Text fontSize="16px" fontWeight="">
                          {brandData?.brandDetails?.aboutBrand}
                        </Text>
                      </Box>
                      <Box pt="3" px="5">
                        <Text fontSize="14px" fontWeight="1000">
                          Phone
                        </Text>
                        <Text fontSize="16px" fontWeight="">
                          {brandData?.brandDetails?.phone}
                        </Text>
                      </Box>
                      <Box pt="3" px="5">
                        <Text fontSize="14px" fontWeight="1000">
                          Email
                        </Text>
                        <Text fontSize="16px" fontWeight="">
                          {brandData?.brandDetails?.email}
                        </Text>
                      </Box>
                      <Box pt="3" px="5">
                        <Text fontSize="14px" fontWeight="1000">
                          Website
                        </Text>
                        <Text fontSize="16px" fontWeight="">
                          {brandData?.brandDetails?.website}
                        </Text>
                      </Box>
                    </Box>
                    <Box mx="15px" pt="2" rounded="sm" bg="#F6F6F6">
                      <Box pt="3" px="5">
                        <Text fontSize="14px" fontWeight="1000">
                          Address
                        </Text>
                        <Text fontSize="16px" fontWeight="">
                          {brandData?.address}
                        </Text>
                      </Box>
                    </Box>
                  </>
                )}
                {loadingCouponList ? (
                  <Box>
                    <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
                  </Box>
                ) : (
                  <>
                    {dealsView && (
                      <Box
                        mx="15px"
                        // pt="2"
                        rounded="sm"
                      >
                        {coupons?.map((coupon: any, index: any) => (
                          <Box my="" key={index}>
                            <Box
                              px="5"
                              // pt="3"
                              bg="#ffffff"
                              rounded="sm"
                              boxShadow="md"
                            >
                              <Flex>
                                <Flex p="1" align="center">
                                  <BsCheckCircleFill color="green" />
                                  <Text fontSize="12px" m="1">
                                    Verified
                                  </Text>
                                </Flex>
                                <Flex p="1" align="center">
                                  <MdPeopleAlt color="#02020250" />
                                  <Text fontSize="12px" m="1">
                                    {coupon?.rewardCoupons.length} people used
                                  </Text>
                                </Flex>
                              </Flex>
                              <Box pb="3">
                                <Flex justify="space-between">
                                  <Text fontSize="25" fontWeight="600">
                                    {coupon?.offerName}
                                  </Text>
                                  <Button
                                    color="#ffffff"
                                    bgColor="#D7380E"
                                    fontSize="md"
                                    px={{ base: "20", lg: "" }}
                                    onClick={() =>
                                      navigate("/couponDetils", {
                                        state: {
                                          data: coupon,
                                          path: "/",
                                          showCouponCode: false,
                                        },
                                      })
                                    }
                                  >
                                    Grab Deal
                                  </Button>
                                </Flex>
                                <Text fontSize="12px">
                                  {coupon?.offerDetails}
                                </Text>
                              </Box>
                            </Box>
                            <Box mb="3" bg="#F6F6F6">
                              <Flex
                                // p="1"
                                align="center"
                                onClick={() => setDetailsView(!detailsView)}
                              >
                                <Text
                                  color="#0EBCF5"
                                  pl="20px"
                                  pr="10px"
                                  mt="13px"
                                >
                                  {detailsView
                                    ? "Hide Details"
                                    : "Show Details"}
                                </Text>
                                {detailsView ? (
                                  <SlArrowUp color="#0EBCF5" fontSize="" />
                                ) : (
                                  <SlArrowDown color="#0EBCF5" fontSize="" />
                                )}
                              </Flex>
                              {detailsView ? (
                                <Stack px="5">
                                  {/* <Text fontSize="16px">
                                      {coupon.offerDetails}
                                    </Text> */}
                                  <UnorderedList>
                                    <ListItem fontSize="12px">
                                      {coupon?.couponRewardInfo?.validity?.to
                                        ? `Coupon valid till ${convertIntoDateAndTime(
                                            coupon?.couponRewardInfo?.validity
                                              ?.to
                                          )} `
                                        : `This coupon has no expiry date, you can use when ever you want.`}{" "}
                                    </ListItem>
                                    <ListItem fontSize="12px">{`This is a ${coupon?.couponRewardInfo?.couponType} coupon.`}</ListItem>
                                    <ListItem fontSize="12px">{`You can use this coupon only ${coupon?.couponRewardInfo?.redeemFrequency} times.`}</ListItem>
                                  </UnorderedList>
                                </Stack>
                              ) : null}
                            </Box>
                          </Box>
                        ))}
                      </Box>
                    )}
                  </>
                )}
              </Stack>
            </Flex>
          </Box>
        )}
      </Hide>
    </Box>
  );
}
