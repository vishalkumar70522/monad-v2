import {
  Box,
  Button,
  Divider,
  Flex,
  HStack,
  Image,
  Input,
  InputGroup,
  InputLeftAddon,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import TextArea from "antd/es/input/TextArea";
import { useEffect, useState } from "react";
import { AiOutlinePlusCircle } from "react-icons/ai";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";

import {
  changeUserAsCreator,
  getCreatorDetailsById,
  updateCreatorDetails,
} from "../../Actions/creatorAction";
import { message } from "antd";
import { addFileOnWeb3 } from "../../utils/web3";

export function EditCreatorProfileData(props: any) {
  const dispatch = useDispatch<any>();
  const [creatorName, setCreatorName] = useState<any>("");
  const [aboutCreator, setAboutCreator] = useState<any>("");
  const [creatorLogo, setCreatorLogo] = useState<any>(null);
  const [file, setFile] = useState<any>(null);
  const [images, setImages] = useState<any>([]);
  const [files, setFiles] = useState<any>([]);
  const [contactNumber, setContactNumber] = useState<any>("");
  const [email, setEmail] = useState<any>("");
  const [instagramId, setInstagramId] = useState<any>("");
  const [facebookId, setFacebookId] = useState<any>("");
  const [snapchatId, setSnapchatId] = useState<any>("");
  const [tiktokId, setTiktokId] = useState<any>("");
  const [loading, setLoading] = useState<any>(false);

  let hiddenInput: any = null;
  let hiddenInput1: any = null;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const createNewCreator = useSelector((state: any) => state.createNewCreator);
  const { loading: loadingBrnad, creator, success, error } = createNewCreator;

  const updateCreator = useSelector((state: any) => state.updateCreator);
  const {
    loading: loadingUpdateCreator,
    creator: updatedCreator,
    success: successUpdateCreator,
    error: errorUpdateCreator,
  } = updateCreator;

  const creatorDetails = useSelector((state: any) => state.creatorDetails);
  const {
    loading: loadingCreatorDetails,
    creator: creatorData,
    error: errorCreatorDetails,
  } = creatorDetails;

  useEffect(() => {
    if (error) {
      message.error(error);
    }
    if (success) {
      message.success("Creator created!");
    }
    if (successUpdateCreator) {
      message.success("Creator updated succesfully!");
      dispatch({ type: "UPDATE_CREATOR_RESET" });
    }
    if (errorUpdateCreator) {
      message.success(errorUpdateCreator);
      dispatch({ type: "UPDATE_CREATOR_RESET" });
    }
  }, [dispatch, error, success, errorUpdateCreator, successUpdateCreator]);

  function handleLogoSelect(file: any) {
    const fileThumbnail = URL.createObjectURL(file);
    setCreatorLogo(fileThumbnail);
    setFile(file);
  }
  async function handlePhotoSelect(file: any) {
    if (file?.type.split("/")[0] === "image") {
      const fileThumbnail = URL.createObjectURL(file);

      setImages([...images, { fileThumbnail, type: "image", cid: "" }]);
      setFiles([...files, file]);
    } else if (
      file?.type.split("/")[0] === "video" &&
      file?.type.split("/")[1] === "mp4"
    ) {
      const fileThumbnail = URL.createObjectURL(file);

      setImages([...images, { fileThumbnail, type: "video", cid: "" }]);
      setFiles([...files, file]);
    }
  }
  const deleteImages = (image: any, index: any) => {
    setImages([...images.filter((data: any) => data.fileThumbnail !== image)]);
    setFiles([...files.filter((file: any, i: any) => i !== index)]);
  };

  const handelValidate = () => {
    if (!creatorName) {
      message.error("Please Enter creator Name");
      return false;
    } else if (!creatorLogo) {
      message.error("Please upload creator logo");
      return false;
    } else if (images?.length === 0) {
      message.error("Please upload product images");
      return false;
    } else {
      return true;
    }
  };

  const handleSave = async () => {
    // check all data present or not
    if (handelValidate()) {
      if (files?.length > 0) {
        setLoading(true);
        const cids = [];
        let creatorLogoCid = null;
        for (let file of files) {
          try {
            const cid = await addFileOnWeb3(file);
            cids.push(cid);
          } catch (error) {
            message.error("Error in uploading product images");
          }
        }
        if (file) {
          const cid = await addFileOnWeb3(file);
          creatorLogoCid = cid;
        }
        const socialId = [{}];
        dispatch(
          changeUserAsCreator({
            creatorName,
            socialId,
            aboutCreator,
            logo: `https://ipfs.io/ipfs/${creatorLogoCid}`,
            images: cids,
            phone: contactNumber,
            email,
            instagramId,
            facebookId,
          })
        );

        setLoading(false);
      }
    }
  };

  const updateCreatorData = async () => {
    // check all data present or not
    if (handelValidate()) {
      if (files?.length > 0) {
        setLoading(true);
        const cids = [];
        let creatorLogoCid = null;
        for (let file of files) {
          try {
            const cid = await addFileOnWeb3(file);
            cids.push(cid);
          } catch (error) {
            message.error("Error in uploading product images");
          }
        }
        if (file) {
          const cid = await addFileOnWeb3(file);
          creatorLogoCid = cid;
        }
        const socialId = [{}];
        dispatch(
          updateCreatorDetails({
            creatorName,
            socialId,
            aboutCreator,
            logo:
              creatorData?.creatorDetails?.logo ||
              `https://ipfs.io/ipfs/${creatorLogoCid}`,
            images: cids,
            phone: contactNumber,
            email,
            instagramId,
            facebookId,
          })
        );

        setLoading(false);
      } else {
        let creatorLogoCid = null;
        if (file) {
          const cid = await addFileOnWeb3(file);
          creatorLogoCid = cid;
          const socialId = [{}];
          dispatch(
            updateCreatorDetails({
              creatorName,
              socialId,
              aboutCreator,
              logo: `https://ipfs.io/ipfs/${creatorLogoCid}` || creatorLogo,
              images: images || creatorData?.creatorDetails?.images,
              phone: contactNumber,
              email,
              instagramId,
              facebookId,
            })
          );
        }

        setLoading(false);
      }
    }
  };

  useEffect(() => {
    if (userInfo?.isCreator && !creatorData && userInfo?.creator?.length > 0) {
      dispatch(getCreatorDetailsById(userInfo?.creator[0]));
    }
    if (creatorData) {
      setCreatorName(creatorData?.creatorName);
      setCreatorLogo(creatorData?.creatorDetails?.logo);
      if (creatorData?.creatorDetails?.images.length > 0) {
        const x =
          creatorData?.creatorDetails?.images?.map(
            (cid: any) => `https://ipfs.io/ipfs/${cid}`
          ) || [];
        setImages(x);
      }
      setContactNumber(creatorData?.creatorDetails?.phone);
      setEmail(creatorData?.creatorDetails?.email);
      setFacebookId(creatorData?.creatorDetails?.facebookId);
      setInstagramId(creatorData?.creatorDetails?.instagramId);
      setAboutCreator(creatorData?.creatorDetails?.aboutCreator);
    }
  }, [dispatch, userInfo, creatorData]);

  return (
    <Box px={{ base: "3" }} py={{ base: "5" }}>
      <Text
        fontSize={{ base: "sm", lg: "lg" }}
        fontWeight="600"
        color="#333333"
        m="0"
      >
        Creator profile details
      </Text>
      <Text
        fontSize={{ base: "sm", lg: "md" }}
        fontWeight="400"
        color="#4E4E4E"
        m="0"
      >
        These details will be visible to public
      </Text>

      <Box width={{ base: "100%", lg: "80%" }}>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Creator name
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your influencer name that will be visible to public
            </Text>
          </Flex>
          <Input
            borderColor="#939393"
            onChange={(e: any) => setCreatorName(e.target.value)}
            placeholder="Enter Creator name"
            py="2"
            value={creatorName}
            borderRadius="4px"
          ></Input>
        </SimpleGrid>
        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>
        <Text
          fontSize={{ base: "sm", lg: "lg" }}
          fontWeight="600"
          color="#333333"
          m="0"
        >
          Social profiles{" "}
        </Text>
        <Text
          fontSize={{ base: "sm", lg: "md" }}
          fontWeight="400"
          color="#4E4E4E"
          m="0"
        >
          Enter your social profiles here{" "}
        </Text>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Instagram
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            ></Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="instagram.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={instagramId}
              onChange={(e: any) => setInstagramId(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              facebook
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            ></Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="facebook.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={facebookId}
              onChange={(e: any) => setFacebookId(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Snapchat
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            ></Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="snapchat.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={snapchatId}
              onChange={(e: any) => setSnapchatId(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Tiktok
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            ></Text>
          </Flex>
          <InputGroup size="sm">
            <InputLeftAddon children="tiktok.com/" />
            <Input
              placeholder="Enter your username here"
              py="3"
              value={tiktokId}
              onChange={(e: any) => setTiktokId(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>
        <SimpleGrid columns={[1, 1, 2]} spacing={{ base: "1", lg: "20" }}>
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              About
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              A quick snapshot of your creator/company{" "}
            </Text>
          </Flex>
          <TextArea
            showCount
            maxLength={500}
            value={aboutCreator}
            style={{ height: 120, marginBottom: 24 }}
            onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
              setAboutCreator(e.target.value);
            }}
            placeholder="Write about your creator"
          />
        </SimpleGrid>
        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>

        <Text
          fontSize={{ base: "sm", lg: "lg" }}
          fontWeight="600"
          color="#333333"
          m="0"
        >
          Photos{" "}
        </Text>
        <Text
          fontSize={{ base: "sm", lg: "md" }}
          fontWeight="400"
          color="#4E4E4E"
          m="0"
        >
          The creator all details will be shown here{" "}
        </Text>

        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "3", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Creator Logo
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Change or add new creator logo that will be visible to public
            </Text>
          </Flex>
          <Flex gap="5">
            {creatorLogo && (
              <Box>
                <Image
                  src={creatorLogo}
                  width="76px"
                  height="86px"
                  borderRadius="100%"
                />
              </Box>
            )}
            <Button
              variant="outline"
              width="76px"
              height="86px"
              onClick={() => hiddenInput.click()}
              borderColor="#8B8B8B"
              color="#8B8B8B"
            >
              <AiOutlinePlusCircle size="30px" />
            </Button>
            <Input
              hidden
              type="file"
              ref={(el) => (hiddenInput = el)}
              accept="image/png, image/jpeg"
              onChange={(e: any) => handleLogoSelect(e.target.files[0])}
            />
          </Flex>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "2", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Creator product images/videos
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Change or add new creator images and videos that will be visible
              to public
            </Text>
          </Flex>
          <HStack spacing={{ base: "1", lg: "3" }}>
            <HStack spacing={{ base: "1", lg: "3" }}>
              {images?.map((data: any, index: any) =>
                data.type === "image" ? (
                  <Image
                    key={index}
                    src={data.fileThumbnail}
                    width="76px"
                    height="86px"
                    onClick={() => deleteImages(data.fileThumbnail, index)}
                  />
                ) : (
                  <Box
                    key={index}
                    as="video"
                    src={data.fileThumbnail}
                    autoPlay
                    loop
                    muted
                    display="inline-block"
                    width="100px"
                    height="100px"
                    onClick={() => deleteImages(data.fileThumbnail, index)}
                  ></Box>
                )
              )}
            </HStack>

            <Button
              variant="outline"
              width="76px"
              height="86px"
              onClick={() => hiddenInput1.click()}
              borderColor="#8B8B8B"
              color="#8B8B8B"
            >
              <AiOutlinePlusCircle size="30px" />
            </Button>

            <Input
              hidden
              type="file"
              ref={(el) => (hiddenInput1 = el)}
              accept="image/png, image/jpeg video/mp4"
              onChange={(e: any) => handlePhotoSelect(e.target.files[0])}
            />
          </HStack>
        </SimpleGrid>
        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>
        <Text
          fontSize={{ base: "sm", lg: "lg" }}
          fontWeight="600"
          color="#333333"
          m="0"
        >
          Contact details
        </Text>
        <Text
          fontSize={{ base: "sm", lg: "md" }}
          fontWeight="400"
          color="#4E4E4E"
          m="0"
        >
          Enter your creator name that will be visible to public{" "}
        </Text>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Helpline number
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your business number
            </Text>
          </Flex>
          <InputGroup>
            <InputLeftAddon children="+91" borderColor="#939393" py="3" />
            <Input
              type="tel"
              placeholder="phone number"
              borderColor="#939393"
              value={contactNumber}
              onChange={(e: any) => setContactNumber(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "1", lg: "20" }}
          pt={{ base: "5", lg: "5" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              Helpline email
            </Text>{" "}
            <Text
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              color="#4E4E4E"
              m="0"
            >
              Enter your creator helpline email
            </Text>
          </Flex>
          <InputGroup>
            <Input
              type="tel"
              py="3"
              placeholder="Enter creator email"
              borderColor="#939393"
              value={email}
              onChange={(e: any) => setEmail(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <Box py="5">
          <Divider color="#E6E6E6" />
        </Box>
        <Flex justifyContent="flex-end" gap="5">
          {userInfo?.isCreator && userInfo?.creator?.length > 0 ? (
            <Button
              py="3"
              px="5"
              variant="outline"
              color="#0EBCF5"
              bgColor="#FFFFFF"
              _hover={{ color: "#FFFFFF", bgColor: "#0EBCF5" }}
              onClick={updateCreatorData}
              isLoading={loadingCreatorDetails}
              loadingText="updating..."
            >
              Update
            </Button>
          ) : (
            <Button
              py="3"
              px="5"
              variant="outline"
              color="#0EBCF5"
              bgColor="#FFFFFF"
              _hover={{ color: "#FFFFFF", bgColor: "#0EBCF5" }}
              onClick={handleSave}
              isLoading={loadingBrnad}
              loadingText="Saving..."
            >
              Save
            </Button>
          )}
        </Flex>
      </Box>
    </Box>
  );
}
