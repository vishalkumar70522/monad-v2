import {
  Box,
  Button,
  Flex,
  Image,
  Input,
  InputGroup,
  InputLeftAddon,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import { Modal, message } from "antd";
import { useEffect, useState } from "react";
import { AiOutlinePlusCircle } from "react-icons/ai";
import { useDispatch } from "react-redux";
import { updateUserProfile } from "../../Actions/userActions";
import { addFileOnWeb3 } from "../../utils/web3";
import { USER_UPDATE_PROFILE_RESET } from "../../Constants/userConstants";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Checkbox } from "antd";
import type { CheckboxChangeEvent } from "antd/es/checkbox";

export function EditUserProfilePopup(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [name, setName] = useState<any>("");
  const [userProfile, setUserProfile] = useState<any>("");
  const [file, setFile] = useState<any>(null);
  const [contactNumber, setContactNumber] = useState<any>("");
  const [email, setEmail] = useState<any>("");
  const [masterAccess, setMasterAccess] = useState<any>("");
  const [creatorAccess, setCreatorAccess] = useState<any>("");
  const [brandAccess, setBrandAccess] = useState<any>("");
  const [loadingOperation, setLoadingOperation] = useState<boolean>(false);

  let hiddenInput: any = null;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/userProfile" } });
    }
  }, [navigate, props, userInfo]);

  function handleProfileSelect(file: any) {
    const fileThumbnail = URL.createObjectURL(file);
    setUserProfile(fileThumbnail);
    setFile(file);
  }

  const userUpdateProfile = useSelector(
    (state: any) => state.userUpdateProfile
  );
  const { loading, error: errorUpdateProfile, success } = userUpdateProfile;

  useEffect(() => {
    if (success) {
      setLoadingOperation(false);
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
      message.success("User updated successFull!");
      props.onCancel();
    }
    if (errorUpdateProfile) {
      message.error(errorUpdateProfile);
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
    }
  }, [dispatch, props, errorUpdateProfile, success]);

  const updateUserInfo = async () => {
    setLoadingOperation(true);
    if (file) {
      try {
        const cid = await addFileOnWeb3(file);
        dispatch(
          updateUserProfile({
            name,
            email,
            phone: contactNumber,
            avatar: `https://ipfs.io/ipfs/${cid}`,
            isBrand: brandAccess,
            isCreator: creatorAccess,
            isMaster: masterAccess,
          })
        );
      } catch (error) {
        message.error("Error in uploading user profile images");
      }
    } else {
      dispatch(
        updateUserProfile({
          name,
          email,
          phone: contactNumber,
          isBrand: brandAccess,
          isCreator: creatorAccess,
          isMaster: masterAccess,
        })
      );
    }
  };

  useEffect(() => {
    setName(userInfo?.name);
    setEmail(userInfo?.email);
    setContactNumber(userInfo?.phone);
    setUserProfile(userInfo?.avatar);
    setBrandAccess(userInfo?.isBrand);
    setCreatorAccess(userInfo?.isCreator);
    setMasterAccess(userInfo?.isMaster);
  }, [userInfo]);
  return (
    <Modal
      title="Edit Profile"
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      style={{ top: 20 }}
      maskClosable={false}
    >
      <Box>
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "0", lg: "5" }} pt="5">
          <Text
            color="#333333"
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="600"
          >
            Name
          </Text>
          <Input
            placeholder="Enter your name"
            py="2"
            value={name}
            borderRadius="4px"
            borderColor="#939393"
            color="#333333"
            onChange={(e) => setName(e.target.value)}
          />
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 2, 2]}
          spacing={{ base: "0", lg: "5" }}
          pt={{ base: "3", lg: "5" }}
        >
          <Text
            color="#333333"
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="600"
          >
            Profile photo
          </Text>
          <Flex gap="5">
            {userProfile && (
              <Box>
                <Image
                  src={userProfile}
                  width="76px"
                  height="86px"
                  borderRadius="100%"
                />
              </Box>
            )}
            <Button
              variant="outline"
              width="76px"
              height="86px"
              onClick={() => hiddenInput.click()}
              borderColor="#8B8B8B"
              color="#8B8B8B"
            >
              <AiOutlinePlusCircle size="30px" />
            </Button>
            <Input
              hidden
              type="file"
              ref={(el) => (hiddenInput = el)}
              accept="image/png, image/jpeg"
              onChange={(e: any) => handleProfileSelect(e.target.files[0])}
            />
          </Flex>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 2, 2]}
          spacing={{ base: "0", lg: "5" }}
          pt={{ base: "3", lg: "5" }}
        >
          <Text
            color="#333333"
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="600"
          >
            Phone number
          </Text>
          <InputGroup>
            <InputLeftAddon children="+91" borderColor="#939393" py="3" />
            <Input
              type="tel"
              placeholder="phone number"
              borderColor="#939393"
              borderRadius="4px"
              value={contactNumber}
              onChange={(e: any) => setContactNumber(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid
          columns={[1, 2, 2]}
          spacing={{ base: "0", lg: "5" }}
          pt={{ base: "3", lg: "5" }}
        >
          <Text
            color="#333333"
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="600"
          >
            Email
          </Text>
          <Input
            disabled={true}
            type="email"
            py="3"
            placeholder="Enter brand email"
            borderColor="#939393"
            value={email}
            onChange={(e: any) => setEmail(e.target.value)}
          />
        </SimpleGrid>
        <Text
          color="#333333"
          fontSize={{ base: "", lg: "lg" }}
          fontWeight="600"
          pt="3"
          m="0"
        >
          Business Access
        </Text>
        <SimpleGrid
          columns={[1, 2, 3]}
          spacing={{ base: "0", lg: "5" }}
          pt={{ base: "3", lg: "5" }}
        >
          <Box>
            <Checkbox
              checked={masterAccess}
              disabled={userInfo?.isMaster}
              onChange={(e: CheckboxChangeEvent) =>
                setMasterAccess(e.target.checked)
              }
            >
              Adspace Access
            </Checkbox>
          </Box>
          <Box>
            <Checkbox
              checked={brandAccess}
              disabled={userInfo?.brand?.length === 0 ? false : true}
              onChange={(e: CheckboxChangeEvent) =>
                setBrandAccess(e.target.checked)
              }
            >
              Brand Access
            </Checkbox>
          </Box>
          <Box>
            <Checkbox
              checked={creatorAccess}
              disabled={userInfo?.isCreator}
              onChange={(e: CheckboxChangeEvent) =>
                setCreatorAccess(e.target.checked)
              }
            >
              Creator Access
            </Checkbox>
          </Box>
        </SimpleGrid>
        <Flex justifyContent="flex-end" gap={{ base: "5", lg: "10" }} pt="5">
          <Button
            py="3"
            px="5"
            variant="null"
            color="#000000"
            bgColor="#FFFFFF"
            fontSize={{ base: "lg", lg: "lg" }}
            _hover={{ color: "#FFFFFF", bgColor: "#0EBCF5" }}
            onClick={() => props?.onCancel()}
          >
            Clear
          </Button>
          <Button
            py="3"
            px="5"
            variant="outline"
            color="#0EBCF5"
            bgColor="#FFFFFF"
            fontSize={{ base: "lg", lg: "lg" }}
            _hover={{ color: "#FFFFFF", bgColor: "#0EBCF5" }}
            onClick={updateUserInfo}
            isLoading={loading || loadingOperation}
            loadingText="Updating...."
          >
            Save
          </Button>
        </Flex>
      </Box>
    </Modal>
  );
}
