import {
  Box,
  Button,
  Center,
  Stack,
  Flex,
  Image,
  Text,
  Divider,
  SimpleGrid,
} from "@chakra-ui/react";
import { AiOutlineEdit } from "react-icons/ai";
import { useSelector } from "react-redux";
import { EditUserProfilePopup } from "./EditUserProfilePopup";
import { useState } from "react";
import { CgProfile } from "react-icons/cg";
import { subscription } from "../../assets/svg";
import { IoSettingsOutline } from "react-icons/io5";
import { IoIosHelpCircleOutline } from "react-icons/io";
import { useNavigate } from "react-router-dom";
import { GrFormEdit } from "react-icons/gr";
import { EditBrandProfileData } from "./EditBrandProfileData";

export function MobileViewUserProfile(props: any) {
  const navigate = useNavigate();
  const [openEditUserProfile, setOpenEditUserProfile] = useState<any>(false);
  const [businessView, setBusinessView] = useState<any>(false);

  const [editPublicProfile, setEditPublicProfile] = useState<any>(false);
  // const [editCreatorProfile, setEditCreatorProfile] = useState<any>(false);

  // const [creatorView, setCreatorView] = useState<any>(false);
  const [brandView, setBrandView] = useState<any>(false);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const handleBrandView = () => {
    // setCreatorView(false);
    setBrandView(true);
  };

  const handleCreatorView = () => {
    setBrandView(false);
    // setCreatorView(true);
  };

  const handleMasterView = () => {
    setBrandView(false);
    // setCreatorView(false);
  };
  return (
    <Box p="2">
      <EditUserProfilePopup
        open={openEditUserProfile}
        onCancel={() => setOpenEditUserProfile(false)}
      />
      <Box border="1px" borderColor="#CFCFCF" px="2" py="5" borderRadius="16px">
        <Flex gap={{ base: "5", lg: "10" }} align="center">
          <Image
            borderRadius="100%"
            height="103px"
            width="103px"
            src={userInfo?.avatar}
            alt=""
          />
          <Stack p={{ base: "", lg: "10" }}>
            <Text
              fontSize={{ base: "md", lg: "lg" }}
              fontWeight="600"
              color="#333333"
              m="0"
            >
              {userInfo?.name}
            </Text>
            <Text
              fontSize={{ base: "12px", lg: "md" }}
              fontWeight="600"
              color="#5C5C5C"
              m="0"
            >
              {userInfo?.email}
            </Text>
            <Text
              fontSize={{ base: "12px", lg: "md" }}
              fontWeight="600"
              color="#5C5C5C"
              m="0"
            >
              {userInfo?.phone}
            </Text>
          </Stack>
        </Flex>
        <Flex justifyContent="flex-end">
          <Button
            py="2"
            px="5"
            variant="outline"
            color="#0EBCF5"
            bgColor="#FFFFFF"
            fontWeight="600"
            fontSize="12px"
            _hover={{ color: "#FFFFFF", bgColor: "#0EBCF5" }}
            rightIcon={<AiOutlineEdit size="12px" />}
            onClick={() => setOpenEditUserProfile(true)}
          >
            {" "}
            Edit
          </Button>
        </Flex>
      </Box>
      <Flex direction="column" gap="2" pt="2">
        {userInfo?.isBrand ? (
          <Box
            px="5"
            py="3"
            border="1px"
            borderColor="#CFCFCF"
            borderRadius="16px"
          >
            <Flex justify="space-between">
              <Flex
                align="center"
                gap="5"
                onClick={() => {
                  if (userInfo?.brand?.length === 0) {
                    navigate("/editBrand");
                  } else {
                    navigate(`/brandProfile/${userInfo?.brand[0]}`);
                  }
                }}
              >
                <CgProfile color="#000000" size="20px" />
                <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
                  Public profile
                </Text>
              </Flex>
              <Flex align="center" onClick={() => navigate("/editBrand")}>
                <AiOutlineEdit size="20px" />
              </Flex>
            </Flex>
          </Box>
        ) : null}
        {/* {userInfo?.isCreator ? (
          <Box
            px="5"
            py="3"
            border="1px"
            borderColor="#CFCFCF"
            borderRadius="16px"
          >
            <Flex justify="space-between">
              <Flex
                align="center"
                gap="5"
                onClick={() =>
                  navigate(`/creatorProfile/${userInfo?.creator[0]}`)
                }
              >
                <CgProfile color="#000000" size="20px" />
                <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
                  Creator profile
                </Text>
              </Flex>
              <Flex align="center" onClick={() => navigate("/editCreator")}>
                <AiOutlineEdit size="20px" />
              </Flex>
            </Flex>
          </Box>
        ) : null} */}
        {/* {(!userInfo?.isMaster ||
          !userInfo?.isBrand ||
          !userInfo?.isCreator) && ( */}
        <Box
          px="5"
          py="3"
          border="1px"
          borderColor="#CFCFCF"
          borderRadius="16px"
        >
          <Flex
            align="center"
            gap="5"
            onClick={() => setBusinessView(!businessView)}
          >
            <Center
              border="1px"
              borderColor="#000000"
              borderRadius="100%"
              p="1"
            >
              <Image src={subscription} alt="" height="12px" width="12px" />
            </Center>
            <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
              Business
            </Text>
          </Flex>
          {businessView && (
            <>
              <Divider py="" />
              <SimpleGrid textAlign="center" columns={[2]} gap="0">
                <Box alignItems="center" onClick={handleMasterView}>
                  <Image src="https://retailsphere.com/wp-content/uploads/2022/05/AdobeStock_340642544-1024x683.jpeg" />
                  {!userInfo?.isMaster && (
                    <Center pl={{ base: "", lg: "" }}>
                      <Button
                        py="2"
                        px="7"
                        variant="outline"
                        color="#020202"
                        bgColor="#FFFFFF"
                        fontWeight="600"
                        fontSize="xs"
                        _hover={{ color: "#FFFFFF", bgColor: "#02020250" }}
                        // rightIcon={<AiOutlineEdit size="20px" />}
                        onClick={() => setOpenEditUserProfile(true)}
                      >
                        {" "}
                        Adspace Access
                      </Button>
                    </Center>
                  )}
                </Box>
                <Box py="2">
                  <Text fontSize="md" fontWeight="600">
                    Adspaces
                  </Text>
                  <Text px="5" fontSize="xs">
                    Turn your store, restaurant, cafe or building into an
                    adspace and monetise your walk-in customers using open
                    ads...
                  </Text>
                  {/* {} */}
                </Box>
              </SimpleGrid>
              <SimpleGrid textAlign="center" columns={[2]} gap="0">
                <Box py="2">
                  <Text fontSize="md" fontWeight="600">
                    Advertisers
                  </Text>
                  <Text px="5" fontSize="xs">
                    Innovate the way you advertise and extend your ad campaigns'
                    reach with audience engagement using open ads...
                  </Text>
                </Box>
                <Box alignItems="center" onClick={handleBrandView}>
                  <Image src="https://media.gq.com/photos/60f82bad1b7faefafbaa3db1/master/w_6000,h_4000,c_limit/CK%20NY%20M-891%205.26.21%20(3).JPG" />
                  {!userInfo?.isBrand && (
                    <Center pl={{ base: "", lg: "" }}>
                      <Button
                        py="2"
                        px="4"
                        variant="outline"
                        color="#020202"
                        bgColor="#FFFFFF"
                        fontWeight="600"
                        fontSize="xs"
                        _hover={{ color: "#FFFFFF", bgColor: "#02020250" }}
                        // rightIcon={<AiOutlineEdit size="20px" />}
                        onClick={() => setOpenEditUserProfile(true)}
                      >
                        {" "}
                        Advertiser Access
                      </Button>
                    </Center>
                  )}
                  {userInfo?.isBrand && brandView ? (
                    <>
                      <Text
                        fontSize="xs"
                        onClick={() =>
                          navigate(`/brandProfile/${userInfo?.brand[0]}`)
                        }
                      >
                        See public profile
                      </Text>
                      {!editPublicProfile ? (
                        <Button
                          color="#3E3E3E"
                          py="2"
                          fontSize="xs"
                          fontWeight="400"
                          bgColor="#C4C4C4"
                          rightIcon={<GrFormEdit size="20px" color="#3E3E3E" />}
                          onClick={() => setEditPublicProfile(true)}
                        >
                          Edit public profile
                        </Button>
                      ) : null}
                    </>
                  ) : null}
                </Box>
              </SimpleGrid>
              {/* <SimpleGrid textAlign="center" columns={[2]} gap="0">
                  <Box alignItems="center" onClick={handleCreatorView}>
                    <Image src="https://wallpaperaccess.com/full/9488927.jpg" />
                  </Box>
                  <Box py="10">
                    <Text fontSize="lg" fontWeight="600">
                      Influences
                    </Text>
                    <Text px="10" fontSize="sm">
                      Find your gigs from the local market and build a social
                      and local reputation for yourself and the brand using open
                      ads...
                    </Text>
                    {!userInfo?.isCreator && (
                      <Center pl={{ base: "5", lg: "20" }}>
                        <Button
                          py="2"
                          px="5"
                          variant="outline"
                          color="#020202"
                          bgColor="#FFFFFF"
                          fontWeight="600"
                          fontSize="md"
                          _hover={{ color: "#FFFFFF", bgColor: "#02020250" }}
                          // rightIcon={<AiOutlineEdit size="20px" />}
                          onClick={() => setOpenEditUserProfile(true)}
                        >
                          {" "}
                          Get Influencer Business Access
                        </Button>
                      </Center>
                    )}
                    {userInfo?.isCreator && creatorView ? (
                      <>
                        <Text
                          onClick={() =>
                            navigate(`/creatorProfile/${userInfo?.creator[0]}`)
                          }
                        >
                          Click here for public profile
                        </Text>
                        {!editCreatorProfile && (
                          <Button
                            color="#3E3E3E"
                            py="3"
                            fontSize="md"
                            fontWeight="400"
                            bgColor="#C4C4C4"
                            onClick={() => setEditCreatorProfile(true)}
                            rightIcon={
                              <GrFormEdit size="20px" color="#3E3E3E" />
                            }
                          >
                            Edit creator profile details
                          </Button>
                        )}
                      </>
                    ) : null}
                  </Box>
                </SimpleGrid> */}
            </>
          )}
        </Box>
        {/* )} */}
        <Box
          px="5"
          py="3"
          border="1px"
          borderColor="#CFCFCF"
          borderRadius="16px"
        >
          <Flex align="center" gap="5">
            <Center
              border="1px"
              borderColor="#000000"
              borderRadius="100%"
              p="1"
            >
              <IoSettingsOutline size="12px" color="#000000" />
            </Center>
            <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
              Setting
            </Text>
          </Flex>
        </Box>
        <Box
          px="5"
          py="3"
          border="1px"
          borderColor="#CFCFCF"
          borderRadius="16px"
        >
          <Flex align="center" gap="5">
            <IoIosHelpCircleOutline color="#000000" size="25px" />
            <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
              Help
            </Text>
          </Flex>
        </Box>
      </Flex>
      {editPublicProfile && (
        <>
          <Divider mx="2" color="#02020250" />
          <Box
            px="5"
            py="3"
            border="1px"
            borderColor="#CFCFCF"
            borderRadius="16px"
          >
            <EditBrandProfileData
              setEditPublicProfile={() => setEditPublicProfile(false)}
            />
          </Box>
        </>
      )}
    </Box>
  );
}
