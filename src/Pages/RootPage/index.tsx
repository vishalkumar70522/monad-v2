import { Box } from "@chakra-ui/react";
import { useSelector } from "react-redux";
import { HomePage } from "../HomePage";
import { CouponHomePage } from "../CouponHomePage";
import { MyScreenList } from "../MyScreenList";

export function RootPage() {
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  return (
    <Box>
      {userInfo?.isMaster && userInfo?.isBrand ? (
        <HomePage />
      ) : userInfo?.isMaster ? (
        <MyScreenList />
      ) : userInfo?.isBrand ? (
        <HomePage />
      ) : (
        <CouponHomePage />
      )}
    </Box>
  );
}
