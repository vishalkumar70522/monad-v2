import {
  Box,
  HStack,
  Text,
  Button,
  SimpleGrid,
  Divider,
  Center,
  Stack,
  Flex,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Skeleton, message } from "antd";
import {
  MediaContainer,
  SingleScreenWithCampaign,
} from "../../components/commans";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { createCamapaignForMultipleScreens } from "../../Actions/campaignForMultipleScreenAction";
import { HiCheckCircle } from "react-icons/hi";
import { CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET } from "../../Constants/campaignForMultipleScreen";

export function SingleCampaignOnMultipleScreen(props: any) {
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  // const items: string | null = window.localStorage.getItem("carts");
  //  Parse stored json or if none return initialValue
  // const value = items ? JSON.parse(items) : [];
  const createCampaignForMultipleScreen = useSelector(
    (state: any) => state.createCampaignForMultipleScreen
  );
  const {
    loading: loadingSlotBooking,
    error: errorSlotBooking,
    success: successSlotBooking,
    // uploadedCampaign,
  } = createCampaignForMultipleScreen;

  const mediaUpload = useSelector((state: any) => state.mediaUpload);
  const {
    loading: loadingMedia,
    media: mediaData,
    success,
    error: errorMedia,
  } = mediaUpload;

  useEffect(() => {
    if (errorMedia) {
      message.error(errorMedia);
    }
    if (errorSlotBooking) {
      message.error(errorSlotBooking);
      dispatch({ type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET });
    }
    if (successSlotBooking) {
      message.success("Campaign created successFull");
      window.localStorage.setItem("carts", JSON.stringify([]));
      dispatch({ type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET });
      setTimeout(() => navigate("/"), 500);
    }
  }, [errorMedia, success, errorSlotBooking, successSlotBooking]);

  const slotBookingHandler = () => {
    dispatch(
      createCamapaignForMultipleScreens({
        campaignWithScreens: location?.state?.cartItems,
        cid: mediaData?.media?.split("/").slice()[4],
        campaignName: location?.state?.campaignName,
        media: mediaData,
      })
    );
  };

  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: 75, lg: 100 }}>
      {successSlotBooking && (
        <Flex gap="2" align="center">
          <HiCheckCircle color="#007E33" size="32px" />
          <Text color="#007E33" fontSize="3xl" fontWeight="600" m="0">
            Confirmation
          </Text>
        </Flex>
      )}
      <Box bgColor="#F7F7F7" p="5" borderRadius="8px">
        <Text
          color="#000000"
          fontSize="sm"
        >{`Your account is pending approval from the screen owner. This approval process is a one-time requirement for each screen. While you wait for approval, you can still post your ad. The approval process typically takes up to 2 hours`}</Text>
      </Box>
      <SimpleGrid columns={[1, 1, 2]}>
        {loadingMedia ? (
          <Skeleton.Image active={true} />
        ) : (
          <HStack pt="5" spacing="5">
            <MediaContainer
              cid={mediaData?.media?.split("/").slice()[4]}
              width={{ base: "184px", lg: "307px" }}
              height={{ base: "180px", lg: "184px" }}
              autoPlay="false"
            />
          </HStack>
        )}
        <Box>
          <Text
            color="#222222"
            fontSize={{ base: "xl", lg: "2xl" }}
            pt="5"
            fontWeight="600"
            m="0"
          >
            Campaign name
          </Text>
          <HStack justifyContent="space-between" m="0">
            <Text
              color="#2F2F2F"
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              {location?.state?.campaignName}
            </Text>
          </HStack>

          <Text
            color="#222222"
            fontSize={{ base: "xl", lg: "2xl" }}
            pt="5"
            fontWeight="600"
            m="0"
          >
            Dates
          </Text>
          <HStack justifyContent="space-between" m="0">
            <Text
              color="#2F2F2F"
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              {convertIntoDateAndTime(Date())}
            </Text>
          </HStack>

          <Text
            color="#222222"
            fontSize={{ base: "xl", lg: "2xl" }}
            pt="5"
            fontWeight="600"
            m="0"
          >
            Total No of slots
          </Text>
          <HStack justifyContent="space-between" m="0">
            <Text
              color="#2F2F2F"
              fontSize={{ base: "sm", lg: "md" }}
              fontWeight="400"
              m="0"
            >
              {location?.state?.cartItems?.reduce(
                (accum: any, current: any) => accum + current?.totalSlotBooked,
                0
              )}
            </Text>
          </HStack>
        </Box>
      </SimpleGrid>
      <Divider color="#DDDDDD" />
      <Center>
        <Text
          color="#222222"
          fontSize={{ base: "xl", lg: "2xl" }}
          pt="2"
          fontWeight="600"
          m="0"
        >
          Selected screens in the campaign
        </Text>
      </Center>

      <SimpleGrid columns={[1, 1, 3]} spacing={5} pt={{ base: "", lg: "10" }}>
        {location?.state?.cartItems?.length > 0 &&
          location?.state?.cartItems?.map((data: any, index: any) => (
            <SingleScreenWithCampaign key={index} data={data} />
          ))}
      </SimpleGrid>

      <Center>
        <Stack pt="5">
          <Text
            color="#000000"
            fontSize={{ base: "sm", lg: "xl" }}
            fontWeight="600"
            m="0"
            textAlign="center"
          >
            Total ₹
            {location?.state?.cartItems
              ?.reduce(
                (accum: any, current: any) => accum + current?.totalAmount,
                0
              )
              .toFixed(2)}
          </Text>
          <Button
            bgColor="#D7380E"
            fontSize="lg"
            fontWeight="600"
            py="2"
            width="307px"
            isLoading={loadingMedia || loadingSlotBooking}
            loadingText="Creating Media"
            onClick={slotBookingHandler}
          >
            Proceed to pay
          </Button>
        </Stack>
      </Center>
    </Box>
  );
}
