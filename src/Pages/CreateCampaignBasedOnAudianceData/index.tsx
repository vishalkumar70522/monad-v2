import {
  Box,
  Button,
  Flex,
  FormControl,
  Image,
  Input,
  InputGroup,
  SimpleGrid,
  Stack,
  Text,
} from "@chakra-ui/react";
import { Divider, message } from "antd";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import {
  campaign,
  goal,
  media,
  timer,
  locationSvg,
  people,
  budgut,
  handSake,
} from "../../assets/svg";
import { GrFormEdit } from "react-icons/gr";
import { MediaContainer } from "../../components/commans";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {
  IconButton as MiuiIconButton,
  InputAdornment,
  Slider,
} from "@material-ui/core";
import { BsCalendar2Date } from "react-icons/bs";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { FilterAudianceProfilePopup } from "../Models/FilterAudianceProfilePopup";
import {
  createCamapaignBasedOnAudiancesProfile,
  getScreensBasedOnAudiancesProfile,
} from "../../Actions/campaignForMultipleScreenAction";
import { CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_RESET } from "../../Constants/campaignForMultipleScreen";

export function CreateCampaignBasedOnAudianceData(props: any) {
  const location = useLocation();
  const navigate = useNavigate();
  const [messageApi, contextHolder] = message.useMessage();
  const dispatch = useDispatch<any>();
  const [openFilterAudianceModel, setOpenFilterAudianceModel] =
    useState<any>(false);
  const [campaignName, setCampaignName] = useState<any>("");
  const [editCampaignName, setEditCampaignName] = useState<any>(false);
  const [campaignGoal, setCampaignGoal] = useState<any>(null);
  const [startDateAndTime, setStartDateAndTime] = useState<any>(null);
  const [endDateAndTime, setEndDateAndTime] = useState<any>(null);
  const [editSchedule, setEditSchedule] = useState<any>(false);
  const [cities, setCities] = useState<any>(`Gurgaon`);
  const [editCities, setEditCities] = useState<any>(false);
  const [amount, setAmount] = useState<any>(800);
  const [numberOfAudiances, setNumberOfAudiances] = useState<any>(1000);
  const [editNumberOfAudiances, setEditNumberOfAudiances] =
    useState<any>(false);

  const [ageRange, setAgeRange] = useState<any>([10, 100]);

  const [croudMobability, setCroudMobability] = useState<any>([
    { status: true, lavel: "Sitting" },
    { status: true, lavel: "Moving" },
    { status: true, lavel: "Walking" },
  ]);

  const [genderPreference, setGenderPreference] = useState<any>([
    { status: true, lavel: "Male" },
    { status: false, lavel: "Female" },
    { status: false, lavel: "Unisex" },
  ]);

  const [employmentTypes, setEmploymentType] = useState<any>([
    { status: true, lavel: "Salaried" },
    { status: true, lavel: "Businessman" },
    { status: true, lavel: "Student" },
    { status: true, lavel: "Others" },
  ]);

  const [screenHighlights, setScreenHighlights] = useState<any>([
    "Appartments",
    "Metro",
  ]);

  // console.log(
  //   " screenHighlights , croudMobability , ageRange , employmentTypes , genderPreference",
  //   screenHighlights,
  //   croudMobability,
  //   ageRange,
  //   employmentTypes,
  //   genderPreference
  // );

  const handleChange = (event: any, newValue: any) => {
    setAmount(newValue);
  };

  const handleEndDate = (value: any) => {
    setEndDateAndTime(value);
  };
  // console.log("days : ", (endDate - startDate) / (1000 * 60 * 60 * 24));
  const handleStartDate = (value: any) => {
    // let time = value.toString().split(" "); // Wed Jan 04 2023 08:22:28 GMT+0530
    // time = time[4];
    setStartDateAndTime(value);
  };

  const mediaUpload = useSelector((state: any) => state.mediaUpload);
  const {
    loading: loadingMedia,
    media: mediaData,
    success,
    error: errorMedia,
  } = mediaUpload;

  const createCampaignBasedOnAudienceProfile = useSelector(
    (state: any) => state.createCampaignBasedOnAudienceProfile
  );
  const {
    loading: loadingSlotBooking,
    error: errorSlotBooking,
    success: successSlotBooking,
    uploadedCampaign,
  } = createCampaignBasedOnAudienceProfile;

  const getScreens = useSelector((state: any) => state.getScreens);
  const { loading: loadingScreens, error: errorScreens, screens } = getScreens;

  const getOnlyTrueValue = (data: any) => {
    const result = [];
    for (let x of data) {
      if (x.status) {
        result.push(x.lavel);
      }
    }
    return result;
  };

  const createCampaign = () => {
    // console.log("create campaign called!");
    if (screens?.length > 0) {
      dispatch(
        createCamapaignBasedOnAudiancesProfile({
          campaignName,
          cid: mediaData?.media?.split("/").slice()[4],
          startDateAndTime,
          endDateAndTime,
          budget: amount,
          screens: screens.map((screen: any) => screen?._id),
          media: mediaData,
        })
      );
    } else {
      message.error(
        "No screens found according to current filter, change the filter options, and try again."
      );
    }
  };

  useEffect(() => {
    if (successSlotBooking) {
      messageApi.open({
        type: "success",
        content: uploadedCampaign,
        duration: 10,
      });
      setTimeout(() => {
        dispatch({
          type: CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_RESET,
        });
        navigate("/");
      }, 2000);
    }
    if (errorSlotBooking) {
      messageApi.open({
        type: "error",
        content: errorSlotBooking,
        duration: 10,
      });
      setTimeout(() => {
        dispatch({
          type: CREATE_CAMPAIGN_BASED_ON_AUDIANCES_PROFILE_RESET,
        });
      }, 2000);
    }
    if (errorScreens) {
      message.error(errorScreens);
    }
  }, [errorSlotBooking, successSlotBooking, errorScreens]);

  useEffect(() => {
    if (errorMedia) {
      message.error(errorMedia);
    }
    setCampaignGoal(location?.state?.campaignGoal);
    setCampaignName(location?.state?.campaignName);
  }, [errorMedia]);

  useEffect(() => {
    dispatch(
      getScreensBasedOnAudiancesProfile({
        numberOfAudiances,
        genderPreference: getOnlyTrueValue(genderPreference),
        employmentTypes: getOnlyTrueValue(employmentTypes),
        croudMobability: getOnlyTrueValue(croudMobability),
        screenHighlights,
        ageRange,
        cities,
      })
    );
  }, [
    numberOfAudiances,
    genderPreference,
    employmentTypes,
    croudMobability,
    screenHighlights,
    ageRange,
    cities,
  ]);

  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: 16, lg: 70 }}>
      {contextHolder}
      <FilterAudianceProfilePopup
        open={openFilterAudianceModel}
        onCancel={() => setOpenFilterAudianceModel(false)}
        ageRange={ageRange}
        setAgeRange={(value: any) => setAgeRange(value)}
        croudMobability={croudMobability}
        setCroudMobability={(value: any) => setCroudMobability(value)}
        genderPreference={genderPreference}
        setGenderPreference={(value: any) => setGenderPreference(value)}
        employmentTypes={employmentTypes}
        setEmploymentType={(value: any) => setEmploymentType(value)}
        screenHighlights={screenHighlights}
        setScreenHighlights={(value: any) => setScreenHighlights(value)}
      />
      <Text fontSize={{ base: "", lg: "3xl" }} fontWeight="600" color="#000000">
        Create new campaign
      </Text>
      <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "5" }}>
        <Flex direction="column" width={{ base: "100%", lg: "100%" }} gap="5">
          {/* campaign Name */}
          <Box
            boxShadow="lg"
            border="1px"
            borderRadius="8px"
            borderColor="rgba(33, 33, 33, 0.20)"
            p="2"
            py="5"
          >
            <Flex gap="5">
              <Stack alignContent="flex-start">
                <Image src={campaign} alt="" />
              </Stack>
              <Box>
                <Text
                  fontSize={{ base: "", lg: "lg" }}
                  fontWeight="600"
                  color="#000000"
                  m="0"
                >
                  Campaign name
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                  pt="1"
                >
                  The campaign is created using this name
                </Text>
                {editCampaignName ? (
                  <Box>
                    <Input
                      placeholder="Enter campaign name"
                      size="large"
                      px="2"
                      mt="2"
                      py="1"
                      borderColor="#C8C8C8"
                      value={campaignName}
                      borderRadius="4px"
                      onChange={(e) => setCampaignName(e.target.value)}
                    />
                    <Button
                      type="submit"
                      color="#FFFFFF"
                      variant="outline"
                      bgColor="#0EBCF5"
                      py="2"
                      mt="2"
                      fontWeight="600"
                      fontSize={{ base: "sm", lg: "md" }}
                      onClick={() => setEditCampaignName(false)}
                    >
                      Save
                    </Button>
                  </Box>
                ) : (
                  <Flex gap={{ base: "", lg: "5" }} align="center" py="5">
                    <Text
                      color="#1E1E1E"
                      fontSize={{ base: "lg", lg: "lg" }}
                      fontWeight="552"
                      m="0"
                    >
                      {campaignName}
                    </Text>
                    <GrFormEdit
                      size="20px"
                      onClick={() => setEditCampaignName(true)}
                    />
                  </Flex>
                )}
              </Box>
            </Flex>
          </Box>
          {/* media */}
          <Box
            boxShadow="lg"
            border="1px"
            borderRadius="8px"
            borderColor="rgba(33, 33, 33, 0.20)"
            p="2"
            py="5"
          >
            <Flex gap="5">
              <Stack alignContent="flex-start">
                <Image src={media} alt="" />
              </Stack>
              <Box>
                <Text
                  fontSize={{ base: "", lg: "lg" }}
                  fontWeight="600"
                  color="#000000"
                  m="0"
                >
                  Media
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                  pt="1"
                >
                  The media will be played on the selected screens
                </Text>
                <Flex gap={{ base: " ", lg: "20" }}>
                  <MediaContainer
                    cid={mediaData?.media?.split("/").slice()[4]}
                    width={{ base: "83px", lg: "83px" }}
                    height={{ base: "56px", lg: "56px" }}
                    autoPlay="false"
                  />
                  <Button
                    fontSize={{ base: "13px", lg: "13px" }}
                    variant="null"
                    color="#000000"
                    fontWeight="511"
                  >
                    View
                  </Button>
                </Flex>
              </Box>
            </Flex>
          </Box>
          {/* goal */}
          <Box
            boxShadow="lg"
            border="1px"
            borderRadius="8px"
            borderColor="rgba(33, 33, 33, 0.20)"
            p="2"
            py="5"
          >
            <Flex gap="5">
              <Stack alignContent="flex-start">
                <Image src={goal} alt="" />
              </Stack>
              <Box>
                <Text
                  fontSize={{ base: "", lg: "lg" }}
                  fontWeight="600"
                  color="#000000"
                  m="0"
                >
                  Goal for the campaign
                </Text>
                <Text
                  fontSize={{ base: "13px", lg: "13px" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                  pt="1"
                >
                  The number of people around the screen per day
                </Text>
                <Flex py="5">
                  <Button
                    fontSize={{ base: "13px", lg: "sm" }}
                    variant="outline"
                    py="3"
                    color={
                      campaignGoal === "Audience enggagement"
                        ? "#FFFFFF"
                        : "#000000"
                    }
                    bgColor={
                      campaignGoal === "Audience enggagement"
                        ? "#0EBCF5"
                        : "#FFFFFF"
                    }
                    fontWeight="400"
                    onClick={() => setCampaignGoal("Audience enggagement")}
                  >
                    Audience enggagement
                  </Button>
                  <Button
                    fontSize={{ base: "13px", lg: "sm" }}
                    variant="outline"
                    py="3"
                    color={
                      campaignGoal === "Audience reach" ? "#FFFFFF" : "#000000"
                    }
                    bgColor={
                      campaignGoal === "Audience reach" ? "#0EBCF5" : "#FFFFFF"
                    }
                    fontWeight="400"
                    onClick={() => setCampaignGoal("Audience reach")}
                  >
                    Audience reach
                  </Button>
                </Flex>
                <Text
                  fontSize={{ base: "13px", lg: "13px" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                >
                  The goal focuses on interaction with audience
                </Text>
              </Box>
            </Flex>
          </Box>
          {/* campaign goal */}
          {campaignGoal === "Audience reach" ? (
            <Box
              boxShadow="lg"
              border="1px"
              borderRadius="8px"
              borderColor="rgba(33, 33, 33, 0.20)"
              p="2"
              py="5"
            >
              <Flex gap="5">
                <Stack alignContent="flex-start">
                  <Image src={handSake} alt="" />
                </Stack>
                <Box>
                  <Text
                    fontSize={{ base: "", lg: "lg" }}
                    fontWeight="600"
                    color="#000000"
                    m="0"
                  >
                    Expected audience engagement
                  </Text>
                  <Text
                    fontSize={{ base: "", lg: "md" }}
                    fontWeight="400"
                    color="#5B5B5B"
                    m="0"
                    pt="1"
                  >
                    Enter the no of audience you want engagement with
                  </Text>
                  {editNumberOfAudiances ? (
                    <Box>
                      <Input
                        placeholder="Enter campaign name"
                        size="large"
                        px="2"
                        mt="2"
                        py="1"
                        borderColor="#C8C8C8"
                        value={numberOfAudiances}
                        borderRadius="4px"
                        onChange={(e) => setNumberOfAudiances(e.target.value)}
                      />
                      <Button
                        type="submit"
                        color="#FFFFFF"
                        variant="outline"
                        bgColor="#0EBCF5"
                        py="2"
                        mt="2"
                        fontWeight="600"
                        fontSize={{ base: "sm", lg: "md" }}
                        onClick={() => setEditNumberOfAudiances(false)}
                      >
                        Save
                      </Button>
                    </Box>
                  ) : (
                    <Flex gap={{ base: "", lg: "5" }} align="center" py="5">
                      <Text
                        color="#1E1E1E"
                        fontSize={{ base: "lg", lg: "lg" }}
                        fontWeight="552"
                        m="0"
                      >
                        {`${numberOfAudiances} members`}
                      </Text>
                      <GrFormEdit
                        size="20px"
                        onClick={() => setEditNumberOfAudiances(true)}
                      />
                    </Flex>
                  )}
                </Box>
              </Flex>
            </Box>
          ) : null}
          {/* campaign period */}
          <Box
            boxShadow="lg"
            border="1px"
            borderRadius="8px"
            borderColor="rgba(33, 33, 33, 0.20)"
            p="2"
            py="5"
          >
            <Stack align="end" px="3">
              <GrFormEdit size="20px" onClick={() => setEditSchedule(true)} />
            </Stack>
            <Flex gap="5">
              <Stack alignContent="flex-start">
                <Image src={timer} alt="" />
              </Stack>
              <Box>
                <Text
                  fontSize={{ base: "", lg: "lg" }}
                  fontWeight="600"
                  color="#000000"
                  m="0"
                >
                  Set campaign period
                </Text>

                <Text
                  fontSize={{ base: "13px", lg: "13px" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                  pt="1"
                >
                  The number of people around the screen per day
                </Text>
                <Text
                  fontSize={{ base: "", lg: "lg" }}
                  fontWeight="600"
                  color="#000000"
                  m="0"
                  pt="2"
                >
                  Start time
                </Text>

                <Text
                  fontSize={{ base: "13px", lg: "13px" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                  pt="1"
                >
                  Start date and time of the campaign
                </Text>
                {editSchedule ? (
                  <InputGroup pt="5">
                    <FormControl id="startDateHere" width="100%">
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DateTimePicker
                          inputVariant="outlined"
                          value={startDateAndTime}
                          onChange={handleStartDate}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="start">
                                <MiuiIconButton>
                                  <BsCalendar2Date />
                                </MiuiIconButton>
                              </InputAdornment>
                            ),
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    </FormControl>
                  </InputGroup>
                ) : (
                  <Text
                    color="#1E1E1E"
                    fontSize={{ base: "md", lg: "md" }}
                    fontWeight="552"
                    m="0"
                    pt="1"
                  >
                    {convertIntoDateAndTime(startDateAndTime)}
                  </Text>
                )}
                <Text
                  fontSize={{ base: "", lg: "lg" }}
                  fontWeight="600"
                  color="#000000"
                  m="0"
                  pt="2"
                >
                  End time
                </Text>

                <Text
                  fontSize={{ base: "13px", lg: "13px" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                  pt="1"
                >
                  End date and time of the campaign
                </Text>
                {editSchedule ? (
                  <InputGroup pt="5">
                    <FormControl id="startDateHere" width="100%">
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DateTimePicker
                          inputVariant="outlined"
                          value={endDateAndTime}
                          onChange={handleEndDate}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="start">
                                <MiuiIconButton>
                                  <BsCalendar2Date />
                                </MiuiIconButton>
                              </InputAdornment>
                            ),
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    </FormControl>
                  </InputGroup>
                ) : (
                  <Text
                    color="#1E1E1E"
                    fontSize={{ base: "md", lg: "md" }}
                    fontWeight="552"
                    m="0"
                    pt="1"
                  >
                    {convertIntoDateAndTime(startDateAndTime)}
                  </Text>
                )}
                {editSchedule ? (
                  <Button
                    type="submit"
                    color="#FFFFFF"
                    variant="outline"
                    bgColor="#0EBCF5"
                    py="2"
                    mt="2"
                    fontWeight="600"
                    fontSize={{ base: "sm", lg: "md" }}
                    onClick={() => setEditSchedule(false)}
                  >
                    Save
                  </Button>
                ) : null}
              </Box>
            </Flex>
          </Box>
          {/* Location */}
          <Box
            boxShadow="lg"
            border="1px"
            borderRadius="8px"
            borderColor="rgba(33, 33, 33, 0.20)"
            p="2"
            py="5"
          >
            <Flex gap="5">
              <Stack alignContent="flex-start">
                <Image src={locationSvg} alt="" />
              </Stack>
              <Box>
                <Text
                  fontSize={{ base: "", lg: "lg" }}
                  fontWeight="600"
                  color="#000000"
                  m="0"
                >
                  Expected locations
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                  pt="1"
                >
                  Locations are covered
                </Text>
                {editCities ? (
                  <Box pt="5">
                    <Input
                      placeholder="Enter location and then press enter "
                      size="large"
                      px="2"
                      mt="2"
                      py="1"
                      borderColor="#C8C8C8"
                      borderRadius="4px"
                      value={cities}
                      onChange={(e: any) => {
                        setCities(e.target.value);
                      }}
                    />
                    <Text
                      fontSize={{ base: "sm", lg: "md" }}
                      fontWeight="503"
                      color="#000000"
                      m="0"
                      pt={{ base: "2", lg: "5" }}
                    >
                      Selected
                    </Text>
                    <SimpleGrid
                      columns={[2, 2, 3]}
                      spacing={{ base: "1", lg: "5" }}
                      pt={{ base: "2", lg: "5" }}
                    >
                      {cities?.split(",")?.map((city: any) => (
                        <Button
                          variant="outline"
                          borderRadius="29px"
                          py="2"
                          color="#515151"
                          fontWeight="400"
                          fontSize={{ base: "sm", lg: "lg" }}
                        >
                          {city}
                        </Button>
                      ))}
                    </SimpleGrid>
                    <Button
                      type="submit"
                      color="#FFFFFF"
                      variant="outline"
                      bgColor="#0EBCF5"
                      py="2"
                      mt="5"
                      fontWeight="600"
                      fontSize={{ base: "sm", lg: "md" }}
                      onClick={() => setEditCities(false)}
                    >
                      Save
                    </Button>
                  </Box>
                ) : (
                  <Flex gap={{ base: "", lg: "5" }} align="center" py="5">
                    <Text
                      color="#1E1E1E"
                      fontSize={{ base: "sm", lg: "md" }}
                      fontWeight="552"
                      m="0"
                    >
                      {`Locality: ${cities} `}
                    </Text>
                    <GrFormEdit
                      size="20px"
                      onClick={() => setEditCities(true)}
                    />
                  </Flex>
                )}
              </Box>
            </Flex>
          </Box>
          {/* types of audiance */}
          <Box
            boxShadow="lg"
            border="1px"
            borderRadius="8px"
            borderColor="rgba(33, 33, 33, 0.20)"
            p="2"
            py="5"
          >
            <Stack align="end" px="3">
              <GrFormEdit
                size="20px"
                onClick={() => setOpenFilterAudianceModel(true)}
              />
            </Stack>
            <Flex gap="5">
              <Stack alignContent="flex-start">
                <Image src={people} alt="" />
              </Stack>
              <Box>
                <Text
                  fontSize={{ base: "", lg: "lg" }}
                  fontWeight="600"
                  color="#000000"
                  m="0"
                >
                  Type of audience profile
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                  pt="1"
                >
                  The number of people around the screen per day
                </Text>
                <Stack pt="5">
                  <Flex gap="2">
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#1E1E1E"
                      m="0"
                      pt="1"
                    >
                      Gender preference:
                    </Text>
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#323232"
                      m="0"
                      pt="1"
                    >
                      {getOnlyTrueValue(genderPreference).join(", ")}
                    </Text>
                  </Flex>
                  <Flex gap="2">
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#1E1E1E"
                      m="0"
                      pt="1"
                    >
                      Employment type:
                    </Text>
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#323232"
                      m="0"
                      pt="1"
                    >
                      {getOnlyTrueValue(employmentTypes).join(", ")}
                    </Text>
                  </Flex>
                  <Flex gap="2">
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#1E1E1E"
                      m="0"
                      pt="1"
                    >
                      Crowd mobility:
                    </Text>
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#323232"
                      m="0"
                      pt="1"
                    >
                      {getOnlyTrueValue(croudMobability).join(", ")}
                    </Text>
                  </Flex>
                  <Flex gap="2">
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#1E1E1E"
                      m="0"
                      pt="1"
                    >
                      Locality:
                    </Text>
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#323232"
                      m="0"
                      pt="1"
                    >
                      {screenHighlights?.join(", ")}
                    </Text>
                  </Flex>
                  <Flex gap="2">
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#1E1E1E"
                      m="0"
                      pt="1"
                    >
                      Age group:
                    </Text>
                    <Text
                      fontSize={{ base: "sm", lg: "sm" }}
                      fontWeight="552"
                      color="#323232"
                      m="0"
                      pt="1"
                    >
                      {`${ageRange[0]} - ${ageRange[1]} years`}
                    </Text>
                  </Flex>
                </Stack>
              </Box>
            </Flex>
          </Box>
          <Box
            boxShadow="lg"
            border="1px"
            borderRadius="8px"
            borderColor="rgba(33, 33, 33, 0.20)"
            p="2"
            py="5"
            px="10"
          >
            <Text
              fontSize={{ base: "", lg: "lg" }}
              fontWeight="600"
              color="#000000"
            >
              Total Screen Found According To Filter
            </Text>
            <Text
              fontSize={{ base: "", lg: "lg" }}
              fontWeight="600"
              color={
                loadingScreens
                  ? "#000000"
                  : screens?.length > 0
                  ? "green"
                  : "red"
              }
            >
              {loadingScreens
                ? "Loading...."
                : screens?.length === 0
                ? "No screens found according to current filter, change the filter options"
                : `${screens?.length} screens found`}
            </Text>
          </Box>
          {/* budgut */}
          <Box
            boxShadow="lg"
            border="1px"
            borderRadius="8px"
            borderColor="rgba(33, 33, 33, 0.20)"
            p="2"
            py="5"
          >
            <Flex gap="5">
              <Stack alignContent="flex-start">
                <Image src={budgut} alt="" />
              </Stack>
              <Box>
                <Text
                  fontSize={{ base: "", lg: "lg" }}
                  fontWeight="600"
                  color="#000000"
                  m="0"
                >
                  Expected budget{" "}
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#5B5B5B"
                  m="0"
                  pt="1"
                >
                  Enter the no of audience you want engagement with
                </Text>
                <Text
                  fontSize={{ base: "lg", lg: "40px" }}
                  fontWeight="600"
                  color="#0EBCF5"
                  m="0"
                  pt="10"
                >
                  ₹{amount}
                </Text>
                <Slider
                  aria-label="Temperature"
                  defaultValue={amount}
                  valueLabelDisplay="auto"
                  marks
                  onChange={handleChange}
                  min={800}
                  max={2000}
                />
                <Flex justifyContent="space-between">
                  <Text
                    fontSize={{ base: "sm", lg: "sm" }}
                    fontWeight="600"
                    color="#A9B4CC"
                    m="0"
                    pt="1"
                  >
                    Min: ₹800
                  </Text>
                  <Text
                    fontSize={{ base: "sm", lg: "sm" }}
                    fontWeight="600"
                    color="#A9B4CC"
                    m="0"
                    pt="1"
                  >
                    2,000
                  </Text>
                </Flex>
              </Box>
            </Flex>
          </Box>
        </Flex>
        <Flex direction="column">
          <Box
            boxShadow="lg"
            border="1px"
            borderRadius="8px"
            borderColor="rgba(33, 33, 33, 0.20)"
            p="10"
          >
            <Box>
              <Text
                fontSize={{ base: "", lg: "2xl" }}
                fontWeight="600"
                color="#000000"
                m="0"
              >
                Reach
              </Text>
              <Text
                fontSize={{ base: "", lg: "lg" }}
                fontWeight="600"
                color="#000000"
                m="0"
                pt="5"
              >
                Expected number of plays
              </Text>
              <Text
                fontSize={{ base: "13px", lg: "13px" }}
                fontWeight="400"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                Enter the no of audience you want engagement with
              </Text>
              <Flex justifyContent="space-between" pt="2">
                <Text
                  fontSize={{ base: "13px", lg: "13px" }}
                  fontWeight="440"
                  color="#0EBCF5"
                  m="0"
                >
                  Expected number of enggagements{" "}
                </Text>
                <Text
                  fontSize={{ base: "md", lg: "lg" }}
                  fontWeight="400"
                  color="#000000"
                  m="0"
                >
                  265 - 2.5K
                </Text>
              </Flex>
              <Text
                fontSize={{ base: "", lg: "lg" }}
                fontWeight="600"
                color="#000000"
                m="0"
                pt="5"
              >
                Expected Total Playtime
              </Text>
              <Text
                fontSize={{ base: "13px", lg: "13px" }}
                fontWeight="400"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                Enter the no of audience you want engagement with
              </Text>
              <Flex justifyContent="space-between" pt="2">
                <Text
                  fontSize={{ base: "13px", lg: "13px" }}
                  fontWeight="440"
                  color="#0EBCF5"
                  m="0"
                >
                  Expected number of enggagements{" "}
                </Text>
                <Text
                  fontSize={{ base: "md", lg: "lg" }}
                  fontWeight="400"
                  color="#000000"
                  m="0"
                >
                  265 - 2.5K
                </Text>
              </Flex>
            </Box>
          </Box>
          <Box>
            <Divider />
            <Text
              fontSize={{ base: "", lg: "2xl" }}
              fontWeight="600"
              color="#000000"
              m="0"
            >
              Price details
            </Text>
            <Flex justifyContent="space-between" pt="2">
              <Text
                fontSize={{ base: "md", lg: "md" }}
                fontWeight="400"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                Total cost of slots
              </Text>
              <Text
                fontSize={{ base: "md", lg: "md" }}
                fontWeight="400"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                ₹6000
              </Text>
            </Flex>
            <Flex justifyContent="space-between" pt="2">
              <Text
                fontSize={{ base: "md", lg: "md" }}
                fontWeight="400"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                SGST
              </Text>
              <Text
                fontSize={{ base: "md", lg: "md" }}
                fontWeight="400"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                ₹123
              </Text>
            </Flex>
            <Flex justifyContent="space-between" py="2">
              <Text
                fontSize={{ base: "md", lg: "md" }}
                fontWeight="400"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                CGST
              </Text>
              <Text
                fontSize={{ base: "md", lg: "md" }}
                fontWeight="400"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                ₹123
              </Text>
            </Flex>

            <Divider />
            <Flex justifyContent="space-between" py="2">
              <Text
                fontSize={{ base: "md", lg: "md" }}
                fontWeight="552"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                Total
              </Text>
              <Text
                fontSize={{ base: "md", lg: "md" }}
                fontWeight="552"
                color="#5B5B5B"
                m="0"
                pt="1"
              >
                ₹6034
              </Text>
            </Flex>
            <Stack align="center">
              <Button
                type="submit"
                color="#FFFFFF"
                variant="outline"
                bgColor="#0EBCF5"
                py="2"
                mt="2"
                fontWeight="600"
                fontSize={{ base: "sm", lg: "md" }}
                isLoading={loadingMedia}
                loadingText="Uploading..."
                onClick={createCampaign}
              >
                Process to pay ₹6034
              </Button>
            </Stack>
          </Box>
        </Flex>
      </SimpleGrid>
    </Box>
  );
}
