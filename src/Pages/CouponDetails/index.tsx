import { Box, Show, Hide } from "@chakra-ui/react";
import { CouponDetailsDesktopView } from "./CouponDetailsDesktopView";
import { useLocation } from "react-router-dom";
import { CouponDetailsMobileView1 } from "./CouponDetailsMobileView1";

export function CouponDetails(props: any) {
  const location = useLocation();
  const couponData = location?.state?.data;
  const path = location?.state?.path || "/";
  // console.log("couponDatta : ", couponData);
  return (
    <Box
      py={{ base: "7", lg: "75" }}
      px={{ base: "1", lg: "20" }}
      height="100%"
    >
      <Hide below="md">
        <CouponDetailsDesktopView couponData={couponData} location={location} />
      </Hide>
      <Show below="md">
        <CouponDetailsMobileView1 couponData={couponData} location={location} />
      </Show>
    </Box>
  );
}
