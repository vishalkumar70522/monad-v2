import {
  Box,
  Flex,
  Stack,
  Image,
  Text,
  Button,
  Divider,
  Center,
  UnorderedList,
  ListItem,
  VStack,
  Link,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { HiOutlineClock } from "react-icons/hi";
import { BiArrowBack } from "react-icons/bi";
import { BrandDetailsPopup } from "./BrandDetailsPopup";
import { AboutTheOfferPop } from "./AboutTheOfferPop";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { getBrandDetailsById } from "../../Actions/brandAction";
import { useDispatch } from "react-redux";
import { TermAndConditionPopup } from "./TermsAndConditionPopup";
import { Skeleton, message } from "antd";
import { getCampaignDetail } from "../../Actions/campaignAction";
import { MdContentCopy } from "react-icons/md";
import { convertIntoDateAndTime, getTimeValue } from "../../utils/dateAndTime";
import { ImageSliderPopup } from "../../components/commans";
import {
  SCREEN_LIST_BY_CAMPAIGN_IDS_RESET,
  SCREEN_LIST_BY_SCREEN_IDS_RESET,
} from "../../Constants/screenConstants";
import {
  getScreenListByCampaignIds,
  getScreenListByScreenIds,
} from "../../Actions/screenActions";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { ShowScreensLocation } from "../MyMap/ShowScreensLocation";
import { addOrRemoveCouponInWishlist } from "../../Actions/couponAction";
import { ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_RESET } from "../../Constants/campaignConstants";
import { StoreListByCoupon } from "./StoreListByCoupon";

const data =
  "Visit the nearest store listed below. Scan the MONAD QR code displayed at the store. Claim your deal";

export function CouponDetailsMobileView1(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const currentPath = window.location.pathname;
  const couponData = props?.couponData;
  const location = props?.location;

  const [modal1Open, setModal1Open] = useState<any>(false);
  const [imageShow, setImageShow] = useState<any>(false);
  const [jsonData, setJsonData] = useState<any>(null);
  const [commonScreens, setCommonScreens] = useState<any>([]);

  const [aboutTheOfferPopupOpen, setAboutTheOfferPopupOpen] =
    useState<any>(false);
  const [tarmAndConditionPopupOpen, setTarmAndConditionPopupOpen] =
    useState<any>(false);
  const [openStoreListByCoupon, setOpenStoreListByCoupon] =
    useState<any>(false);

  const brandDetails = useSelector((state: any) => state.brandDetails);
  const { loading, error, brand } = brandDetails;

  const campaignDetail = useSelector((state: any) => state.campaignDetail);
  const {
    loading: loadingCampaign,
    error: errorInCampaign,
    campaign,
  } = campaignDetail;

  const addRemoveCouponInWishlist = useSelector(
    (state: any) => state.addRemoveCouponInWishlist
  );
  const {
    loading: loadingAddRemoveCouponInWishlist,
    error: errorAddRemoveCouponInWishlist,
    success: successAddRemoveCouponInWishlist,
  } = addRemoveCouponInWishlist;

  // it will give the location where user can go and send plea to user and use that coupon
  const screenListByScreenIds = useSelector(
    (state: any) => state.screenListByScreenIds
  );
  const {
    loading: loadingScreenListByScreenIds,
    error: errorScreenListByScreenIds,
    screens,
  } = screenListByScreenIds;

  // it will give the location where user can go and scan the QRCode
  const screenListByCampaignIds = useSelector(
    (state: any) => state.screenListByCampaignIds
  );
  const {
    loading: loadingscreenListByCampaignIds,
    error: errorscreenListByCampaignIds,
    screens: screensByCampaignIds,
  } = screenListByCampaignIds;

  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  function isShopOpen(startTime: any, endTime: any) {
    const currentTime = getTimeValue(new Date()).split(":");

    const start = getTimeValue(startTime).split(":");

    const end = getTimeValue(new Date(endTime)).split(":");

    if (currentTime[0] >= start[0]) {
      if (currentTime[0] <= end[0]) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  const handelAddCouponInWishlist = () => {
    if (userInfo) {
      dispatch(
        addOrRemoveCouponInWishlist({
          action: "ADD", // ADD or REMOVE
          userId: userInfo?._id,
          couponId: couponData?._id,
        })
      );
    } else {
      message.warning("Please login first");
    }
  };

  const handleRedeemNow = () => {
    if (brand?.brandType == "OFFLINE") {
      setOpenStoreListByCoupon(true);
    } else {
      if (couponData?.couponRewardInfo?.onlineURL) {
        window.open(`${couponData?.couponRewardInfo?.onlineURL}`);
      } else {
        setOpenStoreListByCoupon(true);
      }
    }
  };

  useEffect(() => {
    if (errorAddRemoveCouponInWishlist) {
      message.error(errorAddRemoveCouponInWishlist);
      dispatch({ type: ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_RESET });
    }
    if (successAddRemoveCouponInWishlist) {
      message.success("Added successfully!");
      dispatch({ type: ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_RESET });
      setTimeout(() => {
        navigate("/");
      }, 500);
    }
  }, [successAddRemoveCouponInWishlist, errorAddRemoveCouponInWishlist]);

  useEffect(() => {
    if (errorScreenListByScreenIds) {
      message.error(errorScreenListByScreenIds);
      dispatch({ type: SCREEN_LIST_BY_SCREEN_IDS_RESET });
    }
    if (screens?.length > 0) {
      setCommonScreens(screens);
      const data = screens?.map((screen: any) => {
        return {
          type: "Feature",
          properties: {
            screen: screen?._id,
          },
          geometry: {
            coordinates: [screen?.lat, screen?.lng],
            type: "Point",
          },
        };
      });
      setJsonData({
        features: data,
      });
    }
  }, [errorScreenListByScreenIds, dispatch, screens]);

  useEffect(() => {
    if (errorscreenListByCampaignIds) {
      message.error(errorscreenListByCampaignIds);
      dispatch({ type: SCREEN_LIST_BY_CAMPAIGN_IDS_RESET });
    }
    if (screensByCampaignIds?.length > 0) {
      setCommonScreens(screensByCampaignIds);

      const data = screensByCampaignIds?.map((screen: any) => {
        return {
          type: "Feature",
          properties: {
            screen: screen?._id,
          },
          geometry: {
            coordinates: [screen?.lat, screen?.lng],
            type: "Point",
          },
        };
      });
      setJsonData({
        features: data,
      });
    }
  }, [errorScreenListByScreenIds, dispatch, screensByCampaignIds]);

  const getDays = () => {
    if (couponData?.couponRewardInfo?.validity?.to) {
      const to = new Date(couponData?.couponRewardInfo?.validity?.to);
      const toTime = to.getTime();
      let curentDate = new Date();
      const currentTime = curentDate.getTime();
      const days = ((toTime - currentTime) / (1000 * 60 * 60 * 24)).toFixed();
      return `Expire in ${days} days`;
    } else return "No time limit";
  };

  const onCopy = (value: any) => {
    navigator.clipboard.writeText(value);
  };
  //redeemCoupon

  useEffect(() => {
    dispatch(getBrandDetailsById(couponData?.brand));
    if (couponData?.campaigns?.length > 0) {
      dispatch(getCampaignDetail(couponData?.campaigns[0]));
    }
    if (location?.state?.showCouponCode) {
      // then we need to show shop location where user can go and redeem the coupon
      if (couponData?.rewardOfferPartners?.length > 0) {
        dispatch(getScreenListByScreenIds(couponData?.rewardOfferPartners));
      }
    } else {
      if (couponData?.allCampaigns?.length > 0) {
        dispatch(getScreenListByCampaignIds(couponData?.allCampaigns));
      }
    }
  }, [dispatch, props]);
  return (
    <Box mt="10">
      <BrandDetailsPopup
        modal1Open={modal1Open}
        setModal1Open={(value: any) => setModal1Open(value)}
        couponData={couponData}
        brand={brand}
      />
      <ImageSliderPopup
        imageShow={imageShow}
        setImageShow={(value: any) => setImageShow(value)}
        images={brand?.brandDetails?.images}
      />
      <AboutTheOfferPop
        aboutTheOfferPopupOpen={aboutTheOfferPopupOpen}
        setAboutTheOfferPopupOpen={(value: any) =>
          setAboutTheOfferPopupOpen(value)
        }
        brand={brand}
        coupon={couponData}
      />
      <StoreListByCoupon
        open={openStoreListByCoupon}
        onCancel={(value: any) => setOpenStoreListByCoupon(value)}
        brand={brand}
        coupon={couponData}
        screens={screens}
        jsonData={jsonData}
      />
      <TermAndConditionPopup
        tarmAndConditionPopupOpen={tarmAndConditionPopupOpen}
        setTarmAndConditionPopupOpen={(value: any) =>
          setTarmAndConditionPopupOpen(value)
        }
        brand={brand}
        coupon={couponData}
      />
      <Box width="100%" justifyContent="center">
        <Flex justifyContent="space-between" px="5" zIndex="1">
          <Center
            borderRadius="100%"
            bgColor="#F1F1F1"
            p="1"
            onClick={() => navigate("/")}
          >
            <BiArrowBack size="20px" color="#292929" />
          </Center>
          <Button
            color="#313131"
            fontSize="sm"
            variant="outline"
            py="6px"
            px="12px"
            background="rgba(246, 246, 246, 0.50)"
            fontWeight="511"
            leftIcon={<HiOutlineClock />}
          >
            {getDays()}
          </Button>
        </Flex>
        <Box
          mt="-10"
          as="video"
          src={`https://ipfs.io/ipfs/${campaign?.cid}`}
          autoPlay
          loop
          muted
          width="100%"
          display="inline-block"
          height="250px"
        ></Box>
      </Box>

      <Box px="5" borderTopRadius="16px">
        <Stack mt="-10" align="center">
          <Image
            src={brand?.brandDetails?.logo}
            width="80px"
            height="80px"
            alt="scanner"
            borderRadius="100%"
          />
        </Stack>
        <Text
          fontSize="lg"
          fontWeight="500"
          color="#000000"
          align="center"
          m="0"
        >
          {brand?.brandName}
        </Text>
        <Text
          fontSize="24px"
          fontWeight="621"
          color="#000000"
          align="center"
          m="0"
        >
          {couponData?.offerName?.toUpperCase()}
        </Text>
        {/* <Text
          fontSize="36px"
          fontWeight="621"
          color="#000000"
          align="center"
          m="0"
        >
          {`Flat ${couponData?.couponRewardInfo?.discountPersentage}% OFF`}
        </Text>
        <Text fontSize="md" fontWeight="535" color="#000000" align="center">
          {couponData?.couponRewardInfo?.minimumOrderCondition === "Order value"
            ? `Min spent: ₹${couponData?.couponRewardInfo?.minimumOrderValue}`
            : `Min Quantity: ${couponData?.couponRewardInfo?.minimumOrderQuantity}`}
        </Text> */}
        {location?.state?.showCouponCode ? (
          <Button
            rightIcon={<MdContentCopy size="20px" color="#000000" />}
            onClick={() => onCopy(couponData?.couponCode)}
            py="3"
            px="10"
            border="0.4px dashed"
            bgColor="#FFFFFF"
            color="#000000"
            borderRadius="48px"
            borderColor="#181818"
            width="100%"
            fontWeight="400"
          >
            {couponData?.couponCode}
          </Button>
        ) : null}

        <Text
          color="#484848"
          m="0"
          pt="2"
          fontSize="sm"
          fontWeight="511"
          textAlign="center"
          onClick={() =>
            brand?.brandDetails?.website?.split(":")[0] !== "https"
              ? window.open(`https://${brand?.brandDetails?.website}`)
              : window.open(brand?.brandDetails?.website)
          }
        >
          Visit <Link>{brand?.brandName} </Link>
          store and paste your code at checkout to avail the discount
        </Text>
        <Divider color="#E4E4E4" />

        <>
          <Text color="#484848" fontSize="md" fontWeight="518" textAlign="left">
            Follow these steps to avail the offer
          </Text>
          {data.split(".").map((value: String, index: any) => (
            <Text
              fontSize="sm"
              fontWeight="518"
              color="#484848"
              key={index}
            >{`${index + 1}. ${value}`}</Text>
          ))}
          {jsonData ? (
            <Box height="300px" width="100%" borderRadius="48px">
              <ShowScreensLocation data={jsonData} zoom="30" />
            </Box>
          ) : null}
          {loadingScreenListByScreenIds || loadingscreenListByCampaignIds ? (
            <>
              <Skeleton avatar active paragraph={{ rows: 1 }} />
              <Skeleton avatar active paragraph={{ rows: 1 }} />
            </>
          ) : (
            commonScreens?.length > 0 &&
            commonScreens?.map((screen: any, index: any) => (
              <Box
                mt="5"
                key={index}
                borderRadius="16px"
                boxShadow="md"
                justifyContent="center"
                p="27px 16px 25.379px 16px"
              >
                <Flex justifyContent="space-between">
                  <Stack>
                    <Text
                      fontSize="20px"
                      fontWeight="600"
                      color="#232323"
                      m="0"
                    >
                      {screen?.name}
                    </Text>
                    <Text
                      fontSize="15px"
                      fontWeight="400"
                      color="rgba(0, 0, 0, 0.80)"
                    >
                      {`${screen?.districtCity}, ${screen?.stateUT}`}
                    </Text>
                  </Stack>
                  {isShopOpen(screen?.startTime, screen?.endTime) ? (
                    <VStack>
                      <Box
                        border="2px"
                        borderRadius="2px"
                        borderColor="#37B349"
                        p="0.5"
                      >
                        <Button
                          borderRadius="2px"
                          bgColor="#37B349"
                          color="#FFFFFF"
                          fontSize="12px"
                          fontWeight="600"
                          py="2"
                        >
                          OPEN
                        </Button>
                      </Box>
                      <Text
                        fontSize="12px"
                        fontWeight="600"
                        color="rgba(0, 0, 0, 0.50)"
                      >
                        Closes
                        {convertIntoDateAndTime(screen.endTime)?.split(",")[2]}
                      </Text>
                    </VStack>
                  ) : (
                    <VStack>
                      <Box
                        border="2px"
                        borderRadius="2px"
                        borderColor="#E80000"
                        p="0.5"
                      >
                        <Button
                          borderRadius="2px"
                          bgColor="#E80000"
                          color="#FFFFFF"
                          fontSize="12px"
                          fontWeight="600"
                          py="2"
                        >
                          CLOSE
                        </Button>
                      </Box>
                      <Text
                        fontSize="12px"
                        fontWeight="600"
                        color="rgba(0, 0, 0, 0.50)"
                      >
                        Opens
                        {
                          convertIntoDateAndTime(screen.startTime)?.split(
                            ","
                          )[2]
                        }
                      </Text>
                    </VStack>
                  )}
                </Flex>
              </Box>
            ))
          )}
        </>
        {location?.state?.showCouponCode && (
          <>
            <Text
              color="#161616"
              m="0"
              fontSize="md"
              fontWeight="600"
              textAlign="left"
            >
              Offer details
            </Text>
            <UnorderedList fontSize="sm" fontWeight="400" color="#181818" p="0">
              <ListItem>
                {props?.coupon?.couponRewardInfo?.validity?.to
                  ? `Coupon valid till ${convertIntoDateAndTime(
                      props?.coupon?.couponRewardInfo?.validity?.to
                    )} `
                  : `This coupon has no expiry date, you can use when ever you want.`}{" "}
              </ListItem>
              <ListItem>{`This is a ${props?.coupon?.couponRewardInfo?.couponType} coupon.`}</ListItem>
              <ListItem>{`You can use this coupon only ${props?.coupon?.couponRewardInfo?.redeemFrequency} times.`}</ListItem>
            </UnorderedList>

            <Divider color="#E4E4E4" />
            <Text
              color="#161616"
              m="0"
              fontSize="20px"
              fontWeight="600"
              textAlign="left"
            >
              {couponData?.brandName}
            </Text>
            <Text
              color="#727272"
              m="0"
              fontSize="sm"
              fontWeight="530"
              textAlign="left"
            >
              {brand?.address}
            </Text>
            <Text
              color="#161616"
              fontSize="md"
              fontWeight="600"
              textAlign="left"
            >
              photos
            </Text>
            <div className="scrollmenu">
              {brand?.brandDetails?.images?.length > 0 &&
                brand?.brandDetails?.images?.map((cid: any) => (
                  <Image
                    key={cid}
                    src={`https://ipfs.io/ipfs/${cid}`}
                    borderRadius="8px"
                    alt="berger"
                    height="135px"
                    width="172px"
                    onClick={() => {
                      setImageShow(true);
                    }}
                  />
                ))}
            </div>
          </>
        )}

        <Text
          color="rgba(0, 51, 129, 0.70)"
          fontSize="15px"
          fontWeight="600"
          textAlign="left"
          pt="2"
          onClick={() => setTarmAndConditionPopupOpen(true)}
        >
          Terms and conditions apply
        </Text>
        <Flex gap="2" align="center" pb="20">
          <AiOutlineExclamationCircle
            size="20px"
            color="rgba(0, 51, 129, 0.70)"
          />
          <Text
            color="rgba(0, 51, 129, 0.70)"
            fontSize="15px"
            fontWeight="600"
            textAlign="left"
            m="0"
          >
            Report a problem
          </Text>
        </Flex>
      </Box>

      <div className="footer">
        <Stack align="center" py="5">
          {location?.state?.showCouponCode ? (
            <Button
              py="4"
              borderRadius="57px"
              color="#FFFFFF"
              bgColor="#0EBCF5"
              width="270px"
              onClick={handleRedeemNow}
            >
              Redeem now
            </Button>
          ) : (
            <Button
              py="4"
              borderRadius="57px"
              color="#FFFFFF"
              bgColor="#0EBCF5"
              width="270px"
              disabled={userInfo?.wishlist?.includes(couponData?._id)}
              onClick={handelAddCouponInWishlist}
            >
              Add to Wishlist
            </Button>
          )}
        </Stack>
      </div>
    </Box>
  );
}
