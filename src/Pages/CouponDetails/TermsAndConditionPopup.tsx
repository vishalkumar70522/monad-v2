import { Box, ListItem, OrderedList, Text } from "@chakra-ui/react";
import { Modal } from "antd";

export function TermAndConditionPopup(props: any) {
  const { coupon } = props;
  return (
    <Modal
      footer={[]}
      open={props?.tarmAndConditionPopupOpen}
      onOk={() => props?.setTarmAndConditionPopupOpen(false)}
      onCancel={() => props?.setTarmAndConditionPopupOpen(false)}
      style={{ top: 10 }}
    >
      <Box>
        <Text color="#969696" m="0" fontSize="lg" fontWeight="650" align="left">
          Terms & Conditions
        </Text>
        <OrderedList p="0">
          <ListItem>**ACCEPTANCE OF TERMS**</ListItem>
          <Text>{`By accessing and using the Monad platform (hereinafter referred to as "the Platform''), provided by Vinciis Creations Pvt Ltd. (hereinafter referred to as "the Company''), you agree to be bound by these Terms and Conditions. If you do not agree with any part of these terms, you may not use the Platform.`}</Text>
          <ListItem>**USE OF COUPONS**</ListItem>
          <Text>{`The Platform provides access to discount coupons and promotional offers from various third-party merchants ("Merchants"). The use of these coupons is subject to the terms and conditions set by the respective Merchants. Users are responsible for reviewing and complying with the Merchant-specific terms before redeeming any coupons.`}</Text>
          <Text>{`The Company is not responsible for the quality, accuracy, availability, or legality of the coupons provided by Merchants. Users use the coupons at their own risk.`}</Text>
          <ListItem>**USER ACCOUNTS**</ListItem>
          <Text>{`To access certain features of the Platform, you may be required to create a user account. You agree to provide accurate, complete, and up-to-date information during the registration process.`}</Text>
          <Text>{`You are solely responsible for maintaining the confidentiality of your account information, including your username and password. You are also responsible for all activities that occur under your account.`}</Text>
          <ListItem>**COUPON REDEMPTION**</ListItem>
          <Text>{`Users must adhere to the redemption instructions and expiration dates specified on each coupon. Coupons may have restrictions and limitations imposed by Merchants.`}</Text>
          <Text>{`The Company reserves the right to limit the number of times a user can redeem a specific coupon.`}</Text>
          <ListItem>**USER CONDUCT**</ListItem>
          <Text>{`You agree not to use the Platform for any unlawful or prohibited purpose. This includes, but is not limited to, transmitting any material that is defamatory, obscene, offensive, or otherwise objectionable.`}</Text>
          <Text>{`You may not engage in any activity that could damage, disrupt, or interfere with the operation of the Platform.`}</Text>
          <ListItem>**TERMINATION**</ListItem>
          <Text>{`The Company reserves the right to terminate or suspend user accounts and access to the Platform at its sole discretion, with or without cause, and without prior notice.`}</Text>
          <ListItem>**DISCLAIMER OF WARRANTIES**</ListItem>
          <Text>{`The Platform is provided "as is" and "as available" without any warranties, either expressed or implied. We make no guarantees regarding the availability, accuracy, or reliability of the Platform.`}</Text>
          <Text>{`We do not endorse, warrant, or guarantee any products or services offered by Merchants through the use of coupons on the Platform.`}</Text>
          <ListItem>**LIMITATION OF LIABILITY**</ListItem>
          <Text>{`To the fullest extent permitted by law, the Company and its affiliates shall not be liable for any direct, indirect, incidental, consequential, or punitive damages arising from your use of the Platform or any coupons obtained through it.`}</Text>
          <ListItem>**CHANGES TO TERMS**</ListItem>
          <Text>{`The Company reserves the right to modify, update, or revise these Terms and Conditions at any time. Users will be notified of significant changes, and continued use of the Platform after such modifications constitutes acceptance of the updated terms.`}</Text>
          <ListItem>**CONTACT INFORMATION**</ListItem>
          <Text>{`If you have any questions or concerns regarding these Terms and Conditions, please contact Vinciis Creations Pvt Ltd. at office@vinciis.in.`}</Text>
          <Text>{`By using the Platform, you acknowledge that you have read, understood, and agree to these Terms and Conditions.`}</Text>
        </OrderedList>
      </Box>
    </Modal>
  );
}
