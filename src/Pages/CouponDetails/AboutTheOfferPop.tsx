import { Box, Text, ListItem, UnorderedList } from "@chakra-ui/react";
import { Modal } from "antd";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";

export function AboutTheOfferPop(props: any) {
  // console.log(props);
  return (
    <Modal
      style={{}}
      footer={[]}
      open={props?.aboutTheOfferPopupOpen}
      onOk={() => props?.setAboutTheOfferPopupOpen(false)}
      onCancel={() => props?.setAboutTheOfferPopupOpen(false)}
    >
      <Box>
        <Text color="#969696" m="0" fontSize="lg" fontWeight="650" align="left">
          About the offer
        </Text>
        <Text>{props.coupon.offerDetails} </Text>
        <UnorderedList>
          <ListItem>
            {props?.coupon?.couponRewardInfo?.validity?.to
              ? `Coupon valid till ${convertIntoDateAndTime(
                  props?.coupon?.couponRewardInfo?.validity?.to
                )} `
              : `This coupon has no expiry date, you can use when ever you want.`}{" "}
          </ListItem>
          <ListItem>{`This is a ${props?.coupon?.couponRewardInfo?.couponType} coupon.`}</ListItem>
          <ListItem>{`You can use this coupon only ${props?.coupon?.couponRewardInfo?.redeemFrequency} times.`}</ListItem>
        </UnorderedList>
      </Box>
    </Modal>
  );
}
