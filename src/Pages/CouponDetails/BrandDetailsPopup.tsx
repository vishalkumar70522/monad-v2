import { Modal } from "antd";
import { Text, Button, Flex, Image, Box, SimpleGrid } from "@chakra-ui/react";
import { BsShare } from "react-icons/bs";
import { GiCircleCage } from "react-icons/gi";
import { useState } from "react";
import { ImageSliderPopup } from "../../components/commans";
import { useNavigate } from "react-router";

export function BrandDetailsPopup(props: any) {
  const navigate = useNavigate();
  const [imageShow, setImageShow] = useState<any>(false);
  const { couponData, brand } = props;

  // console.log(props?.brand?.brandDetails?.website);
  return (
    <Box>
      {brand && (
        <ImageSliderPopup
          imageShow={imageShow}
          setImageShow={(value: any) => setImageShow(value)}
          images={brand?.brandDetails?.images}
        />
      )}
      <Modal
        style={{}}
        footer={[]}
        open={props?.modal1Open}
        onOk={() => props?.setModal1Open(false)}
        onCancel={() => props?.setModal1Open(false)}
      >
        <Box>
          <Text
            color="#969696"
            m="0"
            fontSize="2xl"
            fontWeight="650"
            align="left"
          >
            About brand
          </Text>
          <Flex justifyContent="space-between" alignContent="center" mt="5">
            <Flex gap={{ base: "5", lg: "7" }}>
              <Image
                src={brand?.brandDetails?.logo}
                alt="berger"
                height="82px"
                width="82px"
              ></Image>
              <Box>
                <Text
                  color="#000000"
                  m="0"
                  fontSize={{ base: "lg", lg: "2xl" }}
                  fontWeight="530"
                  align="left"
                >
                  {brand?.brandName}
                </Text>
                <Text
                  onClick={() => navigate(`/brandProfile/${brand?._id}`)}
                  fontSize={{ base: "sm", lg: "md" }}
                >
                  See Profile
                </Text>
              </Box>
            </Flex>
            <Flex gap="2">
              <Text
                color="#313131"
                m="0"
                fontSize={{ base: "12px", lg: "sm" }}
                fontWeight="530"
                align="left"
              >
                Share
              </Text>
              <BsShare size="16px" color="#313131" />
            </Flex>
          </Flex>

          <Flex pt={{ base: "2", lg: "5" }} gap={{ base: "2", lg: "5" }}>
            <Button
              color="#FFFFFF"
              bgColor="#D7380E"
              py="3"
              fontSize={{ base: "", lg: "md" }}
              fontWeight="638"
              px="10"
              leftIcon={<GiCircleCage color="#FFFFFF" size="16px" />}
              onClick={() =>
                window.open(`https://${brand?.brandDetails?.website}`, "_blank")
              }
            >
              {/* <a
                href={brand?.brandDetails?.website}
                target="_blank"
                rel="noreferrer"
              > */}
              Visit online store
              {/* </a> */}
            </Button>
          </Flex>

          <Flex gap="1" direction="column">
            <Text
              color="#676767"
              m="0"
              fontSize={{ base: "12px", lg: "md" }}
              fontWeight="400"
              align="left"
              pt="5"
              noOfLines={[3, 2]}
              // isTruncated={false}
            >
              {brand?.brandDetails?.aboutBrand}
            </Text>

            {/* <Text
            color="#D7380E"
            m="0"
            fontSize={{ base: "12px", lg: "md" }}
            fontWeight="400"
            align="left"
            pt="5"
          >
            {" more"}
          </Text> */}
          </Flex>
          <SimpleGrid
            columns={[2, 2, 6]}
            spacing={{ base: "2", lg: "5" }}
            pt="5"
          >
            {brand?.brandDetails?.images?.map((cid: any, index: any) => (
              <Image
                key={index}
                src={`https://ipfs.io/ipfs/${cid}`}
                borderRadius="8px"
                alt="berger"
                height="135px"
                width="172px"
                onClick={() => {
                  setImageShow(true);
                }}
              />
            ))}
          </SimpleGrid>
        </Box>
      </Modal>
    </Box>
  );
}
