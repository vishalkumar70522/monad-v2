import {
  Box,
  Button,
  Center,
  Flex,
  Text,
  Image,
  Stack,
} from "@chakra-ui/react";
import { claping, sun } from "../../assets/svg";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
export function CouponLandingPage(props: any) {
  const navigate = useNavigate();
  const location = useLocation();
  const currentPath = window.location.pathname;
  const coupon = location?.state?.data || props?.coupon;

  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  return (
    <Box p="5">
      <Center flexDirection="column">
        <Flex align="center" gap="1" pt="40">
          <Text color="#008D1F" fontSize="28px" fontWeight="700" m="0">
            Congratulations
          </Text>
          <Image src={claping} alt="claping" />
        </Flex>
        <Text color="#1E1E1E" fontSize="20px" fontWeight="600" m="0">
          You won!
        </Text>
        <Stack pt="10">
          <Image src={sun} alt="" />
        </Stack>
        <Stack mt="-30" borderRadius="100%" bgColor="#D9D9D9">
          <Image src={coupon?.brandLogo} alt="" height="123px" width="153px" />
        </Stack>
        <Text
          color="#000000"
          fontSize="24px"
          fontWeight="665"
          m="0"
          align="center"
        >
          {coupon?.brandName}
        </Text>

        <Text
          color="#008E06"
          fontSize="18px"
          fontWeight="499"
          m="0"
          pt="5"
          pb="20"
        >
          {coupon?.offerName}
        </Text>
        {userInfo ? (
          <Button
            color="#FFFFFF"
            bgColor="#D7380E"
            fontSize="md"
            fontWeight="665"
            py="3"
            width="100%"
            onClick={() =>
              navigate("/couponDetils", {
                state: {
                  data: coupon,
                },
              })
            }
          >
            GET DEAL
          </Button>
        ) : (
          <>
            <Button
              color="#FFFFFF"
              bgColor="#D7380E"
              fontSize="md"
              fontWeight="665"
              py="3"
              width="100%"
              onClick={() =>
                navigate("/signin", {
                  state: {
                    path: currentPath,
                    data: coupon,
                  },
                })
              }
            >
              SIGN IN & GET DEAL
            </Button>
            <Text
              pt="5"
              color="#0EBCF5"
              align="center"
              fontSize="md"
              fontWeight="576"
              onClick={() => navigate("/signup")}
            >
              Don’t have an account ? Sign up
            </Text>
          </>
        )}
      </Center>
    </Box>
  );
}
