import { Box, Text, Flex, Stack, Button } from "@chakra-ui/react";
import { Modal, message } from "antd";
import { ShowScreensLocation } from "../MyMap/ShowScreensLocation";
import { addNewPleaForUserRedeemCouponOffer } from "../../Actions/pleaActions";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { COUPOM_REDEEM_PLEA_RESET } from "../../Constants/pleaConstants";

export function StoreListByCoupon(props: any) {
  // console.log(props);
  const { coupon, jsonData, screens } = props;
  const dispatch = useDispatch<any>();

  const coupomRedeemUserPlea = useSelector(
    (state: any) => state.coupomRedeemUserPlea
  );
  const {
    error: errroCoupomRedeemUserPlea,
    success: successCoupomRedeemUserPlea,
  } = coupomRedeemUserPlea;

  useEffect(() => {
    if (successCoupomRedeemUserPlea) {
      message.success("Plea has sent to the user");
      dispatch({ type: COUPOM_REDEEM_PLEA_RESET });
      props?.onCancel(false);
    }
    if (errroCoupomRedeemUserPlea) {
      message.error(errroCoupomRedeemUserPlea);
      dispatch({ type: COUPOM_REDEEM_PLEA_RESET });
    }
  }, [errroCoupomRedeemUserPlea, successCoupomRedeemUserPlea]);

  const handleCoupomRedeemUserPlea = (toUser: any) => {
    dispatch(
      addNewPleaForUserRedeemCouponOffer({
        couponId: coupon?._id,
        toUser: toUser,
        couponCode: coupon?.couponCode,
      })
    );
  };

  return (
    <Modal
      style={{}}
      footer={[]}
      open={props?.open}
      onOk={() => props?.onCancel(false)}
      onCancel={() => props?.onCancel(false)}
    >
      <Box>
        <Text color="#969696" m="0" fontSize="lg" fontWeight="650" align="left">
          User to send plea request
        </Text>
        {jsonData ? (
          <Box height="300px" width="100%" borderRadius="48px">
            <ShowScreensLocation data={jsonData} zoom="30" />
          </Box>
        ) : null}
        {screens?.length > 0 &&
          screens?.map((screen: any) => (
            <Box
              mt="5"
              key={screen?._id}
              borderRadius="16px"
              boxShadow="md"
              justifyContent="center"
              p="27px 16px 25.379px 16px"
            >
              <Flex justifyContent="space-between" align="center">
                <Stack>
                  <Text fontSize="20px" fontWeight="600" color="#232323" m="0">
                    {screen?.name}
                  </Text>
                  <Text
                    fontSize="15px"
                    fontWeight="400"
                    color="rgba(0, 0, 0, 0.80)"
                  >
                    {`${screen?.districtCity}, ${screen?.stateUT}`}
                  </Text>
                </Stack>

                <Button
                  borderRadius="2px"
                  bgColor="#37B349"
                  color="#FFFFFF"
                  fontSize="16px"
                  fontWeight="600"
                  py="2"
                  height="50px"
                  onClick={() => handleCoupomRedeemUserPlea(screen?.master)}
                >
                  Send Plea
                </Button>
              </Flex>
            </Box>
          ))}
      </Box>
    </Modal>
  );
}
