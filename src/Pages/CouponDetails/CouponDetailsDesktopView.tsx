import {
  Box,
  Center,
  Text,
  Flex,
  Image,
  Stack,
  Button,
  Link,
  Hide,
  SimpleGrid,
  Tooltip,
  UnorderedList,
  ListItem,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { AiOutlineClockCircle } from "react-icons/ai";
import { BsDot, BsShare } from "react-icons/bs";
import { GiCircleCage } from "react-icons/gi";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { getBrandDetailsById } from "../../Actions/brandAction";
import { ImageSliderPopup, MediaContainer } from "../../components/commans";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { Skeleton } from "antd";

const aboutOffer = `Lorem ipsum dolor sit amet consectetur. Sed ultricies integer purus quam id diam. 
Est hendrerit in hac eu leo. At commodo senectus tincidunt tempus massa diam. 
Felis sit non pulvinar et ipsum at. Nisl donec scelerisque id non blandit dui.
Felis sit non pulvinar et ipsum at. Nisl donec scelerisque id non blandit dui.`;

export function CouponDetailsDesktopView(props: any) {
  const dispatch = useDispatch<any>();
  const [show, setShow] = useState<any>(false);
  const navigate = useNavigate();
  const [imageShow, setImageShow] = useState<any>(false);
  const currentPath = window.location.pathname;
  const couponData = props?.couponData;
  const location = props?.location;
  const [isCopied, setIsCopied] = useState<any>(false);

  const onCopy = (value: any) => {
    setIsCopied(true);
    navigator.clipboard.writeText(value);
  };

  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  const brandDetails = useSelector((state: any) => state.brandDetails);
  const { loading, error, brand } = brandDetails;

  const handelGetDel = () => {
    if (!userInfo) {
      navigate("/signin", {
        state: {
          path: currentPath,
          data: couponData,
        },
      });
    } else {
      setShow(true);
    }
  };

  useEffect(() => {
    dispatch(getBrandDetailsById(couponData?.brand));
  }, [dispatch, props, couponData]);
  return (
    <Box px={{ base: "2", lg: "20" }} pt={{ base: "75px", lg: "100px" }}>
      {loading ? (
        <Skeleton />
      ) : (
        <>
          {brand && (
            <ImageSliderPopup
              imageShow={imageShow}
              setImageShow={(value: any) => setImageShow(value)}
              images={brand?.brandDetails?.images}
            />
          )}
          <Stack align="center">
            <Box
              border="1px"
              p="2"
              borderColor="#E6E6E6"
              width="331px"
              height="201px"
              alignItems="center"
              justifyContent="center"
              borderRadius="4px"
            >
              {/* <Center> */}
              <Image
                src={
                  couponData?.couponRewardInfo?.images.length > 0
                    ? couponData?.couponRewardInfo?.images[0]
                    : brand?.brandDetails?.logo
                }
                alt="image"
                width="100%"
                height="100%"
              />
              {/* </Center> */}
            </Box>
            <Text
              onClick={() => navigate(`/brandprofile/${brand._id}`)}
              color="#000000"
              m="0"
              fontSize="2xl"
              fontWeight="530"
            >
              {brand?.brandName}
            </Text>
            <Text color="#585858" m="0" fontSize="lg" fontWeight="400">
              {couponData?.offerName}
            </Text>
            <Text
              fontSize="md"
              fontWeight="400"
              color="#008E06"
              align="left"
              m="0"
            >
              {couponData?.offerDetails}
            </Text>
            {show ? (
              <Center pt="5" flexDirection="column">
                <Flex>
                  <Text
                    color="#000000"
                    m="0"
                    fontSize="md"
                    px="5"
                    py="3"
                    fontWeight="400"
                    align="center"
                    border="1px"
                    borderColor="#D9D9D9"
                    borderLeftRadius="48px"
                  >
                    {couponData?.couponCode}
                  </Text>
                  <Button
                    borderEndRadius="48px"
                    color="#FFFFFF"
                    bgColor="#D7380E"
                    py="1"
                    fontSize={{ base: "", lg: "md" }}
                    fontWeight="638"
                    onClick={() => onCopy(couponData?.couponCode)}
                  >
                    {isCopied ? "Copied!" : "Copy"}
                  </Button>
                </Flex>
                <Text
                  color="#484848"
                  m="0"
                  fontSize="sm"
                  fontWeight="511"
                  pt="5"
                >
                  Visit{" "}
                  <Link color="teal.500" href={brand?.brandDetails?.website}>
                    {brand?.brandName}
                  </Link>{" "}
                  Brand and paste your code
                </Text>
                <Flex
                  gap={{ base: "5", lg: "5" }}
                  alignContent="center"
                  justifyContent="center"
                  align="center"
                >
                  <AiOutlineClockCircle color="#484848" />
                  <Text
                    color="#484848"
                    m="0"
                    fontSize={{ base: "12px", lg: "sm" }}
                    fontWeight="511"
                    pt="2"
                  >
                    Expires in 7 days{" "}
                  </Text>
                </Flex>
              </Center>
            ) : location?.state?.showCouponCode ? (
              <Box pt="5">
                <Button
                  color="#FFFFFF"
                  bgColor="#D7380E"
                  py="3"
                  px="10"
                  onClick={() => handelGetDel()}
                >
                  GET DEAL
                </Button>
              </Box>
            ) : null}
          </Stack>

          <Box
            px={{ base: "2", lg: "10" }}
            py={{ base: "3", lg: "10" }}
            mt="5"
            border="1px"
            borderColor="#D9D9D9"
            borderRadius="4px"
          >
            <Text
              color="#969696"
              m="0"
              fontSize="2xl"
              fontWeight="650"
              align="left"
            >
              About brand
            </Text>
            <Flex justifyContent="space-between" alignContent="center" mt="5">
              <Flex gap={{ base: "5", lg: "7" }}>
                <Image
                  src={brand?.brandDetails?.logo}
                  alt="berger"
                  height="82px"
                  width="82px"
                  onClick={() => navigate(`/brandprofile/${brand._id}`)}
                />
                <Box>
                  <Text
                    color="#000000"
                    m="0"
                    fontSize={{ base: "md", lg: "2xl" }}
                    fontWeight="530"
                    align="left"
                    onClick={() => navigate(`/brandprofile/${brand._id}`)}
                  >
                    {brand?.brandName}
                  </Text>

                  <Hide below="md">
                    <Flex
                      pt={{ base: "2", lg: "5" }}
                      gap={{ base: "2", lg: "5" }}
                    >
                      <Button
                        color="#FFFFFF"
                        bgColor="#D7380E"
                        py="3"
                        fontSize={{ base: "", lg: "md" }}
                        fontWeight="638"
                        px="10"
                        leftIcon={<GiCircleCage color="#FFFFFF" size="16px" />}
                        onClick={() =>
                          brand?.brandDetails?.website.split(":")[0] !== "https"
                            ? window.open(
                                `https://${brand?.brandDetails?.website}`
                              )
                            : window.open(brand?.brandDetails?.website)
                        }
                      >
                        {/* <a
                          href={`https://${brand?.brandDetails?.website}`}
                          target="_blank"
                          rel="noreferrer"
                        > */}
                        Visit online store
                        {/* </a> */}
                      </Button>
                    </Flex>
                  </Hide>
                </Box>
              </Flex>
              <Flex gap="2">
                <Text
                  color="#313131"
                  m="0"
                  fontSize={{ base: "12px", lg: "sm" }}
                  fontWeight="530"
                  align="left"
                >
                  Share
                </Text>
                <BsShare size="16px" color="#313131" />
              </Flex>
            </Flex>
            <Flex gap="1" direction="column">
              <Text
                color="#676767"
                m="0"
                fontSize={{ base: "12px", lg: "md" }}
                fontWeight="400"
                align="left"
                pt="5"
                noOfLines={[3, 2]}
                // isTruncated={false}
              >
                {brand?.brandDetails?.aboutBrand}
              </Text>

              {/* <Text
            color="#D7380E"
            m="0"
            fontSize={{ base: "12px", lg: "md" }}
            fontWeight="400"
            align="left"
            pt="5"
          >
            {" more"}
          </Text> */}
            </Flex>
            <SimpleGrid
              columns={[1, 2, 3, 4, 5]}
              spacing={{ base: "2", lg: "5" }}
              pt="5"
            >
              {brand?.brandDetails?.images?.map((cid: any, index: any) => (
                <Tooltip
                  hasArrow
                  label="See in full size"
                  fontSize="md"
                  bg="gray.300"
                  color="black"
                  key={index}
                >
                  <Box
                    m="2"
                    onClick={() => {
                      setImageShow(true);
                    }}
                  >
                    <MediaContainer
                      cid={cid}
                      key={index}
                      height="135px"
                      width="172px"
                    />
                  </Box>
                </Tooltip>
              ))}
            </SimpleGrid>
          </Box>
          {show && (
            <Box
              px={{ base: "2", lg: "10" }}
              py={{ base: "3", lg: "10" }}
              mt="5"
              border="1px"
              borderColor="#D9D9D9"
              borderRadius="4px"
            >
              <Text
                color="#000000"
                m="0"
                fontSize={{ base: "sm", lg: "lg" }}
                fontWeight="565"
                align="left"
                p="5"
              >
                About this offer
              </Text>
              <Box>
                <UnorderedList>
                  <ListItem>
                    {couponData?.couponRewardInfo?.validity?.to
                      ? `Coupon valid till ${convertIntoDateAndTime(
                          couponData?.couponRewardInfo?.validity?.to
                        )} `
                      : `This coupon has no expiry date, you can use when ever you want.`}{" "}
                  </ListItem>
                  <ListItem>{`This is a ${couponData?.couponRewardInfo?.couponType} coupon.`}</ListItem>
                  <ListItem>{`You can use this coupon only ${couponData?.couponRewardInfo?.redeemFrequency} times.`}</ListItem>
                </UnorderedList>
              </Box>
              <Text
                color="#000000"
                m="0"
                fontSize={{ base: "sm", lg: "lg" }}
                fontWeight="556"
                align="left"
                p="5"
              >
                How to get this offer
              </Text>
              {aboutOffer?.split("\n").map((text: any, index: any) => (
                <Flex key={index} gap={2}>
                  <BsDot color="#595959" />
                  <Text
                    color="#595959"
                    m="0"
                    fontSize={{ base: "12px", lg: "15px" }}
                    fontWeight="400"
                    align="left"
                  >
                    {text}
                  </Text>
                </Flex>
              ))}
              <Text
                color="#000000"
                m="0"
                fontSize={{ base: "sm", lg: "lg" }}
                fontWeight="556"
                align="left"
                p="5"
              >
                Terms and conditions
              </Text>
              {aboutOffer?.split("\n").map((text: any, index: any) => (
                <Flex key={index} gap={2}>
                  <BsDot color="#595959" />
                  <Text
                    color="#595959"
                    m="0"
                    fontSize={{ base: "12px", lg: "15px" }}
                    fontWeight="400"
                    align="left"
                  >
                    {text}
                  </Text>
                </Flex>
              ))}
            </Box>
          )}
        </>
      )}
    </Box>
  );
}
