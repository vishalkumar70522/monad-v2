import { Box, Button, Center, SimpleGrid, Stack, Text } from "@chakra-ui/react";
import { SingleScreenWithCampaign } from "../../components/commans";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { CreateCampaignName } from "../Models/CreateCampaignName";
import { UploadMedia } from "../Models/UploadMedia";
import { useDispatch } from "react-redux";
import { createVideoFromImage } from "../../Actions/videoFromImage";
import { uploadMedia } from "../../Actions/mediaActions";
import { useSelector } from "react-redux";
import { getUserWalletBalance } from "../../Actions/walletAction";
import { message } from "antd";

export function Cart() {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();

  const items: string | null = window.localStorage.getItem("carts");
  const value = items ? JSON.parse(items) : [];

  const [itemList, setItemList] = useState<any>(value);
  const [openCreateCampaignName, setOpenCreateCampaignName] =
    useState<boolean>(false);
  const [openUploadMedia, setOpenUploadMedia] = useState<boolean>(false);

  const [campaignName, setCampaignName] = useState<any>("");
  const [fileUrl, setFileUrl] = useState<any>("");

  const walletBalance = useSelector((state: any) => state.walletBalance);

  const {
    loading: loadingWalletBalance,
    error: errorWalletBalance,
    success: successWalletBalance,
    wallet,
  } = walletBalance;

  //   console.log("wallet : ", wallet);
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  // console.log("itemslist : ", itemList);

  const handelRemoveItemFromCart = (item: any, index: any) => {
    const remainingItems = itemList.filter((data: any, i: any) => i !== index);
    console.log("remainingItems : ", remainingItems);
    window.localStorage.setItem("carts", JSON.stringify(remainingItems));
    setItemList(remainingItems);
  };

  const handleCreateVideoFromImage = (filedata: any) => {
    dispatch(createVideoFromImage(filedata));
    navigate("/singleCampaignOnMultipleScreens", {
      state: {
        campaignName,
        cartItems: itemList,
      },
    });
    setCampaignName("");
    setFileUrl("");
  };

  const handlePay = () => {
    if (wallet?.balance >= getTotalBillAmount()) {
      //  DO NEW PROCESS OF CREATE CAMPAIGN
      setOpenCreateCampaignName(true);
    } else {
      // send notification to user to recharge your wallet
      message.warning(
        "You have not sufficient balance to continue, for continue recharge your wallet"
      );
      setTimeout(() => {
        navigate("/wallet");
      }, 500);
    }
  };

  const getTotalBillAmount = () => {
    const x = itemList?.reduce((accum: any, current: any) => {
      return accum + current?.totalAmount;
    }, 0);
    return x;
  };

  const videoUploadHandler = async (e: any) => {
    e.preventDefault();

    dispatch(
      uploadMedia({
        title: campaignName,
        thumbnail:
          "https://bafybeicduvlghzcrjtuxkro7foazucvuyej25rh3humeujbzt7bmio4hsa.ipfs.w3s.link/raily.png",
        fileUrl,
        media: "",
      })
    );
    navigate("/singleCampaignOnMultipleScreens", {
      state: {
        campaignName,
        cartItems: itemList,
      },
    });
    setCampaignName("");
    setFileUrl("");
  };

  useEffect(() => {
    if (!userInfo) {
      navigate("/signIn", { state: { path: "/cart" } });
    }
    if (userInfo?.userWallet) {
      dispatch(getUserWalletBalance());
    } else {
      message.warning("You have no wallet, Please create new wallet first");
      setTimeout(() => {
        navigate("/wallet");
      }, 500);
    }
  }, [userInfo, dispatch]);

  return (
    <Box px={{ base: 2, lg: 20 }} pt={{ base: 75, lg: 100 }}>
      <CreateCampaignName
        open={openCreateCampaignName}
        onCancel={() => setOpenCreateCampaignName(false)}
        openUploadMedia={() => setOpenUploadMedia(true)}
        setCampaignName={(value: any) => setCampaignName(value)}
        campaignName={campaignName}
      />
      <UploadMedia
        open={openUploadMedia}
        onCancel={() => setOpenUploadMedia(false)}
        createVideo={handleCreateVideoFromImage}
        videoUploadHandler={videoUploadHandler}
        setCampaignName={(value: any) => setCampaignName(value)}
        setFileUrl={(value: any) => setFileUrl(value)}
      />
      {itemList?.length > 0 ? (
        <Stack spacing={{ base: "10", lg: "20" }}>
          <SimpleGrid
            columns={[1, 1, 3]}
            spacing={5}
            pt={{ base: "", lg: "10" }}
          >
            {itemList?.map((data: any, index: any) => (
              <Stack key={index}>
                <SingleScreenWithCampaign key={index} data={data} />
                <Button
                  py="3"
                  fontWeight="600"
                  fontSize={{ base: "md", lg: "lg" }}
                  bgColor="red"
                  color="#FFFFFF"
                  onClick={() => handelRemoveItemFromCart(data, index)}
                >
                  Remove items from cart
                </Button>
              </Stack>
            ))}
          </SimpleGrid>
          <Stack align="center">
            {" "}
            <Button
              width="250px"
              py="3"
              fontWeight="600"
              fontSize={{ base: "md", lg: "lg" }}
              bgColor="red"
              color="#FFFFFF"
              onClick={handlePay}
            >
              Process to pay ₹{getTotalBillAmount()}
            </Button>
          </Stack>
        </Stack>
      ) : (
        <Center flexDirection="column" pt="10" height="500px">
          <Text
            fontSize={{ base: "", lg: "32px" }}
            fontWeight="565"
            color="#000000"
            m="0"
          >
            No screens in cart, Get more screens to add in your cart
          </Text>
          <Text
            fontSize={{ base: "", lg: "md" }}
            fontWeight="600"
            color="#535353"
            m="0"
          >
            For creating campaign on multiple screens at a time
          </Text>
          <Stack pt="10">
            {" "}
            <Button
              color="#D7380E"
              bgColor="#FFFFFF"
              _hover={{ color: "#FFFFFF", bgColor: "#D7380E" }}
              fontSize={"24px"}
              fontWeight={"600"}
              variant="outline"
              px="5"
              py="2"
              onClick={() => navigate("/")}
            >
              Show screns
            </Button>
          </Stack>
        </Center>
      )}
    </Box>
  );
}
