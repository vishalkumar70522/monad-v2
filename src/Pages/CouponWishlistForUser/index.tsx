import {
  Box,
  Stack,
  Image,
  Text,
  SimpleGrid,
  Show,
  Flex,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Skeleton, message } from "antd";
import { ResultNotFound } from "../../components/commans";
import { ScanQRCode } from "../ScanQRCode";
import { FiUser } from "react-icons/fi";
import { scanner } from "../../assets/svg";
import { AiOutlineHome, AiTwotoneHeart } from "react-icons/ai";
import { getCouponFromUserWishlist } from "../../Actions/userActions";
import { addOrRemoveCouponInWishlist } from "../../Actions/couponAction";
import { ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_RESET } from "../../Constants/campaignConstants";
import { generateOfferDetails } from "../../utils/CouponUtils";

export function CouponWishlistForUser() {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [openQRScanner, setOpenQRScanner] = useState<any>(false);

  const userWishlist = useSelector((state: any) => state.userWishlist);
  const {
    loading: loadingCouponList,
    error: errorCouponList,
    coupons,
  } = userWishlist;

  const addRemoveCouponInWishlist = useSelector(
    (state: any) => state.addRemoveCouponInWishlist
  );
  const {
    loading: loadingAddRemoveCouponInWishlist,
    error: errorAddRemoveCouponInWishlist,
    success: successAddRemoveCouponInWishlist,
  } = addRemoveCouponInWishlist;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const handelClickBuuton = (path: string) => {
    if (!userInfo) {
      navigate("/signin", { state: { path } });
    } else {
      navigate(path);
    }
  };

  const handelRemove = (id: any) => {
    dispatch(
      addOrRemoveCouponInWishlist({
        action: "REMOVE", // ADD or REMOVE
        userId: userInfo?._id,
        couponId: id,
      })
    );
  };

  useEffect(() => {
    if (errorAddRemoveCouponInWishlist) {
      message.error(errorAddRemoveCouponInWishlist);
      dispatch({ type: ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_RESET });
    }
    if (successAddRemoveCouponInWishlist) {
      message.success("Coupon removed successfully!");
      dispatch({ type: ADD_OR_REMOVE_COUPON_IN_USER_WISHLIST_RESET });
      dispatch(getCouponFromUserWishlist());
    }
  }, [successAddRemoveCouponInWishlist, errorAddRemoveCouponInWishlist]);

  useEffect(() => {
    if (errorCouponList) {
      message.error(errorCouponList);
    }
  }, [errorCouponList]);

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/myWishlist" } });
    }
    dispatch(getCouponFromUserWishlist());
  }, [dispatch, userInfo, navigate]);
  return (
    <Box px={{ base: 5, lg: 20 }} py={{ base: 75, lg: 100 }}>
      <ScanQRCode
        open={openQRScanner}
        onCancel={() => setOpenQRScanner(false)}
      />
      <Text
        fontSize={{ base: "lg", lg: "4xl" }}
        fontWeight="600"
        color="#000000"
      >
        My Wishlist
      </Text>
      {loadingCouponList ? (
        <Box>
          <Skeleton avatar paragraph={{ rows: 4 }} />
          <Skeleton avatar paragraph={{ rows: 4 }} />
        </Box>
      ) : loadingCouponList === false && coupons?.length === 0 ? (
        <ResultNotFound path="/" />
      ) : (
        <SimpleGrid columns={[2, 2, 5]} spacing={{ base: "3", lg: "10" }}>
          {coupons?.map((coupon: any, index: any) => (
            <Stack
              border="1px"
              borderColor={"#E7E7E7"}
              borderRadius="4.84px"
              key={index}
              spacing={0}
            >
              <Image
                src={
                  coupon?.couponRewardInfo?.images.length > 0
                    ? coupon?.couponRewardInfo?.images[0]
                    : coupon?.brandLogo
                }
                alt="logo"
                borderRadius="4.84px"
                width="100%"
                height="138.22px"
                onClick={() =>
                  navigate("/couponDetils", {
                    state: {
                      data: coupon,
                      path: "/myWishlist",
                      showCouponCode: false,
                    },
                  })
                }
              />

              <Stack p="3">
                <Flex justifyContent="space-between">
                  <Text
                    fontSize={{ base: "12.8px", lg: "lg" }}
                    fontWeight="638"
                    color="#000000"
                    align="left"
                  >
                    {coupon?.brandName}
                  </Text>
                  <AiTwotoneHeart
                    color="#E9B824"
                    size="30px"
                    onClick={() => handelRemove(coupon?._id)}
                  />
                </Flex>
                <Text
                  fontSize={{ base: "17.6px", lg: "lg" }}
                  fontWeight="621"
                  color="#000000 80%"
                  align="left"
                  m="0"
                >
                  {generateOfferDetails(coupon)?.split("|")?.[0]}
                </Text>
                <Text
                  fontSize={{ base: "10.24px", lg: "md" }}
                  fontWeight="535"
                  color="#000000 80%"
                  align="left"
                  m="0"
                >
                  {generateOfferDetails(coupon)?.split("|")?.[1]}
                </Text>
              </Stack>
            </Stack>
          ))}
        </SimpleGrid>
      )}
      <Show below="md">
        <div className="footer">
          <Flex
            justifyContent="space-between"
            px="10"
            bgColor="#E2E2E2"
            py="1"
            borderTopEndRadius="40px"
            borderTopLeftRadius="40px"
          >
            <Stack onClick={() => handelClickBuuton("/")} align="center">
              <AiOutlineHome color="#494949" size="30px" />
              {/* <Image src={couponicon} alt="couponIcon" width="30px" /> */}
              <Text color="" fontSize="12px" fontWeight="400" m="0">
                Home
              </Text>
            </Stack>
            <Stack mt="-10" onClick={() => setOpenQRScanner(true)}>
              <Image
                src={scanner}
                width="80px"
                height="80px"
                alt="scanner"
                onClick={() => setOpenQRScanner(true)}
              />
            </Stack>
            <Stack
              onClick={() => handelClickBuuton("/userProfile")}
              align="center"
            >
              <FiUser color="#494949" size="30px" />
              <Text color="#494949" fontSize="12px" fontWeight="400" m="0">
                Account
              </Text>
            </Stack>
          </Flex>
        </div>
      </Show>
    </Box>
  );
}
