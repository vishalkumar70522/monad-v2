import { Box, Center, Flex, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { AiOutlineRight } from "react-icons/ai";
import { Schedule } from "../ScreenDashBoard/Schedule";
import { Location } from "../ScreenDashBoard/Location";
import { Highlights } from "../ScreenDashBoard/HighLights";
import { Impressions } from "../ScreenDashBoard/Impressions";
import { Setting } from "../ScreenDashBoard/Setting";
import { useSelector } from "react-redux";
import { message } from "antd";
import { useDispatch } from "react-redux";
import { detailsScreen } from "../../Actions/screenActions";

const buttonObject = [
  { value: "1", label: "Schedule" },
  { value: "2", label: "Location" },
  { value: "3", label: "Highlights" },
  { value: "4", label: "Impressions" },
  { value: "5", label: "Settings" },
];
export function EditScreen(props: any) {
  const screenId = window.location.pathname.split("/")[2];
  const [option, setOption] = useState<any>("1");
  const dispatch = useDispatch<any>();

  const screenDetails = useSelector((state: any) => state.screenDetails);
  const { loading: loadingScreen, error: errorScreen, screen } = screenDetails;

  useEffect(() => {
    if (errorScreen) message.error(errorScreen);
  }, [errorScreen]);

  const getScreenDetails = () => {
    dispatch(detailsScreen(screenId));
  };

  useEffect(() => {
    dispatch(detailsScreen(screenId));
  }, [screenId]);

  return (
    <Box px={{ base: 2, lg: 20 }} py={{ base: 5, lg: 10 }}>
      <Box boxShadow="2xl" p="5">
        <Text fontSize="2xl" fontWeight="600" color="#333333">
          Create your screen
        </Text>
        <Flex align="center" gap="10">
          {buttonObject.map((button: any, index: any) => (
            <Flex
              key={index}
              align="center"
              gap="2"
              onClick={() => setOption(index + 1)}
            >
              <Center
                borderRadius="100%"
                border="1px"
                borderColor="#CDCDCD"
                height="30px"
                width="30px"
                fontSize="sm"
                bgColor={index + 1 == option ? "#39E272" : "#FFFFFF"}
              >
                {button?.value}
              </Center>
              <Text fontSize="sm" fontWeight="400" color="#383838" m="0">
                {button?.label}
              </Text>
              <AiOutlineRight size="25px" color="#A8A8A8" />
            </Flex>
          ))}
        </Flex>
      </Box>
      <Box width="100%" p="5" boxShadow="2xl">
        {option == "1" ? (
          <Schedule
            screenId={screenId}
            show={true}
            screen={screen}
            getScreenDetails={getScreenDetails}
            setOption={(value: any) => setOption(value)}
          />
        ) : option == "2" ? (
          <Location
            screenId={screenId}
            show={true}
            screen={screen}
            getScreenDetails={getScreenDetails}
            setOption={(value: any) => setOption(value)}
          />
        ) : option == "3" ? (
          <Highlights
            screenId={screenId}
            show={true}
            screen={screen}
            getScreenDetails={getScreenDetails}
            setOption={(value: any) => setOption(value)}
          />
        ) : option == "4" ? (
          <Impressions
            screenId={screenId}
            show={true}
            screen={screen}
            getScreenDetails={getScreenDetails}
            setOption={(value: any) => setOption(value)}
          />
        ) : option == "5" ? (
          <Setting
            screenId={screenId}
            show={true}
            showScreenCode={false}
            screen={screen}
            getScreenDetails={getScreenDetails}
            setOption={(value: any) => setOption(value)}
          />
        ) : null}
      </Box>
    </Box>
  );
}
