import { Box, Button, Flex, Text } from "@chakra-ui/react";
import { Modal } from "antd";
import {
  BUY_X_GEY_Y,
  FLAT_DISCOUNT,
  FREEBIE,
  FREE_SHIPPING,
  LOYALTY_POINTS,
  PERSENTAGE_DISCOUNT,
} from "../../Constants/typeOfCouponsConstatnts";

export function TypeOfCouponModel(props: any) {
  return (
    <Modal
      title="Select coupon type"
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      maskClosable={false}
    >
      <Box>
        <Flex
          direction="column"
          bgColor="#FFFFFF"
          _hover={{ bgColor: "#D6D6D6" }}
          px={{ base: "2", lg: "3" }}
          py={{ base: "2", lg: "3" }}
          border="1px"
          borderColor="#D6D6D6"
          onClick={() => props?.setCouponType(PERSENTAGE_DISCOUNT)}
        >
          <Text fontSize="lg" fontWeight="400" color="#262626" m="0">
            % discounts
          </Text>
          <Text fontSize="13px" fontWeight="400" color="#0EBCF5" m="0">
            Get percentage off on item or order{" "}
          </Text>
        </Flex>
        <Flex
          direction="column"
          bgColor="#FFFFFF"
          _hover={{ bgColor: "#D6D6D6" }}
          px={{ base: "2", lg: "3" }}
          py={{ base: "2", lg: "3" }}
          border="1px"
          borderColor="#D6D6D6"
          onClick={() => props?.setCouponType(FLAT_DISCOUNT)}
        >
          <Text fontSize="lg" fontWeight="400" color="#262626" m="0">
            Flat discount
          </Text>
          <Text fontSize="13px" fontWeight="400" color="#0EBCF5" m="0">
            Get flat discount on item or order{" "}
          </Text>
        </Flex>
        <Flex
          direction="column"
          bgColor="#FFFFFF"
          _hover={{ bgColor: "#D6D6D6" }}
          px={{ base: "2", lg: "3" }}
          py={{ base: "2", lg: "3" }}
          border="1px"
          borderColor="#D6D6D6"
          onClick={() => props?.setCouponType(BUY_X_GEY_Y)}
        >
          <Text fontSize="lg" fontWeight="400" color="#262626" m="0">
            Buy X, Get Y
          </Text>
          <Text fontSize="13px" fontWeight="400" color="#0EBCF5" m="0">
            But X and get Y for free{" "}
          </Text>
        </Flex>
        <Flex
          direction="column"
          bgColor="#FFFFFF"
          _hover={{ bgColor: "#D6D6D6" }}
          px={{ base: "2", lg: "3" }}
          py={{ base: "2", lg: "3" }}
          border="1px"
          borderColor="#D6D6D6"
          onClick={() => props?.setCouponType(FREEBIE)}
        >
          <Text fontSize="lg" fontWeight="400" color="#262626" m="0">
            Freebie{" "}
          </Text>
          <Text fontSize="13px" fontWeight="400" color="#0EBCF5" m="0">
            Get free gift{" "}
          </Text>
        </Flex>
        <Flex
          direction="column"
          bgColor="#FFFFFF"
          _hover={{ bgColor: "#D6D6D6" }}
          px={{ base: "2", lg: "3" }}
          py={{ base: "2", lg: "3" }}
          border="1px"
          borderColor="#D6D6D6"
          onClick={() => props?.setCouponType(FREE_SHIPPING)}
        >
          <Text fontSize="lg" fontWeight="400" color="#262626" m="0">
            Free shipping{" "}
          </Text>
          <Text fontSize="13px" fontWeight="400" color="#0EBCF5" m="0">
            Get free shipping on online orders{" "}
          </Text>
        </Flex>
        <Flex
          direction="column"
          bgColor="#FFFFFF"
          _hover={{ bgColor: "#D6D6D6" }}
          px={{ base: "2", lg: "3" }}
          py={{ base: "2", lg: "3" }}
          border="1px"
          borderColor="#D6D6D6"
          onClick={() => props?.setCouponType(LOYALTY_POINTS)}
        >
          <Text fontSize="lg" fontWeight="400" color="#262626" m="0">
            Loyalty points
          </Text>
          <Text fontSize="13px" fontWeight="400" color="#0EBCF5" m="0">
            Get X brand loyalty points
          </Text>
        </Flex>
        <Flex
          px={{ base: "2", lg: "3" }}
          py={{ base: "2", lg: "3" }}
          justifyContent="flex-end"
        >
          <Button
            color="#0EBCF5"
            bgColor="#FFFFFF"
            fontSize=""
            fontWeight=""
            variant="outline"
            py="2"
            _hover={{ bgColor: "#0EBCF5", color: "#FFFFFF" }}
            onClick={() => props?.onCancel(false)}
          >
            Cancle
          </Button>
        </Flex>
      </Box>
    </Modal>
  );
}
