import {
  Box,
  Button,
  Center,
  Flex,
  Hide,
  Show,
  SimpleGrid,
  Skeleton,
  Stack,
  Text,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { SingleCouponForBrand } from "../../components/commans";
import { TypeOfCouponModel } from "./TypeOfCouponModel";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  deleteBrandCoupon,
  getCouponListForBrand,
} from "../../Actions/couponAction";
import { Select, message } from "antd";
import {
  COUPON_LIST_FOR_BRAND_RESET,
  DELETE_COUPON_RESET,
} from "../../Constants/couponConstants";

// const buttonOption = [
//   "All",
//   "Loyalty points",
//   "% discounts",
//   "Flat discount",
//   "Buy X, Get Y",
//   "Freebie",
//   "Free shipping",
// ];
const buttonOption = [
  { value: "1", label: "All" },
  { value: "2", label: "Loyalty points" },
  { value: "3", label: "% discounts" },
  { value: "4", label: "Flat discount" },
  { value: "5", label: "Buy X, Get Y" },
  { value: "6", label: "Freebie" },
  { value: "7", label: "Free shipping" },
];

export function CouponDashboardForBrand(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [selectedOption, setSelectedOption] = useState<any>("1");
  const [openCouponTypeModel, setOpenCouponTypeModel] = useState<any>(false);
  const [filterdCoupon, setFilteredCoupon] = useState<any>([]);

  const couponListForBrand = useSelector(
    (state: any) => state.couponListForBrand
  );
  const {
    loading: loadingCouponList,
    error: errorCouponList,
    coupons,
  } = couponListForBrand;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const deleteCoupon = useSelector((state: any) => state.deleteCoupon);
  const { error: errorDeleteCoupon, success: successDeleteCoupon } =
    deleteCoupon;

  const handleFilterCoupons = (value: any) => {
    const index = Number(value) - 1;
    const label = buttonOption[index]?.label;
    console.log("33333333 : ddd ", label);
    if (label !== "All") {
      const data = coupons?.filter(
        (coupon: any) => coupon?.couponRewardInfo?.couponType === label
      );
      setFilteredCoupon(data);
    } else {
      setFilteredCoupon(coupons);
    }
  };

  const handelSelectCouponType = (value: any) => {
    setOpenCouponTypeModel(false);
    navigate("/coupon/createCoupon", {
      state: {
        couponType: value,
      },
    });
  };

  useEffect(() => {
    if (userInfo?.isBrand && errorCouponList) {
      message.error(errorCouponList);
      dispatch({ type: COUPON_LIST_FOR_BRAND_RESET });
    }
    if (coupons) {
      setFilteredCoupon(coupons);
    }
    if (successDeleteCoupon) {
      message.success("Coupon deleted successfully!");
      dispatch({ type: DELETE_COUPON_RESET });
      dispatch(getCouponListForBrand(userInfo?.brand[0]));
    }
    if (errorDeleteCoupon) {
      message.error(errorDeleteCoupon);
      dispatch({ type: DELETE_COUPON_RESET });
    }
  }, [
    errorCouponList,
    coupons,
    userInfo,
    successDeleteCoupon,
    errorDeleteCoupon,
    dispatch,
  ]);

  const handelDeleteCoupon = (couponId: any, status: string) => {
    dispatch(deleteBrandCoupon(couponId, status));
  };

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", {
        state: {
          path: "/coupon/brand",
        },
      });
    } else if (userInfo?.isBrand && userInfo?.brand[0]) {
      dispatch(getCouponListForBrand(userInfo?.brand[0]));
    } else {
      alert("Please create brand profile first for creating coupons");
      navigate("/userProfile");
    }
  }, [navigate, dispatch, userInfo]);

  return (
    <Box
      bgColor="#F8F8F8"
      py={{ base: "75", lg: "100" }}
      px={{ base: "5", lg: "20" }}
      // height="100vh"
    >
      <TypeOfCouponModel
        open={openCouponTypeModel}
        onCancel={(value: any) => setOpenCouponTypeModel(value)}
        setCouponType={handelSelectCouponType}
      />
      <Flex justifyContent="space-between" align="center">
        <Text
          fontSize={{ base: "", lg: "32px" }}
          fontWeight="628"
          color="#333333"
          textAlign="left"
          m="0"
        >
          My offers
        </Text>
        <Button
          color="#D7380E"
          bgColor="#FFFFFF"
          _hover={{ color: "#FFFFFF", bgColor: "#D7380E" }}
          fontSize={{ lg: "18px", base: "" }}
          fontWeight={"600"}
          variant="outline"
          px="5"
          py="2"
          onClick={() => setOpenCouponTypeModel(true)}
        >
          Create offer
        </Button>
      </Flex>
      <Show below="md">
        <Stack p="4">
          <Select
            showSearch
            style={{ width: "100%" }}
            size="large"
            placeholder="Select option"
            optionFilterProp="children"
            onSelect={(value: any) => {
              console.log("value : ", value);
              setSelectedOption(value);
              handleFilterCoupons(value);
            }}
            filterOption={(input, option) =>
              (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
            }
            options={buttonOption}
          />
        </Stack>
      </Show>
      <Hide below="md">
        <SimpleGrid columns={[3, 4, 5, 6]} gap="5" pt="5">
          {buttonOption?.map((data: any, index: any) => (
            <Button
              key={index}
              borderRadius="36px"
              fontSize={{ lg: "md", base: "xs" }}
              fontWeight="565"
              py="2"
              px="5"
              onClick={() => {
                setSelectedOption(data?.value);
                handleFilterCoupons(data?.value);
              }}
              color={index === selectedOption ? "#FFFFFF" : "#2E2E2E"}
              bgColor={index === selectedOption ? "#0EBCF5" : "#FFFFFF"}
            >
              {data?.label}
            </Button>
          ))}
        </SimpleGrid>
      </Hide>
      {loadingCouponList ? (
        <Skeleton pt="5">
          <Box
            height={{ base: "200px", lg: "250px" }}
            width={{ base: "100%", lg: "417px" }}
          />
        </Skeleton>
      ) : filterdCoupon?.length > 0 ? (
        <SimpleGrid
          columns={[1, 2, 3]}
          spacing={{ base: "2", lg: "5" }}
          pt={{ base: "2", lg: "5" }}
        >
          {filterdCoupon?.map((coupon: any, index: any) => (
            <SingleCouponForBrand
              coupon={coupon}
              key={index}
              handelDeleteCoupon={handelDeleteCoupon}
            />
          ))}
        </SimpleGrid>
      ) : (
        <Center flexDirection="column" pt="10">
          <Text
            fontSize={{ base: "", lg: "32px" }}
            fontWeight="565"
            color="#000000"
            m="0"
          >
            Get more sales with coupons
          </Text>
          <Text
            fontSize={{ base: "", lg: "md" }}
            fontWeight="600"
            color="#535353"
            m="0"
          >
            Now you can create coupons to get attention from customers
          </Text>
          <Stack pt="10">
            {" "}
            <Button
              color="#D7380E"
              bgColor="#FFFFFF"
              _hover={{ color: "#FFFFFF", bgColor: "#D7380E" }}
              fontSize={"24px"}
              fontWeight={"600"}
              variant="outline"
              px="5"
              py="2"
              onClick={() => setOpenCouponTypeModel(true)}
            >
              Create offer
            </Button>
          </Stack>
        </Center>
      )}
    </Box>
  );
}
