import {
  Box,
  Text,
  Flex,
  Input,
  SimpleGrid,
  Stack,
  FormControl,
  Button,
  HStack,
  Image,
} from "@chakra-ui/react";
import {
  Checkbox,
  Radio,
  RadioChangeEvent,
  Select,
  SelectProps,
  Switch,
  message,
} from "antd";
import { useEffect, useState } from "react";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import { MuiPickersUtilsProvider, DateTimePicker } from "@material-ui/pickers";
import {
  IconButton as MiuiIconButton,
  InputAdornment,
} from "@material-ui/core";
import { BsCalendar2Date } from "react-icons/bs";
import { AiOutlinePlusCircle } from "react-icons/ai";
import { useSelector } from "react-redux";
import { getUserActiveCampaigns } from "../../Actions/userActions";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  createNewCoupon,
  updateCouponDetails,
} from "../../Actions/couponAction";
import {
  CREATE_COUPON_RESET,
  EDIT_COUPON_RESET,
} from "../../Constants/couponConstants";
import {
  BUY_X_GEY_Y,
  FLAT_DISCOUNT,
  FREEBIE,
  LOYALTY_POINTS,
  PERSENTAGE_DISCOUNT,
} from "../../Constants/typeOfCouponsConstatnts";
import { USER_ACTIVE_CAMPAIGN_RESET } from "../../Constants/userConstants";
import { generateString } from "../../utils";
import { addFileOnWeb3 } from "../../utils/web3";
import { getBrandDetailsById } from "../../Actions/brandAction";
import { getScreenListByUserIds } from "../../Actions/screenActions";
import { GET_BRAND_DETAILS_RESET } from "../../Constants/brandConstants";
import { SCREEN_LIST_BY_USER_IDS_RESET } from "../../Constants/screenConstants";

const orderConditionLable = ["Order quantity", "Order value"];
const orderConditionLable1 = ["On every purchase", "Above certain amount"];

const options: SelectProps["options"] = [
  {
    value: 1,
    label: "Once",
  },
  {
    value: 2,
    label: "Twice",
  },
  {
    value: 3,
    label: "Three times",
  },
];

export function CreateAndEditCouponPage(props: any) {
  const { couponType, editCoupon, coupon: oldCoupon } = props;
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [isGenerateUniqueCode, setIsGenerateUniqueCode] = useState<any>(false);
  const [orderCondition, setOrderCondition] = useState<any>("Order value");

  const [showCouponToCustomer, setShowCouponToCustomer] = useState<any>(true);
  const [validForOnLinePayment, setValidForOnLinePayment] = useState<any>(true);
  const [validForNewCostomer, setValidForNewCustomer] = useState<any>(true);
  const [autoApplyCoupon, setAutoApplyCoupon] = useState<any>(true);

  const [discountAmount, setDiscountAmount] = useState<any>();
  const [loadingAction, setLoadingAction] = useState<any>(false);

  const [couponStartTime, setCouponStartTime] = useState<any>(null);
  const [couponEndTime, setCouponEndTime] = useState<any>(null);
  const [takeEndTme, setTakeEndTime] = useState<any>(false);
  const [couponFrequency, setCouponFrequency] = useState<any>(1);
  const [couponName, setCouponName] = useState<any>("");
  const [offerDetails, setOfferDetails] = useState<any>("");
  const [couponCode, setCouponCode] = useState<any>("");
  const [numberOfCoupons, setNumberOfCoupons] = useState<any>();
  const [selectedCampaigns, setSelectedCampaigns] = useState<string[]>([]);
  const [offerPartners, setofferPartners] = useState<string[]>([]);
  const [onlineURL, setOnlineURL] = useState<string>("");

  const [images, setImages] = useState<any>([]);
  const [oldImages, setOldImages] = useState<any>([]);

  const [files, setFiles] = useState<any>([]);
  let hiddenInput1: any = null;

  //discount %
  const [discountPersentage, setDiscountPersentage] = useState<any>();
  const [minimumOrderQuantity, setMininumOrderQuantity] = useState<any>();
  const [minimumOrderValue, setMininumOrderValue] = useState<any>();
  const [campaignsOption, setCampaignsOption] = useState<any>([]);
  const [offerPartnerOption, setScreensOption] = useState<any>([]);

  //loyati points
  const [loyaltyPoints, setLoyaltyPoint] = useState<any>();
  //freebie items
  const [freebieItems, setFreebieItems] = useState<any>("");
  //buy x get y
  const [freeItems, setFreeItems] = useState<any>("");
  const [buyItems, setBuyItems] = useState<any>("");

  const userActiveCampaigns = useSelector(
    (state: any) => state.userActiveCampaigns
  );
  const { error: errorActiveCampaigns, campaigns } = userActiveCampaigns;
  const createCoupon = useSelector((state: any) => state.createCoupon);
  const {
    loading: loadingCreateCoupon,
    error: errorCreateCoupon,
    success: successCreateCoupon,
  } = createCoupon;

  const updateCoupon = useSelector((state: any) => state.updateCoupon);
  const {
    loading: loadingUpdateCoupon,
    error: errorUpdateCoupon,
    success: successUpadteCoupon,
  } = updateCoupon;

  const brandDetails = useSelector((state: any) => state.brandDetails);
  const { loading, error, brand } = brandDetails;

  const screenListByUserIds = useSelector(
    (state: any) => state.screenListByUserIds
  );
  const {
    loading: loadingScreenListByUserIds,
    error: errorScreenListByUserIds,
    screens,
  } = screenListByUserIds;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  // for generating unique coupon code
  const handelGenerateUniqueCode = (e: RadioChangeEvent) => {
    setIsGenerateUniqueCode(e.target.checked);
    if (e.target.checked) {
      const code = generateString(8);
      setCouponCode(code);
    } else {
      setCouponCode("");
    }
  };
  const onChange1 = ({ target: { value } }: RadioChangeEvent) => {
    setOrderCondition(value);
  };
  const handelSelectCouponUseedByPeruser = (value: string | string[]) => {
    setCouponFrequency(value);
  };

  const deleteImages = (image: any, index: any) => {
    setImages([...images.filter((data: any) => data.fileThumbnail !== image)]);
    setFiles([...files.filter((file: any, i: any) => i !== index)]);
  };
  const deleteOldImage = (src: any) => {
    setOldImages([...oldImages.filter((data: any) => data !== src)]);
  };
  async function handlePhotoSelect(file: any) {
    const dataTransfer = new DataTransfer();

    if (file?.type.split("/")[0] === "image") {
      const fileThumbnail = URL.createObjectURL(file);
      // dataTransfer.items.add(file);
      // const compressedFile = await compressImage(file, {
      //   quality: 0.5,
      //   type: "image/jpeg" || "image/png",
      // });

      const quality = 0.5;
      const type = file?.type;
      // Get as image data
      const imageBitmap = await createImageBitmap(file);

      // Draw to canvas
      const canvas = document.createElement("canvas");
      canvas.width = imageBitmap.width;
      canvas.height = imageBitmap.height;
      const ctx: any = canvas.getContext("2d");
      ctx.drawImage(imageBitmap, 0, 0);

      // Turn into Blob
      const blob: any = await new Promise((resolve) =>
        canvas.toBlob(resolve, type, quality)
      );

      // Turn Blob into File
      file = new File([blob], file.name, {
        type: blob?.type,
      });

      setImages([...images, { fileThumbnail, type: "image", cid: "" }]);
      setFiles([...files, file]);
    }
  }
  // after getting screens list generate screens option for dropdown
  useEffect(() => {
    if (errorScreenListByUserIds) {
      message.error(errorScreenListByUserIds);
      dispatch({ type: SCREEN_LIST_BY_USER_IDS_RESET });
    }
    if (screens) {
      const x = screens?.map((screen: any) => {
        return {
          label: `${screen?.name}, ${screen?.screenAddress}, ${screen?.districtCity}`,
          value: screen?._id,
        };
      });
      setScreensOption(x);
    }
  }, [errorScreenListByUserIds, screens]);

  // after getting brand details call getScreenListByUserIds() to get all screens list attached to that users
  useEffect(() => {
    if (brand?.affiliatedUsers.length > 0) {
      dispatch(getScreenListByUserIds(brand?.affiliatedUsers));
    }
    if (error) {
      message.error(error);
      dispatch({ type: GET_BRAND_DETAILS_RESET });
    }
  }, [brand, error]);

  //after getting campaigns list of that brand generate camapign option for dropdawn

  useEffect(() => {
    if (campaigns?.length > 0) {
      const data = campaigns?.map((campaign: any) => {
        return {
          value: campaign?._id,
          label: campaign?.campaignName,
        };
      });

      setCampaignsOption(data);
    }
    if (editCoupon) {
      setCouponName(oldCoupon?.offerName);
      setOfferDetails(oldCoupon?.offerDetails);
      setCouponCode(oldCoupon?.couponCode);
      setNumberOfCoupons(oldCoupon?.quantity);
      setMininumOrderQuantity(
        oldCoupon?.couponRewardInfo?.minimumOrderQuantity
      );
      setMininumOrderValue(oldCoupon?.couponRewardInfo?.minimumOrderValue);
      setOrderCondition(oldCoupon?.couponRewardInfo?.minimumOrderCondition);
      setCouponFrequency(oldCoupon?.couponRewardInfo?.redeemFrequency);

      setShowCouponToCustomer(
        oldCoupon?.couponRewardInfo?.showCouponToCustomer
      );
      setValidForNewCustomer(oldCoupon?.couponRewardInfo?.validForNewCostomer);
      setValidForOnLinePayment(
        oldCoupon?.couponRewardInfo?.validForOnLinePayment
      );
      setAutoApplyCoupon(oldCoupon?.couponRewardInfo?.autoApplyCoupon);
      setCouponStartTime(oldCoupon?.couponRewardInfo?.validity?.from);

      setLoyaltyPoint(oldCoupon?.couponRewardInfo?.loyaltyPoints);
      setDiscountAmount(oldCoupon?.couponRewardInfo?.discountAmount);
      setDiscountPersentage(oldCoupon?.couponRewardInfo?.discountPersentage);
      setFreebieItems(
        oldCoupon?.couponRewardInfo?.freebieItemsName?.join(", ")
      );
      setOldImages(oldCoupon?.couponRewardInfo?.images);
      setFreeItems(oldCoupon?.couponRewardInfo?.freeItems?.join(", "));
      setofferPartners(oldCoupon?.rewardOfferPartners);
      setBuyItems(oldCoupon?.couponRewardInfo?.buyItems?.join(", "));
      if (oldCoupon?.couponRewardInfo?.validity?.to) {
        setCouponEndTime(oldCoupon?.couponRewardInfo?.validity?.to);
        setTakeEndTime(true);
      }
      setSelectedCampaigns(oldCoupon?.campaigns);
      setOnlineURL(oldCoupon?.couponRewardInfo?.onlineURL);
    }
  }, [campaigns, editCoupon, oldCoupon]);

  // this useEffect will called first time when page load and at that time we calling 2 function
  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", {
        state: {
          path: "/coupon/brand",
        },
      });
    } else {
      dispatch(getUserActiveCampaigns(userInfo));
      dispatch(getBrandDetailsById(userInfo?.brand));
    }
  }, [navigate, dispatch, userInfo]);

  // validating form for mandatory field
  const validateForm = () => {
    if (!couponName) {
      message.error("Please enter coupon name");
      return false;
    } else if (!offerDetails) {
      message.error("Please enter offerDetails");
      return false;
    } else if (!couponCode) {
      message.error("Please enter  your coupon code");
      return false;
    } else if (numberOfCoupons < 1) {
      message.error("Please enter how many coupon you want to genetrate");
      return false;
    } else if (!couponStartTime) {
      message.error("Please select coupon start date");
      return false;
    } else if (takeEndTme && !couponEndTime) {
      message.error("Please select coupon end date");
      return false;
    } else {
      return true;
    }
  };

  const handelUpdateCoupon = async () => {
    setLoadingAction(true);
    if (validateForm()) {
      const cids = [];
      if (files?.length > 0) {
        for (let file of files) {
          try {
            const cid = await addFileOnWeb3(file);
            cids.push(`https://ipfs.io/ipfs/${cid}`);
          } catch (error) {
            message.error("Error in uploading product images");
          }
        }
      }

      dispatch(
        updateCouponDetails(
          {
            offerName: couponName,
            offerDetails,
            quantity: numberOfCoupons,
            couponCode,
            couponType,
            minimumOrderCondition: orderCondition,
            minimumOrderValue,
            minimumOrderQuantity,
            discountPersentage,
            redeemFrequency: couponFrequency,
            endDateAndTime: couponEndTime,
            startDateAndTime: couponStartTime,
            showCouponToCustomer,
            loyaltyPoints,
            buyItems: buyItems?.split(","),
            freeItems: freeItems?.split(","),
            discountAmount,
            freebieItemsName: freebieItems?.split(","),
            validForOnLinePayment,
            validForNewCostomer,
            autoApplyCoupon,
            campaigns: selectedCampaigns,
            rewardOfferPartners: offerPartners,
            onlineURL,
            images: [...oldImages, ...cids],
          },
          oldCoupon?._id
        )
      );
    } else setLoadingAction(false);
  };

  const handelCreateCoupon = async () => {
    setLoadingAction(true);
    if (validateForm()) {
      const cids = [];
      if (files?.length > 0) {
        for (let file of files) {
          try {
            const cid = await addFileOnWeb3(file);
            cids.push(`https://ipfs.io/ipfs/${cid}`);
          } catch (error) {
            message.error("Error in uploading product images");
          }
        }
      }

      dispatch(
        createNewCoupon({
          offerName: couponName,
          offerDetails,
          quantity: numberOfCoupons,
          couponCode,
          couponType,
          minimumOrderCondition: orderCondition,
          minimumOrderValue,
          minimumOrderQuantity,
          discountPersentage,
          redeemFrequency: couponFrequency,
          endDateAndTime: couponEndTime,
          startDateAndTime: couponStartTime,
          showCouponToCustomer,
          loyaltyPoints,
          buyItems: buyItems?.split(","),
          freeItems: freeItems?.split(","),
          discountAmount,
          freebieItemsName: freebieItems?.split(","),
          validForOnLinePayment,
          validForNewCostomer,
          autoApplyCoupon,
          campaigns: selectedCampaigns,
          images: cids,
          onlineURL,
          rewardOfferPartners: offerPartners,
        })
      );
    } else setLoadingAction(false);
  };

  useEffect(() => {
    if (errorCreateCoupon) {
      message.error(errorCreateCoupon);
      setLoadingAction(false);
      dispatch({ type: CREATE_COUPON_RESET });
    }
    if (successCreateCoupon) {
      setLoadingAction(false);
      message.success("Coupon created successfully");
      setTimeout(() => {
        dispatch({ type: CREATE_COUPON_RESET });
        navigate("/coupon/brand");
      }, 1000);
    }
    if (errorUpdateCoupon) {
      message.error(errorUpdateCoupon);
      dispatch({ type: EDIT_COUPON_RESET });
      setLoadingAction(false);
    }
    if (successUpadteCoupon) {
      setLoadingAction(false);
      message.success("Coupon updated successfully.");
      setTimeout(() => {
        dispatch({ type: EDIT_COUPON_RESET });
        navigate("/coupon/brand");
      }, 1000);
    }
    if (errorActiveCampaigns) {
      message.error(errorActiveCampaigns);
      dispatch({ type: USER_ACTIVE_CAMPAIGN_RESET });
    }
  }, [
    errorCreateCoupon,
    successCreateCoupon,
    errorUpdateCoupon,
    successUpadteCoupon,
    errorActiveCampaigns,
    dispatch,
    navigate,
  ]);

  return (
    <Stack>
      <Box
        px={{ base: "2", lg: "10" }}
        py={{ base: "2", lg: "18" }}
        width={{ base: "100%", lg: "858px" }}
        bgColor="#FFFFFF"
        borderRadius="4px"
      >
        <Text
          fontSize={{ base: "", lg: "24px" }}
          fontWeight="550"
          color="#000000"
          textAlign="left"
          m=""
        >
          {editCoupon ? "Edit Coupon" : "Create coupon"}
        </Text>
        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "2", lg: "40" }}
          pt={{ base: "3", lg: "5" }}
        >
          <Flex direction="column" gap="1">
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Enter offer name
              </Text>
            </Flex>

            <Input
              placeholder=""
              py="2"
              borderColor="#939393"
              borderRadius="4px"
              width={{ base: "", lg: "278px" }}
              value={couponName}
              onChange={(e: any) => setCouponName(e.target.value)}
            />
          </Flex>
          <Flex direction="column" gap="1">
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Enter offer Details
              </Text>
            </Flex>

            <Input
              placeholder=""
              py="2"
              borderColor="#939393"
              borderRadius="4px"
              width={{ base: "", lg: "278px" }}
              value={offerDetails}
              onChange={(e: any) => setOfferDetails(e.target.value)}
            />
          </Flex>
        </SimpleGrid>

        <SimpleGrid
          columns={[1, 1, 2]}
          spacing={{ base: "2", lg: "40" }}
          pt={{ base: "3", lg: "5" }}
        >
          <Flex direction="column" gap="1">
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Enter coupon code
              </Text>
            </Flex>
            <Input
              placeholder=""
              py="2"
              borderColor="#939393"
              borderRadius="4px"
              width={{ base: "", lg: "278px" }}
              value={couponCode}
              onChange={(e: any) => setCouponCode(e.target.value)}
            />
          </Flex>
          <Flex direction="column" gap="1">
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                No of coupouns
              </Text>
            </Flex>

            <Input
              placeholder=""
              py="2"
              type="number"
              min="1"
              borderColor="#939393"
              borderRadius="4px"
              width={{ base: "", lg: "278px" }}
              value={numberOfCoupons}
              onChange={(e: any) => setNumberOfCoupons(e.target.value)}
            />
          </Flex>
        </SimpleGrid>
        <Stack pt={{ base: "3", lg: "5" }}>
          <Checkbox onChange={handelGenerateUniqueCode}>
            Generate unique coupon codes
          </Checkbox>
          ;
        </Stack>
      </Box>
      {/* add images  */}
      <Box
        bgColor="#FFFFFF"
        borderRadius="4px"
        px={{ base: "2", lg: "10" }}
        py={{ base: "2", lg: "18" }}
        width={{ base: "100%", lg: "858px" }}
      >
        <Flex align="center" gap="0.5">
          <Text color="red" m="0">
            *
          </Text>
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="550"
            color="#000000"
            textAlign="left"
            m=""
          >
            {editCoupon
              ? "Change images or add new"
              : "Add images related to coupon"}
          </Text>
        </Flex>

        <HStack spacing={{ base: "1", lg: "3" }}>
          <HStack spacing={{ base: "1", lg: "3" }}>
            {oldImages?.map((src: any, index: any) => (
              <Image
                key={index}
                src={src}
                width="76px"
                height="86px"
                onClick={() => deleteOldImage(src)}
              />
            ))}
          </HStack>
          <HStack spacing={{ base: "1", lg: "3" }}>
            {images?.map((data: any, index: any) => (
              <Image
                key={index}
                src={data.fileThumbnail}
                width="76px"
                height="86px"
                onClick={() => deleteImages(data.fileThumbnail, index)}
              />
            ))}
          </HStack>

          <Button
            variant="outline"
            width="76px"
            height="86px"
            onClick={() => hiddenInput1.click()}
            borderColor="#8B8B8B"
            color="#8B8B8B"
          >
            <AiOutlinePlusCircle size="30px" />
          </Button>

          <Input
            hidden
            type="file"
            ref={(el) => (hiddenInput1 = el)}
            accept="image/png, image/jpeg video/mp4"
            onChange={(e: any) => handlePhotoSelect(e.target.files[0])}
          />
        </HStack>
      </Box>
      {/* 2 */}
      {couponType === PERSENTAGE_DISCOUNT ? (
        <Box
          bgColor="#FFFFFF"
          borderRadius="4px"
          px={{ base: "2", lg: "10" }}
          py={{ base: "2", lg: "18" }}
          width={{ base: "100%", lg: "858px" }}
        >
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="550"
            color="#000000"
            textAlign="left"
            m=""
          >
            {editCoupon ? "Edit details" : "Create details"}
          </Text>

          <SimpleGrid
            columns={[1, 1, 2]}
            spacing={{ base: "2", lg: "40" }}
            pt={{ base: "3", lg: "5" }}
          >
            <Flex direction="column" gap="1">
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Discount percentage
                </Text>
              </Flex>
              <Input
                placeholder=""
                py="2"
                type="number"
                min="1"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={discountPersentage}
                onChange={(e: any) => setDiscountPersentage(e.target.value)}
              />
            </Flex>
            <Flex direction="column" gap="1">
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Usage limit per coupon
                </Text>
              </Flex>

              <Select
                size="large"
                value={couponFrequency}
                onChange={handelSelectCouponUseedByPeruser}
                style={{ width: "100%" }}
                options={options}
              />
            </Flex>
          </SimpleGrid>
          <Stack pt={{ base: "3", lg: "5" }}>
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Minimum order condition
              </Text>
            </Flex>

            <Radio.Group
              options={orderConditionLable}
              onChange={onChange1}
              value={orderCondition}
            />
          </Stack>
          {orderCondition === "Order quantity" ? (
            <Flex direction="column" gap="1" pt={{ base: "2", lg: "5" }}>
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Minimum order quantity
                </Text>
              </Flex>

              <Input
                placeholder=""
                type="number"
                min="1"
                py="2"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={minimumOrderQuantity}
                onChange={(e: any) => setMininumOrderQuantity(e.target.value)}
              />
            </Flex>
          ) : (
            <Flex direction="column" gap="1" pt={{ base: "2", lg: "5" }}>
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Minimum order value
                </Text>
              </Flex>

              <Input
                placeholder=""
                py="2"
                type="number"
                min="1"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={minimumOrderValue}
                onChange={(e: any) => setMininumOrderValue(e.target.value)}
              />
            </Flex>
          )}
        </Box>
      ) : couponType === LOYALTY_POINTS ? (
        <Box
          bgColor="#FFFFFF"
          borderRadius="4px"
          px={{ base: "2", lg: "10" }}
          py={{ base: "2", lg: "18" }}
          width={{ base: "100%", lg: "858px" }}
        >
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="550"
            color="#000000"
            textAlign="left"
            m=""
          >
            Create details
          </Text>
          <Flex direction="column" gap="1">
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Enter the loyalty points for copoun
              </Text>
            </Flex>

            <Input
              placeholder=""
              py="2"
              type="number"
              min="1"
              borderColor="#939393"
              borderRadius="4px"
              width={{ base: "", lg: "278px" }}
              value={loyaltyPoints}
              onChange={(e: any) => setLoyaltyPoint(e.target.value)}
            />
          </Flex>
        </Box>
      ) : couponType === FREEBIE ? (
        <Box
          bgColor="#FFFFFF"
          borderRadius="4px"
          px={{ base: "2", lg: "10" }}
          py={{ base: "2", lg: "18" }}
          width={{ base: "100%", lg: "858px" }}
        >
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="550"
            color="#000000"
            textAlign="left"
            m=""
          >
            Create details
          </Text>
          <Stack pt={{ base: "3", lg: "5" }}>
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Provide Freebie
              </Text>
            </Flex>

            <Radio.Group
              options={orderConditionLable1}
              onChange={onChange1}
              value={orderCondition}
            />
          </Stack>
          <Flex direction="column" gap="1" pt={{ base: "2", lg: "5" }}>
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Minimum order quantity
              </Text>
            </Flex>

            <Input
              placeholder=""
              type="number"
              min="1"
              py="2"
              borderColor="#939393"
              borderRadius="4px"
              width={{ base: "", lg: "278px" }}
              value={minimumOrderQuantity}
              onChange={(e: any) => setMininumOrderQuantity(e.target.value)}
            />
          </Flex>
          <Flex direction="column" gap="1" pt={{ base: "2", lg: "5" }}>
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Add freebie item name
              </Text>
            </Flex>

            <Input
              placeholder=""
              min="1"
              py="2"
              borderColor="#939393"
              borderRadius="4px"
              width={{ base: "100%", lg: "100%" }}
              value={freebieItems}
              onChange={(e: any) => setFreebieItems(e.target.value)}
            />
          </Flex>
        </Box>
      ) : couponType === BUY_X_GEY_Y ? (
        <Box
          bgColor="#FFFFFF"
          borderRadius="4px"
          px={{ base: "2", lg: "10" }}
          py={{ base: "2", lg: "18" }}
          width={{ base: "100%", lg: "858px" }}
        >
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="550"
            color="#000000"
            textAlign="left"
            m=""
          >
            Create details
          </Text>

          <SimpleGrid
            columns={[1, 1, 2]}
            spacing={{ base: "2", lg: "40" }}
            pt={{ base: "3", lg: "5" }}
          >
            <Flex direction="column" gap="1">
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Buy Items List
                </Text>
              </Flex>
              <Input
                placeholder="ex. pens,  copy, .."
                py="2"
                min="1"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={buyItems}
                onChange={(e: any) => setBuyItems(e.target.value)}
              />
            </Flex>
            <Flex direction="column" gap="1">
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Free items list
                </Text>
              </Flex>
              <Input
                placeholder="ex. book, bottel, ..."
                py="2"
                min="1"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={freeItems}
                onChange={(e: any) => setFreeItems(e.target.value)}
              />
            </Flex>
          </SimpleGrid>
          <Stack pt={{ base: "3", lg: "5" }}>
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Minimum order condition
              </Text>
            </Flex>

            <Radio.Group
              options={orderConditionLable}
              onChange={onChange1}
              value={orderCondition}
            />
          </Stack>
          {orderCondition === "Order quantity" ? (
            <Flex direction="column" gap="1" pt={{ base: "2", lg: "5" }}>
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Minimum order quantity
                </Text>
              </Flex>

              <Input
                placeholder=""
                type="number"
                min="1"
                py="2"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={minimumOrderQuantity}
                onChange={(e: any) => setMininumOrderQuantity(e.target.value)}
              />
            </Flex>
          ) : (
            <Flex direction="column" gap="1" pt={{ base: "2", lg: "5" }}>
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Minimum order value
                </Text>
              </Flex>

              <Input
                placeholder=""
                py="2"
                type="number"
                min="1"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={minimumOrderValue}
                onChange={(e: any) => setMininumOrderValue(e.target.value)}
              />
            </Flex>
          )}
        </Box>
      ) : couponType === FLAT_DISCOUNT ? (
        <Box
          bgColor="#FFFFFF"
          borderRadius="4px"
          px={{ base: "2", lg: "10" }}
          py={{ base: "2", lg: "18" }}
          width={{ base: "100%", lg: "858px" }}
        >
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="550"
            color="#000000"
            textAlign="left"
            m=""
          >
            Create details
          </Text>

          <SimpleGrid
            columns={[1, 1, 2]}
            spacing={{ base: "2", lg: "40" }}
            pt={{ base: "3", lg: "5" }}
          >
            <Flex direction="column" gap="1">
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Discount amount
                </Text>
              </Flex>
              <Input
                placeholder=""
                py="2"
                type="number"
                min="1"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={discountAmount}
                onChange={(e: any) => setDiscountAmount(e.target.value)}
              />
            </Flex>
            <Flex direction="column" gap="1">
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Usage limit per coupon
                </Text>
              </Flex>

              <Select
                size="large"
                value={couponFrequency}
                onChange={handelSelectCouponUseedByPeruser}
                style={{ width: "100%" }}
                options={options}
              />
            </Flex>
          </SimpleGrid>
          <Stack pt={{ base: "3", lg: "5" }}>
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Minimum order condition
              </Text>
            </Flex>

            <Radio.Group
              options={orderConditionLable}
              onChange={onChange1}
              value={orderCondition}
            />
          </Stack>
          {orderCondition === "Order quantity" ? (
            <Flex direction="column" gap="1" pt={{ base: "2", lg: "5" }}>
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Minimum order quantity
                </Text>
              </Flex>

              <Input
                placeholder=""
                type="number"
                min="1"
                py="2"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={minimumOrderQuantity}
                onChange={(e: any) => setMininumOrderQuantity(e.target.value)}
              />
            </Flex>
          ) : (
            <Flex direction="column" gap="1" pt={{ base: "2", lg: "5" }}>
              <Flex align="center" gap="0.5">
                <Text color="red" m="0">
                  *
                </Text>
                <Text
                  fontSize={{ base: "", lg: "md" }}
                  fontWeight="400"
                  color="#333333"
                  textAlign="left"
                  m="0"
                >
                  Minimum order value
                </Text>
              </Flex>

              <Input
                placeholder=""
                py="2"
                type="number"
                min="1"
                borderColor="#939393"
                borderRadius="4px"
                width={{ base: "", lg: "278px" }}
                value={minimumOrderValue}
                onChange={(e: any) => setMininumOrderValue(e.target.value)}
              />
            </Flex>
          )}
        </Box>
      ) : null}
      {/* 3 */}
      <Box
        bgColor="#FFFFFF"
        borderRadius="4px"
        px={{ base: "2", lg: "10" }}
        py={{ base: "2", lg: "18" }}
        width={{ base: "100%", lg: "858px" }}
      >
        <Text
          fontSize={{ base: "", lg: "24px" }}
          fontWeight="550"
          color="#000000"
          textAlign="left"
          m=""
        >
          Coupon functionality
        </Text>
        <Flex justifyContent="space-between">
          <Text
            fontSize={{ base: "", lg: "md" }}
            fontWeight="400"
            color="#000000"
            textAlign="left"
            m="0"
          >
            Show coupon to customer
          </Text>
          <Switch
            checked={showCouponToCustomer}
            onChange={(checked: boolean) => setShowCouponToCustomer(checked)}
          />
        </Flex>
        <Flex justifyContent="space-between" pt={{ base: "2", lg: "3" }}>
          <Text
            fontSize={{ base: "", lg: "md" }}
            fontWeight="400"
            color="#000000"
            textAlign="left"
            m="0"
          >
            Valid only for online payments{" "}
          </Text>
          <Switch
            checked={validForOnLinePayment}
            onChange={(checked: boolean) => setValidForOnLinePayment(checked)}
          />
        </Flex>
        <Flex justifyContent="space-between" pt={{ base: "2", lg: "3" }}>
          <Text
            fontSize={{ base: "", lg: "md" }}
            fontWeight="400"
            color="#000000"
            textAlign="left"
            m="0"
          >
            Valid only for new customers{" "}
          </Text>
          <Switch
            checked={validForNewCostomer}
            onChange={(checked: boolean) => setValidForNewCustomer(checked)}
          />
        </Flex>
        <Flex justifyContent="space-between" pt={{ base: "2", lg: "3" }}>
          <Text
            fontSize={{ base: "", lg: "md" }}
            fontWeight="400"
            color="#000000"
            textAlign="left"
            m="0"
          >
            Auto apply coupon{" "}
          </Text>
          <Switch
            checked={autoApplyCoupon}
            onChange={(checked: boolean) => setAutoApplyCoupon(checked)}
          />
        </Flex>
      </Box>
      {/* 4 */}
      <Box
        bgColor="#FFFFFF"
        borderRadius="4px"
        px={{ base: "2", lg: "10" }}
        py={{ base: "2", lg: "18" }}
        width={{ base: "100%", lg: "858px" }}
      >
        <Text
          fontSize={{ base: "", lg: "24px" }}
          fontWeight="550"
          color="#000000"
          textAlign="left"
          m=""
        >
          Link coupons to campaign
        </Text>
        <Flex direction="column" gap="1">
          <Flex align="center" gap="0.5">
            <Text
              fontSize={{ base: "", lg: "md" }}
              fontWeight="400"
              color="#333333"
              textAlign="left"
              m="0"
            >
              Select campaigns
            </Text>
          </Flex>

          <Select
            mode="multiple"
            placeholder=""
            size="large"
            value={selectedCampaigns}
            onChange={setSelectedCampaigns}
            style={{ width: "100%" }}
            options={campaignsOption}
          />
        </Flex>
      </Box>
      {brand?.brandType === "ONLINE" || brand?.brandType === "HYBRID" ? (
        <Box
          bgColor="#FFFFFF"
          borderRadius="4px"
          px={{ base: "2", lg: "10" }}
          py={{ base: "2", lg: "18" }}
          width={{ base: "100%", lg: "858px" }}
        >
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="550"
            color="#000000"
            textAlign="left"
            m=""
          >
            Add online URL for this coupon
          </Text>
          <Flex direction="column" gap="1">
            <Flex align="center" gap="0.5">
              <Text color="red" m="0">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Add online URL
              </Text>
            </Flex>

            <Input
              type="text"
              size="lg"
              py="2"
              borderColor="#939393"
              borderRadius="4px"
              value={onlineURL}
              onChange={(e: any) => setOnlineURL(e.target.value)}
            />
          </Flex>
        </Box>
      ) : null}
      {brand?.brandType === "OFFLINE" || brand?.brandType === "HYBRID" ? (
        <Box
          bgColor="#FFFFFF"
          borderRadius="4px"
          px={{ base: "2", lg: "10" }}
          py={{ base: "2", lg: "18" }}
          width={{ base: "100%", lg: "858px" }}
        >
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="550"
            color="#000000"
            textAlign="left"
            m=""
          >
            Link coupons to offer partners
          </Text>
          <Flex direction="column" gap="1">
            <Flex align="center" gap="0.5">
              <Text
                fontSize={{ base: "", lg: "md" }}
                fontWeight="400"
                color="#333333"
                textAlign="left"
                m="0"
              >
                Select partners
              </Text>
            </Flex>

            <Select
              mode="multiple"
              placeholder=""
              size="large"
              value={offerPartners}
              onChange={setofferPartners}
              style={{ width: "100%" }}
              options={offerPartnerOption}
              filterOption={(input: any, option: any) =>
                (option?.label ?? "")?.includes(input)
              }
              filterSort={(optionA: any, optionB: any) =>
                (optionA?.label ?? "")
                  ?.toLowerCase()
                  .localeCompare((optionB?.label ?? "")?.toLowerCase())
              }
            />
          </Flex>
        </Box>
      ) : null}
      <Box
        bgColor="#FFFFFF"
        borderRadius="4px"
        px={{ base: "2", lg: "10" }}
        py={{ base: "2", lg: "18" }}
        width={{ base: "100%", lg: "858px" }}
      >
        <Text
          fontSize={{ base: "", lg: "24px" }}
          fontWeight="550"
          color="#000000"
          textAlign="left"
          m=""
        >
          Coupon validity
        </Text>
        <SimpleGrid columns={[1, 2, 2]}>
          <Flex align="center" gap="0.5">
            <Text color="red" m="0">
              *
            </Text>
            <Text
              fontSize={{ base: "", lg: "lg" }}
              fontWeight="400"
              color="#000000"
              textAlign="left"
              m="0"
            >
              Start date
            </Text>
          </Flex>

          <FormControl id="startDateHere" width="100%" pt="5">
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DateTimePicker
                inputVariant="outlined"
                value={couponStartTime}
                onChange={setCouponStartTime}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <MiuiIconButton>
                        <BsCalendar2Date />
                      </MiuiIconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
        </SimpleGrid>

        {takeEndTme && (
          <SimpleGrid columns={[1, 2, 2]} pt={{ base: "2", lg: "5" }}>
            <Flex align="center" gap="0.5">
              <Text color="red" m="">
                *
              </Text>
              <Text
                fontSize={{ base: "", lg: "lg" }}
                fontWeight="400"
                color="#000000"
                textAlign="left"
                m=""
              >
                End date
              </Text>
            </Flex>

            <FormControl id="endDateHere" width="100%">
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DateTimePicker
                  inputVariant="outlined"
                  value={couponEndTime}
                  onChange={setCouponEndTime}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <MiuiIconButton>
                          <BsCalendar2Date />
                        </MiuiIconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </MuiPickersUtilsProvider>
            </FormControl>
          </SimpleGrid>
        )}
        <Flex gap={5} pt="5">
          <Text
            fontSize={{ base: "", lg: "lg" }}
            fontWeight="400"
            color="#000000"
            textAlign="left"
            m=""
          >
            Do you want set end time?
          </Text>
          <Switch
            onChange={(checked: boolean) => setTakeEndTime(checked)}
            checked={takeEndTme}
          />
        </Flex>
      </Box>
      <Flex
        justifyContent="flex-end"
        width={{ base: "100%", lg: "858px" }}
        px={{ base: "2", lg: "10" }}
        py={{ base: "2", lg: "18" }}
        bgColor="#FFFFFF"
        borderRadius="4px"
      >
        {editCoupon ? (
          <Button
            variant={"outline"}
            color="#D7380E"
            bgColor="#FFFFFF"
            py="3"
            px="5"
            fontWeight="600"
            fontSize="md"
            isLoading={loadingUpdateCoupon || loadingAction}
            loadingText="Updating Coupon"
            _hover={{ color: "#FFFFFF", bgColor: "#D7380E" }}
            onClick={handelUpdateCoupon}
          >
            Update Coupon
          </Button>
        ) : (
          <Button
            variant={"outline"}
            color="green"
            bgColor="#FFFFFF"
            py="3"
            px="5"
            fontWeight="600"
            fontSize="md"
            isLoading={loadingCreateCoupon || loadingAction}
            loadingText="Creating Coupon"
            _hover={{ color: "#FFFFFF", bgColor: "green" }}
            onClick={handelCreateCoupon}
          >
            Create Coupon
          </Button>
        )}
      </Flex>
    </Stack>
  );
}
