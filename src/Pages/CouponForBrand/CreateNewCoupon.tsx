import { Box, Text, Flex, Stack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { BsChevronDown } from "react-icons/bs";
import { useLocation } from "react-router-dom";
import { TypeOfCouponModel } from "./TypeOfCouponModel";
import { CreateAndEditCouponPage } from "./CreateAndEditCouponPage";

export function CreateNewCoupon(props: any) {
  const location = useLocation();
  const [couponType, setCouponType] = useState<any>("");
  const [openCouponTypeModel, setOpenCouponTypeModel] = useState<any>(false);

  const handelSetCouponType = (value: any) => {
    setOpenCouponTypeModel(false);
    setCouponType(value);
  };

  useEffect(() => {
    setCouponType(location?.state?.couponType);
  }, [location]);
  return (
    <Box bgColor="#F8F8F8" py={{ base: "5", lg: "70" }}>
      <TypeOfCouponModel
        open={openCouponTypeModel}
        onCancel={(value: any) => setOpenCouponTypeModel(value)}
        setCouponType={handelSetCouponType}
      />
      <Stack
        gap={{ base: "2", lg: "5" }}
        flexDirection="column"
        px={{ base: "2", lg: "0" }}
        align="center"
      >
        <Box
          bgColor="#FFFFFF"
          borderRadius="4px"
          px={{ base: "2", lg: "10" }}
          py={{ base: "2", lg: "18" }}
          width={{ base: "100%", lg: "858px" }}
        >
          <Flex direction="column">
            <Text
              fontSize={{ base: "", lg: "24px" }}
              fontWeight="600"
              color="#000000"
              m=""
            >
              Change offer
            </Text>
            <Flex gap={8} align="center">
              <Text
                fontSize={{ base: "", lg: "sm" }}
                fontWeight="400"
                color="#0EBCF5"
                textAlign="left"
                m="0"
              >
                {couponType}
              </Text>
              <BsChevronDown
                color="#0EBCF5"
                size="20px"
                onClick={() => setOpenCouponTypeModel(true)}
              />
            </Flex>
          </Flex>
        </Box>
        <CreateAndEditCouponPage couponType={couponType} />
      </Stack>
    </Box>
  );
}
