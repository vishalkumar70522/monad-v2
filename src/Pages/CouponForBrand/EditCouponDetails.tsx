import { Box, Stack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { CreateAndEditCouponPage } from "./CreateAndEditCouponPage";

export function EditCouponDetails(props: any) {
  const location = useLocation();

  const [couponType, setCouponType] = useState<any>("");

  useEffect(() => {
    setCouponType(location?.state?.couponType);
  }, [location]);
  return (
    <Box bgColor="#F8F8F8" py={{ base: "5", lg: "85" }}>
      <Stack
        gap={{ base: "2", lg: "5" }}
        flexDirection="column"
        px={{ base: "2", lg: "0" }}
        align="center"
      >
        <CreateAndEditCouponPage
          couponType={couponType}
          editCoupon={true}
          coupon={location?.state?.coupon}
        />
      </Stack>
    </Box>
  );
}
