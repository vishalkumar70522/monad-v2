import { Box } from "@chakra-ui/react";
import data from "../../utils/data.json";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

export function ShowDataInChar1(props: any) {
  const peopleCount = Array.from(new Set([...data.peopleCounter]));
  //   console.log("genderAge : ", genderAge);
  //   console.log("po=eopleCout : ", peopleCount);
  const dd = peopleCount.map((data: any) => {
    return {
      Entered: data?.In_Time
        ? convertIntoDateAndTime(data?.In_Time)
        : convertIntoDateAndTime(data?.Out_Time),
      Exited:
        data?.In_Time && data?.Out_Time && data?.Move_Right === data?.Move_Left
          ? Math.abs(
              Math.floor(
                new Date(data.In_Time).getTime() -
                  new Date(data.Out_Time).getTime()
              )
            ) / 1000
            ? Math.abs(
                Math.floor(
                  new Date(data.In_Time).getTime() -
                    new Date(data.Out_Time).getTime()
                )
              ) / 1000
            : 0
          : 0,

      Stay: Number(data?.Move_Right) || Number(data?.Move_Left),
    };
  });

  const options = {
    chart: {
      zoomType: "xy",
    },
    title: {
      text: "Number of perseon stay infront of camera",
      align: "center",
    },
    subtitle: {
      text: "Source: WorldClimate.com",
      align: "left",
    },
    xAxis: [
      {
        categories: dd.map((data: any) => data?.Entered),
        crosshair: true,
      },
    ],
    yAxis: [
      {
        // Secondary yAxis

        title: {
          text: "People stay",
          style: {
            color: "green",
          },
        },
        labels: {
          format: "{value}",
          style: {
            color: "green",
          },
        },
        opposite: true,
      },
      {
        // Primary yAxis
        gridLineWidth: 0,
        labels: {
          format: "{value} Sec",
          style: {
            color: "red",
          },
        },
        title: {
          text: "Time",
          style: {
            color: "red",
          },
        },
      },
    ],
    tooltip: {
      shared: true,
    },
    legend: {
      layout: "vertical",
      align: "left",
      x: 80,
      verticalAlign: "top",
      y: 55,
      floating: true,
      backgroundColor:
        Highcharts?.defaultOptions?.legend?.backgroundColor || // theme
        "rgba(255,255,255,0.25)",
    },
    series: [
      {
        name: "Exited",
        type: "column",
        yAxis: 1,
        data: dd.map((data: any) => data?.Exited),
        tooltip: {
          valueSuffix: " S",
        },
      },

      {
        name: "Stay",
        type: "spline",
        data: dd.map((data: any) => data?.Stay),
        tooltip: {
          valueSuffix: "",
        },
      },
    ],
    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500,
          },
          chartOptions: {
            legend: {
              floating: false,
              layout: "horizontal",
              align: "center",
              verticalAlign: "bottom",
              x: 0,
              y: 0,
            },
            yAxis: [
              {
                labels: {
                  align: "right",
                  x: 0,
                  y: -6,
                },
                showLastLabel: false,
              },
              {
                labels: {
                  align: "left",
                  x: 0,
                  y: -6,
                },
                showLastLabel: false,
              },
              {
                visible: false,
              },
            ],
          },
        },
      ],
    },
  };

  return (
    <Box gap="10">
      <HighchartsReact highcharts={Highcharts} options={options} />
    </Box>
  );
}
