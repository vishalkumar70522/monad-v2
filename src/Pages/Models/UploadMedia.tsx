import React, { useState } from "react";
import { Modal, message } from "antd";
import {
  Box,
  Text,
  InputGroup,
  Input,
  Button,
  Stack,
  HStack,
  Image,
} from "@chakra-ui/react";
import { useAnimation } from "framer-motion";
import { AiOutlineCloudUpload } from "react-icons/ai";

export function UploadMedia(props: any) {
  let hiddenInput: any = null;
  const controls = useAnimation();
  const [file, setFile] = useState<any>(null);
  const [selectedMedia, setSelectedMedia] = useState<any>("");
  const [selectedMediaType, setSelectedMediaType] = useState<any>("");
  const [messageApi, contextHolder] = message.useMessage();

  const startAnimation = () => controls.start("hover");
  const stopAnimation = () => controls.stop();

  const validateSelectedFile = (file: any) => {
    const MIN_FILE_SIZE = 1024; // 1MB
    const MAX_FILE_SIZE = 1024 * 50; // 5MB
    const fileExtension = file.type.split("/")[1];
    const fileSizeKiloBytes = file.size / 1024;

    if (fileSizeKiloBytes > MAX_FILE_SIZE) {
      mediaError(
        "File size is greater than maximum limit. File size must be less then 50 MB "
      );
      return false;
    }
    if (
      !(
        fileExtension == "mp4" ||
        fileExtension == "jpg" ||
        fileExtension == "jpeg" ||
        fileExtension == "png"
      )
    ) {
      mediaError("File format must be .mp4 or jpg or jpeg and png");
      return false;
    } else {
      setSelectedMediaType(fileExtension);
    }
    return true;
  };

  const mediaError = (error: any) => {
    messageApi.open({
      type: "error",
      content: error,
    });
  };

  function handleFileSelect(file: any) {
    if (validateSelectedFile(file)) {
      const fileURL = URL.createObjectURL(file);
      setSelectedMedia(fileURL);
      props?.setFileUrl(file);
      setFile(file);
    }
  }

  const handelDiscard = () => {
    props?.setCampaignName("");
    setSelectedMedia(null);
    props?.onCancel();
  };

  const handleNext = (e: any) => {
    //if selected media type === mp4
    if (selectedMediaType === "mp4") {
      props.videoUploadHandler(e);
      setSelectedMedia(null);
      setFile(null);
      props.onCancel();
    }

    //if selected media type == image
    if (selectedMediaType !== "mp4") {
      const formData = new FormData();
      formData.append("photo", file);
      props.createVideo(formData);
      setSelectedMedia(null);
      setFile(null);
      props.onCancel();
    }
  };

  return (
    <>
      <Modal
        title=""
        centered
        open={props?.open}
        onCancel={() => props.onCancel()}
        footer={[]}
        closable={true}
        maskClosable={false}
      >
        <Box>
          {contextHolder}

          {selectedMedia ? (
            <Box>
              <Text
                color="#333333"
                fontSize={{ base: "xl", lg: "2xl" }}
                fontWeight="semibold"
                align="center"
              >
                Uploaded media
              </Text>
              {selectedMediaType === "mp4" ? (
                <Box
                  as="video"
                  src={selectedMedia}
                  autoPlay
                  loop
                  muted
                  display="inline-block"
                  borderRadius="8px"
                  height={{ base: "300px", lg: "300px" }}
                  width="100%"
                ></Box>
              ) : (
                <Image
                  src={selectedMedia}
                  alt=""
                  width="100%"
                  height="100%"
                ></Image>
              )}

              <HStack justifyContent="space-between" pt="5">
                <Button
                  fontSize={{ base: "lg", lg: "xl" }}
                  variant="null"
                  onClick={() => {
                    setSelectedMedia(null);
                  }}
                >
                  Discard
                </Button>
                <Button
                  type="submit"
                  color="#D7380E"
                  variant="outline"
                  borderColor="#000000"
                  py="2"
                  px="10"
                  fontSize={{ base: "lg", lg: "xl" }}
                  onClick={handleNext}
                >
                  Next
                </Button>
              </HStack>
            </Box>
          ) : (
            <Box>
              <Text
                color="#333333"
                fontSize={{ base: "xl", lg: "2xl" }}
                fontWeight="semibold"
                align="center"
              >
                Upload media
              </Text>
              <Text color="#5B5B5B" fontSize={{ base: "lg", lg: "xl" }}>
                Upload content
              </Text>
              <InputGroup>
                <Box
                  height="112px"
                  width="100%"
                  border="1px"
                  borderRadius="16px"
                  alignItems="center"
                  borderColor="#E4E4E4"
                  pt="5"
                  onClick={() => hiddenInput.click()}
                >
                  <Stack align="center">
                    <Button
                      leftIcon={<AiOutlineCloudUpload size="20px" />}
                      py="2"
                      bgColor="#0EBCF5"
                      borderRadius="20px"
                      fontSize="md"
                    >
                      Upload a photo or video
                    </Button>
                  </Stack>
                  <Input
                    hidden
                    type="file"
                    ref={(el) => (hiddenInput = el)}
                    // accept="image/png, image/jpeg"
                    onDragEnter={startAnimation}
                    onDragLeave={stopAnimation}
                    onChange={(e: any) => handleFileSelect(e.target.files[0])}
                  />
                </Box>
              </InputGroup>
              <Text color="red" align="right">{`Max file size < 10 MB`}</Text>
              <HStack justifyContent="space-between">
                <Button
                  fontSize={{ base: "lg", lg: "xl" }}
                  variant="null"
                  onClick={handelDiscard}
                >
                  Discard
                </Button>
              </HStack>
            </Box>
          )}
        </Box>
      </Modal>
    </>
  );
}
