import React, { useState } from "react";
import { Modal, message } from "antd";
import { Box, Text, Flex, Image, HStack, Button } from "@chakra-ui/react";
import { handSake, audiance } from "../../assets/svg";

export function CampaignGoal(props: any) {
  const [campaignGoal, setCampaignGoal] = useState<any>(null);
  const handleNext = () => {
    if (campaignGoal) {
      props?.handelNext();
    } else {
      message.error("Please select campaign goal!");
    }
  };
  return (
    <>
      <Modal
        title=""
        open={props?.open}
        onCancel={() => props.onCancel()}
        footer={[]}
        closable={false}
        maskClosable={false}
      >
        <Box px={{ base: "2", lg: "10" }}>
          <Text
            color="#000000"
            fontSize={{ base: "lg", lg: "2xl" }}
            fontWeight="600"
            m="0"
            textAlign="center"
          >
            Campaign Goal
          </Text>
          <Text color="#403F49" fontSize={{ base: "md", lg: "sm" }} pt="5">
            The goal for the campaign is decided by you for the audience
            ewnggagement or make the audience reach{" "}
          </Text>
          <Flex gap="2">
            <Box
              border="1px"
              borderColor="#E4E4E4"
              p={{ base: "2", lg: "2" }}
              borderRadius="4px"
              bgColor={campaignGoal === "Audience reach" ? "#E4E4E4" : ""}
              onClick={() => {
                setCampaignGoal("Audience reach");
                props?.setCampaignGoal("Audience reach");
              }}
            >
              <Image src={audiance} />

              <Text
                color="#403F49"
                fontSize={{ base: "lg", lg: "lg" }}
                fontWeight="600"
                m="0"
              >
                Audience reach
              </Text>
              <Text
                color="#403F49"
                fontSize={{ base: "12px", lg: "13px" }}
                fontWeight="400"
                m="0"
              >
                The main goal is to make the audience interact with
                advertisement
              </Text>
            </Box>
            <Box
              border="1px"
              borderColor="#E4E4E4"
              p={{ base: "2", lg: "2" }}
              borderRadius="4px"
              bgColor={campaignGoal === "Audience enggagement" ? "#E4E4E4" : ""}
              onClick={() => {
                setCampaignGoal("Audience enggagement");
                props?.setCampaignGoal("Audience enggagement");
              }}
            >
              <Image src={handSake} />
              <Text
                color="#403F49"
                fontSize={{ base: "lg", lg: "lg" }}
                fontWeight="600"
                m="0"
              >
                Audience enggagement
              </Text>
              <Text
                color="#403F49"
                fontSize={{ base: "12px", lg: "13px" }}
                fontWeight="400"
                m="0"
              >
                The main goal is to make the audience interact with
                advertisement
              </Text>
            </Box>
          </Flex>
          <HStack justifyContent="space-between" pt="5">
            <Button
              fontSize={{ base: "sm", lg: "md" }}
              variant="null"
              color="#0EBCF5"
              fontWeight="511"
            >
              Back
            </Button>

            <Button
              type="submit"
              color="#FFFFFF"
              variant="outline"
              bgColor="#0EBCF5"
              py="2"
              fontWeight="600"
              fontSize={{ base: "sm", lg: "md" }}
              onClick={handleNext}
            >
              Next
            </Button>
          </HStack>
        </Box>
      </Modal>
    </>
  );
}
