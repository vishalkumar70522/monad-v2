import React, { useState } from "react";
import { Modal, message } from "antd";
import {
  Box,
  Text,
  InputGroup,
  Input,
  Button,
  Stack,
  HStack,
  Image,
  Flex,
} from "@chakra-ui/react";
import { useAnimation } from "framer-motion";
import { AiOutlineCloudUpload } from "react-icons/ai";
import { GrFormEdit } from "react-icons/gr";

export function UploadMediaWithCampaignNamePopup(props: any) {
  let hiddenInput: any = null;
  const controls = useAnimation();
  const [file, setFile] = useState<any>(null);
  const [selectedMedia, setSelectedMedia] = useState<any>("");
  const [selectedMediaType, setSelectedMediaType] = useState<any>("");
  const [messageApi, contextHolder] = message.useMessage();
  const [editCampaignName, setEditCampaignName] = useState<any>(false);

  const startAnimation = () => controls.start("hover");
  const stopAnimation = () => controls.stop();

  const validateSelectedFile = (file: any) => {
    const MIN_FILE_SIZE = 1024; // 1MB
    const MAX_FILE_SIZE = 1024 * 50; // 5MB
    const fileExtension = file.type.split("/")[1];
    const fileSizeKiloBytes = file.size / 1024;

    if (fileSizeKiloBytes > MAX_FILE_SIZE) {
      mediaError(
        "File size is greater than maximum limit. File size must be less then 50 MB "
      );
      return false;
    }
    if (
      !(
        fileExtension == "mp4" ||
        fileExtension == "jpg" ||
        fileExtension == "jpeg" ||
        fileExtension == "png"
      )
    ) {
      mediaError("File format must be .mp4 or jpg or jpeg and png");
      return false;
    } else {
      setSelectedMediaType(fileExtension);
    }
    return true;
  };

  const mediaSuccess = () => {
    messageApi.open({
      type: "success",
      content: "Login Success full",
    });
  };
  const mediaError = (error: any) => {
    messageApi.open({
      type: "error",
      content: error,
    });
  };

  function handleFileSelect(file: any) {
    if (validateSelectedFile(file)) {
      const fileURL = URL.createObjectURL(file);
      setSelectedMedia(fileURL);
      props?.setFileUrl(file);
      setFile(file);
    }
  }

  const handelDiscard = () => {
    props?.setCampaignName("");
    setSelectedMedia(null);
    props?.onCancel();
  };

  const handleNext = (e: any) => {
    //if selected media type === mp4
    if (selectedMediaType === "mp4") {
      props.videoUploadHandler(e);
      setSelectedMedia(null);
      setFile(null);
      props.onCancel();
      props.setOpenCampaignGoalModel();
    }

    //if selected media type == image
    if (selectedMediaType !== "mp4") {
      const formData = new FormData();
      formData.append("photo", file);
      props.createVideo(formData);
      setSelectedMedia(null);
      setFile(null);
      props.onCancel();
      props.setOpenCampaignGoalModel();
    }
  };

  return (
    <>
      <Modal
        title=""
        open={props?.open}
        onCancel={() => props.onCancel()}
        footer={[]}
        closable={true}
        maskClosable={false}
      >
        <Box px={{ base: "2", lg: "10" }}>
          {contextHolder}
          <Text
            color="#000000"
            fontSize={{ base: "lg", lg: "lg" }}
            fontWeight="600"
            m="0"
          >
            Enter campaign name
          </Text>
          <Text color="#5B5B5B" fontSize={{ base: "md", lg: "sm" }} m="0">
            Enter the name of the campaign that you want to start with
          </Text>
          {editCampaignName ? (
            <Input
              placeholder="Enter campaign name"
              size="large"
              py="3"
              px="2"
              mt="5"
              value={props?.campaignName}
              borderRadius="4px"
              onChange={(e) => props.setCampaignName(e.target.value)}
            />
          ) : (
            <Flex gap={{ base: "", lg: "5" }} align="center" py="5">
              <Text
                color="#1E1E1E"
                fontSize={{ base: "lg", lg: "lg" }}
                fontWeight="552"
                m="0"
              >
                {props?.campaignName}
              </Text>
              <GrFormEdit
                size="20px"
                onClick={() => setEditCampaignName(true)}
              />
            </Flex>
          )}

          {selectedMedia ? (
            <Box>
              <Text
                color="#333333"
                fontSize={{ base: "xl", lg: "2xl" }}
                fontWeight="semibold"
                align="center"
              >
                Uploaded media
              </Text>
              {selectedMediaType === "mp4" ? (
                <Box
                  as="video"
                  src={selectedMedia}
                  autoPlay
                  loop
                  muted
                  display="inline-block"
                  borderRadius="8px"
                  borderRight="4px"
                  height={{ base: "300px", lg: "300px" }}
                  width="100%"
                ></Box>
              ) : (
                <Image
                  src={selectedMedia}
                  alt=""
                  width="100%"
                  height="100%"
                ></Image>
              )}

              <HStack justifyContent="space-between" pt="5">
                <Button
                  fontSize={{ base: "sm", lg: "md" }}
                  variant="null"
                  onClick={() => {
                    setSelectedMedia(null);
                  }}
                  color="#0EBCF5"
                  fontWeight="511"
                >
                  Change
                </Button>

                <Button
                  type="submit"
                  color="#FFFFFF"
                  variant="outline"
                  bgColor="#0EBCF5"
                  py="2"
                  fontWeight="600"
                  fontSize={{ base: "sm", lg: "md" }}
                  onClick={handleNext}
                >
                  Next
                </Button>
              </HStack>
            </Box>
          ) : (
            <Box>
              <Text
                color="#000000"
                fontSize={{ base: "lg", lg: "lg" }}
                fontWeight="600"
              >
                Upload content
              </Text>
              <InputGroup>
                <Box
                  height="112px"
                  width="100%"
                  border="1px"
                  borderRadius="16px"
                  alignItems="center"
                  pt="5"
                  borderColor="#E4E4E4"
                  onClick={() => hiddenInput.click()}
                >
                  <Stack align="center">
                    <Button
                      leftIcon={<AiOutlineCloudUpload size="20px" />}
                      py="2"
                      bgColor="#0EBCF5"
                      borderRadius="20px"
                      fontSize="md"
                    >
                      Upload a photo or video
                    </Button>
                  </Stack>

                  <Input
                    hidden
                    type="file"
                    ref={(el) => (hiddenInput = el)}
                    // accept="image/png, image/jpeg"
                    onDragEnter={startAnimation}
                    onDragLeave={stopAnimation}
                    onChange={(e: any) => handleFileSelect(e.target.files[0])}
                  />
                </Box>
              </InputGroup>
              <Text
                color="red"
                align="right"
              >{`Max file size less then 50 MB`}</Text>
              <HStack justifyContent="space-between">
                <Button
                  fontSize={{ base: "lg", lg: "xl" }}
                  variant="null"
                  onClick={handelDiscard}
                >
                  Discard
                </Button>
              </HStack>
            </Box>
          )}
        </Box>
      </Modal>
    </>
  );
}
