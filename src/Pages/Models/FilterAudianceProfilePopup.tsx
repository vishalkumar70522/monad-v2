import { Box, Button, Flex, Stack, Text } from "@chakra-ui/react";
import { Checkbox, Modal, Select, message } from "antd";
import { useState } from "react";
import { Slider } from "antd";
import { highlights } from "../../utils/rawData";

const buttonObject = [
  { value: "1", label: "Locality" },
  { value: "2", label: "Gender preference" },
  { value: "3", label: "Employment type" },
  { value: "4", label: "Crowd mobility" },
  { value: "5", label: "age group" },
];

export function FilterAudianceProfilePopup(props: any) {
  const [selectedOption, setSelectedOption] = useState<any>("1");
  const [ageRange, setAgeRange] = useState<any>(props?.ageRange);

  const [selectAllCroudMobability, setSelectAllCroudMobability] =
    useState<any>(false);

  const [croudMobability, setCroudMobability] = useState<any>(
    props?.croudMobability
  );

  const [selectAllGenterPreference, setSelectAllGenterPreference] =
    useState<any>(false);

  const [genderPreference, setGenderPreference] = useState<any>(
    props?.genderPreference
  );

  const [selectAllEmploymentType, setSelectAllEmploymentType] =
    useState<any>(false);

  const [employmentTypes, setEmploymentType] = useState<any>(
    props?.employmentTypes
  );

  const [screenHighlights, setScreenHighlights] = useState<any>(
    props?.screenHighlights
  );

  const handelAddScreenHighlights = (value: string | string[]) => {
    setScreenHighlights(value);
  };

  const isAllChecked = (data: any) => {
    for (let x of data) {
      if (x.status === false) return false;
    }
    return true;
  };

  const handelSaveHighlights = () => {
    props?.setScreenHighlights(screenHighlights);
    message.success("Saved!");
  };

  const handelSaveGenterPreference = () => {
    props?.setGenderPreference(genderPreference);
    message.success("Saved!");
  };

  const handelSaveEmploymentType = () => {
    props?.setEmploymentType(employmentTypes);
    message.success("Saved!");
  };

  const handelSaveCroudMobability = () => {
    props?.setCroudMobability(croudMobability);
    message.success("Saved!");
  };

  const handelSaveAgeRangeValue = () => {
    props?.setAgeRange(ageRange);
    message.success("Saved!");
  };

  const setAgeRangeValue = (value: number | [number, number]) => {
    setAgeRange(value);
  };

  const handleCheckCroudMobability = async (value: any, index: any) => {
    await setCroudMobability([
      ...croudMobability.map((data: any, i: any) => {
        if (i === index) {
          data.status = value;
        }
        return data;
      }),
    ]);
    if (isAllChecked(croudMobability)) {
      setSelectAllCroudMobability(true);
    } else {
      setSelectAllCroudMobability(false);
    }
  };

  const handelSelectAllCroudMobability = (value: any) => {
    if (value)
      setCroudMobability([
        ...croudMobability.map((data: any, i: any) => {
          data.status = value;
          return data;
        }),
      ]);
    setSelectAllCroudMobability(value);
  };

  const handelSelectAllGenterPreference = (value: any) => {
    if (value)
      setGenderPreference([
        ...genderPreference.map((data: any, i: any) => {
          data.status = value;
          return data;
        }),
      ]);
    setSelectAllGenterPreference(value);
  };

  const handleCheckGenterPreference = async (value: any, index: any) => {
    await setGenderPreference([
      ...genderPreference.map((data: any, i: any) => {
        if (i === index) {
          data.status = value;
        }
        return data;
      }),
    ]);
    if (isAllChecked(genderPreference)) {
      setSelectAllGenterPreference(true);
    } else {
      setSelectAllGenterPreference(false);
    }
  };

  const handelSelectAllEmploymentType = (value: any) => {
    if (value)
      setEmploymentType([
        ...employmentTypes.map((data: any, i: any) => {
          data.status = value;
          return data;
        }),
      ]);
    setSelectAllEmploymentType(value);
  };

  const handleCheckEmploymentType = async (value: any, index: any) => {
    await setEmploymentType([
      ...employmentTypes.map((data: any, i: any) => {
        if (i === index) {
          data.status = value;
        }
        return data;
      }),
    ]);
    if (isAllChecked(employmentTypes)) {
      setSelectAllEmploymentType(true);
    } else {
      setSelectAllEmploymentType(false);
    }
  };

  return (
    <Modal
      title=""
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={false}
      style={{ padding: 0 }}
    >
      <Box borderRadius="16px" p="0">
        <Text
          color="#000000"
          fontWeight="600"
          fontSize={{ base: "md", lg: "lg" }}
        >
          Choose your audience
        </Text>
        <Flex gap={{ base: "2", lg: "5" }}>
          <Stack
            borderRadius="8px"
            alignItems="center"
            height="512px"
            bgColor="#525358"
            width={{ base: "", lg: "201px" }}
            spacing="0"
          >
            {buttonObject?.map((single: any, index: any) => (
              <Text
                borderRadius="8px"
                textAlign="left"
                key={index}
                color={selectedOption === single?.value ? "#3A3A3A" : "#E2E2E2"}
                bgColor={
                  selectedOption === single?.value ? "#D9D9D9" : "#525358"
                }
                _hover={{
                  bgColor: "#EEEEEE",
                  color: "#3A3A3A",
                  fontWeight: "600",
                }}
                py="3"
                pl="5"
                width={{ base: "", lg: "201px" }}
                fontWeight="600"
                fontSize={{ base: "12px", lg: "15px" }}
                onClick={() => setSelectedOption(single.value)}
                m="0"
              >
                {single.label}
              </Text>
            ))}
          </Stack>

          <Box width="100%">
            {selectedOption === "1" ? (
              <Stack justifyContent="space-between" height="512px">
                <Select
                  mode="multiple"
                  size={"large"}
                  placeholder="Please select"
                  value={screenHighlights}
                  onChange={handelAddScreenHighlights}
                  style={{ width: "100%" }}
                  options={highlights}
                />
                <Flex justifyContent="flex-end">
                  <Button
                    type="submit"
                    color="#FFFFFF"
                    variant="outline"
                    bgColor="#0EBCF5"
                    py="2"
                    mt="2"
                    fontWeight="600"
                    width="100px"
                    fontSize={{ base: "sm", lg: "md" }}
                    onClick={handelSaveHighlights}
                  >
                    Save
                  </Button>
                </Flex>
              </Stack>
            ) : selectedOption === "2" ? (
              <Stack justifyContent="space-between" height="512px">
                <Flex direction="column" p="0" gap="2">
                  <Checkbox
                    onChange={(e: any) =>
                      handelSelectAllGenterPreference(e.target.checked)
                    }
                    checked={selectAllGenterPreference}
                  >
                    Select all
                  </Checkbox>
                  {genderPreference.map((data: any, index: any) => (
                    <Checkbox
                      key={index}
                      onChange={(e: any) =>
                        handleCheckGenterPreference(e.target.checked, index)
                      }
                      checked={data?.status}
                    >
                      {data?.lavel}
                    </Checkbox>
                  ))}
                </Flex>
                <Flex justifyContent="flex-end">
                  <Button
                    type="submit"
                    color="#FFFFFF"
                    variant="outline"
                    bgColor="#0EBCF5"
                    py="2"
                    mt="2"
                    fontWeight="600"
                    width="100px"
                    fontSize={{ base: "sm", lg: "md" }}
                    onClick={handelSaveGenterPreference}
                  >
                    Save
                  </Button>
                </Flex>
              </Stack>
            ) : selectedOption === "3" ? (
              <Stack justifyContent="space-between" height="512px">
                <Flex direction="column" p="0" gap="2">
                  <Checkbox
                    onChange={(e: any) =>
                      handelSelectAllEmploymentType(e.target.checked)
                    }
                    checked={selectAllEmploymentType}
                  >
                    Select all
                  </Checkbox>
                  {employmentTypes.map((data: any, index: any) => (
                    <Checkbox
                      key={index}
                      onChange={(e: any) =>
                        handleCheckEmploymentType(e.target.checked, index)
                      }
                      checked={data?.status}
                    >
                      {data?.lavel}
                    </Checkbox>
                  ))}
                </Flex>
                <Flex justifyContent="flex-end">
                  <Button
                    type="submit"
                    color="#FFFFFF"
                    variant="outline"
                    bgColor="#0EBCF5"
                    py="2"
                    mt="2"
                    fontWeight="600"
                    width="100px"
                    fontSize={{ base: "sm", lg: "md" }}
                    onClick={handelSaveEmploymentType}
                  >
                    Save
                  </Button>
                </Flex>
              </Stack>
            ) : selectedOption === "4" ? (
              <Stack justifyContent="space-between" height="512px">
                <Flex direction="column" p="0" gap="2">
                  <Checkbox
                    onChange={(e: any) =>
                      handelSelectAllCroudMobability(e.target.checked)
                    }
                    checked={selectAllCroudMobability}
                  >
                    Select all
                  </Checkbox>
                  {croudMobability.map((data: any, index: any) => (
                    <Checkbox
                      key={index}
                      onChange={(e: any) =>
                        handleCheckCroudMobability(e.target.checked, index)
                      }
                      checked={data?.status}
                    >
                      {data?.lavel}
                    </Checkbox>
                  ))}
                </Flex>
                <Flex justifyContent="flex-end">
                  <Button
                    type="submit"
                    color="#FFFFFF"
                    variant="outline"
                    bgColor="#0EBCF5"
                    py="2"
                    mt="2"
                    fontWeight="600"
                    width="100px"
                    fontSize={{ base: "sm", lg: "md" }}
                    onClick={handelSaveCroudMobability}
                  >
                    Save
                  </Button>
                </Flex>
              </Stack>
            ) : selectedOption === "5" ? (
              <Stack justifyContent="space-between" height="512px">
                <Stack>
                  <Slider
                    range
                    step={10}
                    defaultValue={ageRange}
                    onChange={setAgeRangeValue}
                  />
                  <Text
                    color="#000000"
                    fontWeight="400"
                    fontSize={{ base: "12px", lg: "sm" }}
                    pt="5"
                  >
                    {`Age group: ${ageRange[0]} - ${ageRange[1]} years`}
                  </Text>
                </Stack>
                <Flex justifyContent="flex-end">
                  <Button
                    type="submit"
                    color="#FFFFFF"
                    variant="outline"
                    bgColor="#0EBCF5"
                    py="2"
                    mt="2"
                    fontWeight="600"
                    width="100px"
                    fontSize={{ base: "sm", lg: "md" }}
                    onClick={handelSaveAgeRangeValue}
                  >
                    Save
                  </Button>
                </Flex>
              </Stack>
            ) : null}
          </Box>
        </Flex>
      </Box>
    </Modal>
  );
}
