import React from "react";
import { Form, Input, Modal } from "antd";
import { Box, HStack, Text, Button } from "@chakra-ui/react";

export function CreateCampaignName(props: any) {
  const onFinish = (values: any) => {
    props.onCancel();
    props.openUploadMedia();
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  const handelCancle = () => {
    props.setCampaignName("");
    props?.onCancel();
  };

  return (
    <>
      <Modal
        title=""
        centered
        open={props?.open}
        onCancel={() => props.onCancel()}
        footer={[]}
        closable={true}
        maskClosable={false}
      >
        <Box>
          <Text
            color="#333333"
            fontSize={{ base: "xl", lg: "2xl" }}
            fontWeight="semibold"
          >
            Enter campaign name
          </Text>
          <Text color="#5B5B5B" fontSize={{ base: "md", lg: "sm" }}>
            Enter the name of the campaign that you want to start with
          </Text>
          <Form
            layout="vertical"
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            fields={[
              {
                name: ["campaignName"],
                value: props.campaignName,
              },
            ]}
          >
            <Form.Item
              name="campaignName"
              style={{ fontSize: "20px" }}
              rules={[
                { required: true, message: "Please input campaign name!" },
              ]}
            >
              <Input
                placeholder="Enter campaign name"
                size="large"
                onChange={(e) => props.setCampaignName(e.target.value)}
              />
            </Form.Item>

            <Form.Item>
              <HStack justifyContent="space-between" px="20">
                <Button
                  fontSize={{ base: "lg", lg: "xl" }}
                  variant="null"
                  onClick={handelCancle}
                >
                  Back
                </Button>
                {props.campaignName ? (
                  <Button
                    type="submit"
                    color="#D7380E"
                    variant="outline"
                    borderColor="#000000"
                    py="2"
                    px="10"
                    fontSize={{ base: "lg", lg: "xl" }}
                  >
                    Next
                  </Button>
                ) : null}
              </HStack>
            </Form.Item>
          </Form>
        </Box>
      </Modal>
    </>
  );
}
