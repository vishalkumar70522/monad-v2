import React, { useState } from "react";
import {
  Box,
  Text,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  Stack,
  Flex,
  Button,
} from "@chakra-ui/react";
import { Modal, Skeleton, Tabs } from "antd";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { useSelector } from "react-redux";
import { ResultNotFound } from "../../components/commans";
import { useDispatch } from "react-redux";
import { getCampaignLogs } from "../../Actions/campaignAction";
import { ShowDataInChar } from "./ShowDataInChar";
import { ShowDataInChar1 } from "./ShowDataInChar1";

function CampaignLogs({ props }: any) {
  const campaign = props?.campaign;
  const [start, setStart] = useState<any>(0);
  const [end, setEnd] = useState<any>(50);
  const dispatch = useDispatch<any>();
  const campaignLogs = useSelector((state: any) => state.campaignLogs);
  const {
    loading: loadingCampaignLogs,
    error: errorCampaignLogs,
    last50,
    totalCount,
  } = campaignLogs;

  const handleGetCampaignLogs = (start: any, end: any) => {
    dispatch(
      getCampaignLogs({
        start,
        end,
        cid: campaign?.cid,
        screenId: campaign?.screen,
      })
    );
    setStart(start);
    setEnd(end);
  };

  const getDeviceId = (deviceInfo: string) => {
    const info = deviceInfo ? JSON.parse(deviceInfo) : null;
    return info ? info.deviceId : null;
  };
  const getDeviceIp = (deviceInfo: string) => {
    const info = deviceInfo ? JSON.parse(deviceInfo) : null;
    return info ? info.deviceIp : null;
  };
  const getDeviceDisplay = (deviceInfo: string) => {
    const info = deviceInfo ? JSON.parse(deviceInfo) : null;
    return info ? info.deviceDisplay : null;
  };

  return (
    <Box color="black.500" p={{ base: "0", lg: "0" }}>
      {loadingCampaignLogs ? (
        <Skeleton active />
      ) : errorCampaignLogs ? (
        <ResultNotFound />
      ) : last50?.length > 0 ? (
        <>
          <TableContainer borderRadius="5px" bgColor="#FFFFFF">
            <Table variant="simple" size="lg">
              <Stack overflowY="scroll" height={{ base: "700px", lg: "700px" }}>
                <Thead>
                  <Tr>
                    <Th>Device ID</Th>
                    <Th>Device </Th>
                    <Th>Playing Time</Th>
                    <Th>Address</Th>
                  </Tr>
                </Thead>

                <Tbody>
                  {last50?.map((SingleLog: any, index: any) => (
                    <Tr
                      key={index + 1}
                      onClick={() =>
                        props.handleShowCampaignLogs(SingleLog.cid)
                      }
                      _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
                    >
                      <Td>
                        <Text color=" #403F49 " fontSize="sm">
                          {getDeviceId(SingleLog.deviceInfo)}
                        </Text>
                      </Td>
                      <Td>
                        <Text color=" #403F49 " fontSize="sm">
                          {getDeviceDisplay(SingleLog.deviceInfo)}
                        </Text>
                      </Td>
                      <Td color="#575757" fontSize="sm">
                        {convertIntoDateAndTime(SingleLog.playTime)}
                      </Td>
                      <Td
                        isNumeric
                        fontSize="sm"
                        color="#403F49"
                        fontWeight="semibold"
                      >
                        {getDeviceIp(SingleLog.deviceInfo)}
                      </Td>
                    </Tr>
                  ))}
                </Tbody>
              </Stack>
            </Table>
          </TableContainer>
          <Flex gap={10}>
            {start > 0 ? (
              <Button
                color="#FFFFFF"
                bgColor="red.500"
                py="3"
                width="100px"
                onClick={() => handleGetCampaignLogs(start - 50, end - 50)}
              >
                Previous 50
              </Button>
            ) : null}
            {totalCount > end ? (
              <Button
                color="#FFFFFF"
                bgColor="red.500"
                py="3"
                width="100px"
                onClick={() => handleGetCampaignLogs(start + 50, end + 50)}
              >
                Next 50
              </Button>
            ) : null}
          </Flex>
        </>
      ) : (
        <Text
          color="#D7380E"
          fontSize="lg"
          align="left"
          fontWeight="semibold"
          pt="5"
        >
          No Campaign Logs
        </Text>
      )}
    </Box>
  );
}

export function CampaignLogPopup(props: any) {
  const [key, setKey] = useState<any>("1");
  const onChange = (key: string) => {
    console.log(key);
    setKey(key);
  };

  return (
    <Modal
      title="Campaign Logs"
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      width={1000}
      // width="auto"
      style={{ top: 5 }}
    >
      <Tabs
        defaultActiveKey="1"
        items={[
          {
            key: "1",
            label: "Campaign Log Report",
            children: <CampaignLogs props={props} />,
          },
          {
            key: "2",
            label: "Show In Chart",
            children: <ShowDataInChar screen={props?.screen} />,
          },
          {
            key: "23",
            label: "Show In Chart 2",
            children: <ShowDataInChar1 screen={props?.screen} />,
          },
        ]}
      />
    </Modal>
  );
}
