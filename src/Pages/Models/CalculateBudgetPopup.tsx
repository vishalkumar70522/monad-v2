import {
  Box,
  Text,
  Flex,
  HStack,
  Stack,
  Button,
  Divider,
  InputGroup,
  FormControl,
  SimpleGrid,
} from "@chakra-ui/react";

import { useEffect, useState } from "react";
import { Input, Modal, Slider, message } from "antd";
import { useSelector } from "react-redux";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { BsCalendar2Date } from "react-icons/bs";
import {
  IconButton as MiuiIconButton,
  InputAdornment,
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";

export function CalculateBudgetPopup(props: any) {
  const { screen } = props;
  const [numberOfSlots, setNumberOfSlots] = useState<number>(30);
  const [startDateTime, setStartdateTime] = useState<any>(null);
  const [endDateTime, setEndDateTime] = useState<any>(null);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const handleEndDate = (value: any) => {
    setEndDateTime(value);
  };
  // console.log("days : ", (endDate - startDate) / (1000 * 60 * 60 * 24));
  const handleStartDate = (value: any) => {
    // let time = value.toString().split(" "); // Wed Jan 04 2023 08:22:28 GMT+0530
    // time = time[4];
    setStartdateTime(value);
  };
  const handleAddToCard = () => {
    if (!startDateTime) message.error("Please enter start date");
    else if (!endDateTime) message.error("Please enter end date");
    else if (numberOfSlots === 0)
      message.error("Please select no. of slots > 0");
    else {
      const items: string | null = window.localStorage.getItem("carts");
      // Parse stored json or if none return initialValue
      const value = items ? JSON.parse(items) : [];
      if (!value?.find((data: any) => data.screen === screen?._id)) {
        window.localStorage.setItem(
          "carts",
          JSON.stringify([
            ...value,
            {
              screen: screen?._id,
              startDateAndTime: startDateTime,
              endDateAndTime: endDateTime,
              totalSlotBooked: numberOfSlots,
              totalAmount: screen?.rentPerSlot * numberOfSlots,
              // totalAmount:
              //   screen?.rentPerSlot * numberOfSlots +
              //   (screen?.rentPerSlot * numberOfSlots * 5) / 100,
            },
          ])
        );
      }
      props.setShowCalculateBudgetPopup();
    }
  };

  useEffect(() => {
    setStartdateTime(null);
    setEndDateTime(null);
  }, [props]);

  return (
    <Modal
      title=""
      centered
      open={props?.showCalculateBudget}
      onCancel={() => props.setShowCalculateBudgetPopup()}
      footer={[]}
      maskClosable={false}
    >
      {" "}
      <Box width="100%">
        <HStack justifyContent="space-between">
          <Flex align="baseline" gap={1}>
            <Text
              color="#222222"
              fontSize={{ base: "16px", lg: "xl" }}
              align="left"
            >
              {`₹${screen?.rentPerSlot}/`}
            </Text>
            <Text color="#222222" align="left" fontSize={{ base: "15px" }}>
              slot
            </Text>
          </Flex>
          <Stack pr="5">
            <Text
              fontSize={{ base: "11px", lg: "md" }}
              color="#000000"
              align="left"
              px="5"
              py="2"
              bg="#FFD700"
              borderRadius="8px"
            >
              {`${screen?.slotsTimePeriod} sec / slot`}
            </Text>
          </Stack>
        </HStack>
        <Divider color="#DDDDDD" />
        <Text
          align="center"
          color="#222222"
          fontSize="xl"
          fontWeight="semibold"
        >
          Calculate budget
        </Text>
        <SimpleGrid columns={[1, 1, 2]} spacing={{ base: "5", lg: "1" }}>
          <InputGroup width="100%">
            <FormControl id="startDateHere">
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DateTimePicker
                  inputVariant="outlined"
                  placeholder="Enter start date"
                  value={startDateTime}
                  onChange={handleStartDate}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <MiuiIconButton>
                          <BsCalendar2Date />
                        </MiuiIconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </MuiPickersUtilsProvider>
            </FormControl>
          </InputGroup>
          <InputGroup width="100%">
            <FormControl id="startDateHere">
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DateTimePicker
                  placeholder="Enter end date"
                  inputVariant="outlined"
                  value={endDateTime}
                  onChange={handleEndDate}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <MiuiIconButton>
                          <BsCalendar2Date />
                        </MiuiIconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </MuiPickersUtilsProvider>
            </FormControl>
          </InputGroup>
        </SimpleGrid>

        <Text align="left" color="#222222" fontSize="md" pt="5">
          Total number of slots
        </Text>

        <Flex justifyContent={"space-between"} gap="20">
          <Stack width="180%">
            <Slider
              defaultValue={numberOfSlots}
              trackStyle={{ height: "10px", borderRadius: "8px" }}
              onChange={(value: any) => setNumberOfSlots(value)}
              // handleStyle={{
              //   height: "30px",
              //   width: "30px",
              //   background: "#0EBCF5",
              //   borderRadius: "100%",
              // }}
            />
          </Stack>
          <Input
            value={numberOfSlots}
            htmlSize={4}
            onChange={(e: any) => {
              if (e.target.value >= 1) {
                setNumberOfSlots(e.target.value);
              }
            }}
            size="large"
            width="auto"
            type="number"
          />
        </Flex>
        <Text color="#222222" fontSize="md" m="0">
          Average Impressions: 1245
        </Text>
        <Text color="#222222" fontSize="md">
          Average footfall: {screen?.additionalData?.averageDailyFootfall}
        </Text>
        {userInfo ? (
          <Button
            color="#ffffff"
            bgColor="#D7380E"
            fontSize="md"
            width="100%"
            py="3"
            onClick={handleAddToCard}
          >
            Add to card
          </Button>
        ) : null}
        <HStack
          justifyContent="space-between"
          color="#222222"
          fontSize="md"
          pt="5"
        >
          <Text align="left">{`₹${screen?.rentPerSlot} x ${numberOfSlots} slots`}</Text>
          <Text align="left">{`₹${screen?.rentPerSlot * numberOfSlots}`}</Text>
        </HStack>
        <HStack justifyContent="space-between" color="#222222" fontSize="md">
          <Text align="left" m="0">
            GST 5 %
          </Text>
          <Text align="left" m="0">{`₹${
            (screen?.rentPerSlot * numberOfSlots * 5) / 100
          }`}</Text>
        </HStack>
        <Divider color="#DDDDDD" />
        <HStack
          justifyContent="space-between"
          color="#222222"
          fontSize="md"
          fontWeight="semibold"
        >
          <Text align="left" m="0">
            Total
          </Text>
          <Text align="left" m="0">{`₹${
            screen?.rentPerSlot * numberOfSlots +
            (screen?.rentPerSlot * numberOfSlots * 5) / 100
          }`}</Text>
        </HStack>
      </Box>
    </Modal>
  );
}
