import { Box } from "@chakra-ui/react";
import data from "../../utils/data.json";
import Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";
import moment from "moment";
// import more from "highcharts/highcharts-more.src";
import HCMore from "highcharts/highcharts-more";

HCMore(Highcharts);

const convertStringToArray = (inputString: string) => {
  const withoutParentheses = inputString.slice(1, -1); // Remove parentheses
  const numberStrings = withoutParentheses
    .split(",")
    .map((str) => parseInt(str.trim(), 10));
  // console.log("numberStrings : ", numberStrings.length);
  return numberStrings;
};

export function ShowDataInChar(props: any) {
  const genderAge = data.genderAge;
  // console.log("genderAge : ", genderAge);
  // console.log("e : ", moment(1639665000000).format());
  const timemalefemele = genderAge.reduce((accum: any, current: any) => {
    let Time = current.Time;
    if (Time == "2023-10-12 16:28:12") {
      // console.log("current : ", current.Age);
    }
    if (accum[Time]) {
      // console.log("timemalefemele[key].MaleAge : ", accum[Time].MaleAge.length);

      if (current?.Gender == "Female") {
        accum[Time].Female += 1;
        accum[Time].FemaleAge = [
          ...accum[Time].FemaleAge,
          ...convertStringToArray(current.Age),
        ];
      } else if (current?.Gender == "Male") {
        accum[Time].Male += 1;
        accum[Time].MaleAge = [
          ...accum[Time].MaleAge,
          ...convertStringToArray(current.Age),
        ];
      }
    } else {
      accum[Time] = {
        Female: current?.Gender == "Female" ? 1 : 0,
        FemaleAge:
          current?.Gender == "Female"
            ? [...convertStringToArray(current.Age)]
            : [],
        MaleAge:
          current?.Gender == "Male"
            ? [...convertStringToArray(current.Age)]
            : [],
        Male: current?.Gender == "Male" ? 1 : 0,
      };
    }
    // console.log("accum : ", accum);
    return accum;
  }, {});
  // console.log("timemalefemele : ", timemalefemele);
  let timeMale = [];
  let timeFemale = [];
  let timeMaleAgeRange = [];
  let timeFemaleAgeRange = [];
  for (let key in timemalefemele) {
    timeMale.push([moment(key).valueOf(), timemalefemele[key].Male]);
    timeFemale.push([moment(key).valueOf(), timemalefemele[key].Female]);
    const maleAge = new Set(timemalefemele[key].MaleAge);
    const femaleAge = new Set(timemalefemele[key].FemaleAge);
    const ageRange =
      timemalefemele[key].MaleAge.length > 0 ? Array.from(maleAge) : [0, 0];
    const femaleAgeRange =
      timemalefemele[key].FemaleAge.length > 0 ? Array.from(femaleAge) : [0, 0];

    // if (timemalefemele[key].MaleAge.length === 0) {
    //   console.log(
    //     "timemalefemele[key].MaleAge : ",
    //     timemalefemele[key].MaleAge.length
    //   );
    // }
    // console.log(
    //   "timemalefemele[key].MaleAge : ",
    //   timemalefemele[key].MaleAge.length
    // );
    timeMaleAgeRange.push([moment(key).valueOf(), ...ageRange]);
    timeFemaleAgeRange.push([moment(key).valueOf(), ...femaleAgeRange]);
  }
  timeMale.sort(sortFunction);
  timeFemale.sort(sortFunction);
  timeMaleAgeRange.sort(sortFunction);
  timeFemaleAgeRange.sort(sortFunction);

  // console.log("timeMaleAgeRange : ", timeMaleAgeRange);

  function sortFunction(a: any, b: any) {
    if (a[0] === b[0]) {
      return 0;
    } else {
      return a[0] < b[0] ? -1 : 1;
    }
  }
  // console.log(" timeMale : ", timeMale);
  const dd = genderAge.reduce((accum: any, current: any) => {
    const age = `${current?.Age}`;
    if (accum[`${age}`]) {
      if (current?.Gender == "Female") {
        accum[age].Female += 1;
      } else if (current?.Gender == "Male") {
        accum[age].Male += 1;
      }
    } else {
      accum[`${age}`] = {
        Female: current?.Gender == "Female" ? 1 : 0,
        Male: current?.Gender == "Male" ? 1 : 0,
      };
    }
    // console.log("accum : ", accum);
    return accum;
  }, {});

  const male = [];
  const female = [];
  const categories = [];
  for (let key in dd) {
    categories.push(key);
    male.push(dd[key].Male);
    female.push(dd[key].Female);
  }

  const options = {
    chart: {
      type: "column",
    },
    title: {
      text: "Peoples capture by web cam",
      align: "center",
    },
    xAxis: {
      categories: categories,
      title: {
        text: "Age range",
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Count people",
      },
      stackLabels: {
        enabled: true,
      },
    },
    legend: {
      align: "left",
      x: 70,
      verticalAlign: "top",
      y: 70,
      floating: true,
      backgroundColor:
        Highcharts?.defaultOptions?.legend?.backgroundColor || "white",
      borderColor: "#CCC",
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: "<b>{point.x}</b><br/>",
      pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
    },
    plotOptions: {
      column: {
        stacking: "normal",
        dataLabels: {
          enabled: true,
        },
      },
    },
    series: [
      {
        name: "Male",
        data: male,
      },
      {
        name: "Female",
        data: female,
      },
    ],
  };
  const options1 = {
    chart: {
      type: "column",
    },
    title: {
      text: "Peoples capture by web cam in persent",
      align: "center",
    },
    xAxis: {
      categories: categories,
      title: {
        text: "Age range",
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Count people in %",
      },
    },
    tooltip: {
      pointFormat:
        '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
      shared: true,
    },
    plotOptions: {
      column: {
        stacking: "percent",
      },
    },
    series: [
      {
        name: "Male",
        data: male,
      },
      {
        name: "Female",
        data: female,
      },
    ],
  };
  const options3 = {
    rangeSelector: {
      selected: 1,
    },

    title: {
      text: "Male count value",
    },

    series: [
      {
        name: "Male",
        data: timeMale,
        tooltip: {
          valueDecimals: 0,
        },
      },
    ],
  };
  const options4 = {
    rangeSelector: {
      selected: 1,
    },

    title: {
      text: "Female count value",
    },

    series: [
      {
        name: "Female",
        data: timeFemale,
        tooltip: {
          valueDecimals: 0,
        },
      },
    ],
  };

  const option5 = {
    chart: {
      type: "columnrange",
    },

    rangeSelector: {
      selected: 2,
    },

    title: {
      text: "Male Age variation by day",
    },

    tooltip: {
      valueSuffix: " age",
    },

    series: [
      {
        name: "Male Age range",
        data: timeMaleAgeRange,
      },
    ],
  };
  const option6 = {
    chart: {
      type: "columnrange",
    },

    rangeSelector: {
      selected: 2,
    },

    title: {
      text: "Female Age variation by day",
    },

    tooltip: {
      valueSuffix: " age",
    },

    series: [
      {
        name: "Female Age range",
        data: timeFemaleAgeRange,
      },
    ],
  };

  return (
    <Box gap="10" overflowY="scroll">
      <>
        <HighchartsReact highcharts={Highcharts} options={options} />{" "}
        <HighchartsReact highcharts={Highcharts} options={options1} />
        <HighchartsReact
          highcharts={Highcharts}
          options={options3}
          constructorType={"stockChart"}

          // constructorType={"stockChart"}
        />
        <HighchartsReact
          highcharts={Highcharts}
          options={option5}
          constructorType={"stockChart"}

          // constructorType={"stockChart"}
        />
        <HighchartsReact
          highcharts={Highcharts}
          options={options4}
          constructorType={"stockChart"}

          // constructorType={"stockChart"}
        />
        <HighchartsReact
          highcharts={Highcharts}
          options={option6}
          constructorType={"stockChart"}

          // constructorType={"stockChart"}
        />
      </>
    </Box>
  );
}
