import {
  Box,
  Stack,
  TableContainer,
  Table,
  Thead,
  Tr,
  Th,
  Td,
  Tbody,
  Badge,
  Button,
  Text,
} from "@chakra-ui/react";

import { useDispatch, useSelector } from "react-redux";
import { listAllPleasByUserId, listMyPleas } from "../../Actions/pleaActions";
import { useEffect, useState } from "react";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import {
  grantScreenAllyPlea,
  rejectScreenAllyPlea,
} from "../../Actions/screenActions";
import {
  SCREEN_ALLY_GRANT_RESET,
  SCREEN_ALLY_REJECT_RESET,
} from "../../Constants/screenConstants";
import { VideoPlayerModel } from "./VideoPlayerModel";
import { UserDetailModel } from "./UserDetailModel";
import {
  ALL_PLEA_LIST_BY_USER_REST,
  CAMPAIGN_ALLY_GRANT_RESET,
  CAMPAIGN_ALLY_REJECT_RESET,
  LIST_MY_PLEAS_RESET,
} from "../../Constants/pleaConstants";
import { useNavigate } from "react-router-dom";
import { Skeleton, Tabs, message } from "antd";
import { CouponDetailsModel } from "./CouponDetailsModel";
// import { AlertDialogBox } from "../../components/common/AlertDilogBox/AlertDilogBox";

export function PleaRequestListByUser() {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [selectedPlea, setSelectedPlea] = useState<any>();
  const [showAccept, setShowAccept] = useState<any>(true);

  const [openCouponDetailsModel, setOpenCouponDetailsModel] =
    useState<any>(false);
  const [videoPlayerModelShow, setVideoPlayerModelShow] =
    useState<boolean>(false);
  const [userDetailModelShow, setUserDetailModelShow] =
    useState<boolean>(false);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    } else {
      dispatch(listAllPleasByUserId());
      dispatch(listMyPleas());
    }
  }, [navigate, userInfo]);

  const handelOpenModel = (plea: any, show: boolean) => {
    setSelectedPlea(plea);
    if (plea.pleaType === "CAMPAIGN_ALLY_PLEA") {
      setShowAccept(show);
      setVideoPlayerModelShow(true);
    } else if (plea.pleaType === "COUPON_REDEEM_PLEA") {
      setOpenCouponDetailsModel(true);
    } else {
      setUserDetailModelShow(true);
    }
  };

  const campaignAllyPleaGrant = useSelector(
    (state: any) => state.campaignAllyPleaGrant
  );
  const {
    loading: loadingCampaignPleaGrant,
    success: successCampaignPleaGrant,
    error: errorCampaignPleaGrant,
  } = campaignAllyPleaGrant;

  const campaignAllyPleaReject = useSelector(
    (state: any) => state.campaignAllyPleaReject
  );
  const {
    loading: loadingCampaignRejectPlea,
    success: successCampaignRejectPlea,
    error: errorCampaignRejectPlea,
  } = campaignAllyPleaReject;

  const getStatus = (status: boolean, reject: boolean) => {
    if (!status && !reject) {
      return "Pending";
    } else if (status && !reject) {
      return "Accepted";
    } else {
      return "Rejected";
    }
  };

  const screenAllyPleaGrant = useSelector(
    (state: any) => state.screenAllyPleaGrant
  );
  const {
    loading: loadingScreenAllyPleaGrant,
    error: errorScreenAllyPleaGrant,
    success: successScreenAllyPleaGrant,
  } = screenAllyPleaGrant;

  const screenAllyPleaReject = useSelector(
    (state: any) => state.screenAllyPleaReject
  );
  const {
    loading: loadingScreenAllyPleaReject,
    error: errorScreenAllyPleaReject,
    success: successScreenAllyPleaReject,
  } = screenAllyPleaReject;

  const handelAcceptPlea = (pleaId: any) => {
    // console.log("handelAcceptPlea called! : ", pleaId);
    dispatch(grantScreenAllyPlea(pleaId));
  };
  const handelRejectPlea = (pleaId: any) => {
    // console.log("handelRejectPlea called!");
    dispatch(rejectScreenAllyPlea(pleaId));
  };

  const allPleasListByUser = useSelector(
    (state: any) => state.allPleasListByUser
  );
  const {
    allPleas,
    loading: loadingAllPleas,
    error: errorAllPleas,
  } = allPleasListByUser;
  const myAllPleas = useSelector((state: any) => state.myAllPleas);
  const {
    loading: loadingMyPleas,
    error: errorMyPleas,
    allPleas: myPleas,
  } = myAllPleas;

  useEffect(() => {
    if (successCampaignPleaGrant) {
      message.success(
        "You have given access to user to run this campaign on your screen"
      );
      dispatch({ type: CAMPAIGN_ALLY_GRANT_RESET });
    }
    if (errorCampaignPleaGrant) {
      message.error(errorCampaignPleaGrant);
      dispatch({ type: CAMPAIGN_ALLY_GRANT_RESET });
    }
    if (successCampaignRejectPlea) {
      message.success(
        "You have not given access to user to run this campaign on your screen"
      );
      dispatch({ type: CAMPAIGN_ALLY_REJECT_RESET });
    }
    if (errorCampaignRejectPlea) {
      message.error(errorCampaignRejectPlea);
      dispatch({ type: CAMPAIGN_ALLY_REJECT_RESET });
    }

    // for scrren plea
    if (successScreenAllyPleaGrant) {
      message.success(
        "You have given access to brand to direct deploy campaign on your screen"
      );
      dispatch({ type: SCREEN_ALLY_GRANT_RESET });
    }
    if (errorScreenAllyPleaGrant) {
      message.error(errorScreenAllyPleaGrant);
      dispatch({ type: SCREEN_ALLY_GRANT_RESET });
    }
    if (successScreenAllyPleaReject) {
      message.success(
        "You have not given access to brand to direct deploy campaign on your screen"
      );
      dispatch({ type: SCREEN_ALLY_REJECT_RESET });
    }
    if (errorScreenAllyPleaReject) {
      message.error(errorScreenAllyPleaReject);
      dispatch({ type: SCREEN_ALLY_REJECT_RESET });
    }
    if (errorAllPleas) {
      message.error(errorAllPleas);
      dispatch({ type: ALL_PLEA_LIST_BY_USER_REST });
    }
    if (errorMyPleas) {
      message.error(errorMyPleas);
      dispatch({ type: LIST_MY_PLEAS_RESET });
    }
  }, [
    dispatch,
    successCampaignPleaGrant,
    successCampaignRejectPlea,
    successScreenAllyPleaGrant,
    successScreenAllyPleaReject,
    errorCampaignPleaGrant,
    errorCampaignRejectPlea,
    errorScreenAllyPleaGrant,
    errorScreenAllyPleaReject,
    errorAllPleas,
    errorMyPleas,
  ]);

  return (
    <>
      <VideoPlayerModel
        open={videoPlayerModelShow}
        onClose={() => setVideoPlayerModelShow(false)}
        plea={selectedPlea}
        show={showAccept}
        // handelAcceptCampaignPlea={handelAcceptCampaignPlea}
        // handelRejectCampaignPlea={handelRejectCampaignPlea}
      />
      <UserDetailModel
        open={userDetailModelShow}
        onClose={() => setUserDetailModelShow(false)}
        plea={selectedPlea}
        handelAcceptPlea={handelAcceptPlea}
        handelRejectPlea={handelRejectPlea}
      />
      <CouponDetailsModel
        open={openCouponDetailsModel}
        onClose={() => setOpenCouponDetailsModel(false)}
        plea={selectedPlea}
      />
      <Box px={{ base: 2, lg: 20 }} py={{ base: 75, lg: 100 }}>
        <Tabs
          defaultActiveKey="1"
          items={[
            {
              label: "Plea request came from brand",
              key: "1",
              children: (
                <Box>
                  {loadingScreenAllyPleaGrant || loadingAllPleas ? (
                    <Skeleton active paragraph={{ rows: 10 }} />
                  ) : allPleas?.length === 0 ? (
                    <Box justifyContent="center">
                      <Text color="red" fontSize="2xl" fontWeight="500">
                        Currently No any plea request came from any one
                      </Text>
                    </Box>
                  ) : (
                    <Stack pt="10">
                      <TableContainer borderRadius="5px" bgColor="#FFFFFF">
                        <Table variant="simple">
                          <Thead>
                            <Tr>
                              <Th>Plea Date</Th>
                              <Th>Plea type</Th>
                              <Th>Remark</Th>
                              <Th>Status</Th>
                              <Th>Action</Th>
                            </Tr>
                          </Thead>
                          <Tbody>
                            {allPleas?.map((plea: any, index: any) => (
                              <Tr
                                key={index}
                                onClick={() => handelOpenModel(plea, true)}
                                _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
                              >
                                <Td color="#575757" fontSize="sm">
                                  {convertIntoDateAndTime(plea.createdAt)}
                                </Td>
                                <Td
                                  fontSize="sm"
                                  color="#403F49"
                                  fontWeight="semibold"
                                >
                                  {plea.pleaType === "CAMPAIGN_ALLY_PLEA" ? (
                                    <Badge colorScheme="red">
                                      {plea.pleaType}
                                    </Badge>
                                  ) : (
                                    plea.pleaType
                                  )}
                                </Td>
                                <Td
                                  fontSize="sm"
                                  color="#403F49"
                                  fontWeight="semibold"
                                >
                                  {plea.remarks[plea.remarks.length - 1]}
                                </Td>
                                <Td
                                  fontSize="sm"
                                  color="#403F49"
                                  fontWeight="semibold"
                                >
                                  <Badge
                                    colorScheme={
                                      getStatus(plea.status, plea.reject) ===
                                      "Pending"
                                        ? "purple"
                                        : getStatus(
                                            plea.status,
                                            plea.reject
                                          ) === "Accepted"
                                        ? "green"
                                        : "red"
                                    }
                                  >
                                    {getStatus(plea.status, plea.reject)}
                                  </Badge>
                                </Td>
                                <Td>
                                  <Button
                                    py="3"
                                    onClick={() => handelOpenModel(plea, true)}
                                  >
                                    See details
                                  </Button>
                                </Td>
                              </Tr>
                            ))}
                          </Tbody>
                        </Table>
                      </TableContainer>
                    </Stack>
                  )}
                </Box>
              ),
            },
            {
              label: "Plea request made",
              key: "2",
              children: (
                <Box>
                  {loadingMyPleas ? (
                    <Skeleton active paragraph={{ rows: 10 }} />
                  ) : myPleas?.length === 0 ? (
                    <Box justifyContent="center">
                      <Text color="red" fontSize="2xl" fontWeight="500">
                        Currently No any plea request made by You
                      </Text>
                    </Box>
                  ) : (
                    <Stack pt="10">
                      <TableContainer borderRadius="5px" bgColor="#FFFFFF">
                        <Table variant="simple">
                          <Thead>
                            <Tr>
                              <Th>Plea Date</Th>
                              <Th>Plea type</Th>
                              <Th>Remark</Th>
                              <Th>Status</Th>
                            </Tr>
                          </Thead>
                          <Tbody>
                            {myPleas?.map((plea: any, index: any) => (
                              <Tr
                                key={index}
                                _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
                              >
                                <Td color="#575757" fontSize="sm">
                                  {convertIntoDateAndTime(plea.createdAt)}
                                </Td>
                                <Td
                                  fontSize="sm"
                                  color="#403F49"
                                  fontWeight="semibold"
                                >
                                  {plea.pleaType === "CAMPAIGN_ALLY_PLEA" ? (
                                    <Badge colorScheme="red">
                                      {plea.pleaType}
                                    </Badge>
                                  ) : (
                                    plea.pleaType
                                  )}
                                </Td>
                                <Td
                                  fontSize="sm"
                                  color="#403F49"
                                  fontWeight="semibold"
                                >
                                  {plea.remarks[plea.remarks.length - 1]}
                                </Td>
                                <Td
                                  fontSize="sm"
                                  color="#403F49"
                                  fontWeight="semibold"
                                >
                                  <Badge
                                    colorScheme={
                                      getStatus(plea.status, plea.reject) ===
                                      "Pending"
                                        ? "purple"
                                        : getStatus(
                                            plea.status,
                                            plea.reject
                                          ) === "Accepted"
                                        ? "green"
                                        : "red"
                                    }
                                  >
                                    {getStatus(plea.status, plea.reject)}
                                  </Badge>
                                </Td>
                              </Tr>
                            ))}
                          </Tbody>
                        </Table>
                      </TableContainer>
                    </Stack>
                  )}
                </Box>
              ),
            },
          ]}
        />
      </Box>
    </>
  );
}
