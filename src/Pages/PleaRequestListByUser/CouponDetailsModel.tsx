import {
  Box,
  Button,
  Input,
  Text,
  Stack,
  Center,
  Image,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Modal, message } from "antd";
import {
  addPurchaseDetailsInCoupon,
  getCouponFullDetails,
} from "../../Actions/couponAction";
import {
  ADD_PURCHASE_DETAILS_TO_COUPON_RESET,
  GET_COUPON_FULL_DETAILS_RESET,
} from "../../Constants/couponConstants";
import { generateOfferDetails } from "../../utils/CouponUtils";

export function CouponDetailsModel(props: any) {
  const dispatch = useDispatch<any>();
  const plea = props?.plea;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const couponFullDetails = useSelector(
    (state: any) => state.couponFullDetails
  );
  const {
    loading: loadingCouponDetails,
    error: errorinCouponDetails,
    screens,
    campaigns,
    coupon,
  } = couponFullDetails;

  const addPurchaseDetails = useSelector(
    (state: any) => state.addPurchaseDetails
  );
  const {
    loading: loadingaddPurchaseDetails,
    error: erroraddPurchaseDetails,
    success: successaddPurchaseDetails,
  } = addPurchaseDetails;

  useEffect(() => {
    if (errorinCouponDetails) {
      message.error(errorinCouponDetails);
      dispatch({ type: GET_COUPON_FULL_DETAILS_RESET });
    }
    if (erroraddPurchaseDetails) {
      message.error(erroraddPurchaseDetails);
      dispatch({ type: ADD_PURCHASE_DETAILS_TO_COUPON_RESET });
    }
    if (successaddPurchaseDetails) {
      message.success("Successfully added purchase details in coupon");
      dispatch({ type: ADD_PURCHASE_DETAILS_TO_COUPON_RESET });
    }
  }, [
    errorinCouponDetails,
    successaddPurchaseDetails,
    erroraddPurchaseDetails,
  ]);

  useEffect(() => {
    if (plea) dispatch(getCouponFullDetails(plea?.couponId));
  }, [props]);

  const onFinish = (values: any) => {
    dispatch(
      addPurchaseDetailsInCoupon({
        couponId: plea?.couponId,
        fromUser: plea?.from,
        toUser: userInfo?._id,
        plaeId: plea?._id,
        ...values,
      })
    );
  };
  return (
    <Modal
      title="Coupon details"
      open={props?.open}
      onCancel={() => props.onClose()}
      footer={[]}
      maskClosable={false}
    >
      <Box bgColor="#FFFFFF">
        <Stack
          width={{ base: "", lg: "300px" }}
          border="1px"
          borderColor={"#E7E7E7"}
          borderRadius="4px"
          align="center"
          justifyContent="space-between"
        >
          <Center p={{ base: "2", lg: "5" }} flexDirection="column">
            <Image
              src={coupon?.brandLogo}
              alt="logo"
              boxSize={{ base: "70px", lg: "150px" }}
            />
            <Text
              fontSize={{ base: "12px", lg: "lg" }}
              fontWeight="533"
              color="#000000"
              align="center"
            >
              {coupon?.brandName}
            </Text>
            <Text
              className="text"
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="533"
              color="#000000"
              m="0"
              align="center"
              // isTruncated={true}
              noOfLines={2}
            >
              {coupon?.offerName}
            </Text>

            <Text
              className="text"
              pt="2"
              fontSize={{ base: "12px", lg: "lg" }}
              fontWeight="533"
              color="#000000"
              m="0"
              align="center"
              // isTruncated={true}
              noOfLines={2}
            >
              {generateOfferDetails(coupon)}
            </Text>
            <Text
              className="text"
              fontSize={{ base: "sm", lg: "lg" }}
              fontWeight="533"
              color="#000000"
              m="0"
              align="center"
              // isTruncated={true}
              noOfLines={2}
            >
              Coupon Code: {coupon?.couponCode}
            </Text>
          </Center>
        </Stack>
        {plea?.status === false ? (
          <Form
            layout="vertical"
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Enter purchase amount
                </Text>
              }
              name="purchaseAmount"
              rules={[
                {
                  required: true,
                  message: "Please input your purchaseAmount!",
                },
              ]}
            >
              <Input
                type="number"
                placeholder="Enter purchase amount"
                size="large"
              />
            </Form.Item>
            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Saved amount
                </Text>
              }
              name="savedAmount"
              rules={[
                {
                  required: true,
                  message: "Please enter user saved amount",
                },
              ]}
            >
              <Input
                placeholder="Enter user saved amount"
                size="large"
                type="number"
                style={{ padding: "5" }}
              />
            </Form.Item>

            <Form.Item>
              <Button
                type="submit"
                width="100%"
                backgroundColor="#D7380E"
                py="2"
                fontSize={{ base: "xl", lg: "2xl" }}
                isLoading={loadingCouponDetails}
                loadingText="Saving.."
              >
                Save
              </Button>
            </Form.Item>
          </Form>
        ) : null}
      </Box>
    </Modal>
  );
}
