import {
  Box,
  HStack,
  Button,
  FormControl,
  FormLabel,
  Input,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { detailsUser } from "../../Actions/userActions";
import { Modal } from "antd";

export function UserDetailModel(props: any) {
  const dispatch = useDispatch<any>();

  const userDetails = useSelector((state: any) => state.userDetails);
  const { loading, user, errro } = userDetails;

  useEffect(() => {
    dispatch(
      detailsUser({ userId: props.plea?.from, walletAddress: "231132" })
    );
  }, [props]);
  return (
    <Modal
      title="User details"
      open={props?.open}
      onCancel={() => props.onClose()}
      footer={[]}
      maskClosable={false}
    >
      <Box bgColor="#FFFFFF">
        {user ? (
          <Box p="5">
            <FormControl id="name">
              <FormLabel fontSize="sm" color="#333333">
                Name
              </FormLabel>
              <Input
                color="#333333"
                value={user.user.name}
                type="text"
                disabled
                py="2"
                rounded="md"
                size="md"
                borderColor="#888888"
              />
            </FormControl>
            <FormControl id="name">
              <FormLabel fontSize="sm" color="#333333">
                Email
              </FormLabel>
              <Input
                color="#333333"
                value={user.user.email}
                type="text"
                disabled
                py="2"
                rounded="md"
                size="md"
                borderColor="#888888"
              />
            </FormControl>
            <FormControl id="name">
              <FormLabel fontSize="sm" color="#333333">
                Phone
              </FormLabel>
              <Input
                color="#333333"
                value={user.user.phone}
                type="text"
                disabled
                py="2"
                rounded="md"
                size="md"
                borderColor="#888888"
              />
            </FormControl>
            <FormControl id="name">
              <FormLabel fontSize="sm" color="#333333">
                Address
              </FormLabel>
              <Input
                color="#333333"
                value={user.user.address}
                type="text"
                disabled
                py="2"
                rounded="md"
                size="md"
                borderColor="#888888"
              />
            </FormControl>
            <FormControl id="name">
              <FormLabel fontSize="sm" color="#333333">
                State
              </FormLabel>
              <Input
                color="#333333"
                value={user.user.stateUt}
                type="text"
                disabled
                py="2"
                rounded="md"
                size="md"
                borderColor="#888888"
              />
            </FormControl>
            <FormControl id="name">
              <FormLabel fontSize="sm" color="#333333">
                District
              </FormLabel>
              <Input
                color="#333333"
                value={user.user.districtCity}
                type="text"
                disabled
                py="2"
                rounded="md"
                size="md"
                borderColor="#888888"
              />
            </FormControl>
            <FormControl id="name">
              <FormLabel fontSize="sm" color="#333333">
                PinCode
              </FormLabel>
              <Input
                color="#333333"
                value={user.user.pincode}
                type="text"
                disabled
                py="2"
                rounded="md"
                size="md"
                borderColor="#888888"
              />
            </FormControl>
          </Box>
        ) : null}
        {/* {!props?.plea?.reject && ( */}
        <HStack py="5" px="5" justifyContent="space-around">
          <Button
            py="2"
            onClick={() => {
              props.handelAcceptPlea(props?.plea?._id);
              props.onClose();
            }}
            bgColor="green"
          >
            Accept
          </Button>
          <Button
            py="2"
            onClick={() => {
              props.handelRejectPlea(props?.plea?._id);
              props.onClose();
            }}
            bgColor="red"
          >
            Reject
          </Button>
        </HStack>
        {/* )} */}
      </Box>
    </Modal>
  );
}
