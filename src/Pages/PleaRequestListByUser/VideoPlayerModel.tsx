import { Box, Stack, IconButton, HStack, Button } from "@chakra-ui/react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { Modal } from "antd";
import { MediaContainer } from "../../components/commans";
import { useDispatch } from "react-redux";

import {
  grantCampaignAllyPlea,
  rejectCampaignAllyPlea,
} from "../../Actions/pleaActions";

export function VideoPlayerModel(props: any) {
  const dispatch = useDispatch<any>();
  const handelAcceptCampaignPlea = (pleaId: any) => {
    // console.log("handelAcceptCampaignPlea called! : ", pleaId);
    dispatch(grantCampaignAllyPlea(pleaId));
  };
  const handelRejectCampaignPlea = (pleaId: any) => {
    // console.log("handelRejectCampaignPlea called! : ", pleaId);
    dispatch(rejectCampaignAllyPlea(pleaId));
  };
  return (
    <Modal
      title=""
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={false}
      maskClosable={false}
    >
      <Box bgColor="#FFFFFF">
        <Stack align="end" justifyContent="flex-end">
          <IconButton
            bg="none"
            icon={
              <AiOutlineCloseCircle
                size="30px"
                fontWeight="10"
                color="#00000090"
                onClick={props.onClose}
              />
            }
            aria-label="Close"
          />
        </Stack>
        <Box p="5">
          <MediaContainer
            cid={props.plea?.video?.split("/").slice(4)[0]}
            width={{ base: "100%", lg: "100%" }}
            height={{ base: "100px", lg: "250px" }}
            autoPlay="false"
          />
        </Box>
        {/* {!props?.plea?.status && !props?.plea?.reject ? ( */}
        {props?.show ? (
          <HStack py="5" px="5" justifyContent="space-around">
            <Button
              py="2"
              onClick={() => {
                handelAcceptCampaignPlea(props.plea?._id);
                props.onClose();
              }}
              bgColor="green"
            >
              Accept
            </Button>
            <Button
              py="2"
              onClick={() => {
                handelRejectCampaignPlea(props.plea?._id);
                props.onClose();
              }}
              bgColor="red"
            >
              Reject
            </Button>
          </HStack>
        ) : null}
      </Box>
    </Modal>
  );
}
