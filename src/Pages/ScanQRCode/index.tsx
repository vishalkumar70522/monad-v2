// versi "react-qr-reader" 1.0.0. component API harus disesuaikan dengan yg baru

import { Modal } from "antd";
// import "./style.css";
import { QrReader } from "react-qr-reader";
import { Stack } from "@chakra-ui/react";
import { useNavigate } from "react-router";

export const ScanQRCode = (props: any) => {
  const navigate = useNavigate();
  return (
    <Modal
      title=""
      centered
      open={props?.open}
      // open={true}
      onCancel={() => {
        props.onCancel();
        window.location.reload();
      }}
      footer={null}
      closable={true}
      maskClosable={true}
      destroyOnClose={true}
      closeIcon={true}
      // maskStyle={{ backgroundColor: "red" }}
      // backdropOpacity={1}
      // animationIn={"slideInLeft"}
      // animationOut={"slideOutRight"}
      style={{
        width: "100%",
        zIndex: "1",
        backgroundColor: "transparent",
      }}
      bodyStyle={{
        background: "transparent",
        margin: 0,
        padding: 0,
        // height: "90vh",
      }}
    >
      <Stack m="">
        <QrReader
          constraints={{ facingMode: "environment" }}
          onResult={(result: any, error: any) => {
            if (result) {
              const path = result.getText();
              // console.log("path :", path);
              // console.log("result :", result);
              props.onCancel();
              window.open(path, "_blank");
              window.location.reload();
              navigate(`${path.replace(/^.*\/\/[^\/]+/, "")}`);
              window.location.replace(`${path.replace(/^.*\/\/[^\/]+/, "")}`);
            }

            if (error) {
              console.info(error);
            }
          }}
          // videoContainerStyle={{
          //   height: "100vh",
          // }}
        />
      </Stack>
    </Modal>
  );
};
