import { Box } from "@chakra-ui/react";
import { BrandTableForAction } from "../../components/commans/BrandTableForAction";

export function Action(props: any) {
  return (
    <Box>
      <BrandTableForAction screenId={props?.screenId} />
    </Box>
  );
}
