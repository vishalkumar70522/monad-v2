import { Modal, Skeleton } from "antd";
import { useEffect } from "react";
import { getQrScanData } from "../../Actions/screenActions";
import { useDispatch, useSelector } from "react-redux";
import {
  Stack,
  Text,
  TableContainer,
  Table,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from "@chakra-ui/react";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";

export function ScanReportLogModel(props: any) {
  const dispatch = useDispatch<any>();
  const { screenId } = props;

  const qrScanDataGet = useSelector((state: any) => state.qrScanDataGet);
  const {
    loading: loadingDataDetailsGet,
    error: errorDataDetailsGet,
    data: qrScanData,
  } = qrScanDataGet;

  useEffect(() => {
    dispatch(getQrScanData(screenId));
  }, [dispatch, screenId]);
  return (
    <Modal
      title="scan log report"
      centered
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={true}
      maskClosable={false}
    >
      <>
        {loadingDataDetailsGet ? (
          <Skeleton />
        ) : (
          <Stack>
            <Text fontSize="xs">{qrScanData?.scanText}</Text>
            <TableContainer borderRadius="5px" bgColor="#FFFFFF">
              <Table>
                <Thead>
                  <Tr>
                    <Th>
                      <Text fontSize="8px">Sl No.</Text>
                    </Th>
                    <Th>
                      <Text fontSize="8px">Scan Time</Text>
                    </Th>
                    <Th>
                      <Text fontSize="8px">Scan User</Text>
                    </Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {qrScanData?.scanDetails.map((deet: any, index: any) => (
                    <Tr key={index}>
                      <Td>{index + 1}.</Td>
                      <Td>{convertIntoDateAndTime(deet?.scanTime)}</Td>
                      <Td>{deet?.scanUser ? deet?.scanUser : "null"}</Td>
                    </Tr>
                  ))}
                </Tbody>
              </Table>
            </TableContainer>
          </Stack>
        )}
      </>
    </Modal>
  );
}
