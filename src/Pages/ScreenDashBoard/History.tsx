import { Box, Hide, Show } from "@chakra-ui/react";
import {
  AdsListOfSignleScreenMobileView,
  BrandTable,
  ResultNotFound,
} from "../../components/commans";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getAllCampaignListByScreenId } from "../../Actions/campaignAction";
import { Skeleton } from "antd";
// import { getScreenLogs } from "../../Actions/screenActions";

export function History(props: any) {
  const dispatch = useDispatch<any>();
  // console.log(props?.screenId);

  const campaignListByScreenId = useSelector(
    (state: any) => state.campaignListByScreenId
  );
  const {
    loading: loadingCampaign,
    error: errorCampaign,
    campaigns: videosList,
  } = campaignListByScreenId;

  useEffect(() => {
    // if (errorCampaign) {
    //   message.error(errorCampaign);
    // }
    dispatch(getAllCampaignListByScreenId(props.screenId));
    // dispatch(getScreenLogs(props.screenId));
  }, [dispatch, props, errorCampaign]);

  return (
    <Box>
      {loadingCampaign ? (
        <Box>
          <Skeleton avatar active paragraph={{ rows: 3 }} />
        </Box>
      ) : errorCampaign ? (
        <ResultNotFound />
      ) : (
        <>
          <Hide below="md">
            <BrandTable
              videosList={videosList}
              // handleShowCampaignLogs={handleShowCampaignLogs}
              // loadingScreenLogs={loadingScreenLogs}
              isDisabled={false}
              screen={props?.screen}
            />
          </Hide>
          <Show below="md">
            <AdsListOfSignleScreenMobileView
              videos={videosList || []}
              show={false}
              // handleShowCampaignLogs={handleShowCampaignLogs}
            />
          </Show>
        </>
      )}
    </Box>
  );
}
