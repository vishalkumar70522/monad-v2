import {
  Box,
  InputGroup,
  SimpleGrid,
  Text,
  VStack,
  HStack,
  Button,
  Input,
  Image,
  Skeleton,
} from "@chakra-ui/react";
import { Select, message } from "antd";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { updateScreen } from "../../Actions/screenActions";
import { SCREEN_UPDATE_RESET } from "../../Constants/screenConstants";
import { AiOutlinePlusCircle } from "react-icons/ai";
import { addFileOnWeb3 } from "../../utils/web3";
import { MediaContainer } from "../../components/commans";

export function Highlights(props: any) {
  const { screen } = props;
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();

  const [tag, setTag] = useState<any>("");
  const [screenHighlights, setScreenHighlights] = useState<any>([]);
  const [screenImages, setScreenImages] = useState<any>([]);
  const [images, setImages] = useState<any>([]);
  const [files, setFiles] = useState<any>([]);
  const [screenTags, setScreenTags] = useState<any>([]);
  const [loading, setLoading] = useState<boolean>(false);

  let hiddenInput: any = null;
  const options = [
    "School",
    "Colleges",
    "Swimming pool",
    "Appartments",
    "Metro",
    "Airports",
    "Military",
    "Tourist attractions",
    "Railways",
    "Beachs",
  ];

  const filteredOptions = options.filter(
    (data: any) => !screenHighlights.includes(data)
  );

  async function handlePhotoSelect(file: any) {
    if (file?.type.split("/")[0] === "image") {
      const fileThumbnail = URL.createObjectURL(file);

      setScreenImages([
        ...screenImages,
        { fileThumbnail, type: "image", cid: "" },
      ]);
      setFiles([...files, file]);
    } else if (
      file?.type.split("/")[0] === "video" &&
      file?.type.split("/")[1] === "mp4"
    ) {
      const fileThumbnail = URL.createObjectURL(file);

      setScreenImages([
        ...screenImages,
        { fileThumbnail, type: "video", cid: "" },
      ]);
      setFiles([...files, file]);
    }
  }
  const handleAddTags = (event: any) => {
    if (event.which === 13) {
      setScreenTags([...screenTags, tag]);
      setTag("");
    }
  };
  const handelImageDelete = (cid: any) => {
    setImages([...images.filter((data: any) => data !== cid)]);
  };
  const deleteImage = (image: any) => {
    setScreenImages([
      ...screenImages.filter((data: any) => data.fileThumbnail !== image),
    ]);
  };
  const deleteTags = (tag: any) => {
    const newTags = screenTags.filter((eachtag: any) => eachtag !== tag);
    setScreenTags(newTags);
  };

  const handelAddScreenHighlights = (value: string | string[]) => {
    setScreenHighlights(value);
  };

  useEffect(() => {}, [props]);
  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  const screenUpdate = useSelector((state: any) => state.screenUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = screenUpdate;

  const submitScreenHandler = async () => {
    if (files?.length > 0) {
      setLoading(true);
      const cids = [];
      for (let file of files) {
        try {
          const cid = await addFileOnWeb3(file);
          console.log("cid-------- : ", cid);
          cids.push(cid);
        } catch (error) {
          console.log("error in uploading file : ", error);
        }
      }
      console.log("cids : = ", cids);
      dispatch(
        updateScreen({
          _id: screen?._id,
          screenTags,
          screenHighlights,
          images: [...images, ...cids],
        })
      );
      setLoading(false);
    } else {
      dispatch(
        updateScreen({
          _id: screen?._id,
          screenTags,
          screenHighlights,
          images: [...images],
        })
      );
    }
  };
  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }
    if (screen) {
      setScreenHighlights(screen?.screenHighlights);
      setScreenTags(screen?.screenTags);
      console.log("screen?.images useEffect ", screen?.images);
      setImages(screen?.images);
    }
  }, [props.screen]);
  useEffect(() => {
    if (successUpdate) {
      message.success("Ad space updated successfully");
      if (props?.show) {
        props?.setOption("4");
      }
    }
    if (errorUpdate) {
      message.error(errorUpdate);
    }
    dispatch({ type: SCREEN_UPDATE_RESET });
    props.getScreenDetails();
  }, [errorUpdate, successUpdate]);

  return (
    <Box>
      <Skeleton isLoaded={screen}>
        <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Ad Space photo
            </Text>
            <Text color="#4D4D4D" align="left">
              Upload the Ad Space photo
            </Text>
          </VStack>
          <HStack>
            <HStack>
              {images?.length > 0 &&
                images?.map((cid: any, index: any) => (
                  <Box onClick={() => handelImageDelete(cid)}>
                    <MediaContainer
                      cid={cid}
                      key={index}
                      width="76px"
                      height="86px"
                    />
                  </Box>
                ))}
              {screenImages?.map((data: any, index: any) =>
                data.type === "image" ? (
                  <Image
                    key={index}
                    src={data.fileThumbnail}
                    width="76px"
                    height="86px"
                    onClick={() => deleteImage(data.fileThumbnail)}
                  />
                ) : (
                  <Box
                    as="video"
                    src={data.fileThumbnail}
                    autoPlay
                    loop
                    muted
                    display="inline-block"
                    width="100px"
                    height="100px"
                    onClick={() => deleteImage(data.fileThumbnail)}
                  ></Box>
                )
              )}
            </HStack>

            <Button
              variant="outline"
              width="76px"
              height="86px"
              onClick={() => hiddenInput.click()}
              borderColor="#8B8B8B"
              color="#8B8B8B"
            >
              <AiOutlinePlusCircle size="30px" />
            </Button>

            <Input
              hidden
              type="file"
              ref={(el) => (hiddenInput = el)}
              accept="image/png, image/jpeg video/mp4"
              onChange={(e: any) => handlePhotoSelect(e.target.files[0])}
            />
          </HStack>
        </SimpleGrid>
      </Skeleton>
      <Skeleton isLoaded={screen}>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Ad space tags
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter the hash Tags for Ad space
            </Text>
          </VStack>
          <VStack>
            <InputGroup size="lg" width={{ base: "100%", lg: "100%" }} pl="0">
              <Input
                placeholder="Enter tags"
                size="lg"
                borderRadius="md"
                fontSize="lg"
                border="1px"
                color="#555555"
                value={tag}
                onChange={(e) => setTag(e.target.value)}
                onKeyPress={(e) => handleAddTags(e)}
                py="2"
              />
            </InputGroup>
            <SimpleGrid columns={[2, 3, 5]} spacing={3} width="100%">
              {screenTags &&
                screenTags.map((tag: any, index: number) => (
                  <InputGroup key={index + 1} size="lg">
                    <Button
                      variant="outline"
                      color="#515151"
                      bgColor="#FAFAFA"
                      fontSize="sm"
                      p="3"
                      borderColor="#555555"
                      _hover={{
                        bg: "rgba(14, 188, 245, 0.3)",
                        color: "#674780",
                      }}
                      onClick={() => {
                        deleteTags(tag);
                        return true;
                      }}
                    >
                      {`#${tag}`}
                      <Text pl="2" fontSize="sm" m="0">{` x`}</Text>
                    </Button>
                  </InputGroup>
                ))}
            </SimpleGrid>
          </VStack>
        </SimpleGrid>
      </Skeleton>
      <Skeleton isLoaded={screen}>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Enter Ad space highlights
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter your Ad space highlights
            </Text>
          </VStack>
          <VStack>
            <Select
              mode="multiple"
              size={"large"}
              maxTagCount="responsive"
              placeholder="Please select"
              value={screenHighlights}
              onChange={handelAddScreenHighlights}
              style={{ width: "100%" }}
              options={filteredOptions.map((item: any) => ({
                value: item,
                label: item,
              }))}
            />
          </VStack>
        </SimpleGrid>
      </Skeleton>
      <Skeleton isLoaded={screen}>
        <HStack justifyContent="flex-end" pr="30" pb="30" pt="30" spacing={5}>
          {props?.show ? (
            <Button
              variant="outline"
              color="#515151"
              bgColor="#FAFAFA"
              fontSize="sm"
              borderColor="#0EBCF5"
              py="3"
              px="10"
              isLoading={loadingUpdate || loading}
              loadingText="Screen data saving"
              _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
              onClick={submitScreenHandler}
            >
              Save & Next
            </Button>
          ) : (
            <Button
              variant="outline"
              color="#515151"
              bgColor="#FAFAFA"
              fontSize="sm"
              borderColor="#0EBCF5"
              py="3"
              px="10"
              isLoading={loadingUpdate || loading}
              loadingText="Screen data saving"
              _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
              onClick={submitScreenHandler}
            >
              Save
            </Button>
          )}
        </HStack>
      </Skeleton>
    </Box>
  );
}
