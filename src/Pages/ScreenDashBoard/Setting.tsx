import {
  Box,
  InputGroup,
  SimpleGrid,
  Text,
  VStack,
  Button,
  Input,
  Flex,
  HStack,
  Select,
  Skeleton,
  FormControl,
  FormLabel,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { BiDownArrow, BiUpArrow } from "react-icons/bi";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { updateScreen } from "../../Actions/screenActions";
import { Switch, message } from "antd";
import { saveAs } from "file-saver";
import { SCREEN_UPDATE_RESET } from "../../Constants/screenConstants";
import { createQRCodeForScreen } from "../../Actions/qrcodeAction";
import { GENERATE_QRCODE_FOR_SCREEN_RESET } from "../../Constants/generateQRCodeConstants";

export function Setting(props: any) {
  const { screen } = props;
  const screenSizeUnit = ["Px", "Ft", "Inch"];
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();

  const [name, setName] = useState<any>("");
  const [screenCategory, setScreenCategory] = useState<any>("");
  const [screenType, setScreenType] = useState<any>("");
  const [screenDiagonal, setScreenDiagonal] = useState<any>();

  const [screenLength, setScreenLength] = useState<any>("");
  const [screenWidth, setScreenWidth] = useState<any>("");
  const [syncCode, setSyncCode] = useState<any>("");
  const [measurementUnit, setMeasurementUnit] = useState<any>("");
  const [screenSizeUnitIndex, setScreenSizeUnitIndex] = useState<any>(2);
  const [getCamData, setGetCamData] = useState<boolean>(false);
  const [toPlay, setToPlay] = useState<boolean>(false);

  const handleSelectScreenType = (value: any) => {
    console.log("handleSelectScreenType  : ", value);
    setScreenType(value);
  };

  const hancleSelectSizeTypeUp = () => {
    if (screenSizeUnitIndex < screenSizeUnit.length - 1) {
      setScreenSizeUnitIndex(screenSizeUnitIndex + 1);
      setMeasurementUnit(screenSizeUnit[screenSizeUnitIndex]);
    }
  };
  const hancleSelectSizeTypeDown = () => {
    if (screenSizeUnitIndex > 0) {
      setScreenSizeUnitIndex(screenSizeUnitIndex - 1);
      setMeasurementUnit(screenSizeUnit[screenSizeUnitIndex]);
    }
  };

  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  const screenUpdate = useSelector((state: any) => state.screenUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = screenUpdate;

  const generateQRCode = useSelector((state: any) => state.generateQRCode);
  const { loading, error, success, message: qrcodeResponse } = generateQRCode;

  const handelGeterateQRCodeForScreen = (checked: any) => {
    if (checked === true && !screen?.qrCode) {
      dispatch(createQRCodeForScreen(screen?._id));
    }
  };

  const downloadImage = (imageUrl: any) => {
    saveAs(imageUrl, `qrcode.png`);
  };

  const submitScreenHandler = () => {
    dispatch(
      updateScreen({
        _id: screen?._id,
        name,
        screenCategory,
        screenLength,
        screenWidth,
        syncCode,
        measurementUnit,
        screenDiagonal,
        screenType,
        getCamData,
        toPlay,
      })
    );
  };
  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }
    if (screen) {
      setName(screen?.name);
      setScreenLength(screen?.size?.length);
      setScreenWidth(screen?.size?.width);
      setMeasurementUnit(screen?.size?.measurementUnit);
      if (props?.showScreenCode) {
        setSyncCode(screen?.screenCode);
      }
      setScreenCategory(screen?.category);
      setScreenDiagonal(screen?.size?.diagonal);
      setScreenType(screen?.screenType);
      setGetCamData(screen?.getCamData);
      setToPlay(screen?.defaultMediaPlayback);
      handelGeterateQRCodeForScreen(true);
    }
  }, [props?.screen]);
  useEffect(() => {
    if (successUpdate) {
      message.success("Ad space updated successfully");
      dispatch({ type: SCREEN_UPDATE_RESET });
      props.getScreenDetails();
      if (props?.show) {
        navigate("/myScreens");
      }
    }
    if (errorUpdate) {
      message.error(errorUpdate);
    }
    if (success) {
      message.success(qrcodeResponse);
      dispatch({ type: GENERATE_QRCODE_FOR_SCREEN_RESET });
      props.getScreenDetails();
    }
    if (error) {
      message.error(error);
    }
  }, [
    dispatch,
    errorUpdate,
    successUpdate,
    success,
    error,
    dispatch,
    navigate,
  ]);

  return (
    <Box>
      <Skeleton isLoaded={screen}>
        <SimpleGrid columns={[1, 1, 2]} spacing={3}>
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              New screen name
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter your screen name here
            </Text>
          </VStack>
          <InputGroup size="lg" width="100%">
            <Input
              width={{ base: "100%", lg: "50%" }}
              style={{ height: "40px" }}
              placeholder="Screen Name"
              size="lg"
              borderRadius="md"
              fontSize="lg"
              border="1px"
              color="#555555"
              py="2"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Screen Category
            </Text>
            <Text color="#4D4D4D" align="left">
              Select screen category here
            </Text>
          </VStack>
          <SimpleGrid columns={[2, 2, 4]} spacing={3}>
            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={screenCategory === "InDoors" ? "#4C4C4C" : "#515151"}
              bgColor={screenCategory === "InDoors" ? "#D6FFFF" : "#FAFAFA"}
              fontSize="sm"
              py={{ base: "2", lg: "3" }}
              _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#4C4C4C" }}
              onClick={() => setScreenCategory("InDoors")}
            >
              Indoors
            </Button>

            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={screenCategory === "OutDoors" ? "#4C4C4C" : "#515151"}
              bgColor={screenCategory === "OutDoors" ? "#D6FFFF" : "#FAFAFA"}
              fontSize="sm"
              py={{ base: "2", lg: "3" }}
              _hover={{
                bg: "rgba(14, 188, 245, 0.3)",
                color: "#4C4C4C",
              }}
              onClick={() => setScreenCategory("OutDoors")}
            >
              Outdoors
            </Button>
          </SimpleGrid>
        </SimpleGrid>
        <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Screen Type
            </Text>
            <Text color="#4D4D4D" align="left">
              Select screen type here
            </Text>
          </VStack>
          <Select
            borderRadius="8px"
            height="40px"
            width={{ base: "100%", lg: "50%" }}
            variant="outline"
            borderColor="#0EBCF5"
            bgColor="#FFFFFF"
            color="#403F49"
            fontSize="sm"
            value={screenType}
            onChange={(e) => {
              handleSelectScreenType(e.target.value);
            }}
          >
            <option value="">Choose One...</option>
            <option value="LED TV Screen">LED TV Screen</option>
            <option value="Digital Billboard Screen">
              Digital Billboard Screen
            </option>
          </Select>
        </SimpleGrid>
        <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Screen dimentions (height/width)
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter your screen dimensions
            </Text>
          </VStack>
          <InputGroup size="lg" width="50%">
            {screenType === "Digital Billboard Screen" ? (
              <Flex align="center" gap="2">
                <Input
                  width={{ base: "150px", lg: "50%" }}
                  style={{ height: "40px" }}
                  placeholder="Hight"
                  size="lg"
                  fontSize="md"
                  borderRadius="md"
                  border="1px"
                  color="#555555"
                  py="2"
                  type="number"
                  value={screenLength}
                  onChange={(e) => setScreenLength(e.target.value)}
                />
                <Input
                  width={{ base: "150px", lg: "50%" }}
                  style={{ height: "40px" }}
                  placeholder="Width"
                  size="lg"
                  fontSize="md"
                  borderRadius="md"
                  border="1px"
                  color="#555555"
                  py="2"
                  type="number"
                  value={screenWidth}
                  onChange={(e) => setScreenWidth(e.target.value)}
                />

                <VStack align="center">
                  <BiUpArrow
                    color="#555555"
                    size="16px"
                    onClick={hancleSelectSizeTypeUp}
                  />
                  <Text color="#555555" fontSize="sm" fontWeight="bold" m="0">
                    {measurementUnit}
                  </Text>
                  <BiDownArrow
                    color="#555555"
                    size="16px"
                    onClick={hancleSelectSizeTypeDown}
                  />
                </VStack>
              </Flex>
            ) : (
              <Flex align="center" gap={1}>
                <Input
                  width={{ base: "100%", lg: "50%" }}
                  style={{ height: "40px" }}
                  placeholder="diagonal"
                  size="lg"
                  fontSize="md"
                  borderRadius="md"
                  border="1px"
                  color="#555555"
                  py="2"
                  type="number"
                  value={screenDiagonal}
                  onChange={(e) => setScreenDiagonal(e.target.value)}
                />
                <Text color="#000000" fontSize="sm" fontWeight="" m="0">
                  Inch
                </Text>
              </Flex>
            )}
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Screen code
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter code to sync the screen
            </Text>
          </VStack>
          <InputGroup size="lg" width={{ base: "100%", lg: "100%" }}>
            <Input
              width={{ base: "100%", lg: "50%" }}
              style={{ height: "40px" }}
              placeholder="Screen Code"
              size="lg"
              borderRadius="md"
              fontSize="lg"
              border="1px"
              color="#555555"
              value={syncCode}
              onChange={(e) => setSyncCode(e.target.value)}
              py="2"
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Screen QRcode
            </Text>
            <Text color="#4D4D4D" align="left">
              Generate QRCode for screen
            </Text>
          </VStack>
          {!screen?.qrCode ? (
            <Switch
              style={{ width: "30px" }}
              checked={screen?.qrCode}
              onChange={(checked: boolean) =>
                handelGeterateQRCodeForScreen(checked)
              }
            />
          ) : (
            <Button
              width="200px"
              variant="outline"
              color="#515151"
              bgColor="#FAFAFA"
              fontSize="sm"
              borderColor="#0EBCF5"
              py="2"
              px="10"
              isLoading={loadingUpdate}
              loadingText="Screen data saving"
              _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
              onClick={() => downloadImage(screen?.qrCode)}
            >
              Download QRCode
            </Button>
          )}
        </SimpleGrid>
        {/* <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5"> */}
        <FormControl
          display="flex"
          // justifyContent="space-around"
          alignItems="center"
          pt="10"
        >
          <FormLabel htmlFor="camera data" mb="0" color="#4D4D4D">
            Get camera data?
          </FormLabel>
          <Switch
            // defaultChecked={getCamData}
            checked={getCamData}
            onClick={(e: any) => setGetCamData(e)}
          />
        </FormControl>
        <FormControl
          display="flex"
          // justifyContent="space-around"
          alignItems="center"
          pt="10"
        >
          <FormLabel htmlFor="playback default" mb="0" color="#4D4D4D">
            Playback default media?
          </FormLabel>
          <Switch
            // defaultChecked={getCamData}
            checked={toPlay}
            onClick={(e: any) => setToPlay(e)}
          />
        </FormControl>
        {/* </SimpleGrid> */}
        <HStack justifyContent="flex-end" pr="30" pb="30" pt="30" spacing={5}>
          {props?.show ? (
            <Button
              variant="outline"
              color="#515151"
              bgColor="#FAFAFA"
              fontSize="sm"
              borderColor="#0EBCF5"
              py="3"
              px="10"
              isLoading={loadingUpdate}
              loadingText="Screen data saving"
              _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
              onClick={submitScreenHandler}
            >
              Finish
            </Button>
          ) : (
            <Button
              variant="outline"
              color="#515151"
              bgColor="#FAFAFA"
              fontSize="sm"
              borderColor="#0EBCF5"
              py="3"
              px="10"
              isLoading={loadingUpdate}
              loadingText="Screen data saving"
              _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
              onClick={submitScreenHandler}
            >
              Save
            </Button>
          )}
        </HStack>
      </Skeleton>
    </Box>
  );
}
