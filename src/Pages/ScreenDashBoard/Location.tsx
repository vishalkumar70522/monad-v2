import {
  Box,
  InputGroup,
  Input,
  SimpleGrid,
  Text,
  VStack,
  FormControl,
  FormLabel,
  HStack,
  Button,
  Skeleton,
} from "@chakra-ui/react";
import { MyMap } from "../MyMap";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { updateScreen } from "../../Actions/screenActions";
import { message } from "antd";
import { SCREEN_UPDATE_RESET } from "../../Constants/screenConstants";
import { Switch } from "@material-ui/core";

export function Location(props: any) {
  const { screen } = props;
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [landmark, setLandmark] = useState<any>("");
  const [screenAddress, setScreenAddress] = useState<any>("");
  const [districtCity, setDistrictCity] = useState<any>("");
  const [stateUT, setStateUT] = useState<any>("");
  const [country, setCountry] = useState<any>("India");
  const [geometry, setGeometry] = useState<any>();
  const [jsonData, setJsonData] = useState<any>(null);
  const [selectCurrentLocation, setSelectCurrentLocation] =
    useState<any>(false);
  // const screen = props?.screen;
  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  const screenUpdate = useSelector((state: any) => state.screenUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = screenUpdate;

  const [lng, setLng] = useState<number | undefined>(25.52);
  const [lat, setLat] = useState<number | undefined>(82.33);
  const [mapProps, setMapProps] = useState<any>({
    lng: lng,
    lat: lat,
    zoom: 18,
    height: "360px",
  });

  const submitScreenHandler = () => {
    dispatch(
      updateScreen({
        _id: screen?._id,
        landmark,
        screenAddress,
        districtCity,
        stateUT,
        country,
        lat,
        lng,
      })
    );
  };

  const handleAddPinClick = (location: any) => {
    const { lng, lat } = location;
    setLng(lng);
    setLat(lat);
    setGeometry({
      coordinates: [lat, lng],
    });
    setJsonData({
      features: [
        {
          type: "Feature",
          properties: {
            pin: screen?._id,
            screen: screen?._id,
          },
          geometry: {
            coordinates: [lat, lng],
            type: "Point",
          },
        },
      ],
    });
    setMapProps({
      lng: lng,
      lat: lat,
    });
  };

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }
    if (screen) {
      setJsonData({
        features: [
          {
            type: "Feature",
            properties: {
              pin: screen?._id,
              screen: screen?._id,
            },
            geometry: {
              coordinates: [screen?.lat, screen?.lng],
              type: "Point",
            },
          },
        ],
      });
      setGeometry({
        coordinates: [screen?.lat, screen?.lng],
      });
      setScreenAddress(screen?.screenAddress);
      setDistrictCity(screen?.districtCity);
      setCountry(screen?.country);
      setStateUT(screen?.stateUT);
    }
  }, [props?.screen]);
  useEffect(() => {
    if (successUpdate) {
      message.success("Ad space updated successfully");
      if (props?.show) {
        props?.setOption("3");
      }
    }
    if (errorUpdate) {
      message.error(errorUpdate);
    }
    dispatch({ type: SCREEN_UPDATE_RESET });
    props?.getScreenDetails();
  }, [dispatch, errorUpdate, successUpdate]);

  return (
    <Box>
      <SimpleGrid columns={[1, 1, 2]} spacing={3}>
        <Skeleton isLoaded={jsonData}>
          <Box
            width="100%"
            color="black.500"
            height={{ base: "300px", lg: "600px" }}
            borderRadius="16px"
          >
            {jsonData ? (
              <MyMap
                data={jsonData}
                setLocation={handleAddPinClick}
                geometry={geometry}
                selectCurrentLocation={selectCurrentLocation}
                zoom="6"
              />
            ) : null}
          </Box>
        </Skeleton>
        <Skeleton isLoaded={screen}>
          <VStack fontSize="sm" spacing="2" width="100%" align="left" px="5">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              New screen location
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter your screen location here
            </Text>
            <InputGroup size="lg" width="100%" pt="2">
              <VStack>
                <Input
                  placeholder="Enter Landmark or market name"
                  size="lg"
                  borderRadius="md"
                  fontSize="lg"
                  border="1px"
                  color="#555555"
                  py="2"
                  value={landmark}
                  onChange={(e) => setLandmark(e.target.value)}
                />
                <Input
                  placeholder="DistrictCity"
                  size="lg"
                  borderRadius="md"
                  fontSize="lg"
                  border="1px"
                  color="#555555"
                  py="2"
                  value={screenAddress}
                  onChange={(e) => setScreenAddress(e.target.value)}
                />
                <Input
                  placeholder="DistrictCity"
                  size="lg"
                  borderRadius="md"
                  fontSize="lg"
                  border="1px"
                  color="#555555"
                  py="2"
                  value={districtCity}
                  onChange={(e) => setDistrictCity(e.target.value)}
                />
                <Input
                  placeholder="State"
                  size="lg"
                  borderRadius="md"
                  fontSize="lg"
                  border="1px"
                  color="#555555"
                  py="2"
                  value={stateUT}
                  onChange={(e) => setStateUT(e.target.value)}
                />

                <Input
                  placeholder="Country"
                  size="lg"
                  borderRadius="md"
                  fontSize="lg"
                  border="1px"
                  color="#555555"
                  py="2"
                  value={country}
                  onChange={(e) => setCountry(e.target.value)}
                />
              </VStack>
            </InputGroup>
            <FormControl display="flex" alignItems="center" pt="5">
              <FormLabel htmlFor="email-alerts" mb="0" color="#4D4D4D">
                Set screen location as out current location?
              </FormLabel>
              <Switch
                onChange={(e: any) =>
                  setSelectCurrentLocation(e.target.checked)
                }
              />
            </FormControl>
            <Text
              color="#393939"
              fontWeight="semibold"
              align="left"
              pt="5"
              m="0"
            >
              Latitudes & Longitudes
            </Text>
            <Text color="#4D4D4D" align="left">
              The latitudes and longitudes of the map is shown
            </Text>
            <InputGroup size="lg" width="60%" pt="2">
              <Input
                disabled
                placeholder="Screen name"
                size="lg"
                borderRadius="md"
                fontSize="lg"
                border="1px"
                color="#555555"
                py="2"
                value={`${screen?.lat} , ${screen?.lng}`}
              />
            </InputGroup>
          </VStack>
        </Skeleton>
      </SimpleGrid>
      <HStack justifyContent="flex-end" pr="30" pb="30" pt="30" spacing={5}>
        {props?.show ? (
          <Button
            variant="outline"
            color="#515151"
            bgColor="#FAFAFA"
            fontSize="sm"
            borderColor="#0EBCF5"
            py="3"
            px="10"
            isLoading={loadingUpdate}
            loadingText="Screen data saving"
            _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
            onClick={submitScreenHandler}
          >
            Save & Next
          </Button>
        ) : (
          <Button
            variant="outline"
            color="#515151"
            bgColor="#FAFAFA"
            fontSize="sm"
            borderColor="#0EBCF5"
            py="3"
            px="10"
            isLoading={loadingUpdate}
            loadingText="Screen data saving"
            _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
            onClick={submitScreenHandler}
          >
            Save
          </Button>
        )}
      </HStack>
    </Box>
  );
}
