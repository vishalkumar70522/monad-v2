import { Box, Flex, Stack, Hide, Show, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { DashBoard } from "./DashBoard";
// import { useLocation } from "react-router-dom";
import { History } from "./History";
import { Schedule } from "./Schedule";
import { Location } from "./Location";
import { Highlights } from "./HighLights";
import { Impressions } from "./Impressions";
import { Setting } from "./Setting";
import { Action } from "./Action";
import { Select, message } from "antd";
import { detailsScreen, getScreenData } from "../../Actions/screenActions";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { Additional } from "./Additional";

const buttonObject = [
  { value: "1", label: "Dashboard" },
  { value: "2", label: "History" },
  { value: "3", label: "Action" },
  { value: "4", label: "Schedule" },
  { value: "5", label: "Location" },
  { value: "6", label: "Highlights" },
  { value: "7", label: "Impressions" },
  { value: "8", label: "Settings" },
  { value: "9", label: "Additional" },
];

export function ScreenDashBoard(props: any) {
  const [selectedButton, setSelectedButton] = useState<any>("1");
  const screenId = window.location.pathname.split("/")[2];
  // const location = useLocation();
  const dispatch = useDispatch<any>();
  // const screenData = location?.state?.screen;

  const screenDetails = useSelector((state: any) => state.screenDetails);
  const { loading: loadingScreen, error: errorScreen, screen } = screenDetails;

  const screenDataGet = useSelector((state: any) => state.screenDataGet);
  const {
    loading: loadingScreenData,
    error: errorScreenData,
    data: screenData,
  } = screenDataGet;

  useEffect(() => {
    if (errorScreen) message.error(errorScreen);
  }, [errorScreen]);

  const getScreenDetails = () => {
    dispatch(detailsScreen(screenId));
  };

  useEffect(() => {
    dispatch(detailsScreen(screenId));
    dispatch(getScreenData(screenId));
  }, [dispatch, screenId]);

  return (
    <Box py={{ base: 75, lg: 100 }}>
      <Show below="md">
        <Stack p="4">
          <Select
            showSearch
            style={{ width: "100%" }}
            size="large"
            placeholder="Select option"
            optionFilterProp="children"
            onSelect={(value: any) => setSelectedButton(value)}
            filterOption={(input, option) =>
              (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
            }
            options={buttonObject}
          />
        </Stack>
      </Show>
      <Flex>
        <Hide below="md">
          <Stack
            bgColor="#F8F8F8"
            borderTopRightRadius="48px"
            width="267px"
            alignItems="center"
            height="946px"
            p="5"
          >
            <Stack pt="50"></Stack>
            {buttonObject?.map((single: any, index: any) => (
              <Text
                textAlign="left"
                key={index}
                bgColor={
                  selectedButton === single.value ? "#EEEEEE" : "#F8F8F8"
                }
                _hover={{
                  bgColor: "#EEEEEE",
                  color: "#3A3A3A",
                  fontWeight: "600",
                }}
                py="3"
                pl="5"
                m="0"
                width="214px"
                color={selectedButton === single.value ? "#3A3A3A" : "#4E4E4E"}
                fontWeight={selectedButton === single.value ? "600" : "506"}
                fontSize="lg"
                onClick={() => setSelectedButton(single.value)}
              >
                {single.label}
              </Text>
            ))}
          </Stack>
        </Hide>
        <Stack width="100%" p="5">
          {selectedButton === "1" ? (
            <DashBoard screenId={screenId} screen={screen} />
          ) : selectedButton === "2" ? (
            <History screenId={screenId} screen={screen} />
          ) : selectedButton === "3" ? (
            <Action screenId={screenId} screen={screen} />
          ) : selectedButton === "4" ? (
            <Schedule
              screenId={screenId}
              screen={screen}
              getScreenDetails={getScreenDetails}
            />
          ) : selectedButton === "5" ? (
            <Location
              screenId={screenId}
              screen={screen}
              getScreenDetails={getScreenDetails}
            />
          ) : selectedButton === "6" ? (
            <Highlights
              screenId={screenId}
              screen={screen}
              getScreenDetails={getScreenDetails}
            />
          ) : selectedButton === "7" ? (
            <Impressions
              screenId={screenId}
              screen={screen}
              getScreenDetails={getScreenDetails}
            />
          ) : selectedButton === "8" ? (
            <Setting
              screenId={screenId}
              screen={screen}
              showScreenCode={true}
              getScreenDetails={getScreenDetails}
            />
          ) : selectedButton === "9" ? (
            <Additional
              screenId={screenId}
              screen={screen}
              screenData={screenData.screenData}
              getScreenDetails={getScreenDetails}
            />
          ) : null}
        </Stack>
      </Flex>
    </Box>
  );
}
