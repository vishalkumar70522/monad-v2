import {
  Box,
  HStack,
  Text,
  Flex,
  Stack,
  VStack,
  Divider,
  IconButton,
  Hide,
  SimpleGrid,
  Show,
  Tooltip,
  // Button,
} from "@chakra-ui/react";
import { AiFillStar, AiOutlineRight } from "react-icons/ai";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import { useEffect, useState } from "react";
import {
  getAllCampaignListByScreenId,
  getCampaignListByScreenId,
} from "../../Actions/campaignAction";
import { useDispatch, useSelector } from "react-redux";
import { AdsListOfSinglScreen, BrandTable } from "../../components/commans";
import { RiDeleteBin6Line, RiUpload2Line } from "react-icons/ri";
import { Popconfirm, Skeleton, message } from "antd";
import { deleteScreen } from "../../Actions/screenActions";
import { SCREEN_DELETE_RESET } from "../../Constants/screenConstants";
import { useNavigate } from "react-router-dom";
import { createVideoFromImage } from "../../Actions/videoFromImage";
import { uploadMedia } from "../../Actions/mediaActions";
import { userScreensList } from "../../Actions/userActions";
// import { CreateCampaign } from "../ScreenDetails/CreateCampaign";
import { CreateCampaignName } from "../Models/CreateCampaignName";
import { UploadMedia } from "../Models/UploadMedia";
import { CreateCampaignPopup } from "../ScreenDetails/CreateCampaignPopup";
import { TbReport } from "react-icons/tb";
import { ScanReportLogModel } from "./ScanReportLogModel";
import {
  CAMPAIGN_LIST_BY_SCREENID_RESET,
  CAMPAIGN_STATUS_ACTIVE,
  CAMPAIGN_STATUS_DELETED,
  CAMPAIGN_STATUS_PENDING,
} from "../../Constants/campaignConstants";

export function DashBoard(props: any) {
  const screenId = props?.screenId;
  const screen = props.screen;
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();

  const [endDateTime, setEndDateTime] = useState<any>();
  const [startDateTime, setStartdateTime] = useState<any>();
  const [numberOfSlots, setNumberOfSlots] = useState<any>(30);
  const [openCreateCampaignName, setOpenCreateCampaignName] =
    useState<boolean>(false);
  const [openUploadMedia, setOpenUploadMedia] = useState<boolean>(false);
  const [openCreataCampaignPop, setOpenCreataCampaignPop] =
    useState<any>(false);
  const [openScanReport, setOpenScanReport] = useState<boolean>(false);

  const [campaignName, setCampaignName] = useState<any>("");
  const [defaultCampaign, setDefaultCampaign] = useState<any>(false);
  const [fileUrl, setFileUrl] = useState<any>("");
  const [campaignStatus, setCampaignStatus] = useState<any>({
    active: 0,
    pending: 0,
    deleted: 0,
  });

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const activeCampaignListByScreenID = useSelector(
    (state: any) => state.activeCampaignListByScreenID
  );
  const {
    loading: loadingCampaign,
    error: errorCampaign,
    campaigns: videosList,
  } = activeCampaignListByScreenID;
  //screenDelete
  const screenDelete = useSelector((state: any) => state.screenDelete);
  const {
    loading: loadingScreenDelete,
    error: errorScreenDelete,
    success: successScreeDelete,
  } = screenDelete;

  const confirm = (e: React.MouseEvent<HTMLElement> | undefined) => {
    dispatch(deleteScreen(screenId));
  };

  const handleOpenCreateCampaignName = (value: any) => {
    if (!userInfo) {
      message.error("please sign in");
      navigate("/signin", {
        state: {
          path: `/screenDetails/${screenId}`,
        },
      });
    } else {
      setOpenCreateCampaignName(value);
    }
  };

  const handleCreateVideoFromImage = (filedata: any) => {
    dispatch(createVideoFromImage(filedata));
    navigate("/singleCampaign", {
      state: {
        campaignName,
        startDateTime,
        endDateTime,
        numberOfSlots,
        screenId,
        defaultCampaign,
      },
    });
    setCampaignName("");
    setFileUrl("");
  };

  const videoUploadHandler = async (e: any) => {
    e.preventDefault();

    dispatch(
      uploadMedia({
        title: campaignName,
        thumbnail:
          "https://bafybeicduvlghzcrjtuxkro7foazucvuyej25rh3humeujbzt7bmio4hsa.ipfs.w3s.link/raily.png",
        fileUrl,
        media: "",
      })
    );
    navigate("/singleCampaign", {
      state: {
        campaignName,
        startDateTime,
        endDateTime,
        numberOfSlots,
        screenId,
        defaultCampaign,
      },
    });
    setCampaignName("");
    setFileUrl("");
  };

  const campaignListByScreenId = useSelector(
    (state: any) => state.campaignListByScreenId
  );
  const {
    loading: loadingAllCampaign,
    error: errorAllCampaign,
    campaigns: allVideosList,
  } = campaignListByScreenId;

  useEffect(() => {
    if (successScreeDelete) {
      message.success("Screen deleted successfully!");
      dispatch({ type: SCREEN_DELETE_RESET });
      dispatch(userScreensList(userInfo));
      navigate("/myScreens");
    }
    if (errorScreenDelete) {
      message.error(errorScreenDelete);
      dispatch({ type: SCREEN_DELETE_RESET });
    }
    if (errorCampaign) {
      message.error(errorCampaign);
    }
  }, [
    dispatch,
    navigate,
    screenId,
    props,
    errorScreenDelete,
    successScreeDelete,
    errorCampaign,
  ]);

  useEffect(() => {
    if (errorAllCampaign) {
      message.error(errorAllCampaign);
      dispatch({ type: CAMPAIGN_LIST_BY_SCREENID_RESET });
    }
    if (allVideosList?.length > 0) {
      const data = allVideosList?.reduce(
        (accum: any, current: any) => {
          if (current?.status === CAMPAIGN_STATUS_ACTIVE) {
            accum.active += 1;
          } else if (current?.status === CAMPAIGN_STATUS_PENDING) {
            accum.pending += 1;
          } else if (current?.status === CAMPAIGN_STATUS_DELETED) {
            accum.deleted += 1;
          }
          return accum;
        },
        { active: 0, pending: 0, deleted: 0 }
      );
      setCampaignStatus(data);
    }
  }, [errorAllCampaign, allVideosList]);

  useEffect(() => {
    dispatch(getCampaignListByScreenId(screenId));
    dispatch(getAllCampaignListByScreenId(screenId));
  }, [screenId]);

  return (
    <Box>
      <CreateCampaignName
        open={openCreateCampaignName}
        onCancel={() => setOpenCreateCampaignName(false)}
        openUploadMedia={() => setOpenUploadMedia(true)}
        setCampaignName={(value: any) => setCampaignName(value)}
        campaignName={campaignName}
      />
      <UploadMedia
        open={openUploadMedia}
        onCancel={() => setOpenUploadMedia(false)}
        createVideo={handleCreateVideoFromImage}
        videoUploadHandler={videoUploadHandler}
        setCampaignName={(value: any) => setCampaignName(value)}
        setFileUrl={(value: any) => setFileUrl(value)}
      />
      <CreateCampaignPopup
        open={openCreataCampaignPop}
        onCancel={() => setOpenCreataCampaignPop(false)}
        screen={screen}
        isMaster={true}
        createCampaign={() => handleOpenCreateCampaignName(true)}
        setNumberOfSlots={(value: any) => setNumberOfSlots(value)}
        setStartdateTime={(value: any) => setStartdateTime(value)}
        setEndDateTime={(value: any) => setEndDateTime(value)}
        defaultCampaign={defaultCampaign}
        setDefaultCampaign={(value: boolean) => setDefaultCampaign(value)}
      />
      {screen?.qrCode && (
        <ScanReportLogModel
          open={openScanReport}
          onCancel={() => setOpenScanReport(false)}
          screenId={screenId}
        />
      )}
      <HStack
        width={{ base: "100%", lg: "50%" }}
        justifyContent="space-between"
      >
        <Flex align="center" gap="5">
          <Text
            fontSize={{ base: "2xl", lg: "4xl" }}
            fontWeight="600"
            color=""
            m="0"
          >
            {screen?.name}
          </Text>
          <Tooltip
            hasArrow
            label={
              !screen?.lastActive
                ? "Screen Not Synced Yet"
                : Math.floor(
                    new Date().getTime() -
                      new Date(screen?.lastActive).getTime()
                  ) /
                    1000 >
                  200
                ? "NOT ACTIVE"
                : "ACTIVE"
            }
            fontSize="md"
            bg="gray.300"
            color="black"
          >
            <Box
              height="12px"
              width="12px"
              borderRadius="100%"
              bgColor={
                !screen?.lastActive
                  ? "yellow"
                  : Math.floor(
                      new Date().getTime() -
                        new Date(screen?.lastActive).getTime()
                    ) /
                      1000 >
                    200
                  ? "red"
                  : "green"
              }
            ></Box>
          </Tooltip>
        </Flex>
      </HStack>

      <HStack
        spacing=""
        pr="10"
        width={{ base: "100%", lg: "50%" }}
        justifyContent="space-between"
      >
        <HStack>
          <Flex align="center" bgColor="#00EB7A" p="2" m="0">
            <Text
              pr="2"
              color="#403F49"
              fontSize={{ base: "10px", lg: "md" }}
              align="left"
              fontWeight="600"
              m="0"
              justifyContent="center"
            >
              {screen?.rating || 0}
            </Text>
            <AiFillStar size="16px" color="#403F49" />
          </Flex>
          <Flex
            align="center"
            bgColor="#F1F1F1"
            p={{ base: "2", lg: "3" }}
            m="0"
          >
            <Text
              pr="1"
              color="#000000"
              fontSize={{ base: "10px", lg: "12px" }}
              fontWeight="600"
              align="left"
              m="0"
              justifyContent="center"
            >
              5 ratings
            </Text>
          </Flex>
          <Text
            color="#656565"
            fontSize={{ base: "11px", lg: "md" }}
            align="left"
            fontWeight="489"
            m="0"
            p={{ base: "1", lg: "5" }}
            justifyContent="center"
          >
            {`${screen?.screenAddress}, ${screen?.districtCity}, ${screen?.stateUT}`}
          </Text>
        </HStack>
        {userInfo?._id === screen?.master?._id ? (
          <HStack>
            <IconButton
              bg="none"
              icon={<RiUpload2Line size="25px" color="teal" />}
              aria-label="Upload"
              onClick={() => {
                // setOpenCreateCampaignName(true);
                setOpenCreataCampaignPop(true);
              }}
            />
            <Popconfirm
              title="Delete screen"
              description="Do you really want to delete this screen?"
              onConfirm={confirm}
              okText="Yes"
              cancelText="No"
            >
              <IconButton
                bg="nome"
                icon={<RiDeleteBin6Line size="25px" color="red" />}
                aria-label="Delete"
              ></IconButton>
            </Popconfirm>
            {screen?.qrCode && (
              <IconButton
                bg="none"
                icon={<TbReport size="25px" color="#00000080" />}
                aria-label="Scan Logs"
                onClick={() => setOpenScanReport(true)}
              ></IconButton>
            )}
          </HStack>
        ) : null}
      </HStack>

      <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "", lg: "50" }} pt="5">
        <Box width={{ base: "100%", lg: "110%" }}>
          <Box bgColor="#0EBCF5" height="10px"></Box>
          <SimpleGrid
            columns={[1, 1, 2]}
            spacing={{ base: "5", lg: "0" }}
            pt="5"
          >
            <Box
              width={{ base: "100%", lg: "315px" }}
              height="189px"
              bgImage={screen?.image}
              bgPosition="center"
              bgSize="cover"
              borderRadius="8px"
              boxShadow="xl"
              onClick={() => navigate(`/screenDetails/${screen?._id}`)}
            ></Box>
            <Flex gap={{ base: "2", lg: "5" }}>
              <Box>
                <Text color="#454545" fontSize="15px" fontWeight="474">
                  Screen Id:
                </Text>
                <Text color="#454545" fontSize="15px" fontWeight="474">
                  Screen running time:
                </Text>
                <Text color="#454545" fontSize="15px" fontWeight="474">
                  Rent per slot:
                </Text>
                <Text color="#454545" fontSize="15px" fontWeight="474">
                  Slot time:
                </Text>
              </Box>
              <Box>
                <Text color="#454545" fontSize="15px" fontWeight="474">
                  {screen?.screenCode}
                </Text>
                <Text color="#454545" fontSize="15px" fontWeight="474">
                  Daily,
                  {
                    convertIntoDateAndTime(screen?.startTime)?.split(",")[2]
                  } to {convertIntoDateAndTime(screen?.endTime)?.split(",")[2]}
                </Text>
                <Text color="#454545" fontSize="15px" fontWeight="474">
                  {screen?.rentPerSlot}
                </Text>
                <Text color="#454545" fontSize="15px" fontWeight="474">
                  {screen?.slotsTimePeriod} second
                </Text>
              </Box>
            </Flex>
          </SimpleGrid>
        </Box>
        <Stack ml={{ lg: "78" }}>
          <Text
            color="#000000"
            fontSize="lg"
            fontWeight="565"
            m="0"
            pl={{ base: "0", lg: "10" }}
            textAlign="left"
          >
            Schedule
          </Text>
          <Box
            borderRadius="61px"
            border="1px"
            borderColor="#E7E7E7"
            width={{ base: "100%", lg: "50%" }}
            py="3"
            px="6"
          >
            <Flex align="center" justifyContent="space-between">
              <VStack align="left">
                <Text
                  color="#000000"
                  fontWeight="479"
                  fontSize={{ base: "md", lg: "2xl" }}
                  m="0"
                  align="left"
                >
                  {campaignStatus?.active}
                </Text>
                <Text
                  color="#393939"
                  fontWeight="479"
                  fontSize={{ base: "sm", lg: "md" }}
                  m="0"
                >
                  Active campaigns
                </Text>
              </VStack>
              <AiOutlineRight size="20px" color="#393939" />
            </Flex>
          </Box>
          <Box
            borderRadius="61px"
            border="1px"
            borderColor="#E7E7E7"
            width={{ base: "100%", lg: "50%" }}
            py="3"
            px="6"
          >
            <Flex align="center" justifyContent="space-between">
              <VStack align="left">
                <Text
                  color="#000000"
                  fontWeight="479"
                  fontSize={{ base: "md", lg: "2xl" }}
                  m="0"
                  align="left"
                >
                  {campaignStatus?.pending}
                </Text>
                <Text
                  color="#393939"
                  fontWeight="479"
                  fontSize={{ base: "sm", lg: "md" }}
                  m="0"
                >
                  Pending campaigns
                </Text>
              </VStack>
              <AiOutlineRight size="20px" color="#393939" />
            </Flex>
          </Box>
        </Stack>
      </SimpleGrid>
      <Divider width="749px" color="#F3F3F3" pt="5" />
      <Text
        color="#000000"
        fontWeight="600"
        fontSize={{ base: "md", lg: "2xl" }}
        align="left"
      >
        Active campaigns
      </Text>
      {loadingCampaign ? (
        <Box>
          <Skeleton avatar active paragraph={{ rows: 3 }} />
        </Box>
      ) : videosList?.length > 0 ? (
        <Stack px={{ base: "0", lg: "5" }}>
          <Show below="md">
            <AdsListOfSinglScreen videos={videosList} />
          </Show>
          <Hide below="md">
            <BrandTable videosList={videosList} isDisabled={false} />
          </Hide>
        </Stack>
      ) : (
        <Text>No campaigns yet</Text>
      )}
    </Box>
  );
}
