import {
  Box,
  Input,
  InputGroup,
  Select,
  SimpleGrid,
  Skeleton,
  Stack,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  VStack,
  HStack,
  Button,
} from "@chakra-ui/react";
import TextArea from "antd/es/input/TextArea";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getScreenCamData } from "../../Actions/screenActions";
import { useSelector } from "react-redux";
import { updateScreen } from "../../Actions/screenActions";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";

export function Additional(props: any) {
  const { screen, screenData } = props;
  const dispatch = useDispatch<any>();

  const [screenDataType, setScreenDataType] = useState<any>();
  const [screenStation, setScreenStation] = useState<any>();
  const [screenStationCode, setScreenStationCode] = useState<any>();
  const [screenHospital, setScreenHospital] = useState<any>();
  const [screenHospitalTicker, setScreenHospitalTicker] = useState<any>();
  const [defContent, setDefContent] = useState<any>("");
  const [defaultContents, setDefaultContents] = useState<any>([]);
  const [adInt, setAdInt] = useState<any>(60);
  const [adIntervals, setAdIntervals] = useState<any>([]);
  const [viewGenderData, setViewGenderData] = useState<any>(false);
  const [viewCounterData, setViewCounterData] = useState<any>(false);

  const screenUpdate = useSelector((state: any) => state.screenUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = screenUpdate;

  const screenCamDataGet = useSelector((state: any) => state.screenCamDataGet);
  const {
    loading: loadingScreenCamData,
    error: errorScreenCamData,
    data: screenCamData,
  } = screenCamDataGet;

  // // function getData() {

  // // }

  const handleSelectScreenDataType = (value: any) => {
    // console.log("handleSelectScreenType  : ", value);
    setScreenDataType(value);
  };

  const submitScreenHandler = () => {
    dispatch(
      updateScreen({
        _id: screen?._id,
        screenDataType,
        defaultContents,
        adIntervals,
      })
    );
  };

  const handleAddDefaultContent = (event: any) => {
    if (event.which === 13) {
      setDefaultContents([...defaultContents, defContent]);
      setDefContent("");
    }
  };

  const handleAddAdInterval = (event: any) => {
    if (event.which === 13) {
      setAdIntervals([...adIntervals, adInt]);
      setAdInt(60);
    }
  };

  const deleteDefaultContent = (defContent: any) => {
    const newDefaultContents = defaultContents.filter(
      (eachContent: any) => eachContent !== defContent
    );
    setDefaultContents(newDefaultContents);
  };

  const deleteAdInterval = (adInt: any) => {
    const newAdIntervals = adIntervals.filter(
      (eachInterval: any) => eachInterval !== adInt
    );
    setDefaultContents(newAdIntervals);
  };

  useEffect(() => {
    // if (screenData) {
    // console.log(screenData.screenData);
    setScreenDataType(screenData?.dataType);
    setAdIntervals(screenData?.erickshawData?.adIntervals);
    setDefaultContents(screenData?.erickshawData?.defaultContents);

    // }
    dispatch(getScreenCamData(screen._id));
    // console.log(screenData?.dataType);
  }, [dispatch]);

  return (
    <Box>
      <Skeleton isLoaded={screen}></Skeleton>
      <Box>{screen.category}</Box>
      <Box>{screen.screenType}</Box>
      <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
        <VStack fontSize="sm" spacing="2" width="100%" align="left">
          <Text color="#393939" fontWeight="semibold" align="left" m="0">
            Screen Data Type
          </Text>
          <Text color="#4D4D4D" align="left">
            Select screen data type here
          </Text>
        </VStack>
        <Select
          borderRadius="8px"
          height="40px"
          width={{ base: "100%", lg: "50%" }}
          variant="outline"
          borderColor="#0EBCF5"
          bgColor="#FFFFFF"
          color="#403F49"
          fontSize="sm"
          value={screenDataType}
          onChange={(e) => {
            handleSelectScreenDataType(e.target.value);
          }}
        >
          <option value="">Choose One...</option>
          <option value="RAILWAY">Railway</option>
          <option value="HOSPITAL">Hospital</option>
          <option value="ERICKSHAW">eRickshaw</option>
        </Select>
      </SimpleGrid>
      {screenDataType === "RAILWAY" && (
        <>
          <SimpleGrid columns={[1, 1, 2]} spacing={3}>
            <VStack fontSize="sm" spacing="2" width="100%" align="left">
              <Text color="#393939" fontWeight="semibold" align="left" m="0">
                Screen Station Name
              </Text>
              <Text color="#4D4D4D" align="left">
                Please type screen station name
              </Text>
            </VStack>
            <InputGroup size="lg" width="100%">
              <Input
                width={{ base: "100%", lg: "50%" }}
                style={{ height: "40px" }}
                placeholder="Screen Station"
                size="lg"
                borderRadius="md"
                fontSize="lg"
                border="1px"
                color="#555555"
                py="2"
                value={screenStation}
                onChange={(e) => setScreenStation(e.target.value)}
              />
            </InputGroup>
          </SimpleGrid>
          <SimpleGrid columns={[1, 1, 2]} spacing={3}>
            <VStack fontSize="sm" spacing="2" width="100%" align="left">
              <Text color="#393939" fontWeight="semibold" align="left" m="0">
                Screen Station Code
              </Text>
              <Text color="#4D4D4D" align="left">
                Please type screen station code
              </Text>
            </VStack>
            <InputGroup size="lg" width="100%">
              <Input
                width={{ base: "100%", lg: "50%" }}
                style={{ height: "40px" }}
                placeholder="Station Code"
                size="lg"
                borderRadius="md"
                fontSize="lg"
                border="1px"
                color="#555555"
                py="2"
                value={screenStationCode}
                onChange={(e) => setScreenStationCode(e.target.value)}
              />
            </InputGroup>
          </SimpleGrid>
        </>
      )}
      {screenDataType === "HOSPITAL" && (
        <>
          <SimpleGrid columns={[1, 1, 2]} spacing={3}>
            <VStack fontSize="sm" spacing="2" width="100%" align="left">
              <Text color="#393939" fontWeight="semibold" align="left" m="0">
                Screen Hospital Name
              </Text>
              <Text color="#4D4D4D" align="left">
                Please type screen hospital name
              </Text>
            </VStack>
            <InputGroup size="lg" width="100%">
              <Input
                width={{ base: "100%", lg: "50%" }}
                style={{ height: "40px" }}
                placeholder="Screen Hospital"
                size="lg"
                borderRadius="md"
                fontSize="lg"
                border="1px"
                color="#555555"
                py="2"
                value={screenHospital}
                onChange={(e) => setScreenHospital(e.target.value)}
              />
            </InputGroup>
          </SimpleGrid>
          <SimpleGrid columns={[1, 1, 2]} spacing={3}>
            <VStack fontSize="sm" spacing="2" width="100%" align="left">
              <Text color="#393939" fontWeight="semibold" align="left" m="0">
                Screen Hospital Ticker
              </Text>
              <Text color="#4D4D4D" align="left">
                Please type screen hospital ticker
              </Text>
            </VStack>
            <InputGroup size="lg" width="100%">
              <TextArea
                showCount
                maxLength={500}
                value={screenHospitalTicker}
                style={{ height: 120, marginBottom: 24 }}
                onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
                  setScreenHospitalTicker(e.target.value);
                }}
                placeholder="Write your hospital ticker"
              />
            </InputGroup>
          </SimpleGrid>
        </>
      )}
      {screen.defaultMediaPlayback && screenDataType === "ERICKSHAW" && (
        <>
          <SimpleGrid columns={[1, 1, 2]} spacing={3}>
            <VStack fontSize="sm" spacing="2" width="100%" align="left">
              <Text color="#393939" fontWeight="semibold" align="left" m="0">
                Set Default Video
              </Text>
              <Text color="#4D4D4D" align="left">
                Please enter the url of your default content
              </Text>
            </VStack>
            <InputGroup size="lg" width="100%">
              <Input
                width={{ base: "100%", lg: "50%" }}
                style={{ height: "40px" }}
                placeholder="url for your default content"
                size="lg"
                borderRadius="md"
                fontSize="lg"
                border="1px"
                color="#555555"
                py="2"
                value={defaultContents}
                onChange={(e) => setDefContent(e.target.value)}
                onKeyPress={(e) => handleAddDefaultContent(e)}
              />
            </InputGroup>
          </SimpleGrid>
          {defaultContents.map((content: any) => (
            <Text
              onClick={() => {
                deleteDefaultContent(content);
                return true;
              }}
            >
              {content}
            </Text>
          ))}
          <SimpleGrid columns={[1, 1, 2]} spacing={3}>
            <VStack fontSize="sm" spacing="2" width="100%" align="left">
              <Text color="#393939" fontWeight="semibold" align="left" m="0">
                Set Ad Interval
              </Text>
              <Text color="#4D4D4D" align="left">
                Please enter minimum time period of your ad intervals in seconds
              </Text>
            </VStack>
            <InputGroup size="lg" width="100%">
              <Input
                width={{ base: "100%", lg: "50%" }}
                style={{ height: "40px" }}
                placeholder="enter the time period of ad intervals in seconds"
                size="lg"
                borderRadius="md"
                fontSize="lg"
                border="1px"
                color="#555555"
                type="number"
                py="2"
                value={adIntervals}
                onChange={(e) => setAdInt(e.target.value)}
                onKeyPress={(e) => handleAddAdInterval(e)}
              />
            </InputGroup>
          </SimpleGrid>
        </>
      )}
      <HStack justifyContent="flex-end" pr="30" pb="30" pt="30" spacing={5}>
        {props?.show ? (
          <Button
            variant="outline"
            color="#515151"
            bgColor="#FAFAFA"
            fontSize="sm"
            borderColor="#0EBCF5"
            py="3"
            px="10"
            isLoading={loadingUpdate}
            loadingText="Screen data saving"
            _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
            onClick={submitScreenHandler}
          >
            Save & Next
          </Button>
        ) : (
          <Button
            variant="outline"
            color="#515151"
            bgColor="#FAFAFA"
            fontSize="sm"
            borderColor="#0EBCF5"
            py="3"
            px="10"
            isLoading={loadingUpdate}
            loadingText="Screen data saving"
            _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
            onClick={submitScreenHandler}
          >
            Save
          </Button>
        )}
      </HStack>
      {screen?.getCamData && (
        <Stack>
          {loadingScreenCamData ? (
            <Skeleton isLoaded={loadingScreenCamData}></Skeleton>
          ) : (
            <Stack>
              <Text onClick={() => setViewGenderData(!viewGenderData)}>
                Get Gender data {screenCamData?.genderAge?.length}
              </Text>
              {viewGenderData ? (
                <TableContainer>
                  <Table>
                    <Thead>
                      <Tr>
                        <Th>
                          <Text fontSize="8px">Time</Text>
                        </Th>
                        <Th>
                          <Text fontSize="8px">Gender</Text>
                        </Th>
                        <Th>
                          <Text fontSize="8px">Gender Accuracy</Text>
                        </Th>
                        <Th>
                          <Text fontSize="8px">Age</Text>
                        </Th>
                        <Th>
                          <Text fontSize="8px">Age Accuracy</Text>
                        </Th>
                      </Tr>
                    </Thead>
                    {screenCamData &&
                      Array.from(new Set([...screenCamData.genderAge])).map(
                        (data: any, index: any) => (
                          <Tbody key={index}>
                            <Tr>
                              <Td>{convertIntoDateAndTime(data?.Time)}</Td>
                              <Td>{data?.Gender}</Td>
                              <Td>{data?.Gender_Score}</Td>
                              <Td>{data?.Age}</Td>
                              <Td>{data?.Age_Score}</Td>
                            </Tr>
                          </Tbody>
                        )
                      )}
                  </Table>
                </TableContainer>
              ) : null}
              <Text onClick={() => setViewCounterData(!viewCounterData)}>
                Get Counter Data {screenCamData?.peopleCounter?.length}
              </Text>
              {viewCounterData ? (
                <TableContainer>
                  <Table>
                    <Thead>
                      <Tr>
                        <Th>
                          <Text fontSize="8px">Entered</Text>
                        </Th>
                        <Th>
                          <Text fontSize="8px">Exited</Text>
                        </Th>
                        <Th>
                          <Text fontSize="8px">Stay</Text>
                        </Th>
                      </Tr>
                    </Thead>
                    {screenCamData &&
                      Array.from(new Set([...screenCamData.peopleCounter])).map(
                        (data: any, index: any) => (
                          <Tbody key={index}>
                            <Tr>
                              <Td>
                                {data?.In_Time
                                  ? convertIntoDateAndTime(data?.In_Time)
                                  : convertIntoDateAndTime(data?.Out_Time)}
                              </Td>
                              <Td>
                                {data?.In_Time &&
                                data?.Out_Time &&
                                data?.Move_Right === data?.Move_Left
                                  ? `${
                                      Math.abs(
                                        Math.floor(
                                          new Date(data.In_Time).getTime() -
                                            new Date(data.Out_Time).getTime()
                                        )
                                      ) / 1000
                                    } seconds ${
                                      Math.floor(
                                        new Date(data.In_Time).getTime() -
                                          new Date(data.Out_Time).getTime()
                                      ) > 0
                                        ? "from left"
                                        : "from right"
                                    }  `
                                  : "Nopes"}
                              </Td>
                              <Td>{data?.Move_Right || data?.Move_Left}</Td>
                            </Tr>
                          </Tbody>
                        )
                      )}
                  </Table>
                </TableContainer>
              ) : null}
            </Stack>
          )}
        </Stack>
      )}
    </Box>
  );
}
