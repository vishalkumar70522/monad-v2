import {
  Box,
  InputGroup,
  SimpleGrid,
  Text,
  VStack,
  HStack,
  Button,
  Input,
  Flex,
  Skeleton,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { updateScreen } from "../../Actions/screenActions";
import { Radio, RadioChangeEvent, message } from "antd";
import { SCREEN_UPDATE_RESET } from "../../Constants/screenConstants";
import { useSelector } from "react-redux";

export function Impressions(props: any) {
  const { screen } = props;
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [sexRatio, setSexRatio] = useState<any>({});
  const [averagePurchasePower, setAveragePurchasePower] = useState<any>({});
  const [averageAgeGroup, setAverageAgeGroup] = useState<any>({});
  const [employmentStatus, setEmploymentStatus] = useState<any>([]);
  const [regularAudiencePercentage, setRegularAudiencePercentage] =
    useState<any>();
  const [maritalStatus, setMaritalStatus] = useState<any>([]);
  const [workType, setWorkType] = useState<any>([]);
  const [kidsFriendly, setKidsFriendly] = useState<any>("");
  const [crowdMobilityType, setCrowdMobilityType] = useState<any>([]);
  const [averageDailyFootfall, setAverageDailyFootfall] = useState<any>();

  // const screen = props?.screen;

  const addOrRemobeEmploymentStatus = (lable: any) => {
    if (employmentStatus?.length > 0) {
      if (employmentStatus.includes(lable)) {
        setEmploymentStatus([
          ...employmentStatus.filter((data: any) => data !== lable),
        ]);
      } else {
        setEmploymentStatus([...employmentStatus, lable]);
      }
    } else {
      setEmploymentStatus([lable]);
    }
  };
  const addOrRemobeCrowdMobilityType = (lable: any) => {
    if (crowdMobilityType?.length > 0) {
      if (crowdMobilityType.includes(lable)) {
        setCrowdMobilityType([
          ...crowdMobilityType.filter((data: any) => data !== lable),
        ]);
      } else {
        setCrowdMobilityType([...crowdMobilityType, lable]);
      }
    } else {
      setCrowdMobilityType([lable]);
    }
  };
  const addOrRemobeMaritalStatue = (lable: any) => {
    if (maritalStatus?.length) {
      if (maritalStatus.includes(lable)) {
        setMaritalStatus([
          ...maritalStatus.filter((data: any) => data !== lable),
        ]);
      } else {
        setMaritalStatus([...maritalStatus, lable]);
      }
    } else {
      setMaritalStatus([lable]);
    }
  };

  const onChange1 = ({ target: { value } }: RadioChangeEvent) => {
    setKidsFriendly(value);
  };

  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  const screenUpdate = useSelector((state: any) => state.screenUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = screenUpdate;

  const submitScreenHandler = () => {
    dispatch(
      updateScreen({
        _id: screen?._id,
        additionalData: {
          averageDailyFootfall,
          footfallClassification: {
            sexRatio,
            averagePurchasePower,
            regularAudiencePercentage,
            employmentStatus,
            maritalStatus,
            workType,
            kidsFriendly,
            crowdMobilityType,
            averageAgeGroup,
          },
        },
      })
    );
  };
  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }
    if (screen) {
      setAverageDailyFootfall(screen?.additionalData?.averageDailyFootfall);
      setSexRatio(screen?.additionalData?.footfallClassification?.sexRatio);
      setAveragePurchasePower(
        screen?.additionalData?.footfallClassification?.averagePurchasePower
      );
      setEmploymentStatus(
        screen?.additionalData?.footfallClassification?.employmentStatus
      );
      setRegularAudiencePercentage(
        screen?.additionalData?.footfallClassification
          ?.regularAudiencePercentage
      );
      setMaritalStatus(
        screen?.additionalData?.footfallClassification?.maritalStatus
      );
      setWorkType(screen?.additionalData?.footfallClassification?.workType);
      setKidsFriendly(
        screen?.additionalData?.footfallClassification?.kidsFriendly
      );
      setAverageAgeGroup(
        screen?.additionalData?.footfallClassification?.averageAgeGroup
      );
      setCrowdMobilityType(
        screen?.additionalData?.footfallClassification?.crowdMobilityType
      );
    }
  }, [props?.screen]);

  useEffect(() => {
    if (successUpdate) {
      message.success("Ad space updated successfully");
      if (props?.show) {
        props?.setOption("5");
      }
    }
    if (errorUpdate) {
      message.error(errorUpdate);
    }
    dispatch({ type: SCREEN_UPDATE_RESET });
    props.getScreenDetails();
  }, [errorUpdate, successUpdate]);

  useEffect(() => {}, [props]);
  return (
    <Box>
      <Skeleton isLoaded={screen}>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Average daily footfall
            </Text>
            <Text color="#4D4D4D" align="left">
              The number of people around the screen per day
            </Text>
          </VStack>
          <InputGroup size="lg">
            <Input
              placeholder="only number"
              borderRadius="md"
              fontSize="lg"
              width={{ base: "100%", lg: "15%" }}
              style={{ height: "40px" }}
              border="1px"
              color="#555555"
              type="number"
              value={averageDailyFootfall}
              onChange={(e: any) => setAverageDailyFootfall(e.target.value)}
            />
          </InputGroup>
        </SimpleGrid>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Regular audience percentage
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter regular audience percentage
            </Text>
          </VStack>
          <Flex align="center" gap="2">
            <Input
              width={{ base: "100%", lg: "15%" }}
              style={{ height: "40px" }}
              placeholder="only number"
              size="lg"
              borderRadius="md"
              fontSize="lg"
              border="1px"
              color="#555555"
              type="number"
              value={regularAudiencePercentage}
              onChange={(e: any) =>
                setRegularAudiencePercentage(e.target.value)
              }
              py="2"
            />
            <Text color="#000000" fontSize="sm" fontWeight="" m="0">
              %
            </Text>
          </Flex>
        </SimpleGrid>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left" m="0">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Sex ration (Male : Female)
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter each sex ration which cross to this screen
            </Text>
          </VStack>
          <HStack>
            <Input
              width={{ base: "100%", lg: "15%" }}
              style={{ height: "40px" }}
              type="number"
              placeholder="male"
              size="lg"
              borderRadius="md"
              fontSize="lg"
              border="1px"
              color="#555555"
              py="2"
              value={sexRatio?.male}
              onChange={(e: any) => {
                setSexRatio({
                  male: Number(e.target.value),
                  female: Number(100 - e.target.value),
                });
              }}
            />
            <Text color="#393939" fontSize="xl" fontWeight="bold" m="0">
              :
            </Text>
            <Input
              width={{ base: "100%", lg: "15%" }}
              style={{ height: "40px" }}
              placeholder="female"
              size="lg"
              type="number"
              borderRadius="md"
              fontSize="lg"
              border="1px"
              color="#555555"
              py="2"
              value={sexRatio?.female}
              onChange={(e: any) => {
                setSexRatio({
                  male: 100 - Number(e.target.value),
                  female: Number(e.target.value),
                });
              }}
            />
            <Text color="#000000" fontSize="sm" fontWeight="" m="0">
              %
            </Text>
          </HStack>
        </SimpleGrid>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Average purchase power (Start - End)
            </Text>
            <Text color="#4D4D4D" align="left">
              Thew average person comes to your resturant
            </Text>
          </VStack>
          <Flex gap="2" align="center">
            <Input
              width={{ base: "100%", lg: "15%" }}
              style={{ height: "40px" }}
              placeholder="start"
              size="lg"
              type="number"
              borderRadius="md"
              fontSize="lg"
              border="1px"
              color="#555555"
              py="2"
              value={averagePurchasePower?.start}
              onChange={(e: any) => {
                setAveragePurchasePower({
                  ...averagePurchasePower,
                  start: Number(e.target.value),
                });
              }}
            />
            <Text color="#000000" fontSize="sm" fontWeight="" m="0">
              -
            </Text>

            <Input
              width={{ base: "100%", lg: "15%" }}
              style={{ height: "40px" }}
              placeholder="end"
              size="lg"
              borderRadius="md"
              fontSize="lg"
              border="1px"
              color="#555555"
              py="2"
              value={averagePurchasePower?.end}
              onChange={(e: any) => {
                setAveragePurchasePower({
                  ...averagePurchasePower,
                  end: Number(e.target.value),
                });
              }}
            />
            <Text color="#000000" fontSize="sm" fontWeight="" m="0">
              Rupees
            </Text>
          </Flex>
        </SimpleGrid>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Average age group (Start - End)
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter average age group
            </Text>
          </VStack>
          <HStack>
            <Input
              width={{ base: "100%", lg: "15%" }}
              style={{ height: "40px" }}
              placeholder="start"
              size="lg"
              type="number"
              borderRadius="md"
              fontSize="lg"
              border="1px"
              color="#555555"
              py="2"
              value={averageAgeGroup?.averageStartAge}
              onChange={(e: any) => {
                setAverageAgeGroup({
                  ...averageAgeGroup,
                  averageStartAge: Number(e.target.value),
                });
              }}
            />
            <Text color="#000000" fontSize="sm" fontWeight="" m="0">
              -
            </Text>
            <Input
              width={{ base: "100%", lg: "15%" }}
              style={{ height: "40px" }}
              placeholder="end"
              size="lg"
              borderRadius="md"
              fontSize="lg"
              border="1px"
              color="#555555"
              py="2"
              value={averageAgeGroup?.averageEndAge}
              onChange={(e: any) => {
                setAverageAgeGroup({
                  ...averageAgeGroup,
                  averageEndAge: Number(e.target.value),
                });
              }}
            />
            <Text color="#000000" fontSize="sm" fontWeight="" m="0">
              Years
            </Text>
          </HStack>
        </SimpleGrid>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Employment status
            </Text>
            <Text color="#4D4D4D" align="left">
              Who are most persons visit to your place
            </Text>
          </VStack>
          <SimpleGrid columns={[2, 2, 4]} spacing={2}>
            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={
                employmentStatus?.length > 0
                  ? employmentStatus?.includes("Salaried")
                    ? "#4C4C4C"
                    : "#515151"
                  : "#515151"
              }
              bgColor={
                employmentStatus?.includes("Salaried") ? "#D6FFFF" : "#FAFAFA"
              }
              fontSize="sm"
              p="4"
              _hover={{
                bg: "rgba(14, 188, 245, 0.3)",
                color: "#4C4C4C",
              }}
              onClick={() => addOrRemobeEmploymentStatus("Salaried")}
            >
              Salaried
            </Button>
            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={
                employmentStatus?.includes("Businessman")
                  ? "#4C4C4C"
                  : "#515151"
              }
              bgColor={
                employmentStatus?.includes("Businessman")
                  ? "#D6FFFF"
                  : "#FAFAFA"
              }
              fontSize="sm"
              p="4"
              onClick={() => addOrRemobeEmploymentStatus("Businessman")}
            >
              Businessman
            </Button>
            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={
                employmentStatus?.includes("Student") ? "#4C4C4C" : "#515151"
              }
              bgColor={
                employmentStatus?.includes("Student") ? "#D6FFFF" : "#FAFAFA"
              }
              fontSize="sm"
              p="4"
              onClick={() => addOrRemobeEmploymentStatus("Student")}
            >
              Student
            </Button>

            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={
                employmentStatus?.includes("Other") ? "#4C4C4C" : "#515151"
              }
              bgColor={
                employmentStatus?.includes("Other") ? "#D6FFFF" : "#FAFAFA"
              }
              fontSize="sm"
              p="4"
              onClick={() => addOrRemobeEmploymentStatus("Other")}
            >
              Other
            </Button>
          </SimpleGrid>
        </SimpleGrid>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Marital status
            </Text>
            <Text color="#4D4D4D" align="left">
              Enter marital status
            </Text>
          </VStack>
          <HStack spacing={2}>
            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={maritalStatus?.includes("Married") ? "#4C4C4C" : "#515151"}
              bgColor={
                maritalStatus?.includes("Married") ? "#D6FFFF" : "#FAFAFA"
              }
              fontSize="sm"
              p="4"
              onClick={() => addOrRemobeMaritalStatue("Married")}
            >
              Married
            </Button>
            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={
                maritalStatus?.includes("Unmarried") ? "#4C4C4C" : "#515151"
              }
              bgColor={
                maritalStatus?.includes("Unmarried") ? "#D6FFFF" : "#FAFAFA"
              }
              fontSize="sm"
              p="4"
              onClick={() => addOrRemobeMaritalStatue("Unmarried")}
            >
              Unmarried
            </Button>
          </HStack>
        </SimpleGrid>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Crowd mobility
            </Text>
            <Text color="#4D4D4D" align="left">
              How the most crowd comes to your place
            </Text>
          </VStack>
          <SimpleGrid columns={[2, 2, 4]} spacing={3}>
            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={
                crowdMobilityType?.includes("Moving") ? "#4C4C4C" : "#515151"
              }
              bgColor={
                crowdMobilityType?.includes("Moving") ? "#D6FFFF" : "#FAFAFA"
              }
              fontSize="sm"
              p="4"
              onClick={() => addOrRemobeCrowdMobilityType("Moving")}
            >
              Moving
            </Button>
            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={
                crowdMobilityType?.includes("Sitting") ? "#4C4C4C" : "#515151"
              }
              bgColor={
                crowdMobilityType?.includes("Sitting") ? "#D6FFFF" : "#FAFAFA"
              }
              fontSize="sm"
              p="4"
              onClick={() => addOrRemobeCrowdMobilityType("Sitting")}
            >
              Sitting
            </Button>
            <Button
              style={{ height: "40px" }}
              variant="outline"
              color={
                crowdMobilityType?.includes("Walking") ? "#4C4C4C" : "#515151"
              }
              bgColor={
                crowdMobilityType?.includes("Walking") ? "#D6FFFF" : "#FAFAFA"
              }
              fontSize="sm"
              p="4"
              onClick={() => addOrRemobeCrowdMobilityType("Walking")}
            >
              Walking
            </Button>
          </SimpleGrid>
        </SimpleGrid>
        <SimpleGrid columns={[1, 3, 2]} spacing={3} pt="5">
          <VStack fontSize="sm" spacing="2" width="100%" align="left">
            <Text color="#393939" fontWeight="semibold" align="left" m="0">
              Kids Friendly
            </Text>
            <Text color="#4D4D4D" align="left">
              The number of people around the screen per day
            </Text>
          </VStack>
          <Radio.Group
            options={["Yes", "No"]}
            onChange={onChange1}
            value={kidsFriendly}
          />
        </SimpleGrid>
        <HStack justifyContent="flex-end" pr="30" pb="30" pt="0" spacing={5}>
          {props?.show ? (
            <Button
              variant="outline"
              color="#515151"
              bgColor="#FAFAFA"
              fontSize="sm"
              borderColor="#0EBCF5"
              py="3"
              px="10"
              isLoading={loadingUpdate}
              loadingText="Screen data saving"
              _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
              onClick={submitScreenHandler}
            >
              Save & Next
            </Button>
          ) : (
            <Button
              variant="outline"
              color="#515151"
              bgColor="#FAFAFA"
              fontSize="sm"
              borderColor="#0EBCF5"
              py="3"
              px="10"
              isLoading={loadingUpdate}
              loadingText="Screen data saving"
              _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
              onClick={submitScreenHandler}
            >
              Save
            </Button>
          )}
        </HStack>
      </Skeleton>
    </Box>
  );
}
