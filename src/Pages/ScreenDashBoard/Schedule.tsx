import {
  Box,
  Flex,
  FormControl,
  Input,
  InputGroup,
  SimpleGrid,
  Text,
  VStack,
  HStack,
  Button,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import type { Dayjs } from "dayjs";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import { MuiPickersUtilsProvider, TimePicker } from "@material-ui/pickers";
import {
  IconButton as MiuiIconButton,
  InputAdornment,
} from "@material-ui/core";
import { BsClock } from "react-icons/bs";
import { updateScreen } from "../../Actions/screenActions";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { message } from "antd";
import { SCREEN_UPDATE_RESET } from "../../Constants/screenConstants";
import { useNavigate } from "react-router-dom";

export function Schedule(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const { screen } = props;

  const [startTime, setStartTime] = useState<Dayjs | null>();
  const [endTime, setEndTime] = useState<Dayjs | null>();
  const [slotsTimePeriod, setSlotsTimePeriod] = useState<any>();
  const [rentPerSlot, setRentPerSlot] = useState<any>();

  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    loading: loadingUserInfo,
    error: errorUserInfo,
    userInfo,
  } = userSignin;

  //screenDetails

  const screenUpdate = useSelector((state: any) => state.screenUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = screenUpdate;

  const handleEndTime = (value: any) => {
    setEndTime(value);
  };
  const handleStartTime = (value: any) => {
    // let time = value.toString().split(" "); // Wed Jan 04 2023 08:22:28 GMT+0530
    // time = time[4];
    setStartTime(value);
  };

  const submitScreenHandler = () => {
    dispatch(
      updateScreen({
        _id: screen?._id,
        rentPerSlot,
        slotsTimePeriod,
        startTime,
        endTime,
      })
    );
  };
  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }
    if (props?.screen) {
      setStartTime(screen?.startTime);
      setEndTime(screen?.endTime);
      setSlotsTimePeriod(screen?.slotsTimePeriod);
      setRentPerSlot(screen?.rentPerSlot);
    }
  }, [props]);
  useEffect(() => {
    if (successUpdate) {
      message.success("Ad space updated successfully");
      if (props?.show) {
        props?.setOption("2");
      }
    }
    if (errorUpdate) {
      message.error(errorUpdate);
    }
    dispatch({ type: SCREEN_UPDATE_RESET });
    props?.getScreenDetails();
  }, [errorUpdate, successUpdate]);

  return (
    <Box p="5">
      <Text fontSize="2xl" color="#0000000" fontWeight="600" m="0">
        Modify your schedule
      </Text>
      <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="10">
        <VStack fontSize="sm" spacing="2" width="100%" align="left">
          <Text color="#393939" fontWeight="semibold" align="left" m="0">
            Start time
          </Text>
          <Text color="#4D4D4D" align="left">
            Ad Space open Time
          </Text>
        </VStack>

        <Flex>
          <FormControl id="startDateHere" width={{ base: "100%", lg: "30%" }}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <TimePicker
                inputVariant="outlined"
                format="hh:mm"
                value={startTime}
                onChange={handleStartTime}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <MiuiIconButton>
                        <BsClock />
                      </MiuiIconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
        </Flex>
      </SimpleGrid>
      <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
        <VStack fontSize="sm" spacing="2" width="100%" align="left">
          <Text color="#393939" fontWeight="semibold" align="left" m="0">
            End time
          </Text>
          <Text color="#4D4D4D" align="left">
            Ad Space close Time
          </Text>
        </VStack>
        <Flex>
          <FormControl id="startDateHere" width={{ base: "100%", lg: "30%" }}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <TimePicker
                inputVariant="outlined"
                format="hh:mm"
                value={endTime}
                onChange={handleEndTime}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <MiuiIconButton>
                        <BsClock />
                      </MiuiIconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
        </Flex>
      </SimpleGrid>
      <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
        <VStack fontSize="sm" spacing="2" width="100%" align="left">
          <Text color="#393939" fontWeight="semibold" align="left" m="0">
            Slot time (in seconds)
          </Text>
          <Text color="#4D4D4D" align="left">
            Enter the slot time that the user may play per ad
          </Text>
        </VStack>

        <Input
          placeholder="20 Second"
          fontSize="md"
          borderRadius="md"
          color="#555555"
          py="2"
          type="number"
          value={slotsTimePeriod}
          width={{ base: "100%", lg: "30%" }}
          style={{ height: "50px" }}
          onChange={(e) => setSlotsTimePeriod(e.target.value)}
        />
      </SimpleGrid>
      <SimpleGrid columns={[1, 1, 2]} spacing={3} pt="5">
        <VStack fontSize="sm" spacing="2" width="100%" align="left">
          <Text color="#393939" fontWeight="semibold" align="left" m="0">
            Enter price for slot (in credits)
          </Text>
          <Text color="#4D4D4D" align="left">
            Enter price for each slot
          </Text>
        </VStack>
        <InputGroup size="lg">
          <Input
            placeholder="20"
            size="lg"
            fontSize="md"
            borderRadius="md"
            color="#555555"
            py="2"
            type="number"
            value={rentPerSlot}
            width={{ base: "100%", lg: "30%" }}
            style={{ height: "50px" }}
            onChange={(e) => setRentPerSlot(e.target.value)}
          />
        </InputGroup>
      </SimpleGrid>
      <HStack justifyContent="flex-end" pr="30" pb="30" pt="30" spacing={5}>
        {props?.show ? (
          <Button
            variant="outline"
            color="#515151"
            bgColor="#FAFAFA"
            fontSize="sm"
            borderColor="#0EBCF5"
            py="3"
            px="10"
            isLoading={loadingUpdate}
            loadingText="Screen data saving"
            _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
            onClick={submitScreenHandler}
          >
            Save & Next
          </Button>
        ) : (
          <Button
            variant="outline"
            color="#515151"
            bgColor="#FAFAFA"
            fontSize="sm"
            borderColor="#0EBCF5"
            py="3"
            px="10"
            isLoading={loadingUpdate}
            loadingText="Screen data saving"
            _hover={{ bg: "rgba(14, 188, 245, 0.3)", color: "#674780" }}
            onClick={submitScreenHandler}
          >
            Save
          </Button>
        )}
      </HStack>
    </Box>
  );
}
