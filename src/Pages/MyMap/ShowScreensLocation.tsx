import { Box, HStack, Image, Text } from "@chakra-ui/react";
import Axios from "axios";
import React, { useState } from "react";
import { mapIcon } from "../../assets/svg";
import ReactMapGL, { Marker, Popup } from "react-map-gl";
import { useNavigate } from "react-router-dom";

export function ShowScreensLocation(props: any) {
  const navigate = useNavigate();
  const listOfScreens = props?.data;
  //console.log("listOfScreens : ", JSON.stringify(listOfScreens.features));
  // console.log("geometry : ", props.geometry);
  const [viewState, setViewState] = useState({
    longitude:
      props?.geometry?.coordinates[1] ||
      listOfScreens?.features?.[0]?.geometry?.coordinates[1] ||
      85,
    latitude:
      props?.geometry?.coordinates[0] ||
      listOfScreens?.features?.[0]?.geometry?.coordinates[0] ||
      25,
    zoom: props?.zoom || 6,
  });
  const [screenData, setScreenData] = useState<any>(null);
  const [viewSingleScreen, setViewSingleScreen] = useState<any>(false);

  const getSingleScreenData = async (e: any, screenId: any, pinData: any) => {
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenId}`
      );
      setScreenData(data);
      setViewSingleScreen(pinData);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Box height="100%" width="100%" color="black.500">
      <ReactMapGL
        initialViewState={viewState}
        mapStyle="mapbox://styles/vviicckkyy55/cliox9jwm00q001pg18ppcsq3"
        mapboxAccessToken={
          process.env.REACT_APP_MAPBOX ||
          "pk.eyJ1IjoidnZpaWNja2t5eTU1IiwiYSI6ImNrdW5zaGU3czI0Y3gyeG42YnYxczl1aGQifQ.dxSrDMAyf8hfCrM5WMFnEw"
        }
        onMove={(e) => setViewState(e.viewState)}
      >
        {listOfScreens &&
          listOfScreens?.features?.map((singleData: any, index: any) => (
            <Marker
              key={index}
              latitude={singleData?.geometry?.coordinates[0]}
              longitude={singleData?.geometry?.coordinates[1]}
            >
              <Image
                src={mapIcon}
                alt="mapIcon"
                onClick={(e) => {
                  getSingleScreenData(
                    e,
                    singleData.properties.screen,
                    singleData
                  );
                }}
              />
            </Marker>
          ))}

        {viewSingleScreen && screenData ? (
          <Popup
            className="map"
            latitude={viewSingleScreen.geometry.coordinates[0]}
            longitude={viewSingleScreen.geometry.coordinates[1]}
            onClose={() => setViewSingleScreen(null)}
            anchor="left"
            closeButton={false}
            focusAfterOpen={true}
          >
            <Box
              border="1px solid #2BB3E0"
              borderRadius="15px"
              bgGradient={[
                "linear-gradient(156.06deg, rgba(255, 255, 255) -1.7%, rgba(255, 255, 255) 102.25%)",
              ]}
              p="3"
              m="-10"
              onClick={() => navigate(`/screenDetails/${screenData?._id}`)}
            >
              <HStack>
                <Image
                  width="90px"
                  height="100%"
                  src={screenData?.image}
                  alt="screen image"
                  borderRadius="15px"
                />

                <Box alignItems="left">
                  <Text
                    color="#000000"
                    fontSize="14px"
                    fontWeight="bold"
                    align="left"
                    m="0"
                  >
                    {screenData?.name}
                  </Text>

                  <Text
                    m="0"
                    color="#7D7D7D"
                    fontSize="10px"
                    fontWeight="semibold"
                    align="left"
                  >
                    {screenData?.districtCity}
                  </Text>
                  <Text color="#000000" fontSize="10px" align="left" m="0">
                    Start from:
                  </Text>
                  <Text
                    color="#0EBCF5"
                    fontSize="11px"
                    fontWeight="semibold"
                    align="left"
                    m="0"
                  >
                    {`₹${screenData?.rentPerSlot}/ per slot*`}
                  </Text>
                </Box>
              </HStack>
            </Box>
          </Popup>
        ) : null}
      </ReactMapGL>
    </Box>
  );
}
