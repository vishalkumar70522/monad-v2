import React, { useEffect } from "react";
import { Box, Flex, Image, SimpleGrid, Text } from "@chakra-ui/react";
import { persent } from "../../assets/svg";
import { useDispatch } from "react-redux";
import { CouponReport } from "./CouponReport";
import { AdvertiseBox } from "../../components/commans";
import { useSelector } from "react-redux";
import { Skeleton, message } from "antd";
import { getCouponFullDetails } from "../../Actions/couponAction";
import { GET_COUPON_FULL_DETAILS_RESET } from "../../Constants/couponConstants";

export function CouponManagement() {
  const dispatch = useDispatch<any>();
  // const coupon = location?.state.coupon;
  const couponId = window.location.pathname.split("/")[2];

  const couponFullDetails = useSelector(
    (state: any) => state.couponFullDetails
  );
  const {
    loading: loadingCouponDetails,
    error: errorinCouponDetails,
    screens,
    campaigns,
    coupon,
  } = couponFullDetails;

  useEffect(() => {
    if (errorinCouponDetails) {
      message.error(errorinCouponDetails);
      dispatch({ type: GET_COUPON_FULL_DETAILS_RESET });
    }
  }, [errorinCouponDetails]);

  useEffect(() => {
    dispatch(getCouponFullDetails(couponId));
  }, [dispatch, couponId]);
  return (
    <Box
      py={{ base: "75", lg: "100" }}
      px={{ base: "2", lg: "20" }}
      height="100%"
    >
      <Box px="2" py="2" boxShadow="md" background="#ffffff" rounded="sm">
        <Flex px="2" gap="2" align="center">
          <Image src={persent} alt="" height="24px" width="24px" />
          <Text
            fontSize={{ base: "", lg: "24px" }}
            fontWeight="600"
            color="#000000"
            textAlign="left"
            m="0"
          >
            {coupon?.offerName}
          </Text>
        </Flex>
        <Flex py="2" px="2" gap="2" align="center">
          <Text fontSize="sm">{coupon?.offerDetails}</Text>
        </Flex>
        {loadingCouponDetails ? (
          <Skeleton />
        ) : (
          <CouponReport redeemers={coupon?.rewardCoupons} />
        )}
      </Box>
      {loadingCouponDetails ? (
        <Box>
          <Skeleton />
        </Box>
      ) : (
        <>
          <Box p="5">
            <Text fontSize="lg" fontWeight="500">
              Campaigns attached to
            </Text>

            <SimpleGrid columns={[1, 3]} spacing={{ base: "4", lg: "10" }}>
              {campaigns?.map((campaign: any) => (
                <AdvertiseBox video={campaign} key={campaign?._id} />
              ))}
            </SimpleGrid>
          </Box>
          <Box p="5">
            <Text fontSize="lg" fontFamily="500">
              Screens Deployed on
            </Text>

            <SimpleGrid columns={[1, 5]} spacing={{ base: "4", lg: "10" }}>
              {screens?.map((screen: any, index: any) => (
                <Box
                  border="1px"
                  borderColor="#000000"
                  p="2"
                  borderRadius="8px"
                  key={index}
                >
                  <Image src={screen.image} borderRadius="8px" />
                  <Text
                    color="#032321"
                    fontWeight="600"
                    fontSize={{ base: "lg", lg: "xl" }}
                    m="0"
                    py="3"
                  >
                    {screen.name}
                  </Text>
                </Box>
              ))}
            </SimpleGrid>
          </Box>
        </>
      )}
    </Box>
  );
}
