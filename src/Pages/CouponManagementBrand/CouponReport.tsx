import { ColumnsType } from "antd/es/table";
import { Table } from "antd";
import { convertIntoDateAndTime } from "../../utils/dateAndTime";
import {
  Text,
  Show,
  Hide,
  TableContainer,
  Table as MobileTable,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from "@chakra-ui/react";

const columns: ColumnsType<any> = [
  {
    title: (
      <Text color="#000000" fontSize="md" fontWeight="400" m="0">
        Email
      </Text>
    ),
    dataIndex: "redeemer",
    key: "redeemer",
    render: (value) => (
      <Text color="#000000" fontSize="sm" fontWeight="400" m="0">
        {value}
      </Text>
    ),
  },
  {
    title: (
      <Text color="#000000" fontSize="md" fontWeight="400" m="0">
        Status
      </Text>
    ),
    dataIndex: "status",
    key: "status",
    render: (value) => (
      <Text color="#000000" fontSize="sm" fontWeight="400">
        Claimed
      </Text>
    ),
  },

  {
    title: (
      <Text color="#000000" fontSize="md" fontWeight="400" m="0">
        Date
      </Text>
    ),
    dataIndex: "createdAt",
    key: "createdAt",
    render: (value) => (
      <Text color="#000000" fontSize="sm" fontWeight="500">
        {convertIntoDateAndTime(value)}
      </Text>
    ),
  },
  {
    title: (
      <Text color="#000000" fontSize="md" fontWeight="400" m="0">
        Location
      </Text>
    ),
    dataIndex: "claimedLocation",
    key: "claimedLocation",
    render: (value) => (
      <Text color="#000000" fontSize="sm" fontWeight="400">
        hidden
      </Text>
    ),
  },
];

export function CouponReport(props: any) {
  // console.log(props);
  return (
    <>
      <Show below="md">
        <TableContainer borderRadius="5px" bgColor="#FFFFFF">
          <MobileTable variant="simple" size="sm">
            <Thead>
              <Tr>
                <Th>
                  <Text fontSize="8px">Email</Text>
                </Th>
                <Th>
                  <Text fontSize="8px">Status</Text>
                </Th>
                <Th>
                  <Text fontSize="8px">Date</Text>
                </Th>
                <Th>
                  <Text fontSize="8px">Location</Text>
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {props?.redeemers?.map((redeemer: any, index: any) => (
                <Tr
                  key={index}
                  // onClick={() => props.handleShowCampaignLogs(video.cid)}
                  _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
                >
                  <Td isNumeric fontSize="10px" color="#403F49" py="1">
                    {redeemer.email}
                  </Td>
                  <Td color="#575757" fontSize="10px" width="10px" py="1">
                    <Text width="10px" m="0">
                      CLAIMED
                    </Text>
                  </Td>
                  <Td isNumeric fontSize="10px" color="#403F49" py="1">
                    {convertIntoDateAndTime(redeemer.createdAt)}
                  </Td>
                  <Td fontSize="10px" color="#403F49" py="1">
                    {redeemer.claimedLocation}
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </MobileTable>
        </TableContainer>
      </Show>
      <Hide below="md">
        <Table
          rowKey={(value) => value.redeemer}
          columns={columns}
          dataSource={props?.redeemers || []}
        />
      </Hide>
    </>
  );
}
