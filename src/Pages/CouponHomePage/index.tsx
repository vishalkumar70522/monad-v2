import {
  Box,
  Center,
  Divider,
  Flex,
  Hide,
  Image,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Show,
  // SimpleGrid,
  Stack,
  Text,
  Button,
} from "@chakra-ui/react";
import {
  FaAngleDown,
  // FaAngleLeft,
  // FaAngleRight,
  // FaAngleLeft,
  // FaAngleRight,
  // FaMapMarkerAlt,
} from "react-icons/fa";

import { ImLocation } from "react-icons/im";
import { TbLocationFilled } from "react-icons/tb";
import { adventurer, fashion, movies, scanner } from "../../assets/svg";
import { useEffect, useState } from "react";
import { SingleCoupon } from "../../components/commans";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { getAllActiveCouponList } from "../../Actions/couponAction";
import { FiUser } from "react-icons/fi";
import { ScanQRCode } from "../ScanQRCode";
import { useNavigate } from "react-router-dom";
import { Skeleton, message } from "antd";
import { couponicon } from "../../assets/funkfeets";
import { getAllBrands } from "../../Actions/brandAction";

export function CouponHomePage(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [openQRScanner, setOpenQRScanner] = useState<any>(false);
  const [seachCity, setSearchCity] = useState<any>("");
  // console.log("openQRScanner ", openQRScanner);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { loading, error, userInfo } = userSignin;
  const activeCouponList = useSelector((state: any) => state.activeCouponList);
  const {
    loading: loadingCouponList,
    error: errorCouponList,
    coupons,
  } = activeCouponList;

  const allBrandsGet = useSelector((state: any) => state.allBrandsGet);
  const {
    loading: loadingAllBrands,
    error: errorAllBrands,
    allBrands,
  } = allBrandsGet;

  const handelClickBuuton = (path: string) => {
    if (!userInfo) {
      navigate("/signin", { state: { path } });
    } else {
      navigate(path);
    }
  };

  useEffect(() => {
    dispatch(getAllActiveCouponList());
    dispatch(getAllBrands());
    // if (userInfo?.isMaster && !userInfo?.isCreator && !userInfo?.isBrand) {
    //   navigate("/myScreens");
    // }
    // if (!userInfo?.isMaster && !userInfo?.isCreator && userInfo?.isBrand) {
    //   navigate("/allScreen");
    // }
  }, [dispatch, navigate, userInfo]);

  return (
    <>
      <Box px={{ base: "", lg: "" }} pb={{ base: "50", lg: "100" }}>
        <ScanQRCode
          open={openQRScanner}
          onCancel={() => setOpenQRScanner(false)}
        />
        <Center
          id="border-radius-bottom"
          flexDirection="column"
          bgGradient="linear(149deg, rgba(55, 142, 222, 0.60) -25.09%, rgba(55, 142, 222, 0.21) -4.68%, rgba(55, 142, 222, 0.32) 23.45%, rgba(55, 142, 222, 0.34) 26.75%, rgba(55, 142, 222, 0.34) 26.76%, rgba(55, 142, 222, 0.46) 48.28%, rgba(55, 142, 222, 0.00) 80.83%)"
          // bgGradient="linear(to-br, #378EDE75, #378EDE50, #ffffff50, #ffffff)"
          // py="1"
          roundedBottomLeft="250"
        >
          <Text
            fontSize={{ base: "2xl", lg: "40px" }}
            fontWeight="800"
            color="#FF0042"
            align="center"
            pt="20"
            fontFamily="Open Sans"
          >
            GET REWARDS FOR YOUR ATTENTION
          </Text>
          <Text
            fontSize={{ base: "sm", lg: "md" }}
            fontWeight="400"
            color="#444444"
            // fontFamily="Open Sans"
          >
            Choose your rewards for providing your attention
          </Text>
          <Stack pt={{ base: "5", lg: "5" }} mx="5">
            <InputGroup
              // rounded="20"
              // size="lg"
              width={{ base: "356px", lg: "424px" }}
            >
              <InputLeftElement rounded="">
                <Center pt="3.5" px="3">
                  <ImLocation color="#949494" size="17px" />
                </Center>
              </InputLeftElement>
              <InputRightElement p={{ base: "0.4", lg: "1" }}>
                <Button bg="#DEDEDE40" m="" py="2.5">
                  <TbLocationFilled
                    size="15px"
                    color="#808080"
                    onClick={() => {
                      if (seachCity?.length > 0) {
                        message.error(
                          "Great deals coming at your location soon. Stay Tuned"
                        );
                      } else {
                        message.error("Plaese enter your city name!");
                      }
                    }}
                  />
                  <Text
                    m="1"
                    justifySelf="middle"
                    textAlign="center"
                    fontSize="12px"
                    color="#808080"
                    // fontFamily="open sans"
                  >
                    Use Current Location
                  </Text>
                </Button>
              </InputRightElement>
              <Input
                py="3"
                pl="10"
                bgColor="#FFFFFF"
                borderRadius="4px"
                fontSize={{ base: "12px", lg: "md" }}
                color="#949494"
                placeholder="Enter your city"
                // rounded="30"
                value={seachCity}
                onChange={(e) => setSearchCity(e.target.value)}
                onKeyPress={(e) => {
                  if (e.which === 13) {
                    message.error(
                      "Great deals coming at your location soon. Stay Tuned"
                    );
                  }
                }}
              />
            </InputGroup>
          </Stack>
          <Center pt={{ base: "5", lg: "5" }}>
            <Flex
              pt={{ base: "5", lg: "10" }}
              gap={{ base: "5", lg: "20" }}
              width="100%"
              align="center"
            >
              <Center flexDirection="column" gap="2">
                <Image
                  src={fashion}
                  alt="case bonus"
                  width={{ base: "35px", lg: "60px" }}
                  height={{ base: "35px", lg: "60px" }}
                />
                <Text
                  fontSize={{ base: "8px", lg: "12.53px" }}
                  fontWeight="400"
                  color="#515151"
                  align="left"
                >
                  Fashion
                </Text>
              </Center>
              <Center flexDirection="column" gap="2">
                <Image
                  src={movies}
                  alt="case bonus"
                  width={{ base: "35px", lg: "60px" }}
                  height={{ base: "35px", lg: "60px" }}
                />
                <Text
                  fontSize={{ base: "8px", lg: "12.53px" }}
                  fontWeight="400"
                  color="#515151"
                  align="left"
                >
                  Movies
                </Text>
              </Center>
              <Center flexDirection="column" gap="2">
                <Image
                  src={adventurer}
                  alt="case bonus"
                  width={{ base: "35px", lg: "60px" }}
                  height={{ base: "35px", lg: "60px" }}
                />
                <Text
                  fontSize={{ base: "8px", lg: "12.53px" }}
                  fontWeight="400"
                  color="#515151"
                  align="left"
                >
                  Adventure
                </Text>
              </Center>
            </Flex>
          </Center>
          <Hide below="md">
            <Center flexDirection="column" pt={{ base: "5", lg: "5" }}>
              <Text fontSize="md" fontWeight="400" color="#323232" m="0">
                EXPLORE MORE CATEGORIES
              </Text>
              <Divider color="#323232" width="50%" m="0" />
              <Stack pt={{ base: "1", lg: "2" }}>
                <FaAngleDown color="#323232" size="20px" />
              </Stack>
            </Center>
          </Hide>
        </Center>

        <Box px={{ base: "1", lg: "20" }} pb={{ base: "20", lg: "20" }}>
          {loadingCouponList ? (
            <Box>
              <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
            </Box>
          ) : (
            <>
              <Text
                py="5"
                fontSize={{ base: "22.7px", lg: "2xl" }}
                fontWeight="638"
                color="#403F49"
                m="0"
                align="left"
              >
                Exciting rewards for you
              </Text>
              {/* <Hide below="md"> */}
              {/* </Hide> */}
              <div className="scrollmenu">
                {coupons
                  ?.reverse()
                  ?.slice(0, -1)
                  ?.map((coupon: any, index: any) => (
                    <Stack key={index} align="center" px="2">
                      <SingleCoupon
                        userInfo={userInfo}
                        coupon={coupon}
                        key={index}
                      />
                    </Stack>
                  ))}
              </div>
            </>
          )}
          {loadingAllBrands ? (
            <Box>
              <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
            </Box>
          ) : (
            <>
              <Text
                py="5"
                fontSize={{ base: "22.7px", lg: "2xl" }}
                fontWeight="638"
                color="#403F49"
                align="left"
              >
                Top brands for you!
              </Text>
              <div className="scrollmenu">
                {allBrands
                  // ?.reverse()
                  // ?.slice(startExistingRewards, existingRewards)
                  ?.map((brand: any, index: any) => (
                    <Stack key={index} align="center" px="2">
                      <Box
                        border="1px"
                        borderColor="#D9D9D9"
                        p="5"
                        _hover={{ boxShadow: "2xl" }}
                        borderRadius="8px"
                        width="185px"
                        height="131px"
                        onClick={() => navigate(`/brandProfile/${brand?._id}`)}
                      >
                        <Center>
                          <Image
                            src={brand?.brandDetails.logo}
                            alt=" "
                            height="48px"
                          />
                        </Center>
                        <Text
                          pt="5"
                          color="#303030"
                          fontSize={{ base: "12px", lg: "lg" }}
                          fontWeight="638"
                          m="0"
                          align="center"
                          isTruncated={true}
                          noOfLines={1}
                        >
                          {brand?.brandName}
                        </Text>
                      </Box>
                    </Stack>
                  ))}
              </div>
            </>
          )}
          {loadingCouponList ? (
            <Box>
              <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
            </Box>
          ) : (
            <>
              <Text
                py="5"
                fontSize={{ base: "22.7px", lg: "2xl" }}
                fontWeight="638"
                color="#403F49"
                align="left"
              >
                Top picks for you!
              </Text>
              <div className="scrollmenu">
                {coupons
                  ?.reverse()
                  ?.slice(-6)
                  ?.map((coupon: any, index: any) => (
                    <Stack key={index} align="center" px="2">
                      <SingleCoupon
                        userInfo={userInfo}
                        coupon={coupon}
                        key={index}
                      />
                    </Stack>
                  ))}
              </div>
            </>
          )}
        </Box>
      </Box>
      <Show below="md">
        <div className="footer">
          <Flex
            justifyContent="space-between"
            px="10"
            bgColor="#E2E2E2"
            py="1"
            borderTopEndRadius="40px"
            borderTopLeftRadius="40px"
          >
            <Stack
              onClick={() => handelClickBuuton("/userCoupons")}
              align="center"
            >
              <Image src={couponicon} alt="couponIcon" width="30px" />
              <Text color="#FF1111" fontSize="12px" fontWeight="400" m="0">
                My Rewards
              </Text>
            </Stack>
            <Stack mt="-10" onClick={() => setOpenQRScanner(true)}>
              <Image
                src={scanner}
                width="80px"
                height="80px"
                alt="scanner"
                onClick={() => setOpenQRScanner(true)}
              />
            </Stack>
            <Stack
              onClick={() => handelClickBuuton("/userProfile")}
              align="center"
            >
              <FiUser color="#494949" size="30px" />
              <Text color="#494949" fontSize="12px" fontWeight="400" m="0">
                Account
              </Text>
            </Stack>
          </Flex>
        </div>
      </Show>
    </>
  );
}
